package com.mmsone.dmscloud.helper;

import java.io.IOException;

import org.json.JSONException;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.ex.AlreadyExistsException;
import com.mmsone.dmscloud.ex.BadUserNameOrPasswordException;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.ex.DisabledOrBlockedUserException;
import com.mmsone.dmscloud.ex.ErrorException;
import com.mmsone.dmscloud.ex.InsufficientRightsException;
import com.mmsone.dmscloud.ex.InvalidStatusException;
import com.mmsone.dmscloud.ex.ProtocolMismatchException;
import com.mmsone.dmscloud.ex.RaceConditionException;
import com.mmsone.dmscloud.ex.RmmExpiredException;
import com.mmsone.dmscloud.ex.ValidationException;

public class DialogHelper {

    public static AlertDialog.Builder mAlertDialogBuilder;

    /**
     * Show error dialog
     *
     * @param context
     * @param exception
     */
    public static void showErrorDialog(Context context, Exception exception) {
        if (exception != null) {
            if (exception instanceof IOException) {
                createErrorDialog(context, R.string.err_comm);
            } else if (exception instanceof JSONException) {
                createErrorDialog(context, R.string.err_json);
            } else if (exception instanceof DMSCloudDBException) {
                createErrorDialog(context, R.string.err_db);
            } else if (exception instanceof BadUserNameOrPasswordException) {
                createErrorDialog(context, R.string.err_bad_user_name_or_password);
            } else if (exception instanceof AlreadyExistsException) {
                createErrorDialog(context, R.string.err_already_exists);
            } else if (exception instanceof DisabledOrBlockedUserException) {
                createErrorDialog(context, R.string.err_disabled_or_blocked_user);
            } else if (exception instanceof ErrorException) {
                createErrorDialog(context, R.string.err_exceptin);
            } else if (exception instanceof InsufficientRightsException) {
                createErrorDialog(context, R.string.err_insufficient_rights);
            } else if (exception instanceof InvalidStatusException) {
                createErrorDialog(context, R.string.err_invalid_status);
            } else if (exception instanceof ProtocolMismatchException) {
                createErrorDialog(context, R.string.err_protocol_mismatch);
            } else if (exception instanceof RaceConditionException) {
                createErrorDialog(context, R.string.err_race_condition);
            } else if (exception instanceof RmmExpiredException) {
                createErrorDialog(context, R.string.err_rmm_expired);
            } else if (exception instanceof ValidationException) {
                createErrorDialog(context, R.string.err_validation);
            } else if (exception instanceof ActivityNotFoundException) {
                createErrorDialog(context, R.string.activity_not_found_message);
            }
        }
    }

    /**
     * Create error dialog
     *
     * @param context
     * @param message
     */
    public static void createErrorDialog(Context context, String message) {
        mAlertDialogBuilder = new AlertDialog.Builder(context).setTitle(R.string.err_title).setMessage(message).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mAlertDialogBuilder.show();
    }

    /**
     * Create error dialog
     *
     * @param context
     * @param message
     */
    public static void createErrorDialog(Context context, int message) {
        mAlertDialogBuilder = new AlertDialog.Builder(context).setTitle(R.string.err_title).setMessage(message).setNeutralButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mAlertDialogBuilder.show();
    }
}
