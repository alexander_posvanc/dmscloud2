package com.mmsone.dmscloud.logger;

public interface ILoggerConfig {
    boolean isEnabled();
}
