package com.mmsone.dmscloud.logger;

import android.util.Log;

public class Logger implements ILogger {

    private String tag;
    private static boolean isEnabled;

    /**
     * Initialize custom android logger
     * 
     * @param config
     */
    public static void init(ILoggerConfig config) {
        isEnabled = config.isEnabled();
    }

    public static ILogger getLogger(Class<?> clazz) {
        return new Logger(clazz);
    }

    private Logger(Class<?> clazz) {
        tag = clazz.getName();
    }

    @Override
    public void warn(String message) {
        if (isEnabled) {
            Log.w(tag, message);
        }
    }

    @Override
    public void warn(String message, Throwable throwable) {
        if (isEnabled) {
            Log.w(tag, message, throwable);
        }
    }

    @Override
    public void info(String message) {
        if (isEnabled) {
            Log.i(tag, message);
        }
    }

    @Override
    public void info(String message, Throwable throwable) {
        if (isEnabled) {
            Log.i(tag, message, throwable);
        }
    }

    @Override
    public void error(String message) {
        if (isEnabled) {
            Log.e(tag, message);
        }
    }

    @Override
    public void error(String message, Throwable throwable) {
        if (isEnabled) {
            Log.e(tag, message, throwable);
        }
    }

    @Override
    public void debug(String message) {
        if (isEnabled) {
            Log.d(tag, message);
        }
    }

    @Override
    public void debug(String message, Throwable throwable) {
        if (isEnabled) {
            Log.d(tag, message, throwable);
        }
    }

    @Override
    public void verbose(String message) {
        if (isEnabled) {
            Log.v(tag, message);
        }
    }

    @Override
    public void verbose(String message, Throwable throwable) {
        if (isEnabled) {
            Log.v(tag, message, throwable);
        }
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
