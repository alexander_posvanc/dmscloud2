package com.mmsone.dmscloud.bl;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.EPostType;
import com.mmsone.dmscloud.bo.WallResponse;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostChat;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostEmail;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.bo.bean.PostSubTask;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.bo.bean.ShowMeta;
import com.mmsone.dmscloud.bo.bean.WfDef;
import com.mmsone.dmscloud.bo.bean.WfTaskStatus;

public class PostManager {

    private static PostManager mInstance = null;

    public static PostManager getInstance() {
        if (mInstance == null) {
            mInstance = new PostManager();
        }
        return mInstance;
    }


    /**
     * Delete subComment
     *
     * @param id
     * @return
     * @throws Exception
     */
    public boolean deleteSubComment(long id) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().deletePostSubCommentRequest(id)));
        return true;
    }

    /**
     * Rank post
     *
     * @param postId
     * @return
     * @throws Exception
     */
    public boolean rankPost(long postId) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().rankPointRequest(postId)));
        return true;
    }

    /**
     * Rank sub post
     *
     * @param postId
     * @param subPostId
     * @param subPostType
     * @return
     * @throws Exception
     */
    public boolean rankSubPost(long postId, long subPostId, int subPostType) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().rankPointRequest(postId, subPostId, subPostType)));
        return true;
    }

    /**
     * Create post
     *
     * @param title
     * @param message
     * @param userList
     * @param groupList
     * @param distributionList
     * @param metaList
     * @return
     * @throws Exception
     */

    public boolean createPost(final String title, final String message, final Long[] userList, final Long[] groupList, final Long[] distributionList, final List<FillMeta> metaList) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().createPostRequest(title, message, userList, groupList, distributionList, metaList)));
        return true;
    }

    /**
     * Create task
     *
     * @param postId
     * @param message
     * @param dueDate
     * @param everyUser
     * @param userList
     * @param groupList
     * @param distributionList
     * @return
     * @throws Exception
     */
    public boolean createPostTask(final long postId, String message, final long dueDate, final boolean everyUser, final Long[] userList, final Long[] groupList, final Long[] distributionList) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().createPostTaskRequest(postId, message, dueDate, everyUser, userList, groupList, distributionList)));
        return true;
    }

    /**
     * Create comment
     *
     * @param postId
     * @param description
     * @return
     * @throws Exception
     */
    public boolean createPostComment(final long postId, final String description) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().createPostCommentRequest(postId, description)));
        return true;
    }

    /**
     * Create sub comment
     *
     * @param subPostId
     * @param subPostType
     * @param description
     * @return
     * @throws Exception
     */
    public boolean createPostSubComment(final long subPostId, final int subPostType, final String description) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().createPostSubCommentRequest(subPostId, subPostType, description)));
        return true;
    }

    /**
     * Hide post
     *
     * @param postId
     * @return
     * @throws Exception
     */
    public boolean hidePost(final long postId) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().hidePostRequest(postId)));
        return true;
    }

    /**
     * Mark post
     *
     * @param postId
     * @return
     * @throws Exception
     */
    public boolean markPost(final long postId) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().markPostRequest(postId)));
        return true;
    }

    /**
     * Share post
     *
     * @param postId
     * @param description
     * @param userList
     * @param groupList
     * @param distributionList
     * @return
     * @throws Exception
     */
    public boolean sharePost(final long postId, final String description, final Long[] userList, final Long[] groupList, final Long[] distributionList) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().sharePostRequest(postId, description, userList, groupList, distributionList)));
        return true;
    }

    /**
     * Downloads and parse wall response
     *
     * @param from
     * @param rowCount
     * @return
     * @throws Exception
     */
    public WallResponse getWallResponse(final int from, final int rowCount, Context context) throws Exception {
        JSONObject wallResponseObject = new JSONObject(ServerConnector.getInstance().getWallRequestWithFilterList(from, rowCount, SearchManager.getInstance().getFilterList()));

        Log.d("WALL", "" + wallResponseObject);
        ExceptionManager.getInstance().checkException(wallResponseObject);

        WallResponse wallResponse = new WallResponse();

        JSONArray wallJSONArray = wallResponseObject.optJSONArray("elementList");

        for (int i = 0; i < wallJSONArray.length(); i++) {
            wallResponse.elementList.add(parsePost(from, rowCount, wallJSONArray.optJSONObject(i)));
        }
        return wallResponse;
    }

    public List<WfDef> getWfDefList() throws Exception {
        JSONObject wfDefListResponse = new JSONObject(ServerConnector.getInstance().getWfDefListRequest());

        ExceptionManager.getInstance().checkException(wfDefListResponse);
        JSONArray wfDefArray = wfDefListResponse.optJSONArray("list");

        List<WfDef> wfDefList = new ArrayList<WfDef>();

        for (int i = 0; i < wfDefArray.length(); i++) {
            wfDefList.add(parseWfDef(wfDefArray.optJSONObject(i)));
        }

        return wfDefList;
    }


    /**
     * Parse wfDef
     *
     * @param wfDefObject
     * @return
     * @throws Exception
     */
    private WfDef parseWfDef(final JSONObject wfDefObject) throws Exception {
        WfDef wfDef = new WfDef();

        wfDef.id = wfDefObject.optLong("id");
        wfDef.description = wfDefObject.optString("description");
        wfDef.name = wfDefObject.optString("name");

        return wfDef;
    }

    public static Post parsePost(final int from, final int rowCount, final JSONObject postObject) throws Exception {
        Post post = new Post();

        post.id = postObject.optLong("id");
        post.createdBy = postObject.optLong("createdBy");
        post.createdOn = postObject.optLong("createdOn");
        post.documentId = postObject.optLong("documentId");
        post.formatedCreatedOn = postObject.optString("formatedCreatedOn");
        post.formatedModifiedOn = postObject.optString("formatedModifiedOn");
        post.hidden = postObject.optBoolean("hidden");
        post.marked = postObject.optBoolean("marked");
        post.message = postObject.optString("message");
        post.formatedModifiedOn = postObject.optString("formatedModifiedOn");
        post.modifiedOn = postObject.optLong("modifiedOn");
        post.postNumber = postObject.optLong("postNumber");
        post.title = postObject.optString("title");
        post.totalChildCount = postObject.optInt("totalChildCount");
        post.unreaded = postObject.optBoolean("unreaded");
        post.closed = postObject.optBoolean("closed");
        post.overAllCount = postObject.optInt("overAllCount");
        post.hasExplicitRank = postObject.optBoolean("hasExplicitRank");
        post.taskCount = postObject.optLong("taskCount");
        post.commentCount = postObject.optLong("commentCount");

        post.postType = EPostType.getPostType(postObject.optInt("postType"));

        JSONArray metaArray = postObject.optJSONArray("metaList");

        JSONObject metaJsonObject = null;
        for (int i = 0; i < metaArray.length(); i++) {
            metaJsonObject = metaArray.optJSONObject(i);
            post.metaList.add(new ShowMeta(metaJsonObject.optLong("id"), metaJsonObject.optString("label"), metaJsonObject.optString("value")));
        }

        JSONArray childArray = postObject.optJSONArray("childList");

        JSONObject childObject = null;
        for (int i = 0; i < childArray.length(); i++) {
            childObject = childArray.getJSONObject(i);
            if (childObject.getString("beanType").equals("JbPostComment")) {
                post.childList.add(parsePostComment(childObject));
            } else if (childObject.getString("beanType").equals("JbPostEmail")) {
                post.childList.add(parsePostEmail(childObject));
            } else if (childObject.getString("beanType").equals("JbPostWfTask")) {
                post.childList.add(parsePostWfTask(childObject));
            } else if (childObject.getString("beanType").equals("JbPostChat")) {
                post.childList.add(parsePostChat(childObject));
            } else {
                post.childList.add(parsePostTask(childObject));
            }
        }

        JSONArray sharedWithArray = postObject.optJSONArray("sharedWith");
        for (int i = 0; i < sharedWithArray.length(); i++) {
            post.sharedWith.add(sharedWithArray.getLong(i));
        }

        JSONArray hiddenAtArray = postObject.optJSONArray("hiddenAt");
        for (int i = 0; i < hiddenAtArray.length(); i++) {
            post.hiddenAt.add(hiddenAtArray.getLong(i));
        }

        return post;
    }

    public Post parsePostChilds(Post post) throws Exception {
        if (post.totalChildCount > 0) {
            post.childList.clear();
            JSONObject elementListObject = new JSONObject(ServerConnector.getInstance().getWallSubRequest(Config.WALL_FROM, post.totalChildCount, post.id));
            Log.d("Postmanager parsepost", elementListObject.toString());
            JSONArray childArray = elementListObject.optJSONArray("elementList");

            JSONObject childObject = null;

            for (int i = 0; i < childArray.length(); i++) {
                childObject = childArray.optJSONObject(i);
                if (childObject.getString("beanType").equals("JbPostComment")) {
                    post.childList.add(parsePostComment(childObject));
                } else if (childObject.getString("beanType").equals("JbPostEmail")) {
                    post.childList.add(parsePostEmail(childObject));
                } else if (childObject.getString("beanType").equals("JbPostWfTask")) {
                    post.childList.add(parsePostWfTask(childObject));
                } else if (childObject.getString("beanType").equals("JbPostChat")) {
                    post.childList.add(parsePostChat(childObject));
                } else {
                    post.childList.add(parsePostTask(childObject));
                }
            }
        }

        return post;
    }

    private static PostTask parsePostTask(final JSONObject postTaskObject) throws Exception {
        PostTask postTask = new PostTask();

        postTask.id = postTaskObject.optLong("id");
        postTask.subPostType = postTaskObject.optInt("subPostType");
        postTask.status = postTaskObject.optInt("status");
        postTask.formatedCreatedOn = postTaskObject.optString("formatedCreatedOn");
        postTask.totalChildCount = postTaskObject.optInt("totalChildCount");
        postTask.unreaded = postTaskObject.optBoolean("unreaded");
        postTask.recipient = postTaskObject.optBoolean("recipient");
        postTask.formatedDueDate = postTaskObject.optString("formatedDueDate");
        postTask.message = postTaskObject.optString("message");
        postTask.createdOn = postTaskObject.optLong("createdOn");
        postTask.createdBy = postTaskObject.optLong("createdBy");
        postTask.dueDate = postTaskObject.optLong("dueDate");
        postTask.documentId = postTaskObject.optLong("documentId");
        postTask.hasExplicitRank = postTaskObject.optBoolean("hasExplicitRank");

        JSONArray childArray = postTaskObject.optJSONArray("childList");

        JSONObject childObject = null;

        for (int i = 0; i < childArray.length(); i++) {
            childObject = childArray.optJSONObject(i);
            if (childObject.optString("beanType").equals("JbPostSubComment")) {
                postTask.childList.add(parsePostSubComment(childObject));
            } else {
                postTask.childList.add(parsePostSubTask(childObject));
            }
        }

        return postTask;
    }

    private static PostComment parsePostComment(final JSONObject postCommentObject) throws Exception {
        PostComment postComment = new PostComment();

        postComment.id = postCommentObject.optLong("id");
        postComment.subPostType = postCommentObject.optInt("subPostType");
        postComment.formatedCreatedOn = postCommentObject.optString("formatedCreatedOn");
        postComment.totalChildCount = postCommentObject.optInt("totalChildCount");
        postComment.unreaded = postCommentObject.optBoolean("unreaded");
        postComment.message = postCommentObject.optString("message");
        postComment.createdOn = postCommentObject.optLong("createdOn");
        postComment.createdBy = postCommentObject.optLong("createdBy");
        postComment.documentId = postCommentObject.optLong("documentId");
        postComment.technicalFile = postCommentObject.optString("technicalFile");
        postComment.hasExplicitRank = postCommentObject.optBoolean("hasExplicitRank");

        JSONArray quickResponseArray = postCommentObject.optJSONArray("quickResponseList");

        for (int i = 0; i < quickResponseArray.length(); i++) {
            postComment.quickResponseList.add(quickResponseArray.optString((i)));
        }

        JSONArray childArray = postCommentObject.optJSONArray("childList");

        for (int i = 0; i < childArray.length(); i++) {
            postComment.childList.add(parsePostSubComment(childArray.optJSONObject(i)));
        }

        return postComment;
    }

    private static PostEmail parsePostEmail(final JSONObject postEmailObject) throws Exception {
        PostEmail postEmail = new PostEmail();
        postEmail.id = postEmailObject.optLong("id");
        postEmail.body = postEmailObject.optString("body");
        postEmail.subPostType = postEmailObject.optInt("subPostType");
        postEmail.formatedCreatedOn = postEmailObject.optString("formatedCreatedOn");
        postEmail.totalChildCount = postEmailObject.optInt("totalChildCount");
        postEmail.unreaded = postEmailObject.optBoolean("unreaded");
        postEmail.incoming = postEmailObject.optBoolean("incoming");
        postEmail.message = postEmailObject.optString("message");
        postEmail.subject = postEmailObject.optString("subject");
        postEmail.recipient = postEmailObject.optString("recipient");
        postEmail.createdOn = postEmailObject.optLong("createdOn");
        postEmail.createdBy = postEmailObject.optLong("createdBy");
        postEmail.documentId = postEmailObject.optLong("documentId");

        JSONArray childArray = postEmailObject.optJSONArray("childList");

        for (int i = 0; i < childArray.length(); i++) {
            postEmail.childList.add(parsePostSubComment(childArray.optJSONObject(i)));
        }

        return postEmail;
    }

    private static PostWfTask parsePostWfTask(final JSONObject postWfTaskObject) throws Exception {
        PostWfTask postWfTask = new PostWfTask();

        postWfTask.id = postWfTaskObject.optLong("id");
        postWfTask.owner = postWfTaskObject.optLong("owner");
        postWfTask.subPostType = postWfTaskObject.optInt("subPostType");
        postWfTask.formatedCreatedOn = postWfTaskObject.optString("formatedCreatedOn");
        postWfTask.title = postWfTaskObject.optString("title");
        postWfTask.formatedDueDate = postWfTaskObject.optString("formatedDueDate");
        postWfTask.totalChildCount = postWfTaskObject.optInt("totalChildCount");
        postWfTask.postWfTaskNumber = postWfTaskObject.optInt("postWfTaskNumber");
        postWfTask.status = postWfTaskObject.optInt("status");
        postWfTask.unreaded = postWfTaskObject.optBoolean("unreaded");
        postWfTask.dueDate = postWfTaskObject.optLong("dueDate");
        postWfTask.message = postWfTaskObject.optString("message");
        postWfTask.subject = postWfTaskObject.optString("subject");
        postWfTask.recipient = postWfTaskObject.optBoolean("recipient");
        postWfTask.createdOn = postWfTaskObject.optLong("createdOn");
        postWfTask.createdBy = postWfTaskObject.optLong("createdBy");
        postWfTask.documentId = postWfTaskObject.optLong("documentId");
        postWfTask.hasExplicitRank = postWfTaskObject.optBoolean("hasExplicitRank");

        JSONArray childArray = postWfTaskObject.optJSONArray("childList");

        for (int i = 0; i < childArray.length(); i++) {
            postWfTask.childList.add(parsePostSubComment(childArray.optJSONObject(i)));
        }

        return postWfTask;
    }

    public static PostChat parsePostChat(final JSONObject postChatObject) throws Exception {
        PostChat postChat = new PostChat();

        postChat.id = postChatObject.optLong("id");
        postChat.subPostType = postChatObject.optInt("subPostType");
        postChat.formatedCreatedOn = postChatObject.optString("formatedCreatedOn");
        postChat.unreaded = postChatObject.optBoolean("unreaded");
        postChat.message = postChatObject.optString("message");
        postChat.createdOn = postChatObject.optLong("createdOn");
        postChat.createdBy = postChatObject.optLong("createdBy");
        postChat.documentId = postChatObject.optLong("documentId");

        return postChat;
    }

    private static PostSubTask parsePostSubTask(final JSONObject postSubTaskObject) throws Exception {
        PostSubTask postSubTask = new PostSubTask();

        postSubTask.id = postSubTaskObject.optLong("id");
        postSubTask.subPostType = postSubTaskObject.optInt("subPostType");
        postSubTask.status = postSubTaskObject.optInt("status");
        postSubTask.formatedCreatedOn = postSubTaskObject.optString("formatedCreatedOn");
        postSubTask.unreaded = postSubTaskObject.optBoolean("unreaded");
        postSubTask.recipient = postSubTaskObject.optBoolean("recipient");
        postSubTask.formatedDueDate = postSubTaskObject.optString("formatedDueDate");
        postSubTask.message = postSubTaskObject.optString("message");
        postSubTask.createdOn = postSubTaskObject.optLong("createdOn");
        postSubTask.createdBy = postSubTaskObject.optLong("createdBy");
        postSubTask.dueDate = postSubTaskObject.optLong("dueDate");
        postSubTask.documentId = postSubTaskObject.optLong("documentId");

        return postSubTask;
    }

    private static PostSubComment parsePostSubComment(final JSONObject postSubCommentObject) throws JSONException {
        PostSubComment postSubComment = new PostSubComment();
        postSubComment.id = postSubCommentObject.optLong("id");
        postSubComment.subPostType = postSubCommentObject.optInt("subPostType");
        postSubComment.formatedCreatedOn = postSubCommentObject.optString("formatedCreatedOn");
        postSubComment.unreaded = postSubCommentObject.optBoolean("unreaded");
        postSubComment.message = postSubCommentObject.optString("message");
        postSubComment.createdOn = postSubCommentObject.optLong("createdOn");
        postSubComment.createdBy = postSubCommentObject.optLong("createdBy");
        postSubComment.documentId = postSubCommentObject.optLong("documentId");
        postSubComment.hasExplicitRank = postSubCommentObject.optBoolean("hasExplicitRank");
        return postSubComment;
    }

    public PostTask getTaskStatusRequest(long taskId) throws Exception {
        JSONObject taskStatusResponse = new JSONObject(ServerConnector.getInstance().getTaskStatusRequest(taskId));

        ExceptionManager.getInstance().checkException(taskStatusResponse);

        JSONObject postTaskObject = taskStatusResponse.optJSONObject("postTask");

        PostTask postTask = new PostTask();

        postTask.id = postTaskObject.optLong("id");
        postTask.subPostType = postTaskObject.optInt("subPostType");
        postTask.status = postTaskObject.optInt("status");
        postTask.formatedCreatedOn = postTaskObject.optString("formatedCreatedOn");
        postTask.totalChildCount = postTaskObject.optInt("totalChildCount");
        postTask.unreaded = postTaskObject.optBoolean("unreaded");
        postTask.recipient = postTaskObject.optBoolean("recipient");
        postTask.formatedDueDate = postTaskObject.optString("formatedDueDate");
        postTask.message = postTaskObject.optString("message");
        postTask.createdOn = postTaskObject.optLong("createdOn");
        postTask.createdBy = postTaskObject.optLong("createdBy");
        postTask.dueDate = postTaskObject.optLong("dueDate");
        postTask.documentId = postTaskObject.optLong("documentId");

        return postTask;
    }

    public boolean changeTaskStatus(final long taskId, final int status) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().changeTaskStatusRequest(taskId, status)));
        return true;
    }

    public boolean changeTaskStatus(final long taskId, final int status, final String message) throws Exception {
        if (message == null) {
            ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().changeTaskStatusRequest(taskId, status)));
        } else {
            ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().changeTaskStatusRequest(taskId, status, message)));
        }

        return true;
    }

    public boolean createPostWfTask(final long postId, final String description, final long wfDefId) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().createPostWfTaskRequest(postId, description, wfDefId)));
        return true;
    }

    public WfTaskStatus getWfTaskStatusRequest(long wfTaskId) throws Exception {
        JSONObject wfTaskStatusResponse = new JSONObject(ServerConnector.getInstance().getWfTaskStatusRequest(wfTaskId));

        ExceptionManager.getInstance().checkException(wfTaskStatusResponse);

        JSONObject wfTaskStatusObject = wfTaskStatusResponse.optJSONObject("wfTaskStatus");

        WfTaskStatus wfTaskStatus = new WfTaskStatus();

        wfTaskStatus.id = wfTaskStatusObject.optLong("id");
        wfTaskStatus.stepDescription = wfTaskStatusObject.optString("stepDescription");
        wfTaskStatus.status = wfTaskStatusObject.optInt("status");
        wfTaskStatus.formatedCreatedOn = wfTaskStatusObject.optString("formatedCreatedOn");
        wfTaskStatus.resultType = wfTaskStatusObject.optInt("resultType");
        wfTaskStatus.status = wfTaskStatusObject.optInt("status");
        wfTaskStatus.recipient = wfTaskStatusObject.optBoolean("recipient");
        wfTaskStatus.formatedDueDate = wfTaskStatusObject.optString("formatedDueDate");
        wfTaskStatus.message = wfTaskStatusObject.optString("message");
        wfTaskStatus.createdOn = wfTaskStatusObject.optLong("createdOn");
        wfTaskStatus.createdBy = wfTaskStatusObject.optLong("createdBy");
        wfTaskStatus.dueDate = wfTaskStatusObject.optLong("dueDate");
        wfTaskStatus.recipient = wfTaskStatusObject.optBoolean("recipient");
        wfTaskStatus.stepDueDate = wfTaskStatusObject.optLong("stepDueDate");
        wfTaskStatus.stepName = wfTaskStatusObject.optString("stepName");

        JSONArray metaArray = wfTaskStatusObject.optJSONArray("metaList");

        for (int i = 0; i < metaArray.length(); i++) {
            wfTaskStatus.metaList.add(MetaManager.getInstance().parseFillMetaDef(metaArray.optJSONObject(i)));
        }

        return wfTaskStatus;
    }

    public boolean changeWfTaskStatus(final long wfTaskId, final String message, final int status, final Integer resultId, final List<FillMeta> metaList) throws Exception {
        ExceptionManager.getInstance().checkException(new JSONObject(ServerConnector.getInstance().changeWfTaskStatusRequest(wfTaskId, message, status, resultId, metaList)));
        return true;
    }
}
