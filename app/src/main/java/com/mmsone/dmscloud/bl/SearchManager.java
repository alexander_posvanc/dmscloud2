package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.ESearchFilterDesignation;
import com.mmsone.dmscloud.bo.WallResponse;
import com.mmsone.dmscloud.bo.bean.SavedSearch;
import com.mmsone.dmscloud.bo.bean.SearchFilter;

public class SearchManager {

	private static SearchManager mInstance = null;
	private Map<Integer,SearchFilter> mFilterList;
	
	public static SearchManager getInstance() {
		if (mInstance == null) {
			mInstance = new SearchManager();
		}
		return mInstance;
	}
	
	protected SearchManager() {
		mFilterList = new HashMap<Integer, SearchFilter>();
	}
	
	/**
	 * Send saved search request and parse saved search list response
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<SavedSearch> getSavedSearchList() throws Exception {
		JSONObject savedSearchListResponse = new JSONObject(ServerConnector.getInstance().getSavedSearchListRequest());
	
		ExceptionManager.getInstance().checkException(savedSearchListResponse);

		List<SavedSearch> list = new ArrayList<SavedSearch>();

		JSONArray savedSearcJSONArray = savedSearchListResponse.optJSONArray("list");

		for (int i = 0; i < savedSearcJSONArray.length(); i++) {
			list.add(parseSavedSearch( savedSearcJSONArray.optJSONObject(i)));
		}

		return list;
	}
	
	/**
	 * Send save saved search request and parse response
	 * 
	 * @param saveName
	 * @param filterList
	 * @return
	 * @throws Exception
	 */
	public WallResponse saveSavedSearch(final String saveName, final List<SearchFilter> filterList) throws Exception {
		JSONObject responseObject = new JSONObject(ServerConnector.getInstance().saveSavedSearchRequest(saveName, 0, 5, filterList));
	
		ExceptionManager.getInstance().checkException(responseObject);
		
		WallResponse wallResponse = new WallResponse();

		JSONArray wallJSONArray = responseObject.optJSONArray("elementList");

		for (int i = 0; i < wallJSONArray.length(); i++) {
			wallResponse.elementList.add(PostManager.parsePost(0, 5, wallJSONArray.optJSONObject(i)));
		}
		
		return wallResponse;
	}
	
	public SavedSearch parseSavedSearch(final JSONObject savedSearchObject) {
		SavedSearch savedSearch = new SavedSearch();
		savedSearch.id = savedSearchObject.optLong("id");
		savedSearch.name = savedSearchObject.optString("name");
		return savedSearch;
	}
	
	public List<SearchFilter> getSavedSearch(final long savedSearchId) throws Exception {
		JSONObject savedSearchResponse = new JSONObject(ServerConnector.getInstance().getSavedSearchRequest(savedSearchId));
	
		ExceptionManager.getInstance().checkException(savedSearchResponse);

		List<SearchFilter> list = new ArrayList<SearchFilter>();

		JSONArray filterListJSONArray = savedSearchResponse.optJSONArray("filterList");

		for (int i = 0; i < filterListJSONArray.length(); i++) {
			list.add(parseSearchFilter( filterListJSONArray.optJSONObject(i)));
		}

		return list;
	}
	
	public SearchFilter parseSearchFilter(final JSONObject searchFilterObject) throws JSONException {
		SearchFilter searchFilter = new SearchFilter();
		
		searchFilter.dateFrom = searchFilterObject.optLong("dateFrom");
		searchFilter.dateTil = searchFilterObject.optLong("dateTil");
		searchFilter.designation = ESearchFilterDesignation.valueOf(searchFilterObject.optString("designation"));
		searchFilter.metaId = searchFilterObject.optLong("dateFrom");
		searchFilter.operator = searchFilterObject.optString("operator");
		searchFilter.value = searchFilterObject.optString("value");
		
		return searchFilter;
	}
	
	public void addSearcFilter(int id, SearchFilter searchFilter){
		mFilterList.put(id, searchFilter);
	}
	
	public void removeSearcFilter(int id){
		mFilterList.remove(id);
	}
	
	public void crearFilterList(){
		mFilterList.clear();
	}
	
	public boolean isFilterListEmpty(){
		return mFilterList.isEmpty();
	}
	
	public List<SearchFilter> getFilterList(){
		return new ArrayList<SearchFilter>(mFilterList.values());
	}
}