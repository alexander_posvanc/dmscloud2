package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.Customer;

public class CustomerManager {

	private static CustomerManager mInstance = null;

	public static CustomerManager getInstance() {
		if (mInstance == null) {
			mInstance = new CustomerManager();
		}
		return mInstance;
	}

	protected CustomerManager() {
	}

	/**
	 * Parse customer
	 * 
	 * @param customerObject
	 * @return customer
	 */
	private Customer parseCostumer(JSONObject costumerObject){
		Customer customer = new Customer();

		customer.id = costumerObject.optLong("id");
		customer.name = costumerObject.optString("name");
		customer.info = costumerObject.optString("info");
		customer.person = costumerObject.optBoolean("person");

		return customer;
	}

	/**
	 * Parse customer list
	 * 
	 * @return Customer list
	 * @throws Exception
	 */
	public List<Customer> parseCostumerList() throws Exception {
		JSONObject customerListResponse = new JSONObject(ServerConnector.getInstance().getCustomerListRequest());

		ExceptionManager.getInstance().checkException(customerListResponse);

		List<Customer> list = new ArrayList<Customer>();
		JSONArray customerJSONArray = customerListResponse.optJSONArray("list");
		for (int i = 0; i < customerJSONArray.length(); i++) {
			list.add(parseCostumer(customerJSONArray.optJSONObject(i)));
		}
		
		return list;
	}
}
