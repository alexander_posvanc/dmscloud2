package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mmsone.dmscloud.bo.bean.Distribution;
import com.mmsone.dmscloud.bo.bean.Group;

public class GroupAndDistributionManager {
	
	private static GroupAndDistributionManager mInstance = null;

	private List<Group> mGroupList;
	private List<Distribution> mDistributionList;

	public static GroupAndDistributionManager getInstance() {
		if (mInstance == null) {
			mInstance = new GroupAndDistributionManager();
		}
		return mInstance;
	}

	/**
	 * Parse group object
	 * 
	 * @param groupObject
	 * @return group
	 */
	private Group parseGroup(JSONObject groupObject) {
		Group group = new Group();
		
		group.id = groupObject.optLong("id");
		group.name = groupObject.optString("name");
		group.active = groupObject.optBoolean("active");
		
		JSONArray userArray = groupObject.optJSONArray("userList");
		for (int i = 0; i < userArray.length(); i++) {
			group.userList.add(userArray.optLong(i));
		}
		return group;
	}

	/**
	 * Parse distribution object
	 * 
	 * @param distributionObject
	 * @return distribution
	 */
	private Distribution parseDistribution(JSONObject distributionObject)  {
		Distribution distribution = new Distribution();
		
		distribution.id = distributionObject.optLong("id");
		distribution.name = distributionObject.optString("name");
		distribution.active = distributionObject.optBoolean("active");
		
		JSONArray userArray = distributionObject.optJSONArray("userList");
		for (int i = 0; i < userArray.length(); i++) {
			distribution.userList.add(userArray.optLong(i));
		}
		return distribution;
	}

	/**
	 * Parse group list
	 * 
	 * @param groupListResponse
	 * @return group list
	 */
	public List<Group> parseGroupList(JSONObject groupListResponse) {
		mGroupList = new ArrayList<Group>();
		JSONArray groupJSONArray = groupListResponse.optJSONArray("list");
		for (int i = 0; i < groupJSONArray.length(); i++) {
			mGroupList.add(parseGroup(groupJSONArray.optJSONObject(i)));
		}

		return mGroupList;
	}

	/**
	 * Parse distribution list
	 * 
	 * @param distributionList
	 * @return distribution list
	 */
	public List<Distribution> parseDistributionList(JSONObject distributionList){
		mDistributionList = new ArrayList<Distribution>();
		
		JSONArray distributionJSONArray = distributionList.optJSONArray("list");
		for (int i = 0; i < distributionJSONArray.length(); i++) {
			mDistributionList.add(parseDistribution(distributionJSONArray.optJSONObject(i)));
		}

		return mDistributionList;
	}

	public List<Distribution> getDistributionList() {
		return mDistributionList;
	}

	public List<Group> getGroupList() {
		return mGroupList;
	}
}
