package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.CodeStore;

public class CodeStoreManager {

	private static CodeStoreManager mInstance = null;

	public static CodeStoreManager getInstance() {
		if (mInstance == null) {
			mInstance = new CodeStoreManager();
		}
		return mInstance;
	}

	protected CodeStoreManager() {
	}

	/**
	 * Parse code store json object
	 * 
	 * @param codeStoreObject
	 * @return
	 */
	private CodeStore parseCodeStore(JSONObject codeStoreObject) {
		CodeStore codeStore = new CodeStore();

		codeStore.id = codeStoreObject.optInt("id");
		codeStore.text = codeStoreObject.optString("text");

		return codeStore;
	}

	/**
	 * Parse code store list by type id
	 * 
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	public List<CodeStore> parseCodeStoreList(long typeId) throws Exception {
		JSONObject codeStoreListResponse = new JSONObject(ServerConnector.getInstance().getCodeStoreRequest(typeId));

		ExceptionManager.getInstance().checkException(codeStoreListResponse);
	
		List<CodeStore> list = new ArrayList<CodeStore>();

		JSONArray codeStoreJSONArray = codeStoreListResponse.optJSONArray("list");
		for (int i = 0; i < codeStoreJSONArray.length(); i++) {
			list.add(parseCodeStore(codeStoreJSONArray.optJSONObject(i)));
		}

		return list;
	}
	
	/**
	 * Parse code store list by short key
	 * 
	 * @param shortKey
	 * @return
	 * @throws Exception
	 */
	public List<CodeStore> parseCodeStoreList(String shortKey) throws Exception {
		JSONObject codeStoreListResponse = new JSONObject(ServerConnector.getInstance().getCodeStoreRequest(shortKey));

		ExceptionManager.getInstance().checkException(codeStoreListResponse);

		List<CodeStore> list = new ArrayList<CodeStore>();

		JSONArray codeStoreJSONArray = codeStoreListResponse.optJSONArray("list");
		for (int i = 0; i < codeStoreJSONArray.length(); i++) {
			list.add(parseCodeStore(codeStoreJSONArray.optJSONObject(i)));
		}

		return list;
	}
}
