package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.TaskSearch;

public class TaskManager {

	private static TaskManager mInstance = null;

	public static TaskManager getInstance() {
		if (mInstance == null) {
			mInstance = new TaskManager();
		}
		return mInstance;
	}

	protected TaskManager() {
	}

	/**
	 * Send task list request and parse response
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<TaskSearch> getTaskList() throws Exception {
		JSONObject taskListResponse = new JSONObject(ServerConnector.getInstance().getTaskListRequest());

		ExceptionManager.getInstance().checkException(taskListResponse);

		List<TaskSearch> taskList = new ArrayList<TaskSearch>();

		JSONArray taskJSONArray = taskListResponse.optJSONArray("list");

		for (int i = 0; i < taskJSONArray.length(); i++) {
			taskList.add(parseTaskSearch(taskJSONArray.optJSONObject(i)));
		}

		return taskList;
	}

	/**
	 * parse task search json object
	 * 
	 * @param taskSearchObject
	 * @return
	 */
	private TaskSearch parseTaskSearch(final JSONObject taskSearchObject)  {
		TaskSearch taskSearch = new TaskSearch();

		taskSearch.id = taskSearchObject.optLong("id");
		taskSearch.createdBy = taskSearchObject.optString("createdBy");
		taskSearch.createdOn = taskSearchObject.optLong("createdOn");
		taskSearch.formatedCreatedOn = taskSearchObject.optString("formatedCreatedOn");
		taskSearch.dueDate = taskSearchObject.optLong("dueDate");
		taskSearch.formatedDueDate = taskSearchObject.optString("formatedDueDate");
		taskSearch.message = taskSearchObject.optString("message");
		taskSearch.recipient = taskSearchObject.optString("recipient");
		taskSearch.postTitle = taskSearchObject.optString("postTitle");
		taskSearch.postNumber = taskSearchObject.optInt("postNumber");
		taskSearch.status = taskSearchObject.optInt("status");
		taskSearch.type = taskSearchObject.optInt("type");

		return taskSearch;
	}
}