package com.mmsone.dmscloud.bl;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;

public class UserManager {

    private static UserManager mInstance = null;

    public static UserManager getInstance() {
        if (mInstance == null) {
            mInstance = new UserManager();
        }
        return mInstance;
    }

    protected UserManager() {
    }

    /**
     * Parse user json object
     *
     * @param userObject
     * @return
     * @throws JSONException
     * @throws DMSCloudDBException
     */
    public User parseUser(JSONObject userObject) throws JSONException, DMSCloudDBException {
        User user = new User();

        user.id = userObject.optLong("id");
        user.lastName = userObject.optString("lastName");
        user.fax = userObject.optString("fax");
        user.formatedCreatedOn = userObject.optString("formatedCreatedOn");
        user.about = userObject.optString("about");
        user.image = userObject.optString("image");
        user.createdOn = userObject.optLong("createdOn");
        user.title = userObject.optString("title");
        user.mobilePhone = userObject.optString("mobilePhone");
        user.email = userObject.optString("email");
        user.formatedName = userObject.optString("formatedName");
        user.timeOffset = userObject.optLong("timeOffset");
        user.userName = userObject.optString("userName");
        user.active = userObject.optBoolean("active");
        user.blocked = userObject.optBoolean("blocked");
        user.language = userObject.optString("language");
        user.workPhone = userObject.optString("workPhone");
        user.firstName = userObject.optString("firstName");


        DataBaseConnector.getInstance().addUser(user);

        return user;
    }

    /**
     * Parse user list json array
     *
     * @return
     * @throws Exception
     */
    public boolean parseUserList() throws Exception {
        JSONObject userListResponseObject = new JSONObject(ServerConnector.getInstance().getUserListRequest());

        ExceptionManager.getInstance().checkException(userListResponseObject);

        JSONArray userJSONArray = userListResponseObject.optJSONArray("userList");
        for (int i = 0; i < userJSONArray.length(); i++) {
            parseUser(userJSONArray.optJSONObject(i));
        }

        return true;
    }

    /**
     * Get current user from database
     *
     * @return
     * @throws DMSCloudDBException
     */
    public User getCurrentUser() throws DMSCloudDBException {
        return DataBaseConnector.getInstance().getLoginResponse().currentUser;
    }

    /**
     * Check user role
     *
     * @param userRole
     * @return
     * @throws DMSCloudDBException
     */
    public boolean hasUserRole(String userRole) throws DMSCloudDBException {
        return DataBaseConnector.getInstance().getLoginResponse().userRoles.contains(userRole);
    }
}
