package com.mmsone.dmscloud.bl;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

import android.view.View;
import android.view.ViewGroup;

import com.mmsone.dmscloud.bo.EPostChildType;

/**
 * Handle nested listview item recycling  
 */
public class ViewPoolManager {
	
	private Map<EPostChildType, ArrayDeque<View>> mAllObjects;
	private PoolObjectFactory<EPostChildType, View> mPoolObjectFactory;
	
	
	public ViewPoolManager(PoolObjectFactory<EPostChildType, View> factory){
		mAllObjects = new HashMap<EPostChildType, ArrayDeque<View>>();
		mPoolObjectFactory = factory;
	}
	
	/**
	 * If view exists return stored view else return a new view
	 */
	public View getView(EPostChildType key){
		ArrayDeque<View> lView = mAllObjects.get(key);
		if(lView == null){		
			lView = new ArrayDeque<View>();
			View view = mPoolObjectFactory.create(key);
			lView.offer(view);
			mAllObjects.put(key, lView);
		}
		
		View view = lView.pollFirst();
		if(view == null){
			return mPoolObjectFactory.create(key);
		}

		return view;
	}
	
	public void clear(){
		mAllObjects.clear();
	}
	
	public void returnView(EPostChildType key, View view){
		ArrayDeque<View> queue = mAllObjects.get(key);
		if(queue != null){
			((ViewGroup)view.getParent()).removeView(view);
			queue.offer(view);
		}
	}
}
