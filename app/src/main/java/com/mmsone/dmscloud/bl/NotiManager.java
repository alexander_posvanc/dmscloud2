package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotiManager {

	private static NotiManager mInstance = null;

	private List<String> mMessageList;

	public static NotiManager getInstance() {
		if (mInstance == null) {
			mInstance = new NotiManager();
		}
		return mInstance;
	}

	protected NotiManager() {
		mMessageList = new ArrayList<String>();
	}

	public void addMessage(String message) {
		mMessageList.add( message);
	}

	/**
	 * Build notification message
	 * 
	 * @return
	 */
	public String getMessages() {
		String mess = "";
		List<String> actMessList = new ArrayList<String>(mMessageList);
		Collections.reverse(actMessList);
		for (String m : actMessList) {
			mess += m + "\n";
		}
		return mess;
	}

	public String getMessagesCount() {
		return "" + mMessageList.size();
	}
	
	public void clearMessageList() {
		mMessageList.clear();
	}
}
