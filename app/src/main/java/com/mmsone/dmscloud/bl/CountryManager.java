package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.City;
import com.mmsone.dmscloud.bo.bean.County;

public class CountryManager {

	private static CountryManager mInstance = null;

	public static CountryManager getInstance() {
		if (mInstance == null) {
			mInstance = new CountryManager();
		}
		return mInstance;
	}

	protected CountryManager() {
	}

	/**
	 * Parse city
	 * 
	 * @param cityObject
	 * @param countyId
	 * @return
	 * @throws Exception
	 */
	private City parseCity(JSONObject cityObject, long countyId) throws Exception {
		City city = new City();

		city.id = cityObject.optLong("id");
		city.name = cityObject.optString("name");
		city.countyId =  countyId;
		
		DataBaseConnector.getInstance().addCity(city);

		return city;
	}

	/**
	 * Parse county
	 * 
	 * @param countyObject
	 * @param countryId
	 * @return
	 * @throws Exception
	 */
	private County parseCounty(JSONObject countyObject, final long countryId) throws Exception {
		County county = new County();

		county.id = countyObject.optLong("id");
		county.name = countyObject.optString("name");
		county.countryId = countryId;

		JSONArray childArray = countyObject.optJSONArray("childList");
		for (int i = 0; i < childArray.length(); i++) {
			county.childList.add(parseCity(childArray.optJSONObject(i),county.id));
		}
		
		DataBaseConnector.getInstance().addCounty(county);

		return county;
	}

	/**
	 * Parse country
	 * 
	 * @param countryId
	 * @return
	 * @throws Exception
	 */
	public List<County> parseCountryResponse(final long countryId) throws Exception {
		JSONObject countryResponse = new JSONObject(ServerConnector.getInstance().getCountryRequest(countryId));

		ExceptionManager.getInstance().checkException(countryResponse);

		List<County> list = new ArrayList<County>();

		JSONArray countryJSONArray = countryResponse.optJSONArray("list");
		for (int i = 0; i < countryJSONArray.length(); i++) {
			list.add(parseCounty(countryJSONArray.optJSONObject(i), countryId));
		}

		return list;
	}
}