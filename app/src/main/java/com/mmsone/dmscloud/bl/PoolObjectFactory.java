package com.mmsone.dmscloud.bl;

public interface PoolObjectFactory<T1, T2> {
	T2 create(T1 key);
}
