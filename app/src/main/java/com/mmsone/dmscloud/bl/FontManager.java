package com.mmsone.dmscloud.bl;

import com.mmsone.dmscloud.DMSCloudApplication;

import android.graphics.Typeface;

public class FontManager {
	
	private static FontManager mInstance = null;
	private Typeface  mRobotoBold, mRobotoMedium, mRobotoLight;

	public static FontManager getInstance() {
		if (mInstance == null) {
			mInstance = new FontManager();
		}
		return mInstance;
	}

	/**
	 * Load fonts from assets
	 */
	protected FontManager() {
		mRobotoBold = Typeface.createFromAsset(DMSCloudApplication.getContext().getAssets(), "fonts/Roboto-Bold.ttf");
		mRobotoMedium = Typeface.createFromAsset(DMSCloudApplication.getContext().getAssets(), "fonts/Roboto-Medium.ttf");
		mRobotoLight = Typeface.createFromAsset(DMSCloudApplication.getContext().getAssets(), "fonts/Roboto-Light.ttf");
	}

	public Typeface getRobotoBoldTtf() {
		return mRobotoBold;
	}

	public Typeface getRobotoLightTtf() {
		return mRobotoLight;
	}

	public Typeface getRobotoMediumTtf() {
		return mRobotoMedium;
	}
}
