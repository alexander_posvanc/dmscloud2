package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.DrawerItem;
import com.mmsone.dmscloud.bo.bean.Post;

public class DrawerItemManager {

    private static DrawerItemManager mInstance = null;

    private List<DrawerItem> mMainDrawerItemList;
    private String[] mDrawerTextList;
    private Context mContext;

    private DrawerItemManager(Context context) {
        mContext = context;
    }

    public static DrawerItemManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DrawerItemManager(context);
        }
        return mInstance;
    }

    public List<DrawerItem> getMainDrawerItemList(Context c) {
        mDrawerTextList = c.getResources().getStringArray(R.array.main_drawer_text_array);

        mMainDrawerItemList = new ArrayList<DrawerItem>();
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[0], R.drawable.ic_wall_menu_bt_white));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[1], R.drawable.ic_tasks_menu_bt_white));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[2], R.drawable.ujtema_feher));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[3], R.drawable.ic_file_up_menu_bt_white));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[4], R.drawable.ic_file_down));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[5], R.drawable.ic_settings_menu_bt_white));
        mMainDrawerItemList.add(new DrawerItem(mDrawerTextList[6], R.drawable.ic_singout_menu_bt_white));

        return mMainDrawerItemList;
    }

    /**
     * Build file upload drawer list
     *
     * @return
     */
    public List<DrawerItem> getFileUploadDrawerItemList() {
        mDrawerTextList = mContext.getResources().getStringArray(R.array.file_upload_drawer_text_array);

        List<DrawerItem> drawerItemList = new ArrayList<DrawerItem>();

        drawerItemList.add(new DrawerItem(mDrawerTextList[0], R.drawable.ic_photo));
        drawerItemList.add(new DrawerItem(mDrawerTextList[1], R.drawable.ic_picture));
        drawerItemList.add(new DrawerItem(mDrawerTextList[2], R.drawable.ic_file_select));

        return drawerItemList;
    }

    /**
     * Build advanced search drawr item list
     *
     * @return
     */
    public List<DrawerItem> getAdvancedSearchDrawerItemList() {
        mDrawerTextList = mContext.getResources().getStringArray(R.array.advanced_search_drawer_text_array);

        List<DrawerItem> drawerItemList = new ArrayList<DrawerItem>();

        drawerItemList.add(new DrawerItem(mDrawerTextList[0], R.drawable.ic_file_search_save));
        drawerItemList.add(new DrawerItem(mDrawerTextList[1], R.drawable.ic_file_search_load));
        drawerItemList.add(new DrawerItem(mDrawerTextList[2], R.drawable.ic_search_delete));

        return drawerItemList;
    }

    /**
     * Build post drawer item list
     *
     * @param post
     * @return
     */
    public List<DrawerItem> getPostDrawerItemList(Post post) {
        mDrawerTextList = mContext.getResources().getStringArray(R.array.post_drawer_text_array);

        List<DrawerItem> postDrawerItemList = new ArrayList<DrawerItem>();

        postDrawerItemList.add(new DrawerItem(mDrawerTextList[0], R.drawable.ic_tasks_green));
        postDrawerItemList.add(new DrawerItem(mDrawerTextList[1], R.drawable.ic_workflow_green));
        postDrawerItemList.add(new DrawerItem(mDrawerTextList[2], R.drawable.ic_comment_green));
        postDrawerItemList.add(new DrawerItem(mDrawerTextList[3], R.drawable.ic_hide_green));

        if (post.marked) {
            postDrawerItemList.add(new DrawerItem(mDrawerTextList[5], R.drawable.ic_star_green));
        } else {
            postDrawerItemList.add(new DrawerItem(mDrawerTextList[4], R.drawable.ic_star_green));
        }

        postDrawerItemList.add(new DrawerItem(mDrawerTextList[6], R.drawable.ic_share_green));

        if (post.documentId != 0) {
            postDrawerItemList.add(new DrawerItem(mDrawerTextList[7], R.drawable.ic_docs_green));
        }

        postDrawerItemList.add(new DrawerItem(mDrawerTextList[8], R.drawable.ic_share_green));

        return postDrawerItemList;
    }


    /**
     * Build technical post drawer item list
     *
     * @param post
     * @return
     */

    public List<DrawerItem> getTechicalPostDrawerItemList(Post post) {
        mDrawerTextList = mContext.getResources().getStringArray(R.array.post_drawer_text_array);

        List<DrawerItem> postDrawerItemList = new ArrayList<DrawerItem>();

        postDrawerItemList.add(new DrawerItem(mDrawerTextList[6], R.drawable.ic_share_green));
        postDrawerItemList.add(new DrawerItem(mDrawerTextList[3], R.drawable.ic_hide_green));

        if (post.marked) {
            postDrawerItemList.add(new DrawerItem(mDrawerTextList[5], R.drawable.ic_star_green));
        } else {
            postDrawerItemList.add(new DrawerItem(mDrawerTextList[4], R.drawable.ic_star_green));
        }

        return postDrawerItemList;
    }
}
