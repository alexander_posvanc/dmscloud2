package com.mmsone.dmscloud.bl;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.bean.Document;
import com.mmsone.dmscloud.bo.bean.DocumentFile;
import com.mmsone.dmscloud.bo.bean.DocumentVersion;
import com.mmsone.dmscloud.bo.bean.ShowMeta;

public class DocumentManager {

	private static DocumentManager mInstance = null;

	public static DocumentManager getInstance() {
		if (mInstance == null) {
			mInstance = new DocumentManager();
		}
		return mInstance;
	}

	protected DocumentManager() {}

	/**
	 * Download and parse document
	 * 
	 * @param documentId
	 * @return document
	 * @throws Exception
	 */
	public Document getDocument(long documentId) throws Exception {
		JSONObject documentResponse = new JSONObject(ServerConnector.getInstance().getDocumentRequest(documentId));

		ExceptionManager.getInstance().checkException(documentResponse);

		JSONObject documentObject = documentResponse.optJSONObject("document");

		Document document = new Document();

		document.id = documentObject.optLong("id");
		document.createdBy = documentObject.optLong("createdBy");
		document.createdOn = documentObject.optLong("createdOn");
		document.direction = documentObject.optString("direction");
		document.documentNumber = documentObject.optString("documentNumber");
		document.documentType = documentObject.optString("documentType");
		document.formatedCreatedOn = documentObject.optString("formatedCreatedOn");

		JSONArray versionArray = documentObject.optJSONArray("versionList");

		for (int i = 0; i < versionArray.length(); i++) {
			document.versionList.add(parseDocumentVersion(versionArray.optJSONObject(i)));
		}

		return document;
	}

	/**
	 * Parse document version
	 * 
	 * @param documentVersionObject
	 * @return
	 */
	private DocumentVersion parseDocumentVersion(final JSONObject documentVersionObject) {
		DocumentVersion documentVersion = new DocumentVersion();

		documentVersion.id = documentVersionObject.optLong("id");
		documentVersion.versionNumber = documentVersionObject.optLong("versionNumber");
		documentVersion.description = documentVersionObject.optString("description");

		JSONArray fileArray = documentVersionObject.optJSONArray("fileList");
		JSONArray metaArray = documentVersionObject.optJSONArray("metaList");

		JSONObject fileObject = null;
		DocumentFile documentFile = null;

		for (int i = 0; i < fileArray.length(); i++) {
			fileObject = fileArray.optJSONObject(i);
			
			documentFile = new DocumentFile();

			documentFile.id = fileObject.optLong("id");
			documentFile.fileSize = fileObject.optLong("fileSize");
			documentFile.file = fileObject.optString("file");
			documentFile.fileHash = fileObject.optString("fileHash");
			documentFile.fileName = fileObject.optString("fileName");
			documentFile.mimeType = fileObject.optString("mimeType");

			documentVersion.fileList.add(documentFile);
		}

		JSONObject metaJsonObject = null;

		for (int i = 0; i < metaArray.length(); i++) {
			metaJsonObject = metaArray.optJSONObject(i);
			documentVersion.metaList.add(new ShowMeta(metaJsonObject.optLong("id"), metaJsonObject.optString("label"), metaJsonObject.optString("value")));
		}

		return documentVersion;
	}

	public boolean isLocalFileExist(DocumentFile file) {
		File localfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +Config.DOWNLOAD_DIR+ "/" + file.fileName);
		return localfile.exists();
	}

	/**
	 * Open file if exist or downloads with DownloadManager in background
	 * 
	 * @param context
	 * @param file
	 */
	@SuppressLint("NewApi")
	public void openOrDownloadFile(Context context, DocumentFile file) {
		File localfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +Config.DOWNLOAD_DIR+ "/" + file.fileName);
		if (localfile.exists()) {
			Intent intent = new Intent();
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(localfile), file.mimeType);
			context.startActivity(intent);
		} else {
			DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + file.file));
			request.setMimeType(file.mimeType);
			request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
			request.setDescription(file.fileName);
			request.setTitle(context.getString(R.string.document_download_title));
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				request.allowScanningByMediaScanner();
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
			}
			request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, file.fileName);
			((DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
		}
	}
}
