package com.mmsone.dmscloud.bl;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.res.Configuration;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bo.EDMSCloudExceptionType;
import com.mmsone.dmscloud.bo.LoginResponse;
import com.mmsone.dmscloud.ex.AuthenticationException;

public class LoginManager {

	private static LoginManager mInstance = null;

	public static LoginManager getInstance() {
		if (mInstance == null) {
			mInstance = new LoginManager();
		}
		return mInstance;
	}

	protected LoginManager() {}

	/**
	 * Send login request, parse and save in database login response 
	 * 
	 * @param password
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public Void login(String password, String userName) throws Exception {
		JSONObject loginResponseObject = new JSONObject(ServerConnector.getInstance().unpLoginRequest(password, userName));

		ExceptionManager.getInstance().checkException(loginResponseObject);

		LoginResponse loginResponse = new LoginResponse();

		loginResponse.maxFileSize = loginResponseObject.optLong("maxFileSize");

		JSONArray forbiddenFileExtensionsJSONArray = loginResponseObject.optJSONArray("forbiddenFileExtensions");

		for (int i = 0; i < forbiddenFileExtensionsJSONArray.length(); i++) {
			loginResponse.forbiddenFileExtensions.add(forbiddenFileExtensionsJSONArray.optString(i));
		}

		JSONArray userRolesJSONArray = loginResponseObject.optJSONArray("userRoles");

		for (int i = 0; i < userRolesJSONArray.length(); i++) {
			loginResponse.userRoles.add(userRolesJSONArray.optString(i));
		}

		loginResponse.currentUser = UserManager.getInstance().parseUser(loginResponseObject.optJSONObject("currentUser"));

		DataBaseConnector.getInstance().addLoginResponse(loginResponse);
		
		Locale locale = new Locale(loginResponse.currentUser.language);
		Configuration config = new Configuration();
		config.locale = locale;
		DMSCloudApplication.getContext().getResources().updateConfiguration(config, DMSCloudApplication.getContext().getResources().getDisplayMetrics());

		return null;
	}
	
	/**
	 * Send rmm login request, parse and save in database login response 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Void rmmLogin() throws Exception {
		JSONObject loginResponseObject = new JSONObject(ServerConnector.getInstance().rmmLoginRequest());

		if(loginResponseObject.getString("responseType").equals(EDMSCloudExceptionType.AuthenticationException.name())){
			throw new AuthenticationException();
		}
		
		ExceptionManager.getInstance().checkException(loginResponseObject);
		
		LoginResponse loginResponse = new LoginResponse();

		loginResponse.maxFileSize = loginResponseObject.optLong("maxFileSize");

		JSONArray forbiddenFileExtensionsJSONArray = loginResponseObject.optJSONArray("forbiddenFileExtensions");

		for (int i = 0; i < forbiddenFileExtensionsJSONArray.length(); i++) {
			loginResponse.forbiddenFileExtensions.add(forbiddenFileExtensionsJSONArray.optString(i));
		}

		JSONArray userRolesJSONArray = loginResponseObject.optJSONArray("userRoles");

		for (int i = 0; i < userRolesJSONArray.length(); i++) {
			loginResponse.userRoles.add(userRolesJSONArray.optString(i));
		}

		loginResponse.currentUser = UserManager.getInstance().parseUser(loginResponseObject.optJSONObject("currentUser"));

		DataBaseConnector.getInstance().updateLoginResponse(loginResponse);
		
		return null;
	}
}