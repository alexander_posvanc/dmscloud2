package com.mmsone.dmscloud.bl;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;

public class PrefManager {

    private static final ILogger LOG = Logger.getLogger(PrefManager.class);

    private final static String SERVER_URL = "SERVER_URL";
    private final static String PUSH_EK = "PUSH_EK";
    private final static String IS_PUSH = "IS_PUSH";
    private final static String PROTOCOL_VERSION = "PROTOCOL_VERSION";
    private final static String LAST_UPDATE_TIME = "LAST_UPDATE_TIME";
    private final static String USER_IS_LOGGED = "USER_IS_LOGGED";
    private final static String USER_NAME = "USER_NAME";
    public final static String JSESSIONID = "JSESSIONID";
    public final static String C0 = "C0";
    public final static String WALL_IS_UP_TO_DATE = "WALL_IS_UP_TO_DATE";
    public final static String TASKS_IS_UP_TO_DATE = "TASKS_IS_UP_TO_DATE";
    public final static String IS_AUTH_EX = "IS_AUTH_EX";
    public final static String IS_CHANGED = "IS_CHANGED";
    public final static String IS_FIRST = "IS_FIRST";
    public final static String FIRST_SCROLL = "FIRST_SCROLL";
    public final static String ADD_TO_CHILD_COUNT = "ADD_TO_CHILD_COUNT";

    private static PrefManager mInstance = null;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public static PrefManager getInstance() {
        if (mInstance == null) {
            mInstance = new PrefManager();
        }
        return mInstance;
    }

    protected PrefManager() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(DMSCloudApplication.getContext());
        mEditor = mSharedPreferences.edit();
    }

    public boolean isUserLogged() {
        return mSharedPreferences.getBoolean(USER_IS_LOGGED, false);
    }

    public void setUserLogged(boolean isLogged) {
        LOG.debug("USER_IS_LOGGED: " + Boolean.toString(isLogged));
        mEditor.putBoolean(USER_IS_LOGGED, isLogged);
        mEditor.commit();
    }

    public boolean toBeUpdated() {
        if (mSharedPreferences.getLong(LAST_UPDATE_TIME, 0) + Config.UPDATE_TIME_IN_MILLIS < System.currentTimeMillis()) {
            setLastUpdate();
            return true;
        }
        return false;
    }

    public void setLastUpdate() {
        LOG.debug("LAST_UPDATE_TIME: " + Config.DATE_TIME_FORMAT.format(System.currentTimeMillis()));
        mEditor.putLong(LAST_UPDATE_TIME, System.currentTimeMillis());
        mEditor.commit();
    }

    public void setCookie(String name, String value) {
        mEditor.putString(name, value);
        mEditor.commit();
    }

    public String getCookie(String name) {
        return mSharedPreferences.getString(name, "");
    }

    public void setPush(boolean isPush) {
        LOG.debug("IS_PUSH: " + Boolean.toString(isPush));
        mEditor.putBoolean(IS_PUSH, isPush);
        mEditor.commit();
    }

    public boolean isPush() {
        return mSharedPreferences.getBoolean(IS_PUSH, true);
    }

    public void setWallUpToDate(boolean isUpToDate) {
        LOG.debug("WALL_IS_UP_TO_DATE: " + Boolean.toString(isUpToDate));
        mEditor.putBoolean(WALL_IS_UP_TO_DATE, isUpToDate);
        mEditor.commit();
    }

    public boolean isWallUpToDate() {
        return mSharedPreferences.getBoolean(WALL_IS_UP_TO_DATE, true);
    }

    public void setTasksUpToDate(boolean isUpToDate) {
        LOG.debug("TASKS_IS_UP_TO_DATE: " + Boolean.toString(isUpToDate));
        mEditor.putBoolean(TASKS_IS_UP_TO_DATE, isUpToDate);
        mEditor.commit();
    }

    public boolean isTasksUpToDate() {
        return mSharedPreferences.getBoolean(TASKS_IS_UP_TO_DATE, true);
    }

    public void setUserName(String userName) {
        LOG.debug("USER_NAME: " + userName);
        mEditor.putString(USER_NAME, userName);
        mEditor.commit();
    }

    public String getUserName() {
        return mSharedPreferences.getString(USER_NAME, "");
    }

    public void setPushEk(String ek) {
        LOG.debug("PUSH_EK : " + ek);
        mEditor.putString(PUSH_EK, ek);
        mEditor.commit();
    }

    public String getPushEk() {
        return mSharedPreferences.getString(PUSH_EK, "");
    }

    public void setServerAddress(String url) {
        LOG.debug("SERVER_URL : " + url);
        mEditor.putString(SERVER_URL, url);
        mEditor.commit();
    }

    public String getServerUrl() {
        return mSharedPreferences.getString(SERVER_URL, "");
    }

    public void setProtocolVersion(int version) {
        LOG.debug("PROTOCOL_VERSION : " + version);
        mEditor.putInt(PROTOCOL_VERSION, version);
        mEditor.commit();
    }

    public int getProtocolVersion() {
        return mSharedPreferences.getInt(PROTOCOL_VERSION, 7);
    }

    public boolean isChanged() {
        return mSharedPreferences.getBoolean(IS_CHANGED, false);
    }

    public void setIsChanged(boolean isLogged) {
        LOG.debug("USER_IS_LOGGED: " + Boolean.toString(isLogged));
        mEditor.putBoolean(IS_CHANGED, isLogged);
        mEditor.commit();
    }


    public void setAuthEx(boolean isEx) {
        LOG.debug("IS_AUTH_EX : " + Boolean.toString(isEx));
        mEditor.putBoolean(IS_AUTH_EX, isEx);
        mEditor.commit();
    }

    public boolean isAuthEx() {
        return mSharedPreferences.getBoolean(IS_AUTH_EX, false);
    }

    public void setIsFirst(boolean isEx) {
        mEditor.putBoolean(IS_FIRST, isEx);
        mEditor.commit();
    }

    public boolean isFirst() {
        return mSharedPreferences.getBoolean(IS_FIRST, true);
    }

    public void setIsFirstSroll(boolean isEx) {
        mEditor.putBoolean(FIRST_SCROLL, isEx);
        mEditor.commit();
    }

    public boolean isFirstScroll() {
        return mSharedPreferences.getBoolean(FIRST_SCROLL, true);
    }


}