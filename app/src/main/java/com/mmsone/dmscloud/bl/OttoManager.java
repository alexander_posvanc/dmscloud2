package com.mmsone.dmscloud.bl;

import com.squareup.otto.Bus;

/**
 * Otto is an event bus designed to decouple different parts of your application while still allowing them to communicate efficiently.
 */
public class OttoManager {
	
	private static OttoManager mInstance = null;
	private Bus mBus;

	private OttoManager() {
		mBus = new Bus();
	}

	public static OttoManager getInstance() {
		if (mInstance == null) {
			mInstance = new OttoManager();
		}
		return mInstance;
	}
	
	public Bus getBus(){
		return mBus;
	}
}
