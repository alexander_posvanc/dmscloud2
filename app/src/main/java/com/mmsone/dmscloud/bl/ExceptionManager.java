package com.mmsone.dmscloud.bl;

import android.app.Activity;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.mmsone.dmscloud.async.LogoutTask;
import com.mmsone.dmscloud.async.RmmLoginTask;
import com.mmsone.dmscloud.bo.EDMSCloudExceptionType;
import com.mmsone.dmscloud.ex.AlreadyExistsException;
import com.mmsone.dmscloud.ex.AuthenticationException;
import com.mmsone.dmscloud.ex.BadUserNameOrPasswordException;
import com.mmsone.dmscloud.ex.DisabledOrBlockedUserException;
import com.mmsone.dmscloud.ex.ErrorException;
import com.mmsone.dmscloud.ex.InsufficientRightsException;
import com.mmsone.dmscloud.ex.InvalidStatusException;
import com.mmsone.dmscloud.ex.ProtocolMismatchException;
import com.mmsone.dmscloud.ex.RaceConditionException;
import com.mmsone.dmscloud.ex.RmmExpiredException;
import com.mmsone.dmscloud.ex.ValidationException;
import com.mmsone.dmscloud.gcm.GCMManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;
import com.mmsone.dmscloud.ui.MainActivity;

public class ExceptionManager {

    private static final ILogger LOG = Logger.getLogger(ExceptionManager.class);

    private static ExceptionManager mInstance = null;
    private Map<String, Exception> mExceptionMap;

    private ExceptionManager() {
        mExceptionMap = new HashMap<String, Exception>();

        mExceptionMap.put(EDMSCloudExceptionType.AlreadyExistsException.name(), new AlreadyExistsException());
        mExceptionMap.put(EDMSCloudExceptionType.AuthenticationException.name(), new AuthenticationException());
        mExceptionMap.put(EDMSCloudExceptionType.BadUserNameOrPasswordException.name(), new BadUserNameOrPasswordException());
        mExceptionMap.put(EDMSCloudExceptionType.DisabledOrBlockedUserException.name(), new DisabledOrBlockedUserException());
        mExceptionMap.put(EDMSCloudExceptionType.ErrorException.name(), new ErrorException());
        mExceptionMap.put(EDMSCloudExceptionType.InsufficientRightsException.name(), new InsufficientRightsException());
        mExceptionMap.put(EDMSCloudExceptionType.InvalidStatusException.name(), new InvalidStatusException());
        mExceptionMap.put(EDMSCloudExceptionType.ProtocolMismatchException.name(), new ProtocolMismatchException());
        mExceptionMap.put(EDMSCloudExceptionType.RaceConditionException.name(), new RaceConditionException());
        mExceptionMap.put(EDMSCloudExceptionType.RmmExpiredException.name(), new RmmExpiredException());
        mExceptionMap.put(EDMSCloudExceptionType.ValidationException.name(), new ValidationException());
    }

    public static ExceptionManager getInstance() {
        if (mInstance == null) {
            mInstance = new ExceptionManager();
        }
        return mInstance;
    }

    /**
     * Check exception
     *
     * @param exceptionJSONObject
     * @throws Exception
     */
    public void checkException(JSONObject exceptionJSONObject) throws Exception {
        LOG.debug(exceptionJSONObject.toString());
        if (exceptionJSONObject.getBoolean("exception")) {
            if (exceptionJSONObject.getString("responseType").equals(EDMSCloudExceptionType.AuthenticationException.name())) {
                if (!PrefManager.getInstance().isAuthEx()) {
                    new RmmLoginTask().execute();
                } else {
//                    new RmmLoginTask().execute();

                    throw mExceptionMap.get(exceptionJSONObject.getString("responseType"));
                }
            } else {
                throw mExceptionMap.get(exceptionJSONObject.getString("responseType"));
            }
        }
    }


}
