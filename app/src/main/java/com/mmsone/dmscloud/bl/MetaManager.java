package com.mmsone.dmscloud.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.FillMetaDef;
import com.mmsone.dmscloud.bo.bean.MetaDef;

public class MetaManager {

	private static MetaManager mInstance = null;

	private List<MetaDef> mMetaDefList;
	private Map<Long, FillMeta> mFillMetaMap;

	public static MetaManager getInstance() {
		if (mInstance == null) {
			mInstance = new MetaManager();
		}
		return mInstance;
	}

	protected MetaManager() {
		mFillMetaMap = new HashMap<Long, FillMeta>();
	}
	
	public void addFillMeta(Long key, FillMeta meta) {
		mFillMetaMap.put(key, meta);
	}
	
	public void removeFillMeta(Long key) {
		mFillMetaMap.remove(key);
	}
	
	
	public void clearFillMetaMap() {
		mFillMetaMap.clear();
	}
	
	public List<FillMeta> getFillMetaList() {
		return new ArrayList<FillMeta>(mFillMetaMap.values());
	}

	public List<MetaDef> getMetaDefList() {
		return mMetaDefList;
	}

	/**
	 * Parse meta def list
	 * 
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public List<MetaDef> parseMetaDefList(String response) throws Exception {
		JSONObject metaDefListResponse = new JSONObject(response);

		ExceptionManager.getInstance().checkException(metaDefListResponse);
		
		mMetaDefList = new ArrayList<MetaDef>();

		JSONArray metaDefJSONArray = metaDefListResponse.optJSONArray("list");

		for (int i = 0; i < metaDefJSONArray.length(); i++) {
			mMetaDefList.add(parseMetaDef(metaDefJSONArray.optJSONObject(i)));
		}

		return mMetaDefList;
	}

	/**
	 * Parse meta def
	 * 
	 * @param metaDefObject
	 * @return
	 * @throws JSONException
	 */
	private MetaDef parseMetaDef(final JSONObject metaDefObject) throws JSONException {
		MetaDef metaDef = new MetaDef();
		
		metaDef.id = metaDefObject.optLong("id");
		metaDef.fieldName = metaDefObject.optString("fieldName");
		metaDef.codeStoreId = metaDefObject.optInt("codeStoreId");
		metaDef.componentType = metaDefObject.optInt("componentType");
		metaDef.defaultValue = parseFillMeta(metaDefObject.optJSONObject("defaultValue"));
		metaDef.expectedValue = metaDefObject.optInt("expectedValue");
		metaDef.label = metaDefObject.optString("label");
		metaDef.readOnly = metaDefObject.optBoolean("readOnly");
		metaDef.required = metaDefObject.optBoolean("required");
		metaDef.useType = metaDefObject.optInt("useType");

		return metaDef;
	}
	
	/**
	 * Parse fill meta def list
	 * 
	 * @param fillMetaDefObject
	 * @return
	 * @throws JSONException
	 */
	public FillMetaDef parseFillMetaDef(final JSONObject fillMetaDefObject) throws JSONException {
		FillMetaDef metaDef = new FillMetaDef();
		
		metaDef.id = fillMetaDefObject.optLong("id");
		metaDef.fieldName = fillMetaDefObject.optString("fieldName");
		metaDef.codeStoreId = fillMetaDefObject.optInt("codeStoreId");
		metaDef.componentType = fillMetaDefObject.optInt("componentType");
		metaDef.defaultValue = parseFillMeta(fillMetaDefObject.optJSONObject("defaultValue"));
		metaDef.currentValue = parseFillMeta(fillMetaDefObject.optJSONObject("currentValue"));
		metaDef.expectedValue = fillMetaDefObject.optInt("expectedValue");
		metaDef.label = fillMetaDefObject.optString("label");
		metaDef.readOnly = fillMetaDefObject.optBoolean("readOnly");
		metaDef.required = fillMetaDefObject.optBoolean("required");
		metaDef.hidden = fillMetaDefObject.optBoolean("hidden");
		metaDef.useType = fillMetaDefObject.optInt("useType");
		
		return metaDef;
	}
	

	private FillMeta parseFillMeta(final JSONObject fillMetaObject) throws JSONException {
		FillMeta fillMeta = null;
		
		if (fillMetaObject != null) {
			Long dateValue = fillMetaObject.optLong("dateValue");

			if (dateValue == 0) {
				fillMeta = new FillMeta(fillMetaObject.optLong("id"), fillMetaObject.optString("value"));
			} else {
				fillMeta = new FillMeta(fillMetaObject.optLong("id"), dateValue);
			}
		}
		
		return fillMeta;
	}
}
