package com.mmsone.dmscloud.bo;

import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.R;

public class WorkflowStatus {

    public static final int ACCEPTED = 582;
    public static final int READY = 586;
    public static final int CREATED = 587;
    public static final int OTHER_USER_EXECUTE = 691;
    public static final int TERMINATED = 693;

    public static List<String> getAllWorkflowStatusList() {
        List<String> list = new ArrayList<String>();

        list.add(getWorkflowStatusBy(ACCEPTED));
        list.add(getWorkflowStatusBy(READY));
        list.add(getWorkflowStatusBy(CREATED));
        list.add(getWorkflowStatusBy(OTHER_USER_EXECUTE));
        list.add(getWorkflowStatusBy(TERMINATED));

        return list;
    }

    public static List<String> getWorkflowStatusList(int status) {
        List<String> list = new ArrayList<String>();

        switch (status) {
            case ACCEPTED:
                list.add(getWorkflowStatusBy(ACCEPTED));
                list.add(getWorkflowStatusBy(READY));
                break;
            case CREATED:
                list.add(getWorkflowStatusBy(ACCEPTED));
                break;
            default:
                list.add(getWorkflowStatusBy(ACCEPTED));
                list.add(getWorkflowStatusBy(READY));
                break;
        }

        return list;
    }

    public static int getStatusIcon(int status) {
        switch (status) {
            case ACCEPTED:
                return R.drawable.pipa;
            case CREATED:
                return R.drawable.ic_arrow;
            case READY:
                return R.drawable.allapot_pipacsavo;
            case OTHER_USER_EXECUTE:
                return R.drawable.ic_arrow_down;
            case TERMINATED:
                return R.drawable.allapot_tabla;
        }
        return 0;
    }

    public static int getStatusColor(int status) {
        switch (status) {
            case ACCEPTED:
                return R.color.color_dmsclooud_green;
            case CREATED:
                return R.color.color_marked;
            case READY:
                return R.color.color_dmsclooud_green;
            case OTHER_USER_EXECUTE:
                return R.color.color_dmsclooud_blue;
            case TERMINATED:
                return R.color.color_marked;
        }
        return 0;

    }


    public static int getWorkflowStatusBy(String name) {
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.workflow_status_accepted))) {
            return ACCEPTED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.workflow_status_ready))) {
            return READY;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.workflow_status_created))) {
            return CREATED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.workflow_status_other_user_execute))) {
            return OTHER_USER_EXECUTE;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.workflow_status_terminated))) {
            return TERMINATED;
        }
        return 0;
    }

    public static String getWorkflowStatusBy(int id) {
        switch (id) {
            case ACCEPTED:
                return DMSCloudApplication.getContext().getString(R.string.workflow_status_accepted);
            case READY:
                return DMSCloudApplication.getContext().getString(R.string.workflow_status_ready);
            case CREATED:
                return DMSCloudApplication.getContext().getString(R.string.workflow_status_created);
            case OTHER_USER_EXECUTE:
                return DMSCloudApplication.getContext().getString(R.string.workflow_status_other_user_execute);
            case TERMINATED:
                return DMSCloudApplication.getContext().getString(R.string.workflow_status_terminated);
            default:
                return "";
        }
    }
}
