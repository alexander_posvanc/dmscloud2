package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;

public class PostWfTask implements IPostChild, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6686349348179861082L;

    public long id;
    public String formatedCreatedOn;
    public String formatedDueDate;
    public long documentId;
    public long createdOn;
    public long createdBy;
    public long dueDate;
    public String message;
    public long owner;
    public long postWfTaskNumber;
    public boolean recipient;
    public int status;
    public int subPostType;
    public String title;
    public String subject;
    public boolean unreaded;
    public int totalChildCount;
    public User user;
    public boolean hasExplicitRank;


    public List<IPostChild> childList;

    public PostWfTask() {
        childList = new ArrayList<IPostChild>();
    }

    @Override
    public EPostChildType getType() {
        return EPostChildType.PostWfTask;
    }

    @Override
    public int getChildCount() {
        return childList.size();
    }

    @Override
    public List<IPostChild> getChildList() {
        return childList;
    }

    @Override
    public long getChildCreatedBy() {
        return owner;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }
}
