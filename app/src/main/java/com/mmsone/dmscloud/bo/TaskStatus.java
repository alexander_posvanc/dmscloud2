package com.mmsone.dmscloud.bo;

import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.R;

public class TaskStatus {

    public static final int TASK_ACCEPTED = 695;
    public static final int TASK_REJECTED = 696;
    public static final int TASK_CLOSED = 697;
    public static final int TASK_ASSIGNED = 699;
    public static final int TASK_REDIRECTED = 700;

    public static List<String> getAllTaskStatusList() {
        List<String> list = new ArrayList<String>();

        list.add(getTaskStatusBy(TASK_ACCEPTED));
        list.add(getTaskStatusBy(TASK_CLOSED));
        list.add(getTaskStatusBy(TASK_REJECTED));
        list.add(getTaskStatusBy(TASK_ASSIGNED));
        list.add(getTaskStatusBy(TASK_REDIRECTED));

        return list;
    }

    public static List<String> getTaskExecutionStatusList(int actStatus) {
        List<String> list = new ArrayList<String>();

        switch (actStatus) {
            case TASK_ACCEPTED:
//			list.add(getTaskStatusBy(TASK_ACCEPTED));
                list.add(getTaskStatusBy(TASK_CLOSED));
                break;
            case TASK_ASSIGNED:
                list.add(getTaskStatusBy(TASK_ACCEPTED));
                list.add(getTaskStatusBy(TASK_REJECTED));
                break;
            case TASK_REJECTED:
                list.add(getTaskStatusBy(TASK_CLOSED));
                break;
            default:
                break;
        }

        return list;
    }

    public static int getStatusIcon(int status) {
        switch (status) {
            case TASK_ACCEPTED:
                return R.drawable.pipa;
            case TASK_ASSIGNED:
                return R.drawable.ic_arrow;
            case TaskStatus.TASK_CLOSED:
                return R.drawable.allapot_pipacsavo;
            case TaskStatus.TASK_REDIRECTED:
                return R.drawable.ic_arrow_down;
            case TaskStatus.TASK_REJECTED:
                return R.drawable.allapot_tabla;

        }
        return 0;
    }

    public static int getStatusColor(int status) {
        switch (status) {
            case TASK_ACCEPTED:
                return R.color.color_dmsclooud_green;
            case TASK_ASSIGNED:
                return R.color.color_marked;
            case TaskStatus.TASK_CLOSED:
                return R.color.color_dmsclooud_green;
            case TaskStatus.TASK_REDIRECTED:
                return R.color.color_dmsclooud_blue;
            case TaskStatus.TASK_REJECTED:
                return R.color.color_marked;
        }
        return 0;

    }

    public static int getTaskStatusBy(String name) {
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.task_status_accepted))) {
            return TASK_ACCEPTED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.task_status_closed))) {
            return TASK_CLOSED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.task_status_rejected))) {
            return TASK_REJECTED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.task_status_redirected))) {
            return TASK_REDIRECTED;
        }
        if (name.equals(DMSCloudApplication.getContext().getString(R.string.task_status_assigned))) {
            return TASK_ASSIGNED;
        }
        return 0;
    }

    public static String getTaskStatusBy(int id) {
        switch (id) {
            case TASK_ACCEPTED:
                return DMSCloudApplication.getContext().getString(R.string.task_status_accepted);
            case TASK_REJECTED:
                return DMSCloudApplication.getContext().getString(R.string.task_status_rejected);
            case TASK_CLOSED:
                return DMSCloudApplication.getContext().getString(R.string.task_status_closed);
            case TASK_ASSIGNED:
                return DMSCloudApplication.getContext().getString(R.string.task_status_assigned);
            case TASK_REDIRECTED:
                return DMSCloudApplication.getContext().getString(R.string.task_status_redirected);
            default:
                return "";
        }
    }
}
