package com.mmsone.dmscloud.bo.bean;


public class DocumentFile {

	public long id;
	public long fileSize;
	public String file;
	public String fileHash;
	public String fileName;
	public String mimeType;
}
