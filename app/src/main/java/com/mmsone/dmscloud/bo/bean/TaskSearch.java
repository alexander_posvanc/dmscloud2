package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;

public class TaskSearch implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2963379919893220240L;

	public long id;
	public String beanType;
	public long createdOn;
	public String createdBy;
	public long dueDate;
	public String formatedCreatedOn;
	public String formatedDueDate;
	public String message;
	public int postNumber;
	public String postTitle;
	public String recipient;
	public int status;
	public int type;

}
