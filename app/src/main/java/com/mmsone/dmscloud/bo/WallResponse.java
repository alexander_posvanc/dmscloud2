package com.mmsone.dmscloud.bo;

import java.io.Serializable;
import java.util.ArrayList;

import com.mmsone.dmscloud.bo.bean.Post;


public class WallResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8315575897696621991L;
	
	public int id;
	public ArrayList<Post> elementList;
	public int totalCount;
	
	public WallResponse(){
		elementList = new ArrayList<Post>();
	}
}
