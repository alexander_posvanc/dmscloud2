package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.EPostChildType;

public class PostTask implements IPostChild, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5600844110284415683L;

    public long id;
    public int subPostType;
    public int status;
    public String formatedCreatedOn;
    public int totalChildCount;
    public boolean unreaded;
    public boolean recipient;
    public String formatedDueDate;
    public long createdOn;
    public long createdBy;
    public long dueDate;
    public String message;
    public long documentId;
    public User user;
    public boolean hasExplicitRank;

    public List<IPostChild> childList;

    public PostTask() {
        childList = new ArrayList<IPostChild>();
    }

    @Override
    public EPostChildType getType() {
        return EPostChildType.PostTask;
    }

    @Override
    public int getChildCount() {
        return childList.size();
    }

    @Override
    public List<IPostChild> getChildList() {
        return childList;
    }

    @Override
    public long getChildCreatedBy() {
        return createdBy;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }
}
