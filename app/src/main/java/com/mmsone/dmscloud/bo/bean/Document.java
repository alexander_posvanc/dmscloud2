package com.mmsone.dmscloud.bo.bean;

import java.util.ArrayList;
import java.util.List;

public class Document {

	public long id;
	public long createdBy;
	public long createdOn;
	public String direction;
	public String documentNumber;
	public String documentType;
	public String formatedCreatedOn;
	public List<DocumentVersion> versionList;

	public Document(){
		versionList = new ArrayList<DocumentVersion>();
	}
}

