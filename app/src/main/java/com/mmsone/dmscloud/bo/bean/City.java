package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class City implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9046265986729383904L;
	
	@DatabaseField(id = true)
	public long id;
	
	@DatabaseField
	public long countyId;
	
	@DatabaseField
	public String name;

	public City() {

	}

	@Override
	public String toString() {
		return name;
	}
}
