package com.mmsone.dmscloud.bo;

import java.util.List;

import com.mmsone.dmscloud.bo.bean.User;

public interface IPostChild {
	
	public EPostChildType getType();
	public int getChildCount();
	public List<IPostChild> getChildList();
	public long getChildCreatedBy();
	
	public void setUser(User user);

}
