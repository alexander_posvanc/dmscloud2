package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;

public class PostSubComment implements IPostChild, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5181799525851650888L;
    public long id;
    public long createdOn;
    public long createdBy;
    public long documentId;
    public String formatedCreatedOn;
    public String message;
    public int subPostType;
    public boolean unreaded;
    public User user;
    public boolean hasExplicitRank;

    @Override
    public EPostChildType getType() {
        return EPostChildType.PostSubComment;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public List<IPostChild> getChildList() {
        return null;
    }

    @Override
    public long getChildCreatedBy() {
        return createdBy;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }
}
