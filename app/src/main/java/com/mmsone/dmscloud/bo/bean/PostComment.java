package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.EPostChildType;

public class PostComment implements IPostChild, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3085638073299686731L;
    public long id;
    public long createdBy;
    public long createdOn;
    public long documentId;
    public String formatedCreatedOn;
    public String message;
    public int subPostType;
    public int totalChildCount;
    public boolean unreaded;
    public List<IPostChild> childList;
    public User user;
    public boolean hasExplicitRank;


    public List<String> quickResponseList;
    public String technicalFile;

    public PostComment() {
        childList = new ArrayList<IPostChild>();
        quickResponseList = new ArrayList<String>();
    }

    @Override
    public EPostChildType getType() {
        return EPostChildType.PostComment;
    }

    @Override
    public int getChildCount() {
        return childList.size();
    }

    @Override
    public List<IPostChild> getChildList() {
        return childList;
    }

    @Override
    public long getChildCreatedBy() {
        return createdBy;
    }

    @Override
    public void setUser(User user) {
        this.user = user;

    }
}
