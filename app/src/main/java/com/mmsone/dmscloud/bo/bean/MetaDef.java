package com.mmsone.dmscloud.bo.bean;

public class MetaDef {

	public long id;
	public String fieldName;
	public int codeStoreId;
	public int componentType;
	public FillMeta defaultValue;
	public int expectedValue;
	public String label;
	public boolean readOnly;
	public boolean required;
	public int useType;
	
	
	@Override
	public String toString() {
		return label;
	}
	

}
