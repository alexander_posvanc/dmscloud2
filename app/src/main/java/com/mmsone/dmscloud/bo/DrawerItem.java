package com.mmsone.dmscloud.bo;

public class DrawerItem {
	
	public int iconId;
	public String text;
	
	public DrawerItem(String text, int iconId){
		this.text = text;
		this.iconId = iconId;
	}
}
