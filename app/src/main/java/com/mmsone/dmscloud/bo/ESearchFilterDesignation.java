package com.mmsone.dmscloud.bo;

public enum ESearchFilterDesignation {
	FILTER_SEARCH_TEXT, FILTER_SEARCH_POST_NUMBER, FILTER_SEARCH_CREATOR, 
	FILTER_SEARCH_CREATED, FILTER_SEARCH_MODIFIED, FILTER_SEARCH_SUB_TYPE, 
	FILTER_SEARCH_SUB_CREATOR, FILTER_SEARCH_SUB_CREATED, FILTER_SEARCH_SUB_TASK_STATUS, 
	FILTER_SEARCH_WORKFLOW, FILTER_SEARCH_SUB_WF_STATUS, FILTER_SEARCH_META ,
	FILTER_SEARCH_WORKGROUP ,FILTER_SEARCH_INCLUDE_HIDDEN_POSTS,FILTER_SEARCH_POST_TYPE
}
