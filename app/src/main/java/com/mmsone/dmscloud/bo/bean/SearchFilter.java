package com.mmsone.dmscloud.bo.bean;

import org.json.JSONException;
import org.json.JSONObject;

import com.mmsone.dmscloud.bo.ESearchFilterDesignation;

public class SearchFilter {

	public long metaId;
	public boolean active;
	public String value;
	public String operator;
	public ESearchFilterDesignation designation;
	public long dateFrom, dateTil;

	private JSONObject jsonObject;

	public SearchFilter() throws JSONException {
		jsonObject = new JSONObject();
		jsonObject.put("beanType", "JbSearchFilter");
	}

	public SearchFilter(ESearchFilterDesignation designation, String value) throws JSONException {
		this();
		this.designation = designation;
		this.value = value;

		jsonObject.put("designation", designation.name());
		jsonObject.put("value", value);
	}

	public SearchFilter(ESearchFilterDesignation designation, EDateInterval interval ,long dateFrom, long dateTil) throws JSONException {
		this();
		this.designation = designation;
		jsonObject.put("designation", designation.name());
		
		switch (interval) {
		case FROM:
			this.dateFrom = dateFrom;
			jsonObject.put("dateFrom", dateFrom);
			break;
		case TO:
			this.dateTil = dateTil;
			jsonObject.put("dateTil", dateTil);
			break;
		default:
			this.dateFrom = dateFrom;
			this.dateTil = dateTil;
			jsonObject.put("dateFrom", dateFrom);
			jsonObject.put("dateTil", dateTil);
			break;
		}
	}
	
	public enum EDateInterval{
		FROM,TO;
	}
	
	public Object getJsonObject() {
		return jsonObject;
	}
}
