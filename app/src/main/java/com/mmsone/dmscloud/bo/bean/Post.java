package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostType;
import com.mmsone.dmscloud.bo.IPostChild;


public class Post implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1641129522297870944L;

    public long id;
    public long createdOn;
    public long createdBy;
    public Long documentId;
    public String formatedCreatedOn;
    public String formatedModifiedOn;
    public boolean hidden;
    public boolean marked;
    public boolean unreaded;
    public boolean closed;
    public String message;
    public String title;
    public long modifiedOn;
    public int totalChildCount;
    public int overAllCount;
    public long postNumber;
    public User user;
    public boolean hasExplicitRank;
    public long commentCount;
    public long taskCount;

    public List<IPostChild> childList;
    public List<Long> hiddenAt;
    public List<Long> sharedWith;
    public List<ShowMeta> metaList;

    public EPostType postType;

    public Post() {
        sharedWith = new ArrayList<Long>();
        childList = new ArrayList<IPostChild>();
        hiddenAt = new ArrayList<Long>();
        metaList = new ArrayList<ShowMeta>();
    }
}
