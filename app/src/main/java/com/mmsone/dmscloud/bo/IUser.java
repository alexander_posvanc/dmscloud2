package com.mmsone.dmscloud.bo;

public interface IUser {

	public enum EUserType{
		USER, GROUP, DISTRIBUTION;
	}
	
	public String getName();
	public long getId();
	public EUserType getType();
	
}
