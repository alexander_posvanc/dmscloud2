package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;

public class PostChat implements IPostChild, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5827543087066314602L;

    public long id;
    public String message;
    public long createdBy;
    public long createdOn;
    public String formatedCreatedOn;
    public boolean unreaded;
    public long documentId;
    public int subPostType;
    public User user;
    public boolean hasExplicitRank;


    @Override
    public EPostChildType getType() {
        return EPostChildType.PostChat;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public List<IPostChild> getChildList() {
        return null;
    }

    @Override
    public long getChildCreatedBy() {
        return createdBy;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }
}


