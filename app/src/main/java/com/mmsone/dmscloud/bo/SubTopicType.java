package com.mmsone.dmscloud.bo;

import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.R;

public class SubTopicType {
	
	public static final String TASK = "800"; 		
	public static final String WORKFLOW = "801";	
	public static final String COMMENT = "802";		
	public static final String SUB_COMMENT = "805";	
	public static final String CHAT = "806";		
	public static final String EMAIL = "807";	

	public static List<String> getSubTopicTypeList() {
		List<String> list = new ArrayList<String>();
		list.add(getSubTopicNameBy(TASK));
		list.add(getSubTopicNameBy(WORKFLOW));
		list.add(getSubTopicNameBy(COMMENT));
		list.add(getSubTopicNameBy(SUB_COMMENT));
		list.add(getSubTopicNameBy(CHAT));
		list.add(getSubTopicNameBy(EMAIL));
		return list;
	}

	public static String getSubTopicNameBy(String id) {
		if (id.equals(TASK))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_task);
		if (id.equals(WORKFLOW))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_workflow);
		if (id.equals(COMMENT))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_comment);
		if (id.equals(SUB_COMMENT))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_sub_comment);
		if (id.equals(CHAT))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_chat);
		if (id.equals(EMAIL))
			return DMSCloudApplication.getContext().getString(R.string.sub_topic_type_email);
		return "";
	}

	public static String getSubTopicIdBy(String name) {
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_task))) {
			return TASK;
		}
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_workflow))) {
			return WORKFLOW;
		}
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_comment))) {
			return COMMENT;
		}
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_sub_comment))) {
			return SUB_COMMENT;
		}
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_chat))) {
			return CHAT;
		}
		if (name.equals(DMSCloudApplication.getContext().getString(R.string.sub_topic_type_email))) {
			return EMAIL;
		}
		return "";
	}
}
