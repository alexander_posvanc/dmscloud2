package com.mmsone.dmscloud.bo;

public class UserRole {

    public static final String TASK_MANAGEMENT_ROLE = "TASK_MANAGEMENT_ROLE";
    public static final String HIDE_ROLE = "HIDE_ROLE";
    public static final String MARK_ROLE = "MARK_ROLE";
    public static final String SHARE_ROLE = "SHARE_ROLE";

    /* jelenleg minden user megkapja
     */
    public static final String WALL_LIST_ROLE = "WALL_LIST_ROLE";

    /*ezek jelenleg nincsenek
     */
    public static final String CHAT_START_ROLE = "CHAT_START_ROLE";
    public static final String WHO_IS_ONLINE_ROLE = "WHO_IS_ONLINE_ROLE";
    public static final String EDIT_GROUP_ROLE = "EDIT_GROUP_ROLE";
    public static final String DELETE_USER_ROLE = "DELETE_USER_ROLE";
    public static final String EDIT_USER_2_GROUP_ROLE = "EDIT_USER_2_GROUP_ROLE";
    public static final String MULTI_SCAN_ROLE = "MULTI_SCAN_ROLE";
    public static final String POP3_SETTING_ROLE = "POP3_SETTING_ROLE";
    public static final String WORKFLOW_TERMINATE = "WORKFLOW_TERMINATE";
    public static final String DISTRIBUTION_LIST_CREATE = "DISTRIBUTION_LIST_CREATE";
    public static final String EMAIL_SEND_ROLE = "EMAIL_SEND_ROLE";
    public static final String WORKFLOW_CREATE = "WORKFLOW_CREATE";
    public static final String EMAIL_MENU_ROLE = "EMAIL_MENU_ROLE";
    public static final String EXPORT_ROLE = "EXPORT_ROLE";
    public static final String DOCUMENT_CREATE = "DOCUMENT_CREATE";
    public static final String WF_DIAGRAM_CREATE = "WF_DIAGRAM_CREATE";
    public static final String ADD_GROUP_ROLE = "ADD_GROUP_ROLE";
    public static final String MANAGE_REGISTRATION_ROLE = "MANAGE_REGISTRATION_ROLE";
    public static final String ADD_USER_ROLE = "ADD_USER_ROLE";
    public static final String DOCUMENT_TYPE_MODIFY = "DOCUMENT_TYPE_MODIFY";
    public static final String COMMON_SETTINGS_ROLE = "COMMON_SETTINGS_ROLE";
    public static final String CODESTORE_MANGEMENT_ROLE = "CODESTORE_MANGEMENT_ROLE";
    public static final String DOCUMENT_CHECKINOUT = "DOCUMENT_CHECKINOUT";
    public static final String EDIT_COMPANY_ROLE = "EDIT_COMPANY_ROLE";
    public static final String EMAIL_POST_NOTIFICATION_ROLE = "EMAIL_POST_NOTIFICATION_ROLE";
    public static final String UNPUBLISH_USER_ROLE = "UNPUBLISH_USER_ROLE";
    public static final String BLOCK_USER_ROLE = "BLOCK_USER_ROLE";
    public static final String WS_ROLE = "WS_ROLE";
    public static final String DISTRIBUTION_LIST_MODIFY = "DISTRIBUTION_LIST_MODIFY";
    public static final String CUSTOMER_MANAGEMENT_ROLE = "CUSTOMER_MANAGEMENT_ROLE";
    public static final String CHANGE_USERS_ROLE = "CHANGE_USERS_ROLE";
    public static final String METADATA_MANAGEMENT_ROLE = "METADATA_MANAGEMENT_ROLE";
    public static final String WORKFLOW_DELETE = "WORKFLOW_DELETE";
    public static final String WF_DIAGRAM_DELETE = "WF_DIAGRAM_DELETE";
    public static final String EDIT_FUNCTION_2_GROUP_ROLE = "EDIT_FUNCTION_2_GROUP_ROLE";
    public static final String DOCUMENT_TYPE_CREATE = "DOCUMENT_TYPE_CREATE";
    public static final String REGISTERED_ROLE = "REGISTERED_ROLE";
    public static final String EMAIL_TEMPLATES_ROLE = "EMAIL_TEMPLATES_ROLE";
    public static final String WORKFLOW_META_MANAGEMENT_ROLE = "WORKFLOW_META_MANAGEMENT_ROLE";
    public static final String DELETE_GROUP_ROLE = "DELETE_GROUP_ROLE";
    public static final String ADD_FUNCTION_2_USER_ROLE = "ADD_FUNCTION_2_USER_ROLE";
    public static final String LANGUAGE_SETTINGS_ROLE = "LANGUAGE_SETTINGS_ROLE";
    public static final String WORKFLOW_START = "WORKFLOW_START";
    public static final String EDIT_USER_ROLE = "EDIT_USER_ROLE";
    public static final String TECHNICAL_POST_TEMPLATES_ROLE = "TECHNICAL_POST_TEMPLATES_ROLE";
    public static final String MULTI_SCAN_DIRECTORY_RIGHTS_ROLE = "MULTI_SCAN_DIRECTORY_RIGHTS_ROLE";
    public static final String ADD_COMPANY_ROLE = "ADD_COMPANY_ROLE";
    public static final String WF_DIAGRAM_MODIFY = "WF_DIAGRAM_MODIFY";
    public static final String WORKFLOW_MODIFY = "WORKFLOW_MODIFY";
    public static final String DOCUMENT_MODIFY = "DOCUMENT_MODIFY";
}


