package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.IUser;


@DatabaseTable
public class User implements Serializable, IUser{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3349218990708794050L;
	
	@DatabaseField(id = true)
	public long id;
	
	@DatabaseField
	public String about;
	@DatabaseField
	public boolean active;
	@DatabaseField
	public boolean blocked;
	@DatabaseField
	public long createdOn;
	@DatabaseField
	public String email;
	@DatabaseField
	public String fax;
	@DatabaseField
	public String firstName;
	@DatabaseField
	public String formatedCreatedOn;	
	@DatabaseField
	public String formatedName;
	@DatabaseField
	public String image;
	@DatabaseField
	public String language;	
	@DatabaseField
	public String lastName;
	@DatabaseField
	public String mobilePhone;
	@DatabaseField
	public long timeOffset;	
	@DatabaseField
	public String title;
	@DatabaseField
	public String userName;
	@DatabaseField
	public String workPhone;
	
	public  User(){
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public String getName() {
		String name = formatedName;
		if(!active){
			name += DMSCloudApplication.getContext().getString(R.string.create_task_inactive_title);
		}
		return name ;
	}

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public EUserType getType() {
		return EUserType.USER;
	}
}
