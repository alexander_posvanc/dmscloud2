package com.mmsone.dmscloud.bo.bean;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class FillMeta {

	public long id;
	public String value;
	public Long dateValue;
	public JSONObject jsonObject;
	
	private boolean isDateValue = false;

	public FillMeta(long id) throws JSONException {
		this.id = id;
		jsonObject = new JSONObject();
		jsonObject.put("beanType", "JbFillMeta");
		jsonObject.put("id", id);
	}

	public FillMeta(long id, String value) throws JSONException {
		this.id = id;
		this.value = value;
		jsonObject = new JSONObject();
		jsonObject.put("beanType", "JbFillMeta");
		jsonObject.put("id", id);
		if(value == null){
			jsonObject.put("value", JSONObject.NULL);
		}else{
			jsonObject.put("value", value);
		}
	}

	public FillMeta(long id, long value) throws JSONException {
		this.id = id;
		this.dateValue = value;
		
		this.isDateValue = true;
		
		jsonObject = new JSONObject();
		jsonObject.put("beanType", "JbFillMeta");
		jsonObject.put("id", id);
		jsonObject.put("dateValue", value);
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	@Override
	public String toString() {
		if (dateValue == null) {
			return value;
		}
		return "" + dateValue;
	}
	
	public boolean isValid(){
		boolean isValid = false;
		if(isDateValue){
			if(dateValue != null){
				isValid = true;
			}
		}else{
			if(!TextUtils.isEmpty(value)){
				isValid = true;
			}
		}
		return isValid;
	}
}
