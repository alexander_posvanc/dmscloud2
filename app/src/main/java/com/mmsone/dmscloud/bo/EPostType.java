package com.mmsone.dmscloud.bo;


public enum EPostType {
	
	NORMAL , SCAN_FILE , EMAIL , WEB_SERVICE, MOBILE;
	
	public static EPostType getPostType(int id){
		switch (id) {
		case 0:
			return NORMAL;
		case 1:
			return SCAN_FILE;
		case 2:
			return EMAIL;
		case 3:
			return WEB_SERVICE;
		case 4:
			return MOBILE;
		default:
			return NORMAL;
		}
	}
}
