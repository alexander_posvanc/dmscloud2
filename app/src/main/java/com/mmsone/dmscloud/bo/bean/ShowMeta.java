package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;

public class ShowMeta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8266163998328094297L;

	public long id;
	public String label;
	public String value;
	
	public ShowMeta(long id, String label , String value){
		this.id = id;
		this.label = label;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return label + " " + value;
	}
}
