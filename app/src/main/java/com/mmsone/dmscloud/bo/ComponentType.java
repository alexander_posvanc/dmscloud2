package com.mmsone.dmscloud.bo;

public class ComponentType {
	
	public static final int TEXT = 562;
	public static final int NUMBER = 564;
	public static final int COMBO = 565;
	public static final int DATE = 563;
	public static final int LIST = 591;
	public static final int TIME = 588;
	public static final int AUTO_NUMBER = 590;
	public static final int CURRENCY = 589;
	public static final int ADDRESS = 566;
	public static final int CUSTOMER = 567;
	public static final int USER_AND_GROUP = 592;

}
