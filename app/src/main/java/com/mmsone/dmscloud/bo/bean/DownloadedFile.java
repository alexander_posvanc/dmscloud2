package com.mmsone.dmscloud.bo.bean;

import java.io.File;
import java.util.Date;

/**
 * Created by Alexander on 2015. 09. 01..
 */
public class DownloadedFile {

    public File downloadedFile;
    public Date lastModified;

    public DownloadedFile() {
    }

    public DownloadedFile(File file, Date fileDate) {
        downloadedFile = file;
        lastModified = fileDate;
    }
}
