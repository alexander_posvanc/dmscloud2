package com.mmsone.dmscloud.bo.bean;

public class WfDef {
	
	public long id;
	public String name;
	public String description;
	
	@Override
	public String toString() {
		return name;
	}
}
