package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;

public class PostSubTask implements IPostChild, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public long id;
	public String formatedCreatedOn;
	public String formatedDueDate;
	public long createdOn;
	public long createdBy;
	public long dueDate;
	public String message;
	public long documentId;
	public boolean recipient;
	public int status;
	public int subPostType;
	public boolean unreaded;
	public User user;

	@Override
	public EPostChildType getType() {
		return EPostChildType.PostSubTask;
	}

	@Override
	public int getChildCount() {
		return 0;
	}

	@Override
	public List<IPostChild> getChildList() {
		return null;
	}

	@Override
	public long getChildCreatedBy() {
		return createdBy;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}
}
