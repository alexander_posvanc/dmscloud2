package com.mmsone.dmscloud.bo;

public class FileUploadResponse {
	
	public static final String SUCCESS = "1000;SUCCESS";
	public static final String UNSPECIFIED_ERROR = "1100;UNSPECIFIED_ERROR";
	public static final String NOT_AUTHENTICATED = "1200;NOT_AUTHENTICATED";
	public static final String INSUFFICIENT_RIGHTS  = "1300;INSUFFICIENT_RIGHTS";
	public static final String FORBIDDEN_EXTENSION = "1400;FORBIDDEN_EXTENSION";
	public static final String FILE_SIZE_LIMIT_EXCEEDED = "1500;FILE_SIZE_LIMIT_EXCEEDED";

}
