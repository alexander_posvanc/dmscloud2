package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class County implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2679326792376746681L;
	
	@DatabaseField(id = true)
	public long id;
	
	@DatabaseField
	public long countryId;
	
	@DatabaseField
	public String name;
	
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public ArrayList<City> childList;
	
	public County(){
		childList = new ArrayList<City>();
	}
	
	@Override
	public String toString() {
		return name;
	}
}
