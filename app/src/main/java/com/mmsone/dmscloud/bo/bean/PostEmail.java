package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;

public class PostEmail implements IPostChild, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6077816344618736218L;
	public long id;
	public String formatedCreatedOn;
	public long documentId;
	public long createdOn;
	public long createdBy;
	public String body;
	public boolean incoming;
	public String message;
	public String subject;
	public String recipient;
	public int subPostType;
	public boolean unreaded;
	public int totalChildCount;
	public List<IPostChild> childList;
	public User user;
    public boolean hasExplicitRank;


    public PostEmail(){
		childList = new ArrayList<IPostChild>();
	}

	@Override
	public EPostChildType getType() {
		return EPostChildType.PostEmail;
	}
	
	@Override
	public int getChildCount() {
		return childList.size();
	}
	
	@Override
	public List<IPostChild> getChildList() {
		return childList;
	}
	
	@Override
	public long getChildCreatedBy() {
		return createdBy;
	}
	
	@Override
	public void setUser(User user) {
		this.user = user;
		
	}
}
