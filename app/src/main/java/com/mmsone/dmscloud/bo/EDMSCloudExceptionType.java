package com.mmsone.dmscloud.bo;

public enum EDMSCloudExceptionType {
	AlreadyExistsException, AuthenticationException, BadUserNameOrPasswordException, 
	DisabledOrBlockedUserException, DMSCloudDBException, ErrorException, 
	InsufficientRightsException, InvalidStatusException, ProtocolMismatchException, 
	RaceConditionException, RmmExpiredException, ValidationException;
}
