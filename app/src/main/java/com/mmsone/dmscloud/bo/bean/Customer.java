package com.mmsone.dmscloud.bo.bean;

public class Customer {

	public long id;
	public String name;
	public String info;
	public boolean person;
	
	@Override
	public String toString() {
		return name;
	}
}
