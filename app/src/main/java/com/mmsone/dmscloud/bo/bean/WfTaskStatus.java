package com.mmsone.dmscloud.bo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WfTaskStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3467437655070930695L;
	
	public long id;
	public long createdOn;
	public long createdBy;
	public long stepDueDate;
	public long dueDate;
	public String formatedCreatedOn;
	public String formatedDueDate;
	public String message;
	public String stepDescription;
	public String stepName;
	public String postTitle;
	public boolean recipient;
	public int status;
	public int type;
	public int resultType;
	public List<FillMetaDef> metaList;
	
	public WfTaskStatus(){
		metaList = new ArrayList<FillMetaDef>();
	}
}

    