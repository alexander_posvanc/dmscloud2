package com.mmsone.dmscloud.bo;

public class MetaDataType {
	
	public static final int METADATA_WORKFLOW_TYPE = 1;
	public static final int METADATA_COMMON_TYPE =2;
	public static final int METADATA_DOCUMENT_TYPE = 3;
	public static final int METADATA_CUSTOMER_TYPE = 4;

}
