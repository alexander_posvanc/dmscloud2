package com.mmsone.dmscloud.bo;

public enum ERequestType {
    UnpLoginRequest, RmmLoginRequest, LogoutRequest, GetUserListRequest, GetWallRequest, GetWallPostMetaRequest,
    GetWallSubRequest, GetCodeStoreRequest, GetCountryRequest, GetCustomerListRequest,
    GetGroupListRequest, GetDistributionListRequest, GetMetaDefListRequest, GetSavedSearchListRequest,
    GetSavedSearchRequest, GetWfDefListRequest, SharePostRequest, HidePostRequest, MarkPostRequest,
    CreatePostRequest, CreatePostCommentRequest, CreatePostSubCommentRequest, CreatePostTaskRequest,
    GetTaskStatusRequest, ChangeTaskStatusRequest, GetTaskListRequest, GetDocumentRequest,
    GetWfTaskStatusRequest, ChangeWfTaskStatusRequest, RegisterPushRequest, UnregisterPushRequest,
    CreatePostWfTaskRequest, RankPointRequest, DeletePostSubCommentRequest;
}

