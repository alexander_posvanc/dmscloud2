package com.mmsone.dmscloud.bo;

import java.util.ArrayList;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mmsone.dmscloud.bo.bean.User;


@DatabaseTable
public class LoginResponse {

	@DatabaseField(generatedId = true)
	public int id;
	
	@DatabaseField
	public long maxFileSize;
	
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public ArrayList<String> forbiddenFileExtensions;
	
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public ArrayList<String> userRoles;
	
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public User currentUser;

	public LoginResponse(){
		forbiddenFileExtensions = new ArrayList<String>();
		userRoles = new ArrayList<String>();
	}
}