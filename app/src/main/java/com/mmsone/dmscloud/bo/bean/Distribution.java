package com.mmsone.dmscloud.bo.bean;

import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.IUser;

public class Distribution implements IUser{
	
	public long id;
	public boolean active;
	public String name;
	public List<Long> userList;
	
	public Distribution(){
		userList = new ArrayList<Long>();
	}

	@Override
	public String getName() {
		String fullName = name  + DMSCloudApplication.getContext().getString(R.string.create_task_distribution_title);
		if(!active){
			fullName += DMSCloudApplication.getContext().getString(R.string.create_task_inactive_title);
		}
		return fullName ;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public EUserType getType() {
		return EUserType.DISTRIBUTION;
	}
	
	
	@Override
	public String toString() {
		return getName();
	}
}	
