package com.mmsone.dmscloud.bo.bean;

public class SavedSearch {

	public Long id;
	public String name;
	
	@Override
	public String toString() {
		return name;
	}
}
