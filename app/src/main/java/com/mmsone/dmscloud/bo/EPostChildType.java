package com.mmsone.dmscloud.bo;

public enum EPostChildType {
	PostComment, PostTask, PostEmail, PostWfTask, PostChat , PostSubComment ,PostSubTask;
}
