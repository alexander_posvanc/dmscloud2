package com.mmsone.dmscloud.bo.bean;

import java.util.ArrayList;
import java.util.List;

public class DocumentVersion {

	public long id;
	public long versionNumber;
	public String description;
	public List<DocumentFile> fileList;
	public List<ShowMeta> metaList;
	
	public DocumentVersion(){
		fileList = new ArrayList<DocumentFile>();
		metaList = new ArrayList<ShowMeta>();
	}
}
