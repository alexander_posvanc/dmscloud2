package com.mmsone.dmscloud.bo.bean;

public class FillMetaDef {
	
	public long id;
	public String fieldName;
	public int codeStoreId;
	public int componentType;
	public FillMeta currentValue;
	public FillMeta defaultValue;
	public int expectedValue;
	public String label;
	public boolean readOnly;
	public boolean required;
	public boolean hidden;
	public int useType;

}
