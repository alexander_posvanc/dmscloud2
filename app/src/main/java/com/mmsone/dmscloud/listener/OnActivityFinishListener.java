package com.mmsone.dmscloud.listener;

public interface OnActivityFinishListener {

	public void onFinish(boolean isSearch);
}
