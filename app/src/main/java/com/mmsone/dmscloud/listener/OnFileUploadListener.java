package com.mmsone.dmscloud.listener;

import java.io.File;

public interface OnFileUploadListener {
	
	public void onListUpdate(File file);
}
