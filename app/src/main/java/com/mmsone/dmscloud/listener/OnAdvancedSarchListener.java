package com.mmsone.dmscloud.listener;

public interface OnAdvancedSarchListener {

	public void onAdvancedSarchDelete();
	public void onAdvancedSarchLoad();
	public void onAdvancedSarchSave();
}
