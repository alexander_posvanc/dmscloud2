package com.mmsone.dmscloud;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class Config {

    public static final boolean IS_DEVELOPER_MODE = true;

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "dmscloud_db";

    public static final String SERVER_JSON_URL = "JsonServlet";
    public static final String SERVER_FILE_UPLOAD_URL = "tfu";

    public static final long UPDATE_TIME_IN_MILLIS = 604800000;

    public static final long COUNTRY_ID = 500006;

    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MMMM.dd", Locale.getDefault());
    public static final SimpleDateFormat LIST_ITEM_DATE_FORMAT = new SimpleDateFormat("yy.MM.dd", Locale.getDefault());
    public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm", Locale.getDefault());

    public static final int WALL_FROM = 0;
    public static final int WALL_ROW_COUNT = 3;

    public static String PIC_DIR = "/dmscloud/pic";
    public static String DOWNLOAD_DIR = "/dmscloud/download";

}