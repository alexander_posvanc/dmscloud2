package com.mmsone.dmscloud.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bo.TaskSearchType;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.TaskSearch;
import com.mmsone.dmscloud.bo.bean.WfTaskStatus;

public class TaskAdapter extends BaseAdapter {

    private List<TaskSearch> mTaskList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    /**
     * Constructor of TaskAdapter
     *
     * @param context
     * @param taskList List of TaskSearch-s
     */
    public TaskAdapter(Context context, List<TaskSearch> taskList) {
        mLayoutInflater = LayoutInflater.from(context);
        mTaskList = taskList;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mTaskList.size();
    }

    @Override
    public TaskSearch getItem(int arg0) {
        return mTaskList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return mTaskList.get(arg0).id;
    }

    private static class ViewHolder {
        public LinearLayout typeLL, statusLL;
        public TextView messageTv, createdByTv, recipientTv, createdOnTv, dueDateTv, postNumberTv, postTitleTv, statusTv;
        public ImageView statusIv, taskIcon;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.list_item_task_new, null);

            holder = new ViewHolder();

            holder.typeLL = (LinearLayout) view.findViewById(R.id.task_typeLL);
            holder.statusLL = (LinearLayout) view.findViewById(R.id.task_statusLL);
            holder.messageTv = (TextView) view.findViewById(R.id.task_messageTv);
            holder.createdByTv = (TextView) view.findViewById(R.id.task_createdByTv);
            holder.recipientTv = (TextView) view.findViewById(R.id.task_recipientTv);
            holder.createdOnTv = (TextView) view.findViewById(R.id.task_createdOnTv);
            holder.dueDateTv = (TextView) view.findViewById(R.id.task_dueDateTv);
            holder.postNumberTv = (TextView) view.findViewById(R.id.task_postNumberTv);
            holder.postTitleTv = (TextView) view.findViewById(R.id.task_postTitleTv);
            holder.statusIv = (ImageView) view.findViewById(R.id.task_status_Iv);
            holder.statusTv = (TextView) view.findViewById(R.id.task_status_Tv);
            holder.taskIcon = (ImageView) view.findViewById(R.id.task_workflow_item);

            ((TextView) view.findViewById(R.id.task_recipientTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
            ((TextView) view.findViewById(R.id.task_createdOnTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
            ((TextView) view.findViewById(R.id.task_dueDateTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
            holder.statusTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final TaskSearch taskSearch = getItem(pos);

        //set list item icon
        switch (taskSearch.type) {
            case TaskSearchType.TASK:
                holder.statusIv.setBackgroundResource(TaskStatus.getStatusIcon(taskSearch.status));
                holder.statusTv.setText(TaskStatus.getTaskStatusBy(taskSearch.status));
                holder.taskIcon.setImageResource(R.drawable.feladatok_kek);
                holder.statusTv.setTextColor(mContext.getResources().getColor(TaskStatus.getStatusColor(taskSearch.status)));
                break;
            case TaskSearchType.WORKFLOW:
                holder.statusIv.setBackgroundResource(WorkflowStatus.getStatusIcon(taskSearch.status));
                holder.statusTv.setText(WorkflowStatus.getWorkflowStatusBy(taskSearch.status));
                holder.taskIcon.setImageResource(R.drawable.munkafolyamatok);
                holder.statusTv.setTextColor(mContext.getResources().getColor(WorkflowStatus.getStatusColor(taskSearch.status)));


                break;
        }

        if (taskSearch.createdBy == null || taskSearch.createdBy.equals("null")) {
            holder.createdByTv.setText("");
        } else {
            holder.createdByTv.setText(taskSearch.createdBy);
        }

        holder.createdByTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.recipientTv.setText(taskSearch.recipient);
        holder.recipientTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.createdOnTv.setText(taskSearch.formatedCreatedOn);
        holder.createdOnTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.dueDateTv.setText(taskSearch.formatedDueDate);
        holder.dueDateTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.postTitleTv.setText(taskSearch.message);
        holder.postTitleTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());

        return view;
    }
}
