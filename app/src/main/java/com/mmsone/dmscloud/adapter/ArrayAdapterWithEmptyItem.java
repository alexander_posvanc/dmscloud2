package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.mmsone.dmscloud.R;

public class ArrayAdapterWithEmptyItem extends ArrayAdapter<String> {

	private String[] mUserList;
	private Context mContext;

	/**
	 * ArrayAdapterWithEmptyItem constructor
	 * 
	 * @param context
	 * @param txtViewResourceId
	 * @param objects
	 */
	public ArrayAdapterWithEmptyItem(Context context, int txtViewResourceId, String[] objects) {
		super(context, txtViewResourceId, objects);
		mContext = context;
		mUserList = objects;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt, true);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt, false);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDownView) {
		View view = View.inflate(mContext, R.layout.view_user_spinner_item, null);
		TextView textView = (TextView) view.findViewById(R.id.user_spinner_item_nameTv);
		
		//set view margins
		if (isDropDownView) {
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(8, 8, 8, 8);
			textView.setLayoutParams(layoutParams);
		}

		String user = mUserList[position];
		if ((user == null) && (position == 0)) {
			textView.setVisibility(View.GONE);
		} else {
			view.setVisibility(View.VISIBLE);
			textView.setText(user);
		}

		return view;
	}
}
