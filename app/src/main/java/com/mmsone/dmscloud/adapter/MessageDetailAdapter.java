package com.mmsone.dmscloud.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.DeleteSubCommentTask;
import com.mmsone.dmscloud.async.RankPostTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.ui.CreateCommentFragment;
import com.mmsone.dmscloud.ui.MessageActivity;
import com.mmsone.dmscloud.ui.PostActivity;
import com.mmsone.dmscloud.ui.SharePostActivity;
import com.mmsone.dmscloud.ui.SubCommentDialog;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 24..
 */
public class MessageDetailAdapter extends RecyclerView.Adapter<MessageDetailAdapter.MessageViewHolder> {

    private Context mContext;
    private ArrayList<IPostChild> mPostTaskList;
    private Post mPost;

    /**
     * Viewholder of MessageDetailAdapter
     *
     * @param postTaskList List of IPostChilds
     * @param context
     * @param post
     */
    public MessageDetailAdapter(ArrayList<IPostChild> postTaskList, Context context, Post post) {
        mPostTaskList = postTaskList;
        mContext = context;
        mPost = post;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_detail, parent, false);
        MessageViewHolder vhItem = new MessageViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        final PostComment postComment = (PostComment) mPostTaskList.get(position);
        final MessageViewHolder messageViewHolder = holder;
        final List<PostSubComment> postSubComments = new ArrayList<>();

        //Set texts and fonts
        messageViewHolder.nameText.setText(postComment.user.formatedName);
        messageViewHolder.nameText.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        messageViewHolder.timeText.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        messageViewHolder.timeText.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postComment.createdOn));
        messageViewHolder.messageText.setText(postComment.message);
        messageViewHolder.documentId.setVisibility(View.GONE);

        if (postComment.documentId == 0) {
            messageViewHolder.documentId.setVisibility(View.GONE);
        } else {
            messageViewHolder.documentId.setVisibility(View.VISIBLE);
        }

        messageViewHolder.documentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SharePostActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("post", mPost);
                args.putLong("documentId", postComment.documentId);
                args.putInt("fragment", 4);
                intent.putExtras(args);
                mContext.startActivity(intent);
            }
        });

        //Ranking of comment
        messageViewHolder.ratingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    new RankPostTask(mPost.id, postComment.id, postComment.subPostType, (Activity) mContext).execute();
                }
            }
        });
        //Add start according to ranking
        if (postComment.hasExplicitRank) {
            messageViewHolder.rankIv.setImageResource(R.drawable.csillag_sarga);
        } else {
            messageViewHolder.rankIv.setImageResource(R.drawable.csillag);
        }

        //child layout hideing
        messageViewHolder.subChildLayout.removeAllViews();
        messageViewHolder.subChildLayout.setVisibility(View.GONE);
        messageViewHolder.arrowIv.setRotation(270);

        //add postchildlist
        for (IPostChild postChild : postComment.getChildList()) {
            if (postChild.getType().equals(EPostChildType.PostSubComment)) {
                PostSubComment postSubComment = (PostSubComment) postChild;
                postSubComments.add(postSubComment);
            }
        }
        //Set visibility of show/hide layout for subChilds
        if (postSubComments.size() == 0 || postSubComments == null) {
            messageViewHolder.moreSubChildButton.setVisibility(View.GONE);
        } else {
            messageViewHolder.moreSubChildButton.setVisibility(View.VISIBLE);
        }

        messageViewHolder.messageCount.setText(mContext.getResources().getString(R.string.post_detail_submessage_first) + " " + postSubComments.size() + " " + mContext.getResources().getString(R.string.post_detail_submessage));


        //Show hide layout, show ChildLayouts
        messageViewHolder.moreSubChildButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageViewHolder.subChildLayout.getVisibility() == View.GONE) {
                    messageViewHolder.subChildLayout.setVisibility(View.VISIBLE);
                    messageViewHolder.subChildLayout.removeAllViews();
                    messageViewHolder.moreSubChildButton.setVisibility(View.GONE);
//                    messageViewHolder.messageCount.setVisibility(View.GONE);
//                    messageViewHolder.arrowIv.setRotation(90);
                    try {
                        addChildViews(messageViewHolder.subChildLayout, postSubComments, messageViewHolder.moreSubChildButton);
                    } catch (DMSCloudDBException e) {
                        e.printStackTrace();
                    }

                } else {
//                    messageViewHolder.moreSubChildButton.setBackgroundColor(Color.parseColor("#E6E6E6"));
//                    messageViewHolder.messageCount.setVisibility(View.VISIBLE);
//                    messageViewHolder.arrowIv.setRotation(270);
//                    messageViewHolder.subChildLayout.removeAllViews();
//                    messageViewHolder.subChildLayout.setVisibility(View.GONE);
                }
            }
        });

        //add subcomment
        messageViewHolder.addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(mContext, MessageActivity.class);
                    intent.putExtra("postComment", postComment);
                    mContext.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPostTaskList.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        TextView nameText, timeText, messageText, messageCount;
        LinearLayout moreSubChildButton, subChildLayout, ratingButton, addComment;
        ImageView rankIv, arrowIv, documentId;

        public MessageViewHolder(View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.message_detail_name);
            timeText = (TextView) itemView.findViewById(R.id.message_detail_time);
            messageText = (TextView) itemView.findViewById(R.id.message_detail_message);
            messageCount = (TextView) itemView.findViewById(R.id.subchild_count_Tv);
            moreSubChildButton = (LinearLayout) itemView.findViewById(R.id.subchild_more);
            subChildLayout = (LinearLayout) itemView.findViewById(R.id.subchild_holder);
            addComment = (LinearLayout) itemView.findViewById(R.id.message_detail_comment_btn);
            ratingButton = (LinearLayout) itemView.findViewById(R.id.message_detail_rating);
            rankIv = (ImageView) itemView.findViewById(R.id.message_detail_rank_Iv);
            arrowIv = (ImageView) itemView.findViewById(R.id.hide_layout_arrowIv);
            documentId = (ImageView) itemView.findViewById(R.id.comment_item_documentId);
        }
    }

    /**
     * Add subcomment views
     *
     * @param childLayout layout for adding subComments
     * @param subComments list of subcomments
     */
    public void addChildViews(final LinearLayout childLayout, final List<PostSubComment> subComments, final LinearLayout showLayout) throws DMSCloudDBException {
        LinearLayout[] linearLayouts = new LinearLayout[subComments.size()];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        TextView[] messageTexts = new TextView[subComments.size()];
        TextView[] nameTexts = new TextView[subComments.size()];
        TextView[] timeTexts = new TextView[subComments.size()];
        LinearLayout[] deleteLayouts = new LinearLayout[subComments.size()];
        ImageView[] documentIds = new ImageView[subComments.size()];
        RelativeLayout[] hideLayouts = new RelativeLayout[subComments.size()];
        ImageView[] hideButtons = new ImageView[subComments.size()];

        for (int i = 0; i < subComments.size(); i++) {
            linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.message_subchild_layout, null);
            messageTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_message);
            nameTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_name);
            timeTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_time);
            deleteLayouts[i] = (LinearLayout) linearLayouts[i].findViewById(R.id.subchild_layout_button);
            documentIds[i] = (ImageView) linearLayouts[i].findViewById(R.id.subcomment_documentId);
            hideLayouts[i] = (RelativeLayout) linearLayouts[i].findViewById(R.id.subchild_hide_layout);
            hideButtons[i] = (ImageView) linearLayouts[i].findViewById(R.id.subchild_hide_button);

            if (i == 0) {
                hideLayouts[i].setVisibility(View.VISIBLE);
            } else {
                hideLayouts[i].setVisibility(View.GONE);
            }
            hideButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLayout.setVisibility(View.VISIBLE);
                    childLayout.removeAllViews();
                    childLayout.setVisibility(View.GONE);
                }
            });


            //Set delete layout visible if its the last comment and User is the same
            if (i == 0 && UserManager.getInstance().getCurrentUser().getId() == subComments.get(i).user.getId()) {
                deleteLayouts[i].setVisibility(View.VISIBLE);
            } else {
                deleteLayouts[i].setVisibility(View.GONE);
            }

            final int finalI = i;

            //Delete comment
            deleteLayouts[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        new DeleteSubCommentTask(subComments.get(finalI).id, (Activity) mContext).execute();
                    }
                }
            });

            if (subComments.get(i).documentId == 0) {
                documentIds[i].setVisibility(View.GONE);
            } else {
                documentIds[i].setVisibility(View.VISIBLE);
            }
            documentIds[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("post", mPost);
                    args.putLong("documentId", subComments.get(finalI).documentId);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                }
            });

            //Set texts of layout
            messageTexts[i].setText(subComments.get(i).message);
            nameTexts[i].setText(subComments.get(i).user.formatedName);
            nameTexts[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            timeTexts[i].setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - subComments.get(i).createdOn));

            childLayout.addView(linearLayouts[i]);
        }
    }

}
