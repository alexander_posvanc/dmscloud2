package com.mmsone.dmscloud.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mmsone.dmscloud.ui.PostDetailFragmentNew;
import com.mmsone.dmscloud.ui.PostDetailMessageFragment;
import com.mmsone.dmscloud.ui.PostDetailTaskFragment;
import com.mmsone.dmscloud.ui.PostDetailTechnicalFragment;

/**
 * Created by Alexander on 2015. 08. 25..
 */
public class PostViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    private Bundle mArgs;

    /**
     * Adapter of ViewPager of PostDetails
     *
     * @param fm
     * @param args
     */
    public PostViewPagerAdapter(FragmentManager fm, Bundle args) {
        super(fm);
        if (fm.getFragments() != null) {
            fm.getFragments().clear();
        }
        mArgs = args;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //Detail Fragment
                PostDetailFragmentNew postDetailFragmentNew = new PostDetailFragmentNew();
                postDetailFragmentNew.setArguments(mArgs);
                return postDetailFragmentNew;
            case 1:
                //Message Detail Fragment
                PostDetailMessageFragment postDetailMessageFragment = new PostDetailMessageFragment();
                postDetailMessageFragment.setArguments(mArgs);
                return postDetailMessageFragment;
            case 2:
                //Task Detail Fragmnet
                PostDetailTaskFragment postDetailTaskFragment = new PostDetailTaskFragment();
                postDetailTaskFragment.setArguments(mArgs);
                return postDetailTaskFragment;
            case 3:
                //File Detail Fragment
                PostDetailTechnicalFragment postDetailTechnicalFragment = new PostDetailTechnicalFragment();
                postDetailTechnicalFragment.setArguments(mArgs);
                return postDetailTechnicalFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

}
