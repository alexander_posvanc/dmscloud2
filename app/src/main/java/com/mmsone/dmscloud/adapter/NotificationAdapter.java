package com.mmsone.dmscloud.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.bean.Notification;

import java.util.Date;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 31..
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<Notification> mNotificationList;

    public NotificationAdapter(List<Notification> notificationList) {
        mNotificationList = notificationList;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification_item, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);

            return vhItem;
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification_header, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);

            return vhItem;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {
        if (holder.holderId == TYPE_ITEM) {
            holder.notificationText.setText(mNotificationList.get(position).notificationText);
            holder.notificationName.setText(mNotificationList.get(position).notificationName);
            if (position == 1) {
                holder.notificationBackground.setBackgroundColor(Color.parseColor("#F25B3E"));
                holder.notificationText.setTextColor(Color.parseColor("#ffffff"));
                holder.notificationName.setTextColor(Color.parseColor("#ffffff"));
                holder.hourText.setTextColor(Color.parseColor("#ffffff"));
            }
        } else {
            holder.dateText.setText(Config.DATE_FORMAT.format(new Date(mNotificationList.get(position).notificationDate)));
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position, mNotificationList)) {
            return TYPE_HEADER;
        } else return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position, List<Notification> notifications) {
        if (notifications.get(position).notificationName != null) {
            return false;
        } else return true;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        int holderId;
        TextView notificationText, notificationName, dateText, hourText;
        LinearLayout notificationBackground;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == TYPE_ITEM) {
                holderId = TYPE_ITEM;
                notificationBackground = (LinearLayout) itemView.findViewById(R.id.notification_item_background);
                notificationName = (TextView) itemView.findViewById(R.id.notification_item_nameTv);
                notificationText = (TextView) itemView.findViewById(R.id.notification_item_textTv);
                hourText = (TextView) itemView.findViewById(R.id.notification_item_hourTv);

            } else {
                holderId = TYPE_HEADER;
                dateText = (TextView) itemView.findViewById(R.id.notification_header_dateTv);
            }
        }
    }

}
