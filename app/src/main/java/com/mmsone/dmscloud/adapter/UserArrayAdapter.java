package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.IUser;

public class UserArrayAdapter extends ArrayAdapter<IUser> {

	private IUser[] mUserList;
	private Context mContext;

	/**
	 * User array adapter constructor
	 * 
	 * @param context
	 * @param txtViewResourceId
	 * @param objects
	 */
	public UserArrayAdapter(Context context, int txtViewResourceId, IUser[] objects) {
		super(context, txtViewResourceId, objects);
		mContext = context;
		mUserList = objects;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt, true);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt, false);
	}
	
	public View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDownView) {
		View view = View.inflate(mContext, R.layout.view_user_spinner_item, null);
		TextView textView = (TextView) view.findViewById(R.id.user_spinner_item_nameTv);

		//set text view margins
		if (isDropDownView) {
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(8, 8, 8, 8);
			textView.setLayoutParams(layoutParams);
		}
		
		IUser user = mUserList[position];
		if ((user == null) && (position == 0)) {
			textView.setText("");
		} else {
			textView.setText(user.getName());
		}

		return view;
	}
}
