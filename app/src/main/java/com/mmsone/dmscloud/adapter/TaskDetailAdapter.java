package com.mmsone.dmscloud.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.DeleteSubCommentTask;
import com.mmsone.dmscloud.async.RankPostTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.MetaManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostChat;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.bo.bean.PostSubTask;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.bo.bean.WfTaskStatus;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.ex.ValidationException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.MessageActivity;
import com.mmsone.dmscloud.ui.PostActivity;
import com.mmsone.dmscloud.ui.SharePostActivity;
import com.mmsone.dmscloud.ui.SubCommentDialog;
import com.mmsone.dmscloud.ui.TaskExecutionActivity;
import com.mmsone.dmscloud.ui.TaskMessageActivity;
import com.mmsone.dmscloud.ui.WfTaskExecutionActivity;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 17..
 */
public class TaskDetailAdapter extends RecyclerView.Adapter<TaskDetailAdapter.TaskViewHolder> {

    private ArrayList<IPostChild> postTaskList;
    private Context mContext;
    private Post mPost;
    private User mUser;

    /**
     * Constructor of TaskDetailAdapter
     *
     * @param postTaskList List of IPostChilds
     * @param context
     * @param post
     */
    public TaskDetailAdapter(ArrayList<IPostChild> postTaskList, Context context, Post post) {
        this.postTaskList = postTaskList;
        mContext = context;
        mPost = post;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_task_detail, parent, false);
        TaskViewHolder vhItem = new TaskViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        try {
            mUser = UserManager.getInstance().getCurrentUser();
        } catch (DMSCloudDBException e) {
            e.printStackTrace();
        }
        //SIMA TASK
        if (postTaskList.get(position).getType().equals(EPostChildType.PostTask)) {
            final PostTask postTask = (PostTask) postTaskList.get(position);
            final TaskViewHolder taskViewHolder = holder;
            List<IPostChild> taskSubChilds = new ArrayList<>(postTask.getChildList());

            //Set texts
            taskViewHolder.taskTypeIv.setImageResource(R.drawable.feladatok_kek);
            taskViewHolder.descriptionTv.setText(postTask.message);
            taskViewHolder.deadLineTv.setText(mContext.getResources().getString(R.string.due_date) + postTask.formatedDueDate);
            taskViewHolder.nameTv.setText(postTask.user.getName());
            taskViewHolder.nameTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            taskViewHolder.dateTv.setText(postTask.formatedCreatedOn);
            taskViewHolder.taskBtn.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            taskViewHolder.taskBtn.setTransformationMethod(null);
            taskViewHolder.taskBtn.setVisibility(View.VISIBLE);
            taskViewHolder.firstSubTaskLayout.setVisibility(View.GONE);

            //Rating
            taskViewHolder.ratingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        new RankPostTask(mPost.id, postTask.id, postTask.subPostType, (Activity) mContext).execute();
                    }
                }
            });


            //Document show fragment
            taskViewHolder.documentId.setVisibility(View.GONE);
            if (postTask.documentId == 0) {
                taskViewHolder.documentId.setVisibility(View.GONE);
            } else {
                taskViewHolder.documentId.setVisibility(View.VISIBLE);
            }

            taskViewHolder.documentId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putLong("documentId", postTask.documentId);
                    args.putSerializable("post", mPost);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                }
            });

            taskViewHolder.taskHourTv.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postTask.createdOn));

            //Show tasks according to their status and recipient
            if (postTask.recipient) {
                taskViewHolder.taskBtn.setVisibility(View.VISIBLE);
                taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_me));
                taskViewHolder.taskIsRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_purple));
                taskViewHolder.taskIcon.setImageResource(R.drawable.sajatfeladataim_lila);
            } else {

                if (postTask.user.getId() == mUser.getId()) {
                    taskViewHolder.taskBtn.setVisibility(View.GONE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_to));
                    taskViewHolder.taskIsRecipientTv.setTextColor(Color.parseColor("#F25B3E"));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.kiosztottfeladataim_narancs);
                } else {
                    taskViewHolder.taskBtn.setVisibility(View.GONE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned));
                    taskViewHolder.taskIsRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_primary_actionbar));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.feladatok_kek);
                }
            }

            //Hideing of subchilds
            taskViewHolder.subChildLayout.removeAllViews();
            taskViewHolder.subChildLayout.setVisibility(View.GONE);
            taskViewHolder.arrowIv.setRotation(270);

            //Show ranking
            if (postTask.hasExplicitRank) {
                taskViewHolder.rankIv.setImageResource(R.drawable.csillag_sarga);
            } else {
                taskViewHolder.rankIv.setImageResource(R.drawable.csillag);
            }

            //Execution dialog of task
            taskViewHolder.taskBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        createExecutionDialog(postTask.id, postTask.status, null);
                    }
                }
            });

            //Get first subtask
//            Collections.copy(taskSubChilds, postTask.getChildList());
            PostSubTask firstSubTask = null;
            for (int i = 0; i < taskSubChilds.size(); i++) {
                if (taskSubChilds.get(i).getType().equals(EPostChildType.PostSubTask)) {
                    PostSubTask postSubTask = (PostSubTask) taskSubChilds.get(i);
                    if (postSubTask.recipient) {
                        firstSubTask = postSubTask;
                        taskSubChilds.remove(taskSubChilds.get(i));
                        break;
                    }
                }
            }

            addFirstSubTask(firstSubTask, taskViewHolder.firstSubTaskLayout);


            //add postchildlist
            //visibility of show/hide layout
            if (taskSubChilds.size() == 0 || taskSubChilds == null) {
                taskViewHolder.moreSubChildButton.setVisibility(View.GONE);
                taskViewHolder.subChildLayout.setVisibility(View.GONE);
            } else {
                taskViewHolder.moreSubChildButton.setVisibility(View.VISIBLE);
            }

            taskViewHolder.subChildCount.setText(mContext.getResources().getString(R.string.post_detail_submessage_first) + " " + taskSubChilds.size() + " " + mContext.getResources().getString(R.string.post_detail_submessage) + "/feladat");

            final List<IPostChild> finalSubChilds = taskSubChilds;
            //show/hide subchilds layout
            taskViewHolder.moreSubChildButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (taskViewHolder.subChildLayout.getVisibility() == View.GONE) {
                        taskViewHolder.subChildLayout.setVisibility(View.VISIBLE);
                        taskViewHolder.subChildLayout.removeAllViews();
                        taskViewHolder.arrowIv.setRotation(90);
                        try {
                            addChildViews(taskViewHolder.subChildLayout, finalSubChilds);
                        } catch (DMSCloudDBException e) {
                            e.printStackTrace();
                        }
                    } else {
                        taskViewHolder.arrowIv.setRotation(270);
                        taskViewHolder.subChildLayout.removeAllViews();
                        taskViewHolder.subChildLayout.setVisibility(View.GONE);
                    }
                }
            });

            //Start message activity for task
            taskViewHolder.taskComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(mContext, TaskMessageActivity.class);
                        intent.putExtra("postTask", postTask);
                        mContext.startActivity(intent);
                    }
                }
            });
            //WORKFLOW TASK
        } else if (postTaskList.get(position).getType().equals(EPostChildType.PostWfTask)) {
            final PostWfTask postTask = (PostWfTask) postTaskList.get(position);
            final TaskViewHolder taskViewHolder = holder;

            //Set text and fonts
            taskViewHolder.taskTypeIv.setImageResource(R.drawable.munkafolyamatok);
            taskViewHolder.descriptionTv.setText(postTask.title + "  " + postTask.message);
            taskViewHolder.deadLineTv.setText(mContext.getResources().getString(R.string.due_date) + postTask.formatedDueDate);
            taskViewHolder.nameTv.setText(postTask.user.getName());
            taskViewHolder.nameTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            taskViewHolder.dateTv.setText(postTask.formatedCreatedOn);
            taskViewHolder.taskBtn.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            taskViewHolder.taskBtn.setTransformationMethod(null);
            taskViewHolder.taskBtn.setVisibility(View.VISIBLE);
            taskViewHolder.firstSubTaskLayout.setVisibility(View.GONE);
            //Ranking
            taskViewHolder.ratingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();

                    } else {
                        new RankPostTask(mPost.id, postTask.id, postTask.subPostType, (Activity) mContext).execute();
                    }
                }
            });
            taskViewHolder.taskHourTv.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postTask.createdOn));


            //Document show fragment
            taskViewHolder.documentId.setVisibility(View.GONE);
            if (postTask.documentId == 0) {
                taskViewHolder.documentId.setVisibility(View.GONE);
            } else {
                taskViewHolder.documentId.setVisibility(View.VISIBLE);
            }
            taskViewHolder.documentId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putLong("documentId", postTask.documentId);
                    args.putSerializable("post", mPost);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                }
            });

            //Hideing of subchilds
            taskViewHolder.subChildLayout.removeAllViews();
            taskViewHolder.subChildLayout.setVisibility(View.GONE);
            taskViewHolder.arrowIv.setRotation(270);


            //Show ranking
            if (postTask.hasExplicitRank) {
                taskViewHolder.rankIv.setImageResource(R.drawable.csillag_sarga);
            } else {
                taskViewHolder.rankIv.setImageResource(R.drawable.csillag);
            }

            //Show wftasks according to their status and recipient
            if (postTask.recipient) {
                if (postTask.status == WorkflowStatus.ACCEPTED || postTask.status == WorkflowStatus.CREATED) {
                    taskViewHolder.taskBtn.setVisibility(View.VISIBLE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_me));
                    taskViewHolder.taskIsRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_purple));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.sajatfeladataim_lila);
                } else {
                    taskViewHolder.taskBtn.setVisibility(View.GONE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_me));
                    taskViewHolder.taskIsRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_purple));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.sajatfeladataim_lila);
                    taskViewHolder.descriptionTv.setText(postTask.title + "  " + postTask.message + " " + WorkflowStatus.getWorkflowStatusBy(postTask.status));
                }
            } else {
                if (postTask.user.getId() == mUser.getId()) {
                    taskViewHolder.taskBtn.setVisibility(View.GONE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_to));
                    taskViewHolder.taskIsRecipientTv.setTextColor(Color.parseColor("#F25B3E"));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.kiosztottfeladataim_narancs);
                } else {
                    taskViewHolder.taskBtn.setVisibility(View.GONE);
                    taskViewHolder.taskIsRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned));
                    taskViewHolder.taskIsRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_primary_actionbar));
                    taskViewHolder.taskIcon.setImageResource(R.drawable.feladatok_kek);
                }
            }

            //add postchildlist
            if (postTask.getChildList().size() == 0 || postTask.getChildList() == null) {
                taskViewHolder.moreSubChildButton.setVisibility(View.GONE);
                taskViewHolder.subChildLayout.setVisibility(View.GONE);
            } else {
                taskViewHolder.moreSubChildButton.setVisibility(View.VISIBLE);
            }

            taskViewHolder.subChildCount.setText(mContext.getResources().getString(R.string.post_detail_submessage_first) + " " + postTask.getChildList().size() + " " + mContext.getResources().getString(R.string.post_detail_submessage));

            //show/hide subchild layout
            taskViewHolder.moreSubChildButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (taskViewHolder.subChildLayout.getVisibility() == View.GONE) {
                        taskViewHolder.subChildLayout.setVisibility(View.VISIBLE);
                        taskViewHolder.subChildLayout.removeAllViews();
                        taskViewHolder.arrowIv.setRotation(90);
                        try {
                            addChildViews(taskViewHolder.subChildLayout, postTask.getChildList());
                        } catch (DMSCloudDBException e) {
                            e.printStackTrace();
                        }

                    } else {
                        taskViewHolder.arrowIv.setRotation(270);
                        taskViewHolder.subChildLayout.removeAllViews();
                        taskViewHolder.subChildLayout.setVisibility(View.GONE);
                    }
                }
            });

            //Start execution of wftask
            taskViewHolder.taskBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        createExecutionDialog(postTask.id, postTask.status, postTask);
//                        Intent intent = new Intent(mContext, WfTaskExecutionActivity.class);
//                        intent.putExtra("postWfTask", postTask);
//                        mContext.startActivity(intent);
                    }
                }
            });

            //start taskmessage activity
            taskViewHolder.taskComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(mContext, TaskMessageActivity.class);
                        intent.putExtra("postWfTask", postTask);
                        mContext.startActivity(intent);
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return postTaskList.size();
    }

    /**
     * ViewHolder of taskItems
     */
    public class TaskViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv, dateTv, deadLineTv, descriptionTv, subChildCount, taskComment, taskIsRecipientTv, taskHourTv;
        ImageView taskIcon, rankIv, arrowIv, taskTypeIv, documentId;
        Button taskBtn;
        LinearLayout moreSubChildButton, subChildLayout, ratingButton, firstSubTaskLayout;

        public TaskViewHolder(View itemView) {
            super(itemView);

            nameTv = (TextView) itemView.findViewById(R.id.task_item_name_Tv);
            dateTv = (TextView) itemView.findViewById(R.id.task_item_date_Tv);
            deadLineTv = (TextView) itemView.findViewById(R.id.task_item_deadline_Tv);
            descriptionTv = (TextView) itemView.findViewById(R.id.task_item_description_Tv);
            taskIcon = (ImageView) itemView.findViewById(R.id.task_item_icon_Iv);
            taskBtn = (Button) itemView.findViewById(R.id.task_item_button);
            subChildLayout = (LinearLayout) itemView.findViewById(R.id.subchild_holder);
            moreSubChildButton = (LinearLayout) itemView.findViewById(R.id.subchild_more);
            subChildCount = (TextView) itemView.findViewById(R.id.subchild_count_Tv);
            taskComment = (TextView) itemView.findViewById(R.id.task_add_comment);
            ratingButton = (LinearLayout) itemView.findViewById(R.id.task_detail_rank);
            rankIv = (ImageView) itemView.findViewById(R.id.task_detail_rank_iv);
            arrowIv = (ImageView) itemView.findViewById(R.id.hide_layout_arrowIv);
            taskIsRecipientTv = (TextView) itemView.findViewById(R.id.task_item_task_Tv);
            taskTypeIv = (ImageView) itemView.findViewById(R.id.task_item_typeIv);
            taskHourTv = (TextView) itemView.findViewById(R.id.task_item_hour_Tv);
            documentId = (ImageView) itemView.findViewById(R.id.task_item_documentId);
            firstSubTaskLayout = (LinearLayout) itemView.findViewById(R.id.first_subPost_layout);
        }


    }

    public void refreshList(ArrayList<IPostChild> postTaskList) {
        this.postTaskList = postTaskList;
        notifyDataSetChanged();
    }


    /**
     * Add first SubTask Child view
     *
     * @param postSubTask
     * @param firstSubTaskLayout
     */
    public void addFirstSubTask(final PostSubTask postSubTask, LinearLayout firstSubTaskLayout) {
        TextView messageText, nameText, timeText, deadLineText, taskRecipientTv;
        ImageView taskIcon, statusIcon, documentId;
        Button taskBtn;

        if (postSubTask != null) {
            messageText = (TextView) firstSubTaskLayout.findViewById(R.id.task_item_description_Tv);
            nameText = (TextView) firstSubTaskLayout.findViewById(R.id.task_item_name_Tv);
            timeText = (TextView) firstSubTaskLayout.findViewById(R.id.task_item_date_Tv);
            deadLineText = (TextView) firstSubTaskLayout.findViewById(R.id.task_item_deadline_Tv);
            taskRecipientTv = (TextView) firstSubTaskLayout.findViewById(R.id.task_item_task_Tv);
            taskIcon = (ImageView) firstSubTaskLayout.findViewById(R.id.task_item_icon_Iv);
            statusIcon = (ImageView) firstSubTaskLayout.findViewById(R.id.task_subchild_statusIcon);
            documentId = (ImageView) firstSubTaskLayout.findViewById(R.id.subTask_documentId);
            taskBtn = (Button) firstSubTaskLayout.findViewById(R.id.task_item_button);
            taskBtn.setTransformationMethod(null);
            taskBtn.setTypeface(FontManager.getInstance().getRobotoBoldTtf());

            //Document
            if (postSubTask.documentId == 0) {
                documentId.setVisibility(View.GONE);
            } else {
                documentId.setVisibility(View.VISIBLE);
            }
            documentId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("post", mPost);
                    args.putLong("documentId", postSubTask.documentId);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                }
            });


            //status kiírás
            statusIcon.setImageResource(TaskStatus.getStatusIcon(postSubTask.status));
            messageText.setText(TaskStatus.getTaskStatusBy(postSubTask.status));
            messageText.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            messageText.setTextColor(mContext.getResources().getColor(TaskStatus.getStatusColor(postSubTask.status)));


            //Check recipient set text and icon according to it
            if (postSubTask.recipient) {
                taskBtn.setVisibility(View.VISIBLE);
                taskRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned_me));
                taskRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_purple));
                taskIcon.setImageResource(R.drawable.sajatfeladataim_lila);
            } else {
                taskRecipientTv.setText(mContext.getResources().getString(R.string.post_detail_task_assigned));
                taskRecipientTv.setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_blue));
                taskIcon.setImageResource(R.drawable.feladatok_kek);
                taskBtn.setVisibility(View.GONE);
            }

            //Visibility of button according to status
            switch (postSubTask.status) {
                case TaskStatus.TASK_CLOSED:
                    taskBtn.setVisibility(View.GONE);
                    break;
                case TaskStatus.TASK_REJECTED:
                    taskBtn.setVisibility(View.GONE);
                    break;
                default:
                    if (postSubTask.recipient) {
                        taskBtn.setVisibility(View.VISIBLE);
                    } else {
                        taskBtn.setVisibility(View.GONE);
                    }
                    break;
            }

            //Execution dialog for subtask
            taskBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        createExecutionDialog(postSubTask.id, postSubTask.status, null);
                    }
                }
            });

//                messageTexts[i].setText(postSubTask.message);
            nameText.setText(postSubTask.user.formatedName);
            deadLineText.setText(postSubTask.formatedDueDate);
            nameText.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            timeText.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubTask.createdOn));
//            childLayout.addView(linearLayouts[i]);

            firstSubTaskLayout.setVisibility(View.VISIBLE);

        } else {
            firstSubTaskLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Add child views of task and wfTask
     *
     * @param childLayout
     * @param childList
     * @throws DMSCloudDBException
     */
    public void addChildViews(LinearLayout childLayout, List<IPostChild> childList) throws DMSCloudDBException {
        LinearLayout[] linearLayouts = new LinearLayout[childList.size()];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        TextView[] messageTexts = new TextView[childList.size()];
        TextView[] nameTexts = new TextView[childList.size()];
        TextView[] timeTexts = new TextView[childList.size()];
        TextView[] deadLineTexts = new TextView[childList.size()];
        Button[] taskBtns = new Button[childList.size()];
        LinearLayout[] deleteLayouts = new LinearLayout[childList.size()];
        ImageView[] statusIcons = new ImageView[childList.size()];
        ImageView[] taskIcons = new ImageView[childList.size()];
        TextView[] taskRecipientTv = new TextView[childList.size()];
        ImageView[] documentIds = new ImageView[childList.size()];
        boolean isFirst = true;


        for (int i = 0; i < childList.size(); i++) {
            if (childList.get(i).getType().equals(EPostChildType.PostSubComment)) {
                final PostSubComment postSubComment = (PostSubComment) childList.get(i);
                linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.message_subchild_layout, null);
                messageTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_message);
                nameTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_name);
                timeTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_time);
                deleteLayouts[i] = (LinearLayout) linearLayouts[i].findViewById(R.id.subchild_layout_button);
                documentIds[i] = (ImageView) linearLayouts[i].findViewById(R.id.subcomment_documentId);

                //Document show
                if (postSubComment.documentId == 0) {
                    documentIds[i].setVisibility(View.GONE);
                } else {
                    documentIds[i].setVisibility(View.VISIBLE);
                }
                documentIds[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, SharePostActivity.class);
                        Bundle args = new Bundle();
                        args.putLong("documentId", postSubComment.documentId);
                        args.putSerializable("post", mPost);
                        args.putInt("fragment", 4);
                        intent.putExtras(args);
                        mContext.startActivity(intent);
                    }
                });

                //Check if subcomment is first and users match for delete layout visibility
                if (isFirst && UserManager.getInstance().getCurrentUser().getId() == postSubComment.user.getId()) {
                    deleteLayouts[i].setVisibility(View.VISIBLE);
                } else {
                    deleteLayouts[i].setVisibility(View.GONE);
                }

                //Delete subComment
                deleteLayouts[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPost.closed) {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                        } else {
                            new DeleteSubCommentTask(postSubComment.id, (Activity) mContext).execute();
                        }
                    }
                });

                messageTexts[i].setText(postSubComment.message);
                nameTexts[i].setText(postSubComment.user.formatedName);
                nameTexts[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                timeTexts[i].setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubComment.createdOn));
                isFirst = false;
                childLayout.addView(linearLayouts[i]);

            } else if (childList.get(i).getType().equals(EPostChildType.PostSubTask)) {
                final PostSubTask postSubTask = (PostSubTask) childList.get(i);
                linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.task_subchild_layout, null);
                messageTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.task_item_description_Tv);
                nameTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.task_item_name_Tv);
                timeTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.task_item_date_Tv);
                deadLineTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.task_item_deadline_Tv);
                taskRecipientTv[i] = (TextView) linearLayouts[i].findViewById(R.id.task_item_task_Tv);
                taskIcons[i] = (ImageView) linearLayouts[i].findViewById(R.id.task_item_icon_Iv);
                statusIcons[i] = (ImageView) linearLayouts[i].findViewById(R.id.task_subchild_statusIcon);
                documentIds[i] = (ImageView) linearLayouts[i].findViewById(R.id.subTask_documentId);
                taskBtns[i] = (Button) linearLayouts[i].findViewById(R.id.task_item_button);
                taskBtns[i].setTransformationMethod(null);
                taskBtns[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());

                //Document
                if (postSubTask.documentId == 0) {
                    documentIds[i].setVisibility(View.GONE);
                } else {
                    documentIds[i].setVisibility(View.VISIBLE);
                }
                documentIds[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, SharePostActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable("post", mPost);
                        args.putLong("documentId", postSubTask.documentId);
                        args.putInt("fragment", 4);
                        intent.putExtras(args);
                        mContext.startActivity(intent);
                    }
                });


                //status kiírás
                statusIcons[i].setImageResource(TaskStatus.getStatusIcon(postSubTask.status));
                messageTexts[i].setText(TaskStatus.getTaskStatusBy(postSubTask.status));
                messageTexts[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                messageTexts[i].setTextColor(mContext.getResources().getColor(TaskStatus.getStatusColor(postSubTask.status)));


                //Check recipient set text and icon according to it
                if (postSubTask.recipient) {
                    taskBtns[i].setVisibility(View.VISIBLE);
                    taskRecipientTv[i].setText(mContext.getResources().getString(R.string.post_detail_task_assigned_me));
                    taskRecipientTv[i].setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_purple));
                    taskIcons[i].setImageResource(R.drawable.sajatfeladataim_lila);
                } else {
                    taskRecipientTv[i].setText(mContext.getResources().getString(R.string.post_detail_task_assigned));
                    taskRecipientTv[i].setTextColor(mContext.getResources().getColor(R.color.color_dmsclooud_blue));
                    taskIcons[i].setImageResource(R.drawable.feladatok_kek);
                    taskBtns[i].setVisibility(View.GONE);
                }

                //Visibility of button according to status
                switch (postSubTask.status) {
                    case TaskStatus.TASK_CLOSED:
                        taskBtns[i].setVisibility(View.GONE);
                        break;
                    case TaskStatus.TASK_REJECTED:
                        taskBtns[i].setVisibility(View.GONE);
                        break;
                    default:
                        if (postSubTask.recipient) {
                            taskBtns[i].setVisibility(View.VISIBLE);
                        } else {
                            taskBtns[i].setVisibility(View.GONE);
                        }
                        break;
                }

                //Execution dialog for subtask
                taskBtns[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPost.closed) {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                        } else {
                            createExecutionDialog(postSubTask.id, postSubTask.status, null);
                        }
                    }
                });

//                messageTexts[i].setText(postSubTask.message);
                nameTexts[i].setText(postSubTask.user.formatedName);
                deadLineTexts[i].setText(postSubTask.formatedDueDate);
                nameTexts[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                timeTexts[i].setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubTask.createdOn));
                childLayout.addView(linearLayouts[i]);
            }
        }
    }

    /**
     * Create dialog for execution of tasks, subtask and workflow tasks
     *
     * @param taskId
     * @param taskStatus
     */
    public void createExecutionDialog(final long taskId, int taskStatus, final PostWfTask postWfTask) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.task_execute_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        LinearLayout acceptButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_accept);
        LinearLayout rejectButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_reject);
        LinearLayout closeButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_close);
        LinearLayout alreadyClosed = (LinearLayout) dialog.findViewById(R.id.dialog_closed_layout);
        LinearLayout rejectedLayout = (LinearLayout) dialog.findViewById(R.id.dialog_rejected_layout);
        LinearLayout terminateLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_terminate);
        LinearLayout wfAcceptLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_wfaccept);
        LinearLayout wfReadyLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_wfclose);
        LinearLayout wfTerminatedLayout = (LinearLayout) dialog.findViewById(R.id.dialog_terminated_layout);


        Log.d("STATUS OF DIALOG", WorkflowStatus.getWorkflowStatusBy(taskStatus));
        //Set up buttons for execution according to status
        switch (taskStatus) {
            case TaskStatus.TASK_ACCEPTED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_ASSIGNED:
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_CLOSED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_REJECTED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.ACCEPTED:
                acceptButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.READY:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.CREATED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.TERMINATED:
                alreadyClosed.setVisibility(View.GONE);
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                break;
            default:
                break;
        }


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_ACCEPTED, "").execute();
                dialog.dismiss();
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_REJECTED, "").execute();
                dialog.dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_CLOSED, mContext.getResources().getString(R.string.post_detail_task_closed)).execute();
                dialog.dismiss();
            }
        });


        wfAcceptLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wf elfogadás
                new ChangeWfTaskStatusTask(postWfTask, postWfTask.id, "", WorkflowStatus.ACCEPTED, null, MetaManager.getInstance().getFillMetaList()).execute();
                dialog.dismiss();
            }
        });

        wfReadyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wf lezárás
                Intent intent = new Intent(mContext, WfTaskExecutionActivity.class);
                intent.putExtra("postWfTask", postWfTask);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });

        //Window placement and dimensions
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    /**
     * Change task status in background
     */
    public class ChangeTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mTaskId;
        private int mTaskStatus;
        private String mMessage;


        public ChangeTaskStatusTask(final long taskId, final int status, final String message) {
            mTaskId = taskId;
            mTaskStatus = status;
            mMessage = message;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeTaskStatus(mTaskId, mTaskStatus, mMessage);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                Toast.makeText(mContext, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                PrefManager.getInstance().setIsFirst(true);
                Intent intent = ((PostActivity) mContext).getmActivityIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (ex instanceof ValidationException) {
                Intent intent = new Intent(mContext, TaskExecutionActivity.class);
                intent.putExtra("postTaskId", mTaskId);
                mContext.startActivity(intent);
            } else {
                if (!((Activity) mContext).isFinishing()) {
                    DialogHelper.showErrorDialog(mContext, ex);
                }
            }

        }
    }

    /**
     * Change workflow task status in background
     */
    public class ChangeWfTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mWfTaskId;
        private Integer mStatus, mResultId;
        private String mMessage;
        private List<FillMeta> mMetaList;
        private PostWfTask mPostWfTask;

        public ChangeWfTaskStatusTask(PostWfTask postWfTask, final long wfTaskId, final String message, final int status, final Integer resultId, final List<FillMeta> metaList) {
            mWfTaskId = wfTaskId;
            mStatus = status;
            mMessage = message;
            mResultId = resultId;
            mMetaList = metaList;
            mPostWfTask = postWfTask;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeWfTaskStatus(mWfTaskId, mMessage, mStatus, mResultId, mMetaList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            Log.d("response", "" + response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                Toast.makeText(mContext, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                PrefManager.getInstance().setIsFirst(true);
                Intent intent = ((PostActivity) mContext).getmActivityIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            Log.d("response", "" + ex.getMessage());
            if (ex instanceof ValidationException) {
                Intent intent = new Intent(mContext, WfTaskExecutionActivity.class);
                intent.putExtra("postWfTask", mPostWfTask);
                mContext.startActivity(intent);
            } else {
                if (!((Activity) mContext).isFinishing()) {
                    DialogHelper.showErrorDialog(mContext, ex);
                }
            }
        }
    }


}
