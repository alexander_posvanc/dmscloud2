package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.bean.Distribution;
import com.mmsone.dmscloud.bo.bean.Group;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 10. 08..
 */
public class ShareDialogAdapter extends RecyclerView.Adapter<ShareDialogAdapter.ShareViewHolder> {

    private List<IUser> mUserList;
    private DisplayImageOptions mDisplayImageOptions;
    private Post mPost;
    private Context mContext;
    private LinearLayout mRecipientLayout;
    private ArrayList<Long> mListOfUserIds = new ArrayList<>();
    private ArrayList<Long> mListOfGroupIds = new ArrayList<>();
    private ArrayList<Long> mListOfDistributionIds = new ArrayList<>();


    /**
     * Constructor of ShareAdapter
     *
     * @param userList List of Users, Distributions and Groups
     * @param context
     */
    public ShareDialogAdapter(List<IUser> userList, Context context, LinearLayout recipientLayout) {
        mUserList = userList;
        mContext = context;
        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();
        mRecipientLayout = recipientLayout;
    }


    @Override
    public ShareViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_share_dialog, parent, false);
        ShareViewHolder vhItem = new ShareViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(final ShareViewHolder holder, final int position) {
        //Sharing of users
        if (mUserList.get(position).getType().equals(IUser.EUserType.USER)) {
            final User user = (User) mUserList.get(position);
            holder.nameTv.setText(user.getName());
            ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + user.image, holder.profileIv, mDisplayImageOptions);
            holder.addButton.setVisibility(View.VISIBLE);

            if (mListOfUserIds.contains(user.getId())) {
                holder.addButton.setVisibility(View.GONE);
            }

            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setRecipients(user.getId(), user.getName(), user.getType());
                    mListOfUserIds.add(user.getId());
                    holder.addButton.setVisibility(View.GONE);

                }
            });
            //Sharing of Distributions
        } else if (mUserList.get(position).getType().equals(IUser.EUserType.DISTRIBUTION)) {
            final Distribution distribution = (Distribution) mUserList.get(position);
            holder.nameTv.setText(distribution.getName());
//            ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + distribution., holder.profileIv, mDisplayImageOptions);
            holder.profileIv.setImageResource(R.drawable.ic_launcher);
            holder.addButton.setVisibility(View.VISIBLE);

            if (mListOfDistributionIds.contains(distribution.getId())) {
                holder.addButton.setVisibility(View.GONE);
            }

            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setRecipients(distribution.getId(), distribution.getName(), distribution.getType());
                    mListOfDistributionIds.add(distribution.getId());
                    holder.addButton.setVisibility(View.GONE);

                }
            });
            //Sharing of Groups
        } else if (mUserList.get(position).getType().equals(IUser.EUserType.GROUP)) {
            final Group group = (Group) mUserList.get(position);
            holder.nameTv.setText(group.getName());
            holder.profileIv.setImageResource(R.drawable.ic_launcher);
            holder.addButton.setVisibility(View.VISIBLE);

            if (mListOfGroupIds.contains(group.getId())) {
                holder.addButton.setVisibility(View.GONE);
            }

            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setRecipients(group.getId(), group.getName(), group.getType());
                    mListOfGroupIds.add(group.getId());
                    holder.addButton.setVisibility(View.GONE);

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class ShareViewHolder extends RecyclerView.ViewHolder {
        ImageView profileIv;
        TextView nameTv;
        Button addButton;

        public ShareViewHolder(View itemView) {
            super(itemView);

            profileIv = (ImageView) itemView.findViewById(R.id.share_item_profileIv);
            nameTv = (TextView) itemView.findViewById(R.id.share_item_nameTv);
            addButton = (Button) itemView.findViewById(R.id.share_item_button);
        }
    }

    /**
     * Updates RecyclerView with new userList
     *
     * @param userList List of Users, Distributions and Groups
     */
    public void updateList(List<IUser> userList) {
        mUserList = userList;
        notifyDataSetChanged();
    }

    /**
     * Set recipients
     */
    public void setRecipients(final Long id, String name, final IUser.EUserType type) {

        final View userView = View.inflate(mContext, R.layout.view_user, null);
        TextView labelTv = (TextView) userView.findViewById(R.id.view_user_nameTv);
        ImageView labelDelete = (ImageView) userView.findViewById(R.id.delete_user_layout);

        labelTv.setText(name);
        labelTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

        labelDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecipientLayout.removeView(userView);
                if (type.equals(IUser.EUserType.USER)) {
                    mListOfUserIds.remove(id);
                } else if (type.equals(IUser.EUserType.DISTRIBUTION)) {
                    mListOfDistributionIds.remove(id);
                } else if (type.equals(IUser.EUserType.GROUP)) {
                    mListOfGroupIds.remove(id);
                }
            }
        });
        mRecipientLayout.addView(userView);
    }


    public Long[] getmListOfDistributionIds() {
        return mListOfDistributionIds.toArray(new Long[mListOfDistributionIds.size()]);
    }

    public Long[] getmListOfGroupIds() {
        return mListOfGroupIds.toArray(new Long[mListOfGroupIds.size()]);
    }

    public Long[] getmListOfUserIds() {
        return mListOfUserIds.toArray(new Long[mListOfUserIds.size()]);
    }

    public ArrayList<Long> getmArrayListOfUserIds() {
        return mListOfUserIds;
    }


}