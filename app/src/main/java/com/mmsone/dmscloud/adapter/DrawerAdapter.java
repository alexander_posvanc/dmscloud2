package com.mmsone.dmscloud.adapter;

import java.util.ArrayList;
import java.util.List;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bo.DrawerItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter {

    private List<DrawerItem> mDrawerItemList;
    private LayoutInflater mLayoutInflater;

    public DrawerAdapter(Context context, List<DrawerItem> drawerItemList) {
        mLayoutInflater = LayoutInflater.from(context);
        mDrawerItemList = new ArrayList<DrawerItem>(drawerItemList); 
    }

    @Override
    public int getCount() {
        return mDrawerItemList.size();
    }

    @Override
    public DrawerItem getItem(int arg0) {
        return mDrawerItemList.get(arg0);
    }
    
	@Override
	public long getItemId(int arg0) {
		return 0;
	}

    private static class ViewHolder {
        public TextView textTv;
        public ImageView iconIv;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.list_item_drawer, null);

            holder = new ViewHolder();
            holder.textTv = (TextView) view.findViewById(R.id.list_item_drawer_textTv);
            holder.iconIv = (ImageView) view.findViewById(R.id.list_item_drawer_iconIv);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        DrawerItem drawerItem = getItem(pos);

        holder.textTv.setText(drawerItem.text);
        holder.textTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.iconIv.setImageResource(drawerItem.iconId);

        return view;
    }
}