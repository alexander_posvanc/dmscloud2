package com.mmsone.dmscloud.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by Alexander on 2015. 08. 25..
 */
public class SharedWithAdapter extends RecyclerView.Adapter<SharedWithAdapter.SharedWithViewHolder> {

    private Post mPost;
    private List<User> mUserList;
    private DisplayImageOptions mDisplayImageOptions;

    /**
     * Constructor of SharedWithAdapter
     *
     * @param post
     */
    public SharedWithAdapter(Post post) {
        mPost = post;
        try {
            mUserList = DataBaseConnector.getInstance().getSharedWithUsersList(mPost.sharedWith);
//            mUserList.addAll(DataBaseConnector.getInstance().getSharedWithUsersList(mPost.hiddenAt));

        } catch (DMSCloudDBException e) {
            e.printStackTrace();
        }

        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();

    }

    @Override
    public SharedWithViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_shared_with, parent, false);
        SharedWithViewHolder vhItem = new SharedWithViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(SharedWithViewHolder holder, int position) {
        User user = mUserList.get(position);
        holder.profileName.setText(user.getName());
        if (mPost.hiddenAt.contains(user.getId())) {
            holder.profileName.setText(user.getName() + "(h)");
        }
        ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + user.image, holder.profileIv, mDisplayImageOptions);
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class SharedWithViewHolder extends RecyclerView.ViewHolder {

        ImageView profileIv;
        TextView profileName;

        public SharedWithViewHolder(View itemView) {
            super(itemView);
            profileIv = (ImageView) itemView.findViewById(R.id.shared_with_item_profileIv);
            profileName = (TextView) itemView.findViewById(R.id.shared_with_item_nameTv);

        }
    }
}
