package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.bean.CodeStore;

public class CodeStoreArrayAdapter extends ArrayAdapter<CodeStore> {

	private CodeStore[] mCodeStoreList;
	private Context mContext;

	/**
	 * CodeStoreArrayAdapter constructor
	 *
	 * @param context
	 * @param txtViewResourceId
	 * @param objects
	 */
	public CodeStoreArrayAdapter(Context context, int txtViewResourceId, CodeStore[] objects) {
		super(context, txtViewResourceId, objects);
		mContext = context;
		mCodeStoreList = objects;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt, true);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt, false);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDownView) {
		View view = View.inflate(mContext, R.layout.view_user_spinner_item, null);
		TextView textView = (TextView) view.findViewById(R.id.user_spinner_item_nameTv);

		//set view margins
		if (isDropDownView) {
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(8, 8, 8, 8);
			textView.setLayoutParams(layoutParams);
		}

		CodeStore codeStore = mCodeStoreList[position];
		if ((codeStore == null) && (position == 0)) {
			textView.setText("");
		} else {
			textView.setText(codeStore.toString());
		}

		return view;
	}
}