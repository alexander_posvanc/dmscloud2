package com.mmsone.dmscloud.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.HidePostTask;
import com.mmsone.dmscloud.async.MarkPostTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.DrawerItem;
import com.mmsone.dmscloud.bo.UserRole;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.PostActivity;
import com.mmsone.dmscloud.ui.SharePostActivity;
import com.mmsone.dmscloud.ui.helper.OnItemClickListener;

import java.util.List;

/**
 * Created by Alexander on 2015. 08. 13..
 */
public class PostDrawerAdapter extends RecyclerView.Adapter<PostDrawerAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private int mTaskCount;
    private Post mPost;
    private Context mContext;

    public PostDrawerAdapter(Context context, Post post) {
        mPost = post;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_drawer_item_item, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);
            return vhItem;
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_drawer_item_header, parent, false);
            ViewHolder vhHeader = new ViewHolder(v, viewType);
            return vhHeader;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 1) {
            //Show shared users of this post
            holder.textView.setText(mContext.getResources().getString(R.string.post_drawer_share));
            holder.imageView.setImageResource(R.drawable.resztvevok_kek);
            holder.taskCountTv.setText(" (" + mPost.sharedWith.size() + ")");
            holder.itemRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putInt("fragment", 3);
                    args.putSerializable("post", mPost);
                    intent.putExtras(args);
                    mContext.startActivity(intent);

                }
            });
        } else if (position == 2) {
            //Mark post

            holder.textView.setText(mContext.getResources().getString(R.string.post_drawer_mark));
            if (mPost.marked) {
                holder.textView.setText(mContext.getResources().getString(R.string.post_drawer_unmark));
            } else {
                holder.textView.setText(mContext.getResources().getString(R.string.post_drawer_mark));
            }
            holder.imageView.setImageResource(R.drawable.zaszlo_kek);
            holder.taskCountTv.setText("");
            holder.itemRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (UserManager.getInstance().hasUserRole(UserRole.MARK_ROLE)) {
                            new MarkPostTask(mPost.id, (PostActivity) mContext).execute();
                        } else {
                            DialogHelper.createErrorDialog(mContext, mContext.getString(R.string.err_insufficient_rights));
                        }
                    } catch (DMSCloudDBException e) {
                        DialogHelper.showErrorDialog(mContext, e);
                    }
                }
            });
        } else if (position == 3) {
            //Hide post
            holder.textView.setText(mContext.getResources().getString(R.string.post_drawer_hide));
            holder.imageView.setImageResource(R.drawable.elrejtes);
            holder.taskCountTv.setText("");
            holder.divider.setVisibility(View.GONE);
            holder.itemRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (UserManager.getInstance().hasUserRole(UserRole.HIDE_ROLE)) {
                            new HidePostTask(mPost.id, (PostActivity) mContext).execute();
                        } else {
                            DialogHelper.createErrorDialog(mContext, mContext.getString(R.string.err_insufficient_rights));
                        }
                    } catch (DMSCloudDBException e) {
                        DialogHelper.showErrorDialog(mContext, e);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    /**
     * ViewHolder for drawer items of PostDetail navDrawer
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView, taskCountTv;
        ImageView imageView;
        TextView name;
        RelativeLayout itemRow;
        View divider;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                itemRow = (RelativeLayout) itemView.findViewById(R.id.list_item_layout);
                taskCountTv = (TextView) itemView.findViewById(R.id.task_count_Tv);
                divider = itemView.findViewById(R.id.post_drawer_divider);
                Holderid = 1;
            } else {
                name = (TextView) itemView.findViewById(R.id.headerText);
                Holderid = 0;
            }
        }
    }

}
