package com.mmsone.dmscloud.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.bean.DownloadedFile;
import com.mmsone.dmscloud.bo.bean.Notification;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.util.List;

/**
 * Created by Alexander on 2015. 09. 01..
 */
public class DownloadsAdapter extends RecyclerView.Adapter<DownloadsAdapter.ViewHolder> {

    private List<DownloadedFile> mFileList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;
    private DisplayImageOptions mDisplayImageOptions;
    private ImageSize mImageSize;


    /**
     * Constructor of adapter holding files
     *
     * @param fileList list of downloaded files
     * @param context
     */
    public DownloadsAdapter(List<DownloadedFile> fileList, Context context) {
        mContext = context;
        mFileList = fileList;
        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_file_up_menu_bt_white).showImageOnFail(R.drawable.ic_file_up_menu_bt_white).cacheInMemory(true).cacheOnDisk(true).build();
        mImageSize = new ImageSize(100, 100);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_download_file, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);

            return vhItem;
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification_header, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);

            return vhItem;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        switch (holder.holderId) {
            case TYPE_ITEM:
                final File downloadedFile = mFileList.get(position).downloadedFile;
                holder.fileName.setText(mFileList.get(position).downloadedFile.getName());
                holder.listItemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Open selected file
                        try {
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);

                            Uri uri = Uri.fromFile(downloadedFile);

                            if (downloadedFile.toString().contains(".doc") || downloadedFile.toString().contains(".docx")) {
                                intent.setDataAndType(uri, "application/msword");
                            } else if (downloadedFile.toString().contains(".pdf")) {
                                intent.setDataAndType(uri, "application/pdf");
                            } else if (downloadedFile.toString().contains(".ppt") || downloadedFile.toString().contains(".pptx")) {
                                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            } else if (downloadedFile.toString().contains(".xls") || downloadedFile.toString().contains(".xlsx")) {
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                            } else if (downloadedFile.toString().contains(".zip") || downloadedFile.toString().contains(".rar")) {
                                intent.setDataAndType(uri, "application/zip");
                            } else if (downloadedFile.toString().contains(".rtf")) {
                                intent.setDataAndType(uri, "application/rtf");
                            } else if (downloadedFile.toString().contains(".wav") || downloadedFile.toString().contains(".mp3")) {
                                intent.setDataAndType(uri, "audio/x-wav");
                            } else if (downloadedFile.toString().contains(".gif")) {
                                intent.setDataAndType(uri, "image/gif");
                            } else if (downloadedFile.toString().contains(".jpg") || downloadedFile.toString().contains(".jpeg") || downloadedFile.toString().contains(".png")) {
                                intent.setDataAndType(uri, "image/jpeg");
                            } else if (downloadedFile.toString().contains(".txt")) {
                                intent.setDataAndType(uri, "text/plain");
                            } else if (downloadedFile.toString().contains(".3gp") || downloadedFile.toString().contains(".mpg") || downloadedFile.toString().contains(".mpeg") || downloadedFile.toString().contains(".mpe") || downloadedFile.toString().contains(".mp4") || downloadedFile.toString().contains(".avi")) {
                                intent.setDataAndType(uri, "video/*");
                            } else {
                                intent.setDataAndType(uri, "*/*");
                            }

                            mContext.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            DialogHelper.showErrorDialog(mContext, e);
                        }
                    }
                });

                //Load image from url in background
                ImageLoader.getInstance().loadImage(Uri.decode(Uri.fromFile(downloadedFile).toString()), mImageSize, mDisplayImageOptions, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.fileImage.setImageBitmap(loadedImage);
                    }
                });

                break;
            case TYPE_HEADER:
                //Add header for new dates
                holder.headerDate.setText(Config.DATE_FORMAT.format(mFileList.get(position).lastModified));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mFileList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position, mFileList)) {
            return TYPE_HEADER;
        } else return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position, List<DownloadedFile> notifications) {
        if (notifications.get(position).downloadedFile != null) {
            return false;
        } else return true;
    }

    /**
     * View holder of list items and header items
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView fileName, headerDate, fileDate;
        ImageView fileImage;
        int holderId;
        LinearLayout listItemLayout;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            if (viewType == TYPE_ITEM) {
                holderId = TYPE_ITEM;
                fileDate = (TextView) itemView.findViewById(R.id.file_upload_fileDateTv);
                fileName = (TextView) itemView.findViewById(R.id.file_upload_fileNameTv);
                fileImage = (ImageView) itemView.findViewById(R.id.file_upload_imageIv);
                listItemLayout = (LinearLayout) itemView.findViewById(R.id.downloads_list_item_layout);
            } else {
                holderId = TYPE_HEADER;
                headerDate = (TextView) itemView.findViewById(R.id.notification_header_dateTv);
            }
        }
    }
}
