package com.mmsone.dmscloud.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.DrawerItem;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.ui.helper.OnItemClickListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by Alexander on 2015. 08. 05..
 */
public class MainDrawerAdapter extends RecyclerView.Adapter<MainDrawerAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<DrawerItem> mMainDrawerItemList;
    private User user;
    OnItemClickListener mOnItemClickListener;
    private int mTaskCount;


    public MainDrawerAdapter(List<DrawerItem> drawerItemList) {
        mMainDrawerItemList = drawerItemList;
    }

    @Override
    public MainDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_recycle_drawer, viewGroup, false);
            ViewHolder vhItem = new ViewHolder(v, i);
            return vhItem;
        } else if (i == TYPE_HEADER) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_header_recycle_drawer, viewGroup, false);
            ViewHolder vhHeader = new ViewHolder(v, i);
            return vhHeader;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MainDrawerAdapter.ViewHolder viewHolder, final int position) {
        if (viewHolder.Holderid == 1) {
            viewHolder.textView.setText(mMainDrawerItemList.get(position - 1).text);
            viewHolder.countLayout.setVisibility(View.GONE);
            viewHolder.textView.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            viewHolder.imageView.setImageResource(mMainDrawerItemList.get(position - 1).iconId);
            viewHolder.itemRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null && position != 0) {
                        mOnItemClickListener.onItemClick(v, position);
                    }
                }
            });
            if (position == 2) {
                if (mTaskCount != 0) {
                    viewHolder.countLayout.setVisibility(View.VISIBLE);
                    viewHolder.taskCountTv.setText(mTaskCount + "");
                    viewHolder.taskCountTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                }
            }
        } else {
            try {
                user = UserManager.getInstance().getCurrentUser();
            } catch (DMSCloudDBException e) {
                e.printStackTrace();
            }

            DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();
            ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + user.image, viewHolder.profile, mDisplayImageOptions);
            viewHolder.name.setText(user.formatedName);
            viewHolder.name.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        }
    }

    @Override
    public int getItemCount() {
        return mMainDrawerItemList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * View holder of main nav draver items
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView, taskCountTv;
        ImageView imageView;
        ImageView profile;
        TextView name;
        RelativeLayout itemRow, countLayout;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                itemRow = (RelativeLayout) itemView.findViewById(R.id.list_item_layout);
                countLayout = (RelativeLayout) itemView.findViewById(R.id.drawer_item_count_layout);
                taskCountTv = (TextView) itemView.findViewById(R.id.task_count_Tv);
                Holderid = 1;
            } else {
                name = (TextView) itemView.findViewById(R.id.name);
                profile = (ImageView) itemView.findViewById(R.id.profileView);
                Holderid = 0;
            }
        }
    }


    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        mOnItemClickListener = mItemClickListener;
    }

    /**
     * Set task count for nav drawer item and refresh the list
     *
     * @param mTaskCount
     */
    public void setmTaskCount(int mTaskCount) {
        this.mTaskCount = mTaskCount;
        notifyDataSetChanged();
    }

}
