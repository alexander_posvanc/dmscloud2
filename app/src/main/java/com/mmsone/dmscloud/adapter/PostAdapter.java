package com.mmsone.dmscloud.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PoolObjectFactory;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.ViewPoolManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.ShowMeta;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.DocumentFragment;
import com.mmsone.dmscloud.ui.MainActivity;
import com.mmsone.dmscloud.ui.helper.DMSCloudViewBuilder;
import com.mmsone.dmscloud.ui.helper.DMSCloudViewBuilder.ViewTag;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PostAdapter extends BaseAdapter {

	private List<Post> mPostList;
	private LayoutInflater mLayoutInflater;
	private MainActivity mActivity;
	private DisplayImageOptions mDisplayImageOptions;
	private OnWallUpdateListener mOnWallUpdateListener;
	private ViewPoolManager mViewPoolManager;

	public PostAdapter(MainActivity activity, List<Post> postList, OnWallUpdateListener listener) {
		/**
		 * Set wall item views pool
		 */
		mViewPoolManager = new ViewPoolManager(new PoolObjectFactory<EPostChildType, View>() {

			@Override
			public View create(EPostChildType key) {
				switch (key) {
				case PostChat:
					return View.inflate(mActivity, R.layout.list_item_post_chat, null);
				case PostComment:
					return View.inflate(mActivity, R.layout.list_item_post_comment, null);
				case PostEmail:
					return View.inflate(mActivity, R.layout.list_item_post_email, null);
				case PostSubComment:
					return View.inflate(mActivity, R.layout.list_item_post_sub_comment, null);
				case PostSubTask:
					return View.inflate(mActivity, R.layout.list_item_post_sub_task, null);
				case PostTask:
					return View.inflate(mActivity, R.layout.list_item_post_task, null);
				case PostWfTask:
					return View.inflate(mActivity, R.layout.list_item_post_wftask, null);
				}
				return null;
			}
		});

		mActivity = activity;
		mLayoutInflater = LayoutInflater.from(mActivity);
		mOnWallUpdateListener = listener;
		mPostList = postList;
		// Initialize display image option
		mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();
	}

	@Override
	public int getCount() {
		return mPostList.size();
	}

	@Override
	public Post getItem(int arg0) {
		return mPostList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return mPostList.get(arg0).id;
	}

	private static class ViewHolder {
		public ImageView profileIv;
		public TextView createdOnDateTv, createdOnTimeTv, topicIdTv, createdByTv, titleTv, messageTv, metaLayout1LabeTv, metaLayout1ValueTv, metaLayout2LabeTv, metaLayout2ValueTv;
		public LinearLayout metaLayoutContainer, docIconLayout, subLayout, iconLayout, metaLayout1, metaLayout2;
		public RelativeLayout subLayoutContiner;
	}

	@Override
	public View getView(int pos, View view, ViewGroup viewGroup) {
		final ViewHolder holder;
		if (view == null) {
			view = mLayoutInflater.inflate(R.layout.list_item_post, null);

			holder = new ViewHolder();

			holder.profileIv = (ImageView) view.findViewById(R.id.post_profileIv);
			holder.createdOnDateTv = (TextView) view.findViewById(R.id.post_createdOnDateTv);
			holder.createdOnTimeTv = (TextView) view.findViewById(R.id.post_createdOnTimeTv);
			holder.topicIdTv = (TextView) view.findViewById(R.id.post_idTv);
			holder.createdByTv = (TextView) view.findViewById(R.id.post_createdByTv);
			holder.titleTv = (TextView) view.findViewById(R.id.post_titleTv);
			holder.messageTv = (TextView) view.findViewById(R.id.post_messageTv);

			holder.docIconLayout = (LinearLayout) view.findViewById(R.id.post_docIconLL);
			holder.subLayout = (LinearLayout) view.findViewById(R.id.post_subLayout);
			holder.iconLayout = (LinearLayout) view.findViewById(R.id.post_iconLL);
			holder.subLayoutContiner = (RelativeLayout) view.findViewById(R.id.post_subLayoutContiner);

			holder.metaLayoutContainer = (LinearLayout) view.findViewById(R.id.post_metaLayoutContainer);
			holder.metaLayout1 = (LinearLayout) view.findViewById(R.id.post_metaLayout1);
			holder.metaLayout2 = (LinearLayout) view.findViewById(R.id.post_metaLayout2);
			holder.metaLayout1LabeTv = (TextView) view.findViewById(R.id.post_metaLayout1_labelTv);
			holder.metaLayout1ValueTv = (TextView) view.findViewById(R.id.post_metaLayout1_valueTv);
			holder.metaLayout2LabeTv = (TextView) view.findViewById(R.id.post_metaLayout2_labelTv);
			holder.metaLayout2ValueTv = (TextView) view.findViewById(R.id.post_metaLayout2_valueTv);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		final Post post = getItem(pos);

		for (int i = 0; i < holder.subLayout.getChildCount(); i++) {
			View subView = holder.subLayout.getChildAt(i);
			Object tag = subView.getTag();
			if (tag != null) {
				ViewTag viewTag = (ViewTag) tag;
				mViewPoolManager.returnView(viewTag.type, subView);
				if (viewTag.subSubHolder != null) {
					for (int j = 0; j < viewTag.subSubHolder.getChildCount(); j++) {
						View subsubView = viewTag.subSubHolder.getChildAt(j);
						mViewPoolManager.returnView((EPostChildType) subsubView.getTag(), subsubView);
					}
				}
			}
		}


		holder.createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(post.createdOn)));
		holder.createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		holder.createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(post.createdOn)));
		holder.createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		holder.topicIdTv.setText("#" + post.postNumber);
		holder.topicIdTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		holder.titleTv.setText(post.title);
		holder.titleTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());

		holder.messageTv.setText(post.message);
		holder.messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		// set meta views
		if (post.metaList.isEmpty()) {
			holder.metaLayoutContainer.setVisibility(View.GONE);
		} else {
			holder.metaLayoutContainer.setVisibility(View.VISIBLE);

			if (post.metaList.size() == 1) {
				holder.metaLayout1.setVisibility(View.VISIBLE);
				holder.metaLayout2.setVisibility(View.GONE);

				ShowMeta meta1 = post.metaList.get(0);

				holder.metaLayout1LabeTv.setText(meta1.label);
				holder.metaLayout1LabeTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
				holder.metaLayout1ValueTv.setText(meta1.value);
				holder.metaLayout1ValueTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
			} else {
				holder.metaLayout1.setVisibility(View.VISIBLE);
				holder.metaLayout2.setVisibility(View.VISIBLE);
				ShowMeta meta1 = post.metaList.get(0);

				holder.metaLayout1LabeTv.setText(meta1.label);
				holder.metaLayout1LabeTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
				holder.metaLayout1ValueTv.setText(meta1.value);
				holder.metaLayout1ValueTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				ShowMeta meta2 = post.metaList.get(1);

				holder.metaLayout2LabeTv.setText(meta2.label);
				holder.metaLayout2LabeTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
				holder.metaLayout2ValueTv.setText(meta2.value);
				holder.metaLayout2ValueTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
			}
		}

		// Build topic child view hierarchy
		if (post.childList.isEmpty()) {
			holder.subLayoutContiner.setVisibility(View.GONE);
		} else {
			holder.subLayoutContiner.setVisibility(View.VISIBLE);
			List<IPostChild> childList = new ArrayList<IPostChild>();

			IPostChild child = post.childList.get(0);
			childList.add(child);

			if (child.getChildList() != null && !child.getChildList().isEmpty()) {
				childList.add(child.getChildList().get(0));
				DMSCloudViewBuilder.addPostTaskViews(mActivity, mDisplayImageOptions, holder.subLayout, childList, false, null, mActivity, mOnWallUpdateListener, true, mViewPoolManager);
			} else if (post.childList.size() > 1) {
				childList.add(post.childList.get(1));
				DMSCloudViewBuilder.addPostTaskViews(mActivity, mDisplayImageOptions, holder.subLayout, childList, false, null, mActivity, mOnWallUpdateListener, false, mViewPoolManager);
			} else {
				DMSCloudViewBuilder.addPostTaskViews(mActivity, mDisplayImageOptions, holder.subLayout, childList, false, null, mActivity, mOnWallUpdateListener, false, mViewPoolManager);
			}
		}

		// topic document click event handling
		if (post.documentId != 0) {
			holder.docIconLayout.setVisibility(View.VISIBLE);
			holder.docIconLayout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Fragment fragment = new DocumentFragment();
					Bundle args = new Bundle();
					args.putLong("documentId", post.documentId);
					fragment.setArguments(args);
					mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_content_frameLayout, fragment).addToBackStack(null).commit();
				}
			});
			DMSCloudViewBuilder.setMenuTouchDelegateArea(view, holder.docIconLayout);
		} else {
			holder.docIconLayout.setVisibility(View.GONE);
		}

		// set topic icon state
		if (post.unreaded) {
			holder.iconLayout.setBackgroundResource(R.drawable.ic_createtopic_orange);
		} else {
			holder.iconLayout.setBackgroundResource(R.drawable.ic_createtopic);
		}
		
		switch (post.postType) {
		case EMAIL:
			holder.profileIv.setImageResource(R.drawable.email_kek);
			break;
		case MOBILE:
			holder.profileIv.setImageResource(R.drawable.mobil_kek);
			break;
		case SCAN_FILE:
			holder.profileIv.setImageResource(R.drawable.scanner_kek);
			break;
		case WEB_SERVICE:
			holder.profileIv.setImageResource(R.drawable.webservice_kek);
			break;
		default:
			// set user profile image and name
			if (post.user != null && post.user.image != null) {
				ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + post.user.image, holder.profileIv, mDisplayImageOptions);
				holder.createdByTv.setText(post.user.formatedName + " (" + post.overAllCount + ")");
				holder.createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
			}
			break;
		}

		return view;
	}
}