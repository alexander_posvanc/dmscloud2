package com.mmsone.dmscloud.adapter;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class DownloadFileAdapter  extends BaseAdapter {

	private List<File> mFileList;
	private LayoutInflater mLayoutInflater;
	private DisplayImageOptions mDisplayImageOptions;
	private ImageSize mImageSize;

	/**
	 * Download File adapter constructor
	 * 
	 * @param context
	 * @param fileList
	 */
	public DownloadFileAdapter(Context context, List<File> fileList) {
		mLayoutInflater = LayoutInflater.from(context);
		mFileList = fileList;
		//Set image width and height
		mImageSize = new ImageSize(100, 100);
		//Initialize display image option
		mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_file_up_menu_bt_white).showImageOnFail(R.drawable.ic_file_up_menu_bt_white).cacheInMemory(true).cacheOnDisk(true).build();
	}

	@Override
	public int getCount() {
		return mFileList.size();
	}

	@Override
	public File getItem(int arg0) {
		return mFileList.get(arg0);
	}

	public void removeItem(File file) {
		mFileList.remove(file);
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	private static class ViewHolder {
		public TextView textTv;
		public ImageView iconIv;
	}

	@Override
	public View getView(int pos, View view, ViewGroup viewGroup) {
		final ViewHolder holder;
		if (view == null) {
			view = mLayoutInflater.inflate(R.layout.list_item_download_file, null);

			holder = new ViewHolder();
			holder.textTv = (TextView) view.findViewById(R.id.file_upload_fileNameTv);
			holder.iconIv = (ImageView) view.findViewById(R.id.file_upload_imageIv);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		final File file = getItem(pos);

		holder.textTv.setText(file.getName());
		holder.textTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		//Load image from url in background
		ImageLoader.getInstance().loadImage(Uri.decode(Uri.fromFile(file).toString()), mImageSize, mDisplayImageOptions, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.iconIv.setImageBitmap(loadedImage);
			}
		});


		return view;
	}

}
