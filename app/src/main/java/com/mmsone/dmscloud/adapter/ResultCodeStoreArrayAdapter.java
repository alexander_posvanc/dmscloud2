package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.bean.CodeStore;

/**
 * Created by Alexander on 2015. 09. 30..
 */
public class ResultCodeStoreArrayAdapter extends ArrayAdapter<CodeStore> {

    private CodeStore[] mCodeStoreList;
    private Context mContext;

    /**
     * ResultCodeStoreArrayAdapter constructor
     *
     * @param context
     * @param txtViewResourceId
     * @param objects
     */
    public ResultCodeStoreArrayAdapter(Context context, int txtViewResourceId, CodeStore[] objects) {
        super(context, txtViewResourceId, objects);
        mContext = context;
        mCodeStoreList = objects;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt, true);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt, false);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDownView) {
        View view = View.inflate(mContext, R.layout.spinner_dropdown_item, null);
        TextView textView = (TextView) view.findViewById(R.id.spinner_dropdown_Tv);

        //set view margins
        if (isDropDownView) {
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(8, 8, 8, 8);
//            textView.setLayoutParams(layoutParams);
        }

        CodeStore codeStore = mCodeStoreList[position];
        if ((codeStore == null) && (position == 0)) {
            textView.setText("");
        } else {
            textView.setText(codeStore.toString());
        }

        return view;
    }
}