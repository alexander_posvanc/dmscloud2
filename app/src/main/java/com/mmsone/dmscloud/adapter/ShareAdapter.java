package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.bean.Distribution;
import com.mmsone.dmscloud.bo.bean.Group;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 26..
 */
public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.ShareViewHolder> {

    private List<IUser> mUserList;
    private DisplayImageOptions mDisplayImageOptions;
    private Post mPost;
    private Context mContext;


    /**
     * Constructor of ShareAdapter
     *
     * @param userList List of Users, Distributions and Groups
     * @param post
     * @param context
     */
    public ShareAdapter(List<IUser> userList, Post post, Context context) {
        mUserList = userList;
        mContext = context;
        mPost = post;
        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();

    }

    @Override
    public ShareViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_share, parent, false);
        ShareViewHolder vhItem = new ShareViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(ShareViewHolder holder, final int position) {
        //Sharing of users
        if (mUserList.get(position).getType().equals(IUser.EUserType.USER)) {
            final User user = (User) mUserList.get(position);
            holder.nameTv.setText(user.getName());
            ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + user.image, holder.profileIv, mDisplayImageOptions);
            holder.shareButton.setTransformationMethod(null);
            holder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Long[] userIdList = new Long[]{user.getId()};
                    new SharePostTaskTask(mPost.id, new String(), userIdList, new Long[]{}, new Long[]{}, position).execute();
                    notifyDataSetChanged();
                }
            });
            //Sharing of Distributions
        } else if (mUserList.get(position).getType().equals(IUser.EUserType.DISTRIBUTION)) {
            final Distribution distribution = (Distribution) mUserList.get(position);
            holder.nameTv.setText(distribution.getName());
//            ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + distribution., holder.profileIv, mDisplayImageOptions);
            holder.profileIv.setImageResource(R.drawable.ic_launcher);
            holder.shareButton.setTransformationMethod(null);
            holder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Long[] userIdList = new Long[]{distribution.getId()};
                    new SharePostTaskTask(mPost.id, new String(), new Long[]{}, new Long[]{}, userIdList, position).execute();
                    notifyDataSetChanged();

                }
            });
            //Sharing of Groups
        } else if (mUserList.get(position).getType().equals(IUser.EUserType.GROUP)) {
            final Group group = (Group) mUserList.get(position);
            holder.nameTv.setText(group.getName());
            holder.profileIv.setImageResource(R.drawable.ic_launcher);
            holder.shareButton.setTransformationMethod(null);
            holder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Long[] userIdList = new Long[]{group.getId()};
                    new SharePostTaskTask(mPost.id, new String(), new Long[]{}, userIdList, new Long[]{}, position).execute();
                    notifyDataSetChanged();

                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class ShareViewHolder extends RecyclerView.ViewHolder {
        ImageView profileIv;
        TextView nameTv;
        Button shareButton;

        public ShareViewHolder(View itemView) {
            super(itemView);

            profileIv = (ImageView) itemView.findViewById(R.id.share_item_profileIv);
            nameTv = (TextView) itemView.findViewById(R.id.share_item_nameTv);
            shareButton = (Button) itemView.findViewById(R.id.share_item_button);

        }
    }

    /**
     * Updates RecyclerView with new userList
     *
     * @param userList List of Users, Distributions and Groups
     */
    public void updateList(List<IUser> userList) {
        mUserList = userList;
        notifyDataSetChanged();
    }

    /**
     * Share post in background to users
     */
    public class SharePostTaskTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mPostId;
        private String mMessage;
        private Long[] mShareUserList;
        private Long[] mGroupList;
        private Long[] mDistributionList;
        private int mPosition;

        public SharePostTaskTask(long postId, String message, Long[] userList, Long[] groupList, Long[] distributionList, int position) {
            mPostId = postId;
            mMessage = message;
            mShareUserList = userList;
            mGroupList = groupList;
            mDistributionList = distributionList;
            mPosition = position;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().sharePost(mPostId, mMessage, mShareUserList, mGroupList, mDistributionList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setWallUpToDate(false);
                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(mContext, R.string.task_share_succes_message, Toast.LENGTH_SHORT).show();
                mUserList.remove(mPosition);
                notifyItemRemoved(mPosition);
                notifyItemRangeChanged(mPosition, mUserList.size());

            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(mContext, ex);
        }
    }
}