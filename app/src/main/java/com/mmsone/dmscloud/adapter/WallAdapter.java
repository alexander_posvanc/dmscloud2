package com.mmsone.dmscloud.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.MetaManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.bo.bean.PostSubTask;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.ex.ValidationException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.PostActivity;
import com.mmsone.dmscloud.ui.SharePostActivity;
import com.mmsone.dmscloud.ui.TaskExecutionActivity;
import com.mmsone.dmscloud.ui.WallFragment;
import com.mmsone.dmscloud.ui.WfTaskExecutionActivity;
import com.mmsone.dmscloud.ui.helper.DMSHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Manages Wall Items
 * Created by Alexander on 2015. 08. 06..
 */
public class WallAdapter extends RecyclerView.Adapter<WallAdapter.PostViewHolder> {

    private List<Post> mPostList;
    private DisplayImageOptions mDisplayImageOptions;
    private Context mContext;
    private WallFragment mWallFragment;
    private static final int DEFAULT_ITEM = 0;
    private static final int TECHNICAL_ITEM = 1;

    /**
     * Constructor for WallAdapter
     *
     * @param postList
     * @param context
     */
    public WallAdapter(List<Post> postList, Context context, WallFragment wallFragment) {
        mPostList = postList;
        mContext = context;
        mWallFragment = wallFragment;
        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();

    }

    /**
     * Creates ViewHolders according to viewType
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public WallAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == DEFAULT_ITEM) {
            View v = null;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_post_new, parent, false);
            PostViewHolder vhItem = new PostViewHolder(v, DEFAULT_ITEM);
            return vhItem;
        } else if (viewType == TECHNICAL_ITEM) {
            View v = null;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_post_technical, parent, false);
            PostViewHolder vhItem = new PostViewHolder(v, TECHNICAL_ITEM);
            return vhItem;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, final int position) {
        final WallAdapter.PostViewHolder postholder = holder;
        switch (postholder.holderId) {
            //Default Wall item for Post
            case DEFAULT_ITEM:
                if (mPostList != null && !mPostList.isEmpty()) {
                    //Set texts, fonts
                    postholder.isDocumentLayout.setVisibility(View.GONE);
                    postholder.postDate.setText(mPostList.get(position).formatedCreatedOn);
                    postholder.postTitle.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.postTitle.setText(mPostList.get(position).title + " " + "(" + mContext.getResources().getString(R.string.wall_adapter_id) + mPostList.get(position).postNumber + ")");
                    postholder.profileName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.profileName.setText(mPostList.get(position).user.formatedName);
                    postholder.tabTextTask.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.tabTextMessage.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.tabMessageCount.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.tabTaskCount.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.descriptionTitle.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.childMessageName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.childComment.setTypeface(FontManager.getInstance().getRobotoLightTtf());
                    postholder.taskName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.postTask.setTypeface(FontManager.getInstance().getRobotoLightTtf());
                    postholder.moreText.setText(mContext.getResources().getString(R.string.wall_adapter_more_show));
                    postholder.taskBtn.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.taskBtn.setTransformationMethod(null);
                    postholder.taskBtn.setVisibility(View.GONE);
                    postholder.subTaskBtn.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                    postholder.subTaskBtn.setTransformationMethod(null);
                    postholder.subTaskBtn.setVisibility(View.GONE);
                    postholder.postMessage.setTextColor(Color.parseColor("#595959"));

                    //Set profile picture of user
                    ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + mPostList.get(position).user.image, postholder.profilePicture, mDisplayImageOptions);

                    //Hide child views
                    postholder.childCommentLayout.setVisibility(View.GONE);
                    postholder.subTaskDueDateLayout.setVisibility(View.GONE);
                    postholder.messageTab.setClickable(false);
                    postholder.taskTab.setClickable(false);

                    //Show document if needed
                    setUpDocumentLayout(postholder.isDocumentLayout, mPostList.get(position).documentId);

                    //Set post marked(Red background for flag) if marked boolean is true
                    if (mPostList.get(position).marked) {
                        postholder.markedLayout.setBackgroundColor(mContext.getResources().getColor(R.color.color_marked));
                    } else {
                        postholder.markedLayout.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                    }

                    //Hide Tabs when childlist empty and make description visible for post
                    showDescriptionIfNoChild(postholder, mPostList.get(position));

                    // Make List of PostComments and display their count
                    final List<IPostChild> postChildrenComments = getCommentList(mPostList.get(position).childList);
                    postholder.messageCount.setText("(" + mPostList.get(position).commentCount + ")");

                    // Make List of PostTask or PostWfTask and display their count
                    final List<IPostChild> postChildrenTasks = getTaskList(mPostList.get(position).childList);
                    postholder.taskCount.setText("(" + mPostList.get(position).taskCount + ")");

                    //Task Tabs layout
                    setUpTaskLayout(postholder, postChildrenTasks);
                    //Comment Tabs layout
                    setUpCommentLayout(postholder, postChildrenComments);

                    //Hide/Show childLayouts
                    postholder.moreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setUpCommentSubLayout(postholder, postChildrenComments);
                        }
                    });
                    postholder.moreButtonTask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setUpTaskSubLayout(postholder, postChildrenTasks);
                        }
                    });

                    //morebutton visibility
                    postholder.moreButton.setVisibility(View.VISIBLE);
                    postholder.moreButtonTask.setVisibility(View.VISIBLE);

                    if (postChildrenComments != null && !postChildrenComments.isEmpty()) {
                        if (postChildrenComments.get(0).getChildList() == null || postChildrenComments.get(0).getChildList().isEmpty()) {
                            postholder.moreButton.setVisibility(View.GONE);
                        }
                    }
                    if (postChildrenTasks != null && !postChildrenTasks.isEmpty()) {
                        if (postChildrenTasks.get(0).getChildList() == null || postChildrenTasks.get(0).getChildList().isEmpty()) {
                            postholder.moreButtonTask.setVisibility(View.GONE);
                        }
                    }

                    //Sets up tab layout and actions for them
                    setUpTabLayout(postholder, mPostList.get(position), postChildrenTasks, postChildrenComments);

                    //Post click to PostDetail
                    postholder.profileButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, PostActivity.class);
                            intent.putExtra("post", mPostList.get(position));
                            intent.putExtra("type", 1);
                            mContext.startActivity(intent);
                        }
                    });
                }
                break;
            //Technical Post item
            case TECHNICAL_ITEM:
                final PostComment postComment;
                if (mPostList.get(position).childList.get(0).getType().equals(EPostChildType.PostComment)) {
                    postComment = (PostComment) mPostList.get(position).childList.get(0);
                } else {
                    postComment = null;
                }
                //Set texts and fonts
                postholder.fileName.setText("");
                postholder.postTitle.setText(mPostList.get(position).title);
                postholder.postTitle.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                postholder.profileName.setText(mPostList.get(position).user.formatedName);
                postholder.profileName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                postholder.postDate.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(mPostList.get(position).createdOn)));

                if (postComment != null) {
                    if (postComment.technicalFile != null && postComment.technicalFile != "" && postComment.technicalFile != "null") {
                        postholder.fileName.setText(postComment.message);
                    } else {
                        postholder.fileName.setText(mContext.getResources().getString(R.string.wall_adapter_open_topic));
                    }
                }
                postholder.fileName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + mPostList.get(position).user.image, postholder.profilePicture, mDisplayImageOptions);

                //Set icon for type of Technical Item
                switch (mPostList.get(position).postType) {
                    case MOBILE:
                        postholder.technicalImage.setImageResource(R.drawable.mobil);
                        break;
                    case SCAN_FILE:
                        postholder.technicalImage.setImageResource(R.drawable.scanner);
                        break;
                    default:
                        postholder.technicalImage.setImageResource(R.drawable.sokfile);
                }

                //Post click to PostDetail
                postholder.profileButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, PostActivity.class);
                        intent.putExtra("post", mPostList.get(position));
                        intent.putExtra("type", 2);
                        mContext.startActivity(intent);
                    }
                });


                //download file on click
                if (postComment != null) {
                    if (postComment.technicalFile != null && postComment.technicalFile != "" && postComment.technicalFile != "null") {
                        postholder.download.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startDownloadFile(postComment);
                            }
                        });
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mPostList.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (mPostList.get(position).postType) {
            case NORMAL:
                return DEFAULT_ITEM;
            case MOBILE:
                return TECHNICAL_ITEM;
            case SCAN_FILE:
                return TECHNICAL_ITEM;
            case EMAIL:
                return TECHNICAL_ITEM;
            case WEB_SERVICE:
                return TECHNICAL_ITEM;
        }
        return DEFAULT_ITEM;
    }

    /**
     * ViewHolder for Wall Items
     */
    public class PostViewHolder extends RecyclerView.ViewHolder {
        TextView postTitle, profileName, postDate, postMessage, postTask, messageCount, taskCount, idText,
                messageOwner, messageTime, childComment, fileName, postDescription, tabTextMessage, tabTextTask, moreText, moreTextTask,
                tabTaskCount, tabMessageCount, descriptionTitle, childMessageName, childMessageTime, taskTime, taskName, taskDueDateTv, subTaskDueDateTv;
        ImageView profilePicture, download, markedIcon, technicalImage, taskRecipient, subTaskIv, isDocumentLayout;
        RelativeLayout messageTab, taskTab, moreButton, moreButtonTask, childListLayout, descriptionLayout, markedLayout;
        LinearLayout messageLayout, taskLayout, childCommentLayout, profileButton, tabLayout, tabDivider, subTaskDueDateLayout;
        Button taskBtn, subTaskBtn;
        int holderId;

        public PostViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == DEFAULT_ITEM) {
                holderId = DEFAULT_ITEM;
                postTask = (TextView) itemView.findViewById(R.id.post_card_task_details_Tv);
                postTitle = (TextView) itemView.findViewById(R.id.post_card_item_title_Tv);
                profileName = (TextView) itemView.findViewById(R.id.post_card_item_profileName_Tv);
                postDate = (TextView) itemView.findViewById(R.id.post_card_item_post_date_Tv);
                postMessage = (TextView) itemView.findViewById(R.id.post_card_message_details_Tv);
                profilePicture = (ImageView) itemView.findViewById(R.id.post_card_item_profile_Iv);
                messageTab = (RelativeLayout) itemView.findViewById(R.id.post_card_item_comment);
                taskTab = (RelativeLayout) itemView.findViewById(R.id.post_card_item_task);
                messageLayout = (LinearLayout) itemView.findViewById(R.id.post_card_message_details);
                taskLayout = (LinearLayout) itemView.findViewById(R.id.post_card_task_details);
                messageCount = (TextView) itemView.findViewById(R.id.tab_comment_count);
                taskCount = (TextView) itemView.findViewById(R.id.tab_task_count);
//                idText = (TextView) itemView.findViewById(R.id.post_card_item_id_Tv);
                profileButton = (LinearLayout) itemView.findViewById(R.id.post_card_item_upper);
                messageOwner = (TextView) itemView.findViewById(R.id.post_card_message_details_name);
                messageTime = (TextView) itemView.findViewById(R.id.post_card_message_details_time);
                moreButton = (RelativeLayout) itemView.findViewById(R.id.post_card_item_lower);
                moreButtonTask = (RelativeLayout) itemView.findViewById(R.id.post_card_item_lower_task);
                childCommentLayout = (LinearLayout) itemView.findViewById(R.id.child_comment_layout);
                childComment = (TextView) itemView.findViewById(R.id.child_comment);
                childListLayout = (RelativeLayout) itemView.findViewById(R.id.post_card_item_childList_layout);
                descriptionLayout = (RelativeLayout) itemView.findViewById(R.id.post_card_item_descriptionLayout);
                postDescription = (TextView) itemView.findViewById(R.id.post_card_item_descriptionTv);
                tabLayout = (LinearLayout) itemView.findViewById(R.id.post_card_item_tabLayout);
                tabDivider = (LinearLayout) itemView.findViewById(R.id.post_card_item_divider);
                markedLayout = (RelativeLayout) itemView.findViewById(R.id.list_item_post_markedLayout);
                markedIcon = (ImageView) itemView.findViewById(R.id.list_item_post_markedIcon);
                tabTextMessage = (TextView) itemView.findViewById(R.id.tab_text_comment);
                tabTextTask = (TextView) itemView.findViewById(R.id.tab_text_task);
                tabTaskCount = (TextView) itemView.findViewById(R.id.tab_task_count);
                tabMessageCount = (TextView) itemView.findViewById(R.id.tab_comment_count);
                descriptionTitle = (TextView) itemView.findViewById(R.id.post_card_item_descriptionTitleTv);
                childMessageName = (TextView) itemView.findViewById(R.id.message_detail_name);
                childMessageTime = (TextView) itemView.findViewById(R.id.message_detail_time);
                taskTime = (TextView) itemView.findViewById(R.id.task_detail_time);
                taskName = (TextView) itemView.findViewById(R.id.task_detail_name);
                taskDueDateTv = (TextView) itemView.findViewById(R.id.post_card_task_dueDateTv);
                taskRecipient = (ImageView) itemView.findViewById(R.id.post_detail_taskRecipientIv);
                subTaskIv = (ImageView) itemView.findViewById(R.id.subchild_task_Iv);
                subTaskDueDateLayout = (LinearLayout) itemView.findViewById(R.id.subtask_dueDate_layout);
                subTaskDueDateTv = (TextView) itemView.findViewById(R.id.subchild_task_dueDateTv);
                moreText = (TextView) itemView.findViewById(R.id.more_textview);
                moreTextTask = (TextView) itemView.findViewById(R.id.more_textview_task);
                isDocumentLayout = (ImageView) itemView.findViewById(R.id.isDocument_layout);
                taskBtn = (Button) itemView.findViewById(R.id.post_card_taskBtn);
                subTaskBtn = (Button) itemView.findViewById(R.id.post_card_sub_taskBtn);
            } else if (viewType == TECHNICAL_ITEM) {
                holderId = TECHNICAL_ITEM;
                profileButton = (LinearLayout) itemView.findViewById(R.id.post_card_item_upper);
                postTitle = (TextView) itemView.findViewById(R.id.post_card_item_title_Tv);
                profileName = (TextView) itemView.findViewById(R.id.post_card_item_profileName_Tv);
                postDate = (TextView) itemView.findViewById(R.id.post_card_item_post_date_Tv);
                profilePicture = (ImageView) itemView.findViewById(R.id.post_card_item_profile_Iv);
                download = (ImageView) itemView.findViewById(R.id.download_imageView);
                fileName = (TextView) itemView.findViewById(R.id.technical_file_name);
                technicalImage = (ImageView) itemView.findViewById(R.id.technical_file_image);
            }
        }
    }

    /**
     * Sets up tabLayouts and click actions for them
     *
     * @param postholder
     * @param post
     * @param postChildrenTasks
     * @param postChildrenComments
     */
    public void setUpTabLayout(final PostViewHolder postholder, Post post, final List<IPostChild> postChildrenTasks, List<IPostChild> postChildrenComments) {
        //Tabs at start
        if (post.childList != null && !post.childList.isEmpty()) {
            if (post.childList.get(0).getType().equals(EPostChildType.PostComment)) {
                postholder.taskLayout.setVisibility(View.GONE);
                postholder.messageLayout.setVisibility(View.VISIBLE);
                postholder.messageTab.setBackgroundColor(Color.parseColor("#FFFFFF"));
                postholder.taskTab.setBackgroundColor(Color.parseColor("#F8F8F8"));
            } else if (post.childList.get(0).getType().equals(EPostChildType.PostTask)) {
                showSubLayoutAutomatically(postChildrenTasks, postholder.moreButtonTask);
                postholder.messageLayout.setVisibility(View.GONE);
                postholder.taskLayout.setVisibility(View.VISIBLE);
                postholder.messageTab.setBackgroundColor(Color.parseColor("#F8F8F8"));
                postholder.taskTab.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        //message tab
        if (postChildrenComments != null && !postChildrenComments.isEmpty()) {
            postholder.messageTab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postholder.taskLayout.setVisibility(View.GONE);
                    postholder.messageLayout.setVisibility(View.VISIBLE);
                    postholder.childCommentLayout.setVisibility(View.GONE);
                    postholder.messageTab.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    postholder.taskTab.setBackgroundColor(Color.parseColor("#F8F8F8"));
                }
            });
        } else {
            postholder.messageLayout.setVisibility(View.GONE);
            postholder.taskLayout.setVisibility(View.VISIBLE);
            postholder.messageTab.setBackgroundColor(Color.parseColor("#F8F8F8"));
            postholder.taskTab.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        //task tab
        if (postChildrenTasks != null && !postChildrenTasks.isEmpty()) {
            postholder.taskTab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postholder.messageLayout.setVisibility(View.GONE);
                    postholder.taskLayout.setVisibility(View.VISIBLE);
                    postholder.childCommentLayout.setVisibility(View.GONE);
                    showSubLayoutAutomatically(postChildrenTasks, postholder.moreButtonTask);
                    postholder.messageTab.setBackgroundColor(Color.parseColor("#F8F8F8"));
                    postholder.taskTab.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
            });
        }
    }

    /**
     * Checks if there is child in the post, if there is no child it shows description layout
     *
     * @param postholder
     * @param post
     */
    public void showDescriptionIfNoChild(PostViewHolder postholder, Post post) {
        //Hide Tabs when childlist empty and make description visible for post
        if (post.childList == null || post.childList.isEmpty()) {
            postholder.childListLayout.setVisibility(View.GONE);
            postholder.descriptionLayout.setVisibility(View.VISIBLE);
            postholder.tabLayout.setVisibility(View.GONE);
            postholder.tabDivider.setVisibility(View.GONE);
            postholder.postDescription.setText(post.message);
        } else {
            postholder.childListLayout.setVisibility(View.VISIBLE);
            postholder.descriptionLayout.setVisibility(View.GONE);
            postholder.tabLayout.setVisibility(View.VISIBLE);
            postholder.tabDivider.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Set up Comment layout for PostItem
     *
     * @param postholder
     * @param postChildrenComments
     */
    public void setUpCommentLayout(PostViewHolder postholder, List<IPostChild> postChildrenComments) {
        //Set layout for Comments in PostItem.
        //Set up texts fonts and icons for Comments
        if (postChildrenComments != null && postChildrenComments.size() != 0) {
            final PostComment postComment = (PostComment) postChildrenComments.get(0);
            postholder.postMessage.setText("" + postComment.message);
            postholder.postMessage.getTextColors().getDefaultColor();
            postholder.postMessage.setTypeface(FontManager.getInstance().getRobotoLightTtf());
            postholder.messageOwner.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            postholder.messageOwner.setText("" + postComment.user);
            postholder.messageTime.setText("" + DMSHelper.longToTime(mContext, System.currentTimeMillis() - postComment.createdOn));
            //Download file from wall
            if (postComment.technicalFile != null && postComment.technicalFile != "" && postComment.technicalFile != "null") {
                postholder.postMessage.setTextColor(mContext.getResources().getColor(R.color.color_primary_actionbar));
                postholder.postMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startDownloadFile(postComment);
                    }
                });
            }

            if (postComment.getChildList().size() > 0 && postComment.getChildList().get(0).getType().equals(EPostChildType.PostComment)) {
                PostComment postComment1 = (PostComment) postComment.getChildList().get(0);
                postholder.childComment.setText(postComment1.message);
            }
        } else {
            postholder.messageTab.setClickable(false);
            postholder.postMessage.setText(mContext.getResources().getString(R.string.wall_adapter_no_message));
        }

    }

    /**
     * Sets up Task layout for PostItem
     *
     * @param postholder
     * @param postChildrenTasks
     */
    public void setUpTaskLayout(PostViewHolder postholder, List<IPostChild> postChildrenTasks) {
        //Set layout for Taks in PostItem.
        //Set up texts fonts and icons for Tasks and WfTasks
        if (postChildrenTasks != null && postChildrenTasks.size() != 0) {
            if (postChildrenTasks.get(0).getType().equals(EPostChildType.PostTask)) {
                final PostTask postTask = (PostTask) postChildrenTasks.get(0);
                postholder.postTask.setText("" + postTask.message);
                postholder.taskName.setText(postTask.user.formatedName);
                postholder.taskTime.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postTask.createdOn));
                postholder.taskDueDateTv.setText(mContext.getResources().getString(R.string.due_date) + postTask.formatedDueDate);
                if (postTask.recipient) {
                    postholder.taskBtn.setVisibility(View.VISIBLE);
                    postholder.taskRecipient.setImageResource(R.drawable.sajatfeladataim_lila);
                    //Button click execution
                    postholder.taskBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createExecutionDialog(postTask.id, postTask.status, null);
                        }
                    });

                } else {
                    postholder.taskRecipient.setImageResource(R.drawable.kiosztottfeladataim_narancs);
                }
            } else if (postChildrenTasks.get(0).getType().equals(EPostChildType.PostWfTask)) {
                final PostWfTask postTask = (PostWfTask) postChildrenTasks.get(0);
                postholder.postTask.setText(postTask.title + "  " + postTask.message);
                postholder.postTask.setTypeface(FontManager.getInstance().getRobotoLightTtf());
                postholder.taskName.setText(postTask.user.formatedName);
                postholder.taskTime.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postTask.createdOn));
                postholder.taskDueDateTv.setText(mContext.getResources().getString(R.string.due_date) + postTask.formatedDueDate);
                if (postTask.recipient) {
                    if (postTask.status == WorkflowStatus.ACCEPTED || postTask.status == WorkflowStatus.CREATED) {
                        postholder.taskBtn.setVisibility(View.VISIBLE);

                        //Button execution WFTAsk
                        postholder.taskBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                createExecutionDialog(postTask.id, postTask.status, postTask);
                            }
                        });

                    }
                    postholder.taskRecipient.setImageResource(R.drawable.sajatfeladataim_lila);
                } else {
                    postholder.taskRecipient.setImageResource(R.drawable.kiosztottfeladataim_narancs);
                }
            }
        } else {
            postholder.messageTab.setClickable(false);
            postholder.postTask.setText(mContext.getResources().getString(R.string.wall_adapter_no_task));
        }
    }

    /**
     * Sets up sublayout for Comment tab
     *
     * @param postholder
     * @param postChildrenComments
     */
    public void setUpCommentSubLayout(PostViewHolder postholder, List<IPostChild> postChildrenComments) {
        if (postholder.childCommentLayout.getVisibility() == View.GONE) {
            postholder.moreText.setText(mContext.getResources().getString(R.string.wall_adapter_more_hide));
            //childcomment
            if (postChildrenComments != null && !postChildrenComments.isEmpty()) {
                if (postChildrenComments.get(0).getChildList() != null && !postChildrenComments.get(0).getChildList().isEmpty()) {
                    PostSubComment postSubComment = (PostSubComment) postChildrenComments.get(0).getChildList().get(0);
                    postholder.childCommentLayout.setVisibility(View.VISIBLE);
                    postholder.childComment.setText(postSubComment.message);
                    postholder.subTaskDueDateLayout.setVisibility(View.GONE);
                    postholder.subTaskIv.setImageResource(R.drawable.ic_star);
                    postholder.subTaskIv.setVisibility(View.INVISIBLE);
                    postholder.childMessageName.setText(postSubComment.user.formatedName);
                    postholder.childMessageTime.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubComment.createdOn));
                }
            }
        } else {
            postholder.moreText.setText(mContext.getResources().getString(R.string.wall_adapter_more_show));
            postholder.childCommentLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Sets up sublayout for task tab, either Subtask or SubComment
     *
     * @param postholder
     * @param postChildrenTasks
     */
    public void setUpTaskSubLayout(PostViewHolder postholder, List<IPostChild> postChildrenTasks) {
        if (postholder.childCommentLayout.getVisibility() == View.GONE) {
            postholder.childCommentLayout.setVisibility(View.VISIBLE);
            postholder.moreTextTask.setText(mContext.getResources().getString(R.string.wall_adapter_more_hide));
            //child Subcomment and SubTask
            if (postChildrenTasks != null && !postChildrenTasks.isEmpty()) {
                if (postChildrenTasks.get(0).getChildList() != null && !postChildrenTasks.get(0).getChildList().isEmpty()) {
                    if (postChildrenTasks.get(0).getChildList().get(0).getType().equals(EPostChildType.PostSubTask)) {
                        final PostSubTask postSubTask = (PostSubTask) postChildrenTasks.get(0).getChildList().get(0);
                        postholder.moreTextTask.setText(mContext.getResources().getString(R.string.wall_adapter_task_more_hide));
                        postholder.childComment.setText(TaskStatus.getTaskStatusBy(postSubTask.status));
                        postholder.childComment.setTextColor(mContext.getResources().getColor(TaskStatus.getStatusColor(postSubTask.status)));
                        postholder.childComment.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                        postholder.childMessageName.setText(postSubTask.user.formatedName);
                        postholder.childMessageTime.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubTask.createdOn));
                        postholder.subTaskIv.setVisibility(View.VISIBLE);
                        postholder.subTaskDueDateLayout.setVisibility(View.VISIBLE);
                        postholder.subTaskDueDateTv.setText(postSubTask.formatedDueDate);
                        postholder.subTaskIv.setImageResource(TaskStatus.getStatusIcon(postSubTask.status));
                        //Execution button visibility
                        switch (postSubTask.status) {
                            case TaskStatus.TASK_CLOSED:
                                postholder.subTaskBtn.setVisibility(View.GONE);
                                break;
                            case TaskStatus.TASK_REJECTED:
                                postholder.subTaskBtn.setVisibility(View.GONE);
                                break;
                            default:
                                if (postSubTask.recipient) {
                                    postholder.subTaskBtn.setVisibility(View.VISIBLE);
                                } else {
                                    postholder.subTaskBtn.setVisibility(View.GONE);
                                }
                                break;
                        }
                        //SubTask Button Execution
                        postholder.subTaskBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                createExecutionDialog(postSubTask.id, postSubTask.status, null);
                            }
                        });

                    } else if (postChildrenTasks.get(0).getChildList().get(0).getType().equals(EPostChildType.PostSubComment)) {
                        PostSubComment postSubComment = (PostSubComment) postChildrenTasks.get(0).getChildList().get(0);
                        postholder.moreTextTask.setText(mContext.getResources().getString(R.string.wall_adapter_more_hide));
                        postholder.subTaskDueDateLayout.setVisibility(View.GONE);
                        postholder.subTaskIv.setVisibility(View.GONE);
                        postholder.childComment.setText(postSubComment.message);
                        postholder.childMessageName.setText(postSubComment.user.formatedName);
                        postholder.childMessageTime.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postSubComment.createdOn));
                    }
                }
            }
        } else {
            if (postChildrenTasks.get(0).getChildList().get(0).getType().equals(EPostChildType.PostSubTask)) {
                postholder.moreTextTask.setText(mContext.getResources().getString(R.string.wall_adapter_task_more_show));
            } else if (postChildrenTasks.get(0).getChildList().get(0).getType().equals(EPostChildType.PostSubComment)) {
                postholder.moreTextTask.setText(mContext.getResources().getString(R.string.wall_adapter_more_show));
            }
            postholder.childCommentLayout.setVisibility(View.GONE);
        }
    }


    /**
     * Starts Download of file
     *
     * @param postComment
     */
    public void startDownloadFile(PostComment postComment) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + postComment.technicalFile));

        String url = DMSHelper.getExtension(postComment.message);
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            request.setMimeType("application/msword");
        } else if (url.toString().contains(".pdf")) {
            request.setMimeType("application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            request.setMimeType("application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            request.setMimeType("application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            request.setMimeType("application/zip");
            ;
        } else if (url.toString().contains(".rtf")) {
            request.setMimeType("application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            request.setMimeType("audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            request.setMimeType("image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            request.setMimeType("image/jpeg");
        } else if (url.toString().contains(".txt")) {
            request.setMimeType("text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            request.setMimeType("video/*");
        } else {
            request.setMimeType("*/*");
        }

        request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
        request.setDescription(postComment.message);
        request.setTitle(mContext.getString(R.string.document_download_title));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, postComment.message);
        ((DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
        Toast.makeText(mContext, mContext.getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();
    }

    /**
     * Get list of task and workflows
     *
     * @param postChildren
     * @return
     */
    public List<IPostChild> getTaskList(List<IPostChild> postChildren) {
        final List<IPostChild> postChildrenTasks = new ArrayList<IPostChild>();
        for (IPostChild child : postChildren) {
            if (EPostChildType.PostTask.equals(child.getType()) || EPostChildType.PostWfTask.equals(child.getType())) {
                postChildrenTasks.add(child);
            }
        }
        return postChildrenTasks;
    }

    /**
     * Get list of comments from ChildList
     *
     * @param postChildren
     * @return
     */
    public List<IPostChild> getCommentList(List<IPostChild> postChildren) {
        final List<IPostChild> postChildrenComments = new ArrayList<IPostChild>();
        for (IPostChild child : postChildren) {
            if (EPostChildType.PostComment.equals(child.getType())) {
                postChildrenComments.add(child);
            }
        }
        return postChildrenComments;
    }

    /**
     * Set up Document Image if there is document, and set clicklistener
     *
     * @param documentLayout
     * @param documentId
     */
    public void setUpDocumentLayout(ImageView documentLayout, final Long documentId) {
        //Set document if post has document
        if (documentId == 0) {
            documentLayout.setVisibility(View.GONE);
        } else {
            documentLayout.setVisibility(View.VISIBLE);
        }
        documentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SharePostActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("documentId", documentId);
                args.putInt("fragment", 4);
                intent.putExtras(args);
                mContext.startActivity(intent);
            }
        });
    }

    /**
     * Show
     *
     * @param postChildrenTasks
     * @param moreButton
     */
    public void showSubLayoutAutomatically(List<IPostChild> postChildrenTasks, RelativeLayout moreButton) {
        //Check child layout if needs to be showen or not
        if (postChildrenTasks != null && postChildrenTasks.size() != 0) {
            if (postChildrenTasks.get(0).getType().equals(EPostChildType.PostTask)) {
                PostTask postTask = (PostTask) postChildrenTasks.get(0);
                if (postTask.getChildList() != null && postTask.getChildList().size() != 0) {
                    if (postTask.getChildList().get(0).getType().equals(EPostChildType.PostSubTask)) {
                        PostSubTask postSubTask = (PostSubTask) postTask.getChildList().get(0);
                        //Visibility of child layout according to status
                        switch (postSubTask.status) {
                            case TaskStatus.TASK_CLOSED:
                                break;
                            case TaskStatus.TASK_REJECTED:
                                break;
                            default:
                                if (postSubTask.recipient) {
                                    moreButton.performClick();
                                } else {
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Refresh the list of the Wall
     *
     * @param postList
     */
    public void refreshPostList(List<Post> postList) {
        mPostList = postList;
        notifyDataSetChanged();
    }


    /**
     * Create dialog for execution of tasks, subtask and workflow tasks
     *
     * @param taskId
     * @param taskStatus
     */
    public void createExecutionDialog(final long taskId, int taskStatus, final PostWfTask postWfTask) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.task_execute_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        LinearLayout acceptButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_accept);
        LinearLayout rejectButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_reject);
        LinearLayout closeButton = (LinearLayout) dialog.findViewById(R.id.dialog_execute_close);
        LinearLayout alreadyClosed = (LinearLayout) dialog.findViewById(R.id.dialog_closed_layout);
        LinearLayout rejectedLayout = (LinearLayout) dialog.findViewById(R.id.dialog_rejected_layout);
        LinearLayout terminateLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_terminate);
        LinearLayout wfAcceptLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_wfaccept);
        LinearLayout wfReadyLayout = (LinearLayout) dialog.findViewById(R.id.dialog_execute_wfclose);
        LinearLayout wfTerminatedLayout = (LinearLayout) dialog.findViewById(R.id.dialog_terminated_layout);


        Log.d("STATUS OF DIALOG", WorkflowStatus.getWorkflowStatusBy(taskStatus));
        //Set up buttons for execution according to status
        switch (taskStatus) {
            case TaskStatus.TASK_ACCEPTED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_ASSIGNED:
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_CLOSED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case TaskStatus.TASK_REJECTED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.ACCEPTED:
                acceptButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.READY:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.CREATED:
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                alreadyClosed.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfTerminatedLayout.setVisibility(View.GONE);
                break;
            case WorkflowStatus.TERMINATED:
                alreadyClosed.setVisibility(View.GONE);
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
                closeButton.setVisibility(View.GONE);
                rejectedLayout.setVisibility(View.GONE);
                terminateLayout.setVisibility(View.GONE);
                wfAcceptLayout.setVisibility(View.GONE);
                wfReadyLayout.setVisibility(View.GONE);
                break;
            default:
                break;
        }


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_ACCEPTED, "").execute();
                dialog.dismiss();
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_REJECTED, "").execute();
                dialog.dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeTaskStatusTask(taskId, TaskStatus.TASK_CLOSED, mContext.getResources().getString(R.string.post_detail_task_closed)).execute();
                dialog.dismiss();
            }
        });


        wfAcceptLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wf elfogadás
                new ChangeWfTaskStatusTask(postWfTask, postWfTask.id, "", WorkflowStatus.ACCEPTED, null, MetaManager.getInstance().getFillMetaList()).execute();
                dialog.dismiss();
            }
        });

        wfReadyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wf lezárás
                Intent intent = new Intent(mContext, WfTaskExecutionActivity.class);
                intent.putExtra("postWfTask", postWfTask);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });

        //Window placement and dimensions
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    /**
     * Change task status in background
     */
    public class ChangeTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mTaskId;
        private int mTaskStatus;
        private String mMessage;


        public ChangeTaskStatusTask(final long taskId, final int status, final String message) {
            mTaskId = taskId;
            mTaskStatus = status;
            mMessage = message;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeTaskStatus(mTaskId, mTaskStatus, mMessage);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                Toast.makeText(mContext, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                mWallFragment.getFirstWallRequest();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (ex instanceof ValidationException) {
                Intent intent = new Intent(mContext, TaskExecutionActivity.class);
                intent.putExtra("postTaskId", mTaskId);
                mContext.startActivity(intent);
            } else {
                if (!((Activity) mContext).isFinishing()) {
                    DialogHelper.showErrorDialog(mContext, ex);
                }
            }

        }
    }

    /**
     * Change workflow task status in background
     */
    public class ChangeWfTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mWfTaskId;
        private Integer mStatus, mResultId;
        private String mMessage;
        private List<FillMeta> mMetaList;
        private PostWfTask mPostWfTask;

        public ChangeWfTaskStatusTask(PostWfTask postWfTask, final long wfTaskId, final String message, final int status, final Integer resultId, final List<FillMeta> metaList) {
            mWfTaskId = wfTaskId;
            mStatus = status;
            mMessage = message;
            mResultId = resultId;
            mMetaList = metaList;
            mPostWfTask = postWfTask;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeWfTaskStatus(mWfTaskId, mMessage, mStatus, mResultId, mMetaList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            Log.d("response", "" + response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                Toast.makeText(mContext, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                mWallFragment.getFirstWallRequest();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            Log.d("response", "" + ex.getMessage());

            if (ex instanceof ValidationException) {
                Intent intent = new Intent(mContext, WfTaskExecutionActivity.class);
                intent.putExtra("postWfTask", mPostWfTask);
                mContext.startActivity(intent);
            } else {
                if (!((Activity) mContext).isFinishing()) {
                    DialogHelper.showErrorDialog(mContext, ex);
                }
//            }
            }
        }
    }
}
