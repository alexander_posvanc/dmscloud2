package com.mmsone.dmscloud.adapter;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.DeleteSubCommentTask;
import com.mmsone.dmscloud.async.RankPostTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.ui.MessageActivity;
import com.mmsone.dmscloud.ui.SharePostActivity;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 09. 18..
 */
public class TechnicalDetailAdapter extends RecyclerView.Adapter<TechnicalDetailAdapter.TechnicalViewHolder> {

    private Context mContext;
    private ArrayList<IPostChild> mPostDocumentList;
    private Post mPost;

    public TechnicalDetailAdapter(ArrayList<IPostChild> postTaskList, Context context, Post post) {
        mContext = context;
        mPostDocumentList = postTaskList;
        mPost = post;
    }

    @Override
    public TechnicalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_detail, parent, false);
        TechnicalViewHolder vhItem = new TechnicalViewHolder(v);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(final TechnicalViewHolder holder, int position) {
        final PostComment postComment = (PostComment) mPostDocumentList.get(position);
        final List<PostSubComment> postSubComments = new ArrayList<>();

        holder.nameText.setText(postComment.user.formatedName);
        holder.nameText.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        holder.timeText.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        holder.timeText.setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - postComment.createdOn));
        holder.messageText.setText(postComment.message);
        holder.messageText.setTextColor(mContext.getResources().getColor(R.color.color_primary_actionbar));
        holder.documentId.setVisibility(View.GONE);

        //Documentum
        if (postComment.documentId == 0) {
            holder.documentId.setVisibility(View.GONE);
        } else {
            holder.documentId.setVisibility(View.VISIBLE);
        }

        holder.documentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SharePostActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("post", mPost);
                args.putInt("fragment", 4);
                intent.putExtras(args);
                mContext.startActivity(intent);
            }
        });


        holder.ratingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    new RankPostTask(mPost.id, postComment.id, postComment.subPostType, (Activity) mContext).execute();
                }

            }
        });

        Log.d("POSTCOMMENT", postComment.message + " " + postComment.hasExplicitRank);
        if (postComment.hasExplicitRank) {
            holder.rankIv.setImageResource(R.drawable.csillag_sarga);
        } else {
            holder.rankIv.setImageResource(R.drawable.csillag);
        }

        //child layout hideolás
        holder.subChildLayout.removeAllViews();
        holder.subChildLayout.setVisibility(View.GONE);
        holder.arrowIv.setRotation(270);

        //add postchildlist
        for (IPostChild postChild : postComment.getChildList()) {
            if (postChild.getType().equals(EPostChildType.PostSubComment)) {
                PostSubComment postSubComment = (PostSubComment) postChild;
                postSubComments.add(postSubComment);
            }
        }
        if (postSubComments.size() == 0 || postSubComments == null) {
            holder.moreSubChildButton.setVisibility(View.GONE);
        } else {
            holder.moreSubChildButton.setVisibility(View.VISIBLE);
        }

        holder.messageCount.setText(mContext.getResources().getString(R.string.post_detail_submessage_first) + " " + postSubComments.size() + " " + mContext.getResources().getString(R.string.post_detail_submessage));


        //show/hide
        holder.moreSubChildButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.subChildLayout.getVisibility() == View.GONE) {
                    holder.subChildLayout.setVisibility(View.VISIBLE);
                    holder.subChildLayout.removeAllViews();
                    holder.moreSubChildButton.setVisibility(View.GONE);

//                    holder.arrowIv.setRotation(90);
                    try {
                        addChildViews(holder.subChildLayout, postSubComments, holder.moreSubChildButton);
                    } catch (DMSCloudDBException e) {
                        e.printStackTrace();
                    }

                } else {
//                    holder.arrowIv.setRotation(270);
//                    holder.subChildLayout.removeAllViews();
//                    holder.subChildLayout.setVisibility(View.GONE);
                }
            }
        });

        //add subcomment
        holder.addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(mContext, MessageActivity.class);
                    intent.putExtra("postComment", postComment);
                    mContext.startActivity(intent);
                }
            }
        });


        holder.messageText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + postComment.technicalFile));

                String url = DMSHelper.getExtension(postComment.message);
                if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                    request.setMimeType("application/msword");
                } else if (url.toString().contains(".pdf")) {
                    request.setMimeType("application/pdf");
                } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                    request.setMimeType("application/vnd.ms-powerpoint");
                } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                    request.setMimeType("application/vnd.ms-excel");
                } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                    request.setMimeType("application/zip");
                    ;
                } else if (url.toString().contains(".rtf")) {
                    request.setMimeType("application/rtf");
                } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                    request.setMimeType("audio/x-wav");
                } else if (url.toString().contains(".gif")) {
                    request.setMimeType("image/gif");
                } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                    request.setMimeType("image/jpeg");
                } else if (url.toString().contains(".txt")) {
                    request.setMimeType("text/plain");
                } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                    request.setMimeType("video/*");
                } else {
                    request.setMimeType("*/*");
                }

                request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
                request.setDescription(postComment.message);
                request.setTitle(mContext.getString(R.string.document_download_title));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, postComment.message);
                ((DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
                Toast.makeText(mContext, mContext.getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return mPostDocumentList.size();
    }

    public class TechnicalViewHolder extends RecyclerView.ViewHolder {

        TextView nameText, timeText, messageText, messageCount;
        LinearLayout moreSubChildButton, subChildLayout, ratingButton, addComment;
        ImageView rankIv, arrowIv, documentId;


        public TechnicalViewHolder(View itemView) {
            super(itemView);
            nameText = (TextView) itemView.findViewById(R.id.message_detail_name);
            timeText = (TextView) itemView.findViewById(R.id.message_detail_time);
            messageText = (TextView) itemView.findViewById(R.id.message_detail_message);
            moreSubChildButton = (LinearLayout) itemView.findViewById(R.id.subchild_more);
            subChildLayout = (LinearLayout) itemView.findViewById(R.id.subchild_holder);
            rankIv = (ImageView) itemView.findViewById(R.id.message_detail_rank_Iv);
            arrowIv = (ImageView) itemView.findViewById(R.id.hide_layout_arrowIv);
            ratingButton = (LinearLayout) itemView.findViewById(R.id.message_detail_rating);
            addComment = (LinearLayout) itemView.findViewById(R.id.message_detail_comment_btn);
            messageCount = (TextView) itemView.findViewById(R.id.subchild_count_Tv);
            documentId = (ImageView) itemView.findViewById(R.id.comment_item_documentId);

        }
    }

    /**
     * add subcomment views
     *
     * @param childLayout
     * @param subComments
     */
    public void addChildViews(final LinearLayout childLayout, final List<PostSubComment> subComments, final LinearLayout showLayout) throws DMSCloudDBException {
        LinearLayout[] linearLayouts = new LinearLayout[subComments.size()];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        TextView[] messageTexts = new TextView[subComments.size()];
        TextView[] nameTexts = new TextView[subComments.size()];
        TextView[] timeTexts = new TextView[subComments.size()];
        LinearLayout[] deleteLayouts = new LinearLayout[subComments.size()];
        ImageView[] documentIds = new ImageView[subComments.size()];
        RelativeLayout[] hideLayouts = new RelativeLayout[subComments.size()];
        ImageView[] hideButtons = new ImageView[subComments.size()];

        for (int i = 0; i < subComments.size(); i++) {
            linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.message_subchild_layout, null);
            messageTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_message);
            nameTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_name);
            timeTexts[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_time);
            deleteLayouts[i] = (LinearLayout) linearLayouts[i].findViewById(R.id.subchild_layout_button);
            documentIds[i] = (ImageView) linearLayouts[i].findViewById(R.id.subcomment_documentId);
            hideLayouts[i] = (RelativeLayout) linearLayouts[i].findViewById(R.id.subchild_hide_layout);
            hideButtons[i] = (ImageView) linearLayouts[i].findViewById(R.id.subchild_hide_button);

            if (i == 0) {
                hideLayouts[i].setVisibility(View.VISIBLE);
            } else {
                hideLayouts[i].setVisibility(View.GONE);
            }
            hideButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLayout.setVisibility(View.VISIBLE);
                    childLayout.removeAllViews();
                    childLayout.setVisibility(View.GONE);
                }
            });

            //Documents
            if (subComments.get(i).documentId == 0) {
                documentIds[i].setVisibility(View.GONE);
            } else {
                documentIds[i].setVisibility(View.VISIBLE);
            }
            final int finalI1 = i;
            documentIds[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("post", mPost);
                    args.putLong("documentId", subComments.get(finalI1).documentId);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                }
            });

            if (i == 0 && UserManager.getInstance().getCurrentUser().getId() == subComments.get(i).user.getId()) {
                deleteLayouts[i].setVisibility(View.VISIBLE);
            } else {
                deleteLayouts[i].setVisibility(View.GONE);
            }

            final int finalI = i;

            deleteLayouts[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //delete
                    if (mPost.closed) {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                    } else {
                        new DeleteSubCommentTask(subComments.get(finalI).id, (Activity) mContext).execute();
                    }
                }
            });

            messageTexts[i].setText(subComments.get(i).message);
            nameTexts[i].setText(subComments.get(i).user.formatedName);
            nameTexts[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            timeTexts[i].setText(DMSHelper.longToTime(mContext, System.currentTimeMillis() - subComments.get(i).createdOn));

            childLayout.addView(linearLayouts[i]);
        }
    }

}
