package com.mmsone.dmscloud.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.listener.OnFileUploadListener;
import com.mmsone.dmscloud.ui.FileDialog;
import com.mmsone.dmscloud.ui.FileUploadActivity;
import com.mmsone.dmscloud.ui.helper.OnItemClickListener;

import java.io.File;

/**
 * Created by Alexander on 2015. 09. 08..
 */
public class FileUploadDrawerAdapter extends RecyclerView.Adapter<FileUploadDrawerAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;
    private OnFileUploadListener mFileUploadListener;
    private OnItemClickListener mOnItemClickListener;

    /**
     * Constructor of FileUploadDrawerAdapter
     *
     * @param context
     */
    public FileUploadDrawerAdapter(Context context) {
        mContext = context;
        mFileUploadListener = (OnFileUploadListener) mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_drawer_item_item, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType);
            return vhItem;
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_drawer_item_header, parent, false);
            ViewHolder vhHeader = new ViewHolder(v, viewType);
            return vhHeader;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        switch (position) {
            case 1:
                holder.textView.setText(mContext.getResources().getString(R.string.file_upload_photo));
                holder.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.foto_kek));

                holder.itemRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null && position != 0) {
                            mOnItemClickListener.onItemClick(v, position);
                        }
                    }
                });

                break;
            case 2:
                holder.textView.setText(mContext.getResources().getString(R.string.file_upload_gallery));
                holder.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.galeria_kek));
                holder.itemRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null && position != 0) {
                            mOnItemClickListener.onItemClick(v, position);
                        }
                    }
                });
                break;
            case 3:
                holder.textView.setText(mContext.getResources().getString(R.string.file_upload_file));
                holder.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.file_select_kek));
                holder.itemRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null && position != 0) {
                            mOnItemClickListener.onItemClick(v, position);
                        }
                    }
                });

                break;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    /**
     * Viewholder of file upload nav drawer items
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView, taskCountTv;
        ImageView imageView;
        TextView name;
        RelativeLayout itemRow;
        View divider;


        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {

                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                itemRow = (RelativeLayout) itemView.findViewById(R.id.list_item_layout);
                taskCountTv = (TextView) itemView.findViewById(R.id.task_count_Tv);
                divider = itemView.findViewById(R.id.post_drawer_divider);
                Holderid = 1;
            } else {
                name = (TextView) itemView.findViewById(R.id.headerText);
                Holderid = 0;
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        mOnItemClickListener = mItemClickListener;
    }

}
