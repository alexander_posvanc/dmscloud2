package com.mmsone.dmscloud.ui;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.TaskAdapter;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.TaskManager;
import com.mmsone.dmscloud.bo.TaskSearchType;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.TaskSearch;
import com.mmsone.dmscloud.helper.DialogHelper;

public class TasksFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 2;

    private MainActivity mActivity;
    private TaskAdapter mTaskAdapter;
    private ListView mTaskListView;
    private SwipeRefreshLayout mSwipeView;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (MainActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_tasks);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View couponsView = inflater.inflate(R.layout.fragment_tasks, container, false);

        mTaskListView = (ListView) couponsView.findViewById(R.id.tasks_taskListView);
        mSwipeView = (SwipeRefreshLayout) couponsView.findViewById(R.id.tasks_swipe);
        mSwipeView.setEnabled(false);

        mSwipeView.setColorSchemeResources(R.color.color_dmsclooud_purple, R.color.color_dmsclooud_pink, R.color.color_dmsclooud_green, R.color.color_dmsclooud_blue);
        mSwipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetTaskListTask().execute();
            }
        });

        mTaskListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    mSwipeView.setEnabled(true);
                } else {
                    mSwipeView.setEnabled(false);
                }
            }
        });

        return couponsView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetTaskListTask().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PrefManager.getInstance().isTasksUpToDate()) {
            new GetTaskListTask().execute();
        }
    }

    /**
     * Downloads task list in background
     */
    public class GetTaskListTask extends SafeAsyncTask<Void, Void, List<TaskSearch>> {

        public GetTaskListTask() {
        }

        @Override
        protected void onPreExecute() {
            mSwipeView.setRefreshing(true);
            super.onPreExecute();
        }

        @Override
        protected List<TaskSearch> doWorkInBackground(Void... params) throws Exception {
            return TaskManager.getInstance().getTaskList();
        }

        @Override
        protected void onSuccess(List<TaskSearch> taksList) {
            super.onSuccess(taksList);

            Iterator<TaskSearch> i = taksList.iterator();
            while (i.hasNext()) {
                TaskSearch taskSearch = i.next();
                if (taskSearch.status == WorkflowStatus.TERMINATED) {
                    i.remove();
                }
            }

            mTaskAdapter = new TaskAdapter(mActivity, taksList);
            mTaskListView.setAdapter(mTaskAdapter);
            mTaskListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TaskSearch taskSearch = mTaskAdapter.getItem(position);

                    Intent intent = null;

                    switch (taskSearch.type) {
                        case TaskSearchType.TASK:
                            intent = new Intent(mActivity, TaskExecutionActivity.class);
                            break;
                        case TaskSearchType.WORKFLOW:
                            intent = new Intent(mActivity, WfTaskExecutionActivity.class);
                            break;
                    }

                    intent.putExtra("taskSearch", taskSearch);
                    mActivity.startActivity(intent);
                }
            });

            if (taksList != null && ((MainActivity) getActivity()).getmMainDrawerAdapter() != null) {
                ((MainActivity) getActivity()).getmMainDrawerAdapter().setmTaskCount(taksList.size());
            }
            mSwipeView.setRefreshing(false);
            PrefManager.getInstance().setTasksUpToDate(true);
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mSwipeView.setRefreshing(false);
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }


}