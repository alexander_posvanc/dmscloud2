package com.mmsone.dmscloud.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.ui.NotificationActivity;

/**
 * Created by Alexander on 2015. 09. 03..
 */
public class NotificationButton {

    private Activity mActivity;
    private RelativeLayout mToolbarBtn;
    private RelativeLayout mDmsIcon;

    public NotificationButton(Activity activity) {
        mActivity = activity;
        mToolbarBtn = (RelativeLayout) mActivity.findViewById(R.id.tool_bar_notification_btn);
        mDmsIcon = (RelativeLayout) mActivity.findViewById(R.id.dms_cloud_logo);
    }

    public void startNotification() {
        mToolbarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mActivity.startActivity(new Intent(mActivity, NotificationActivity.class));
            }
        });
    }

    public void startNotificationFinish() {
        mToolbarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mActivity.startActivity(new Intent(mActivity, NotificationActivity.class));
//                mActivity.finish();
            }
        });
    }

    public void hideDmsIcon() {
        mDmsIcon.setVisibility(View.GONE);
    }

    public void showDmsIcon() {
        mDmsIcon.setVisibility(View.VISIBLE);
    }

}
