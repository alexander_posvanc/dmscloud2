package com.mmsone.dmscloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.RankPostTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.ExceptionManager;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.ShowMeta;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.SVGHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by Alexander on 2015. 08. 14..
 */
public class PostDetailFragmentNew extends Fragment {

    private TextView mPostTitle, mPostId, mPostUserName, mPostDate, mPostDetail, mMetaTitle;
    private ImageView mProfileIv, mDocumentId, mMarkedIcon, mRankedIcon;
    private Post mPost;
    private DisplayImageOptions mDisplayImageOptions;
    private LinearLayout mMetaLayout, mMessageLayout, mRankLayout;


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View postDetailView = inflater.inflate(R.layout.fragment_post_detail_new, container, false);

        mPostTitle = (TextView) postDetailView.findViewById(R.id.post_card_item_title_Tv);
//        mPostId = (TextView) postDetailView.findViewById(R.id.post_card_item_id_Tv);
        mPostUserName = (TextView) postDetailView.findViewById(R.id.post_card_item_profileName_Tv);
        mPostDate = (TextView) postDetailView.findViewById(R.id.post_card_item_post_date_Tv);
        mPostDetail = (TextView) postDetailView.findViewById(R.id.post_item_description_Tv);
        mProfileIv = (ImageView) postDetailView.findViewById(R.id.post_card_item_profile_Iv);
        mMetaLayout = (LinearLayout) postDetailView.findViewById(R.id.post_item_meta_layout);
        mDocumentId = (ImageView) postDetailView.findViewById(R.id.post_card_item_document_image);
        mMarkedIcon = (ImageView) postDetailView.findViewById(R.id.post_detail_marked_iv);
        mMetaTitle = (TextView) postDetailView.findViewById(R.id.meta_titleTv);
        mMessageLayout = (LinearLayout) postDetailView.findViewById(R.id.post_detail_add_comment);
        mRankLayout = (LinearLayout) postDetailView.findViewById(R.id.post_detail_rank_layout);
        mRankedIcon = (ImageView) postDetailView.findViewById(R.id.post_detail_iv_rate);

        mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();


        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");


        if (mPost.marked) {
            mMarkedIcon.setBackgroundColor(getActivity().getResources().getColor(R.color.color_marked));
        } else {
            mMarkedIcon.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
        }

        if (mPost.hasExplicitRank) {
            mRankedIcon.setImageResource(R.drawable.csillag_sarga);
        } else {
            mRankedIcon.setImageResource(R.drawable.csillag);
        }


        mPostTitle.setText(mPost.title + " " + "(" + getActivity().getResources().getString(R.string.wall_adapter_id) + mPost.postNumber + " )");
        mPostTitle.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
//        mPostId.setText("(azonosító: " + mPost.id + " )");
        mPostUserName.setText(mPost.user.formatedName);
        mPostUserName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        mPostDate.setText(mPost.formatedCreatedOn);
        mPostDetail.setText(mPost.message);
        mPostDetail.setTypeface(FontManager.getInstance().getRobotoLightTtf());

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    new RankPostTask(mPost.id, getActivity()).execute();
                }
            }
        });

        if (mPost.documentId == 0) {
            mDocumentId.setVisibility(View.GONE);
        } else {
            mDocumentId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), SharePostActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("documentId", mPost.documentId);
                    args.putInt("fragment", 4);
                    intent.putExtras(args);
                    startActivity(intent);
                }
            });
        }

        if (mPost.metaList != null && !mPost.metaList.isEmpty()) {
            mMetaTitle.setVisibility(View.VISIBLE);
        }
        addMeta(mPost.metaList);
        ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + mPost.user.image, mProfileIv, mDisplayImageOptions);

        mMessageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    //addCommentToPost();
                    ((PostActivity) getActivity()).slideMessageLayoutUp();
                }
            }
        });


        return postDetailView;
    }

    public void addMeta(List<ShowMeta> metaList) {
        LinearLayout[] linearLayouts = new LinearLayout[metaList.size()];
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        TextView[] textViews = new TextView[metaList.size()];

        for (int i = 0; i < metaList.size(); i++) {
            linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.meta_item, null);
            textViews[i] = (TextView) linearLayouts[i].findViewById(R.id.meta_text);

            textViews[i].setText(metaList.get(i).label + ":" + " " + metaList.get(i).value);
            mMetaLayout.addView(linearLayouts[i]);
        }
    }

    public void addCommentToPost() {
        Intent intent = new Intent(getActivity(), SharePostActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("post", mPost);
        args.putInt("fragment", 0);
        intent.putExtras(args);
        startActivity(intent);
    }

}
