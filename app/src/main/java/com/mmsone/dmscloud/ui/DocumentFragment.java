package com.mmsone.dmscloud.ui;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.DocumentManager;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bo.bean.Document;
import com.mmsone.dmscloud.bo.bean.DocumentFile;
import com.mmsone.dmscloud.bo.bean.DocumentVersion;
import com.mmsone.dmscloud.bo.bean.ShowMeta;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;

public class DocumentFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 6;

    private SharePostActivity mActivity;

    private TextView mDocumentNumberTv, mDocumentVersionTv, mCreatedByTv, mCreatedOnTv, mTypeTv;
    private LinearLayout mFileLayout;
    private long mDocumentId;
    private LayoutInflater mLayoutInflater;
    private TableLayout mTableLayout;

//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//		if(activity instanceof PostActivity){
//			mActivity = (PostActivity) activity;
//			((PostActivity)mActivity).setOptionMenuVisibility(false);
//		}
//		if(activity instanceof MainActivity){
//			mActivity = (MainActivity) activity;
//		}
//		mActivity.setTitle(R.string.title_document);
//	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        View documentView = inflater.inflate(R.layout.fragment_document, container, false);

        ((TextView) documentView.findViewById(R.id.document_fileTv)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        ((TextView) documentView.findViewById(R.id.document_numberTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) documentView.findViewById(R.id.document_versionTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) documentView.findViewById(R.id.document_createdByTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) documentView.findViewById(R.id.document_createdOnTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) documentView.findViewById(R.id.document_typeTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());

        mTableLayout = (TableLayout) documentView.findViewById(R.id.document_tableLayout);
        mDocumentNumberTv = (TextView) documentView.findViewById(R.id.document_numberTv);
        mDocumentNumberTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        mDocumentVersionTv = (TextView) documentView.findViewById(R.id.document_versionTv);
        mDocumentVersionTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        mCreatedByTv = (TextView) documentView.findViewById(R.id.document_createdByTv);
        mCreatedByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        mCreatedOnTv = (TextView) documentView.findViewById(R.id.document_createdOnTv);
        mCreatedOnTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        mTypeTv = (TextView) documentView.findViewById(R.id.document_typeTv);
        mTypeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());


        mFileLayout = (LinearLayout) documentView.findViewById(R.id.document_fileLayout);

        Bundle b = getArguments();
        mDocumentId = b.getLong("documentId");

        new GetDocumentTask(mDocumentId).execute();

        return documentView;
    }

    /**
     * Set document parameters on fragment
     *
     * @param document
     */
    private void setViews(Document document) {
        mDocumentNumberTv.setText(document.documentNumber);
        mDocumentVersionTv.setText("v" + document.versionList.get(document.versionList.size() - 1).versionNumber);
        User user = null;
        try {
            user = DataBaseConnector.getInstance().getUser(document.createdBy);
        } catch (DMSCloudDBException e) {
            DialogHelper.showErrorDialog(getActivity(), e);
        }
        mCreatedByTv.setText(user.formatedName);
        mCreatedOnTv.setText(document.formatedCreatedOn);
        mTypeTv.setText(document.documentType);
        if (!document.versionList.isEmpty()) {
            for (DocumentVersion documentVersion : document.versionList) {
                if (documentVersion.metaList != null && !documentVersion.metaList.isEmpty()) {
                    addDocumentMetaViews(documentVersion.metaList);
                }
                if (documentVersion.fileList != null && !documentVersion.fileList.isEmpty()) {
                    addDocumentFileViews(mFileLayout, documentVersion.fileList);
                }
            }
        }
    }

    /**
     * Add document show meta views to document fragment
     *
     * @param metaList ShowMeta list
     */
    private void addDocumentMetaViews(List<ShowMeta> metaList) {
        for (ShowMeta meta : metaList) {
            TableRow row = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.view_document_meta, null);
            ((TextView) row.findViewById(R.id.document_metaLabelTv)).setText(meta.label + ":");
            ((TextView) row.findViewById(R.id.document_metaLabelTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
            ((TextView) row.findViewById(R.id.document_metaValueTv)).setText(meta.value);
            ((TextView) row.findViewById(R.id.document_metaValueTv)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            mTableLayout.addView(row);
        }
    }

    /**
     * Add document files views
     *
     * @param fileLayout
     * @param fileList
     */
    private void addDocumentFileViews(LinearLayout fileLayout, List<DocumentFile> fileList) {
        fileLayout.removeAllViews();
        View fileView = null;
        for (final DocumentFile file : fileList) {

            fileView = mLayoutInflater.inflate(R.layout.view_document_file, null);

            ((TextView) fileView.findViewById(R.id.file_nameTv)).setText(file.fileName);
            ((TextView) fileView.findViewById(R.id.file_nameTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());

            final Button downloadBtn = (Button) fileView.findViewById(R.id.file_downloadBtn);
            downloadBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());
            downloadBtn.setTransformationMethod(null);

            if (DocumentManager.getInstance().isLocalFileExist(file)) {
                downloadBtn.setText(R.string.document_open_btn);
            }

            downloadBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    DocumentManager.getInstance().openOrDownloadFile(getActivity(), file);
                    downloadBtn.setText(R.string.document_open_btn);
                }
            });

            fileLayout.addView(fileView);
        }
    }

    /**
     * Downloads document in background
     */
    public class GetDocumentTask extends SafeAsyncTask<Void, Void, Document> {

        private ProgressDialog mProgressDialog;
        private long mDocumentId;

        public GetDocumentTask(long documentId) {
            mDocumentId = documentId;
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.document_loading_title));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Document doWorkInBackground(Void... params) throws Exception {
            return DocumentManager.getInstance().getDocument(mDocumentId);
        }

        @Override
        protected void onSuccess(Document document) {
            super.onSuccess(document);
            setViews(document);
            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(getActivity(), ex);
        }
    }
}
