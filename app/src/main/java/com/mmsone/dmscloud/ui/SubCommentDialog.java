package com.mmsone.dmscloud.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;

public class SubCommentDialog {

	private final Dialog mDialog;
	private final Context mContext;
	private OnWallUpdateListener mOnWallUpdateListener;
	private PostDetailFragment mPostDetailFragment;
	
	public SubCommentDialog(final Context context, final long id, final int subPostType , OnWallUpdateListener onWallUpdateListener, PostDetailFragment postDetailFragment, String response) {
		mContext = context;
		mDialog = new Dialog(context);
		
		mOnWallUpdateListener = onWallUpdateListener;
		mPostDetailFragment = postDetailFragment;
		
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.dialog_sub_comment);

		Button negativeButton = (Button) mDialog.findViewById(R.id.negativeButton);
		negativeButton.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) negativeButton.getLayoutParams();
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.weight = 0.5f;
		negativeButton.setLayoutParams(params);

		Button positiveButton = (Button) mDialog.findViewById(R.id.postiveButton);
		positiveButton.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		params = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.weight = 0.5f;
		positiveButton.setLayoutParams(params);

		final EditText descriptionEt = (EditText) mDialog.findViewById(R.id.dialog_sub_comment_descriptionEt);
		descriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		if(response != null){
			descriptionEt.setText(response);
		}
		
		descriptionEt.requestFocus();
         InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
         imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

		negativeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		positiveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (descriptionEt.getText().toString().isEmpty()) {
					descriptionEt.setError(context.getString(R.string.err_empty_field));
				} else {
					new CreatePostSubCommentTask(id, subPostType, descriptionEt.getText().toString()).execute();
				}
			}
		});
	}

	public void show() {
		mDialog.show();
	}

	/**
	 * Create sub comment in background
	 */
	public class CreatePostSubCommentTask extends SafeAsyncTask<Void, Void, Boolean> {

		private long mSubPostId;
		private int mSubPostType;
		private String mDescription;

		public CreatePostSubCommentTask(long subPostId, int subPostType, String description) {
			mSubPostId = subPostId;
			mDescription = description;
			mSubPostType = subPostType;
		}

		@Override
		protected Boolean doWorkInBackground(Void... params) throws Exception {
			return PostManager.getInstance().createPostSubComment(mSubPostId, mSubPostType, mDescription);
		}

		@Override
		protected void onSuccess(Boolean response) {
			super.onSuccess(response);
			if (response) {
				if(mOnWallUpdateListener != null){
					mOnWallUpdateListener.onWallUpdateListener();
				}
				
				if(mPostDetailFragment != null){
					mPostDetailFragment.detailRefresh();
				}
				
				Toast.makeText(mContext, R.string.post_sub_comment_create_succes_message, Toast.LENGTH_LONG).show();
				mDialog.dismiss();
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			mDialog.dismiss();
			DialogHelper.showErrorDialog(mContext, ex);
		}
	}
}
