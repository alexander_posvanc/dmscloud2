package com.mmsone.dmscloud.ui;

import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.LoginManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.helper.DialogHelper;

public class LoginActivity extends Activity {

    private EditText mUserName, mPasswordEt, mServerEt;
    private Button mLoginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Set login screen language
         */
        Configuration config = new Configuration();
        config.locale = Locale.getDefault();
        this.getResources().updateConfiguration(config, null);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        if (PrefManager.getInstance().isUserLogged()) {
            startMainActivity();
        } else {
            mUserName = (EditText) findViewById(R.id.login_emailEt);
            mUserName.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            if (!PrefManager.getInstance().getUserName().isEmpty()) {
                mUserName.setText(PrefManager.getInstance().getUserName());
            }

            mPasswordEt = (EditText) findViewById(R.id.login_passwordEt);
            mPasswordEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

            mServerEt = (EditText) findViewById(R.id.login_serverEt);
            mServerEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            if (!PrefManager.getInstance().getServerUrl().isEmpty()) {
                mServerEt.setText(PrefManager.getInstance().getServerUrl());
            }

            mLoginBtn = (Button) findViewById(R.id.login_loginBtn);
            mLoginBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());
            mLoginBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (isValid()) {
                        String serverEtText = mServerEt.getText().toString().trim();
                        String serverUrl = "";
                        if (!serverEtText.startsWith("https://") && !serverEtText.startsWith("http://")) {
                            serverUrl += "http://";
                        }
                        if (serverEtText.endsWith("/Red")) {
                            serverUrl += serverEtText;
                            serverUrl += "/";
                        } else if (!serverEtText.endsWith("/Red/")) {
                            serverUrl += serverEtText;
                            serverUrl += "/Red/";
                        } else {
                            serverUrl += serverEtText;
                        }

                        PrefManager.getInstance().setServerAddress(serverUrl);
                        PrefManager.getInstance().setUserName(mUserName.getText().toString());
                        new LoginTask(mPasswordEt.getText().toString(), mUserName.getText().toString()).execute();
                    }
                }
            });

            if (!mUserName.getText().toString().isEmpty() && !mServerEt.getText().toString().isEmpty()) {
                mPasswordEt.requestFocus();
            }
        }
    }

    /**
     * Start main activity
     */
    private void startMainActivity() {
        PrefManager.getInstance().setWallUpToDate(false);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    public boolean isValid() {
        if (mUserName.getText().toString().trim().equalsIgnoreCase("")) {
            mUserName.setError(getString(R.string.err_empty_field));
            return false;
        }

        if (mPasswordEt.getText().toString().trim().equalsIgnoreCase("")) {
            mPasswordEt.setError(getString(R.string.err_empty_field));
            return false;
        }

        if (mServerEt.getText().toString().trim().equalsIgnoreCase("")) {
            mServerEt.setError(getString(R.string.err_empty_field));
            return false;
        }

        return true;
    }

    /**
     * Send login request in background
     */
    public class LoginTask extends SafeAsyncTask<Void, Void, Void> {

        private String mPassword, mUserName;
        private ProgressDialog mProgressDialog;

        public LoginTask(String password, String userName) {
            mPassword = password;
            mUserName = userName;

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage(getString(R.string.login_loginBtn));
            mProgressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected Void doWorkInBackground(Void... params) throws Exception {
            LoginManager.getInstance().login(mPassword, mUserName);
            UserManager.getInstance().parseUserList();
            return null;
        }

        @Override
        protected void onSuccess(Void response) {
            super.onSuccess(response);
            PrefManager.getInstance().setUserLogged(true);
            mProgressDialog.dismiss();
            startMainActivity();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(LoginActivity.this, ex);
        }
    }
}