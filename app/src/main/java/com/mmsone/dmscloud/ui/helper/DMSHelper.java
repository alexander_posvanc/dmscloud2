package com.mmsone.dmscloud.ui.helper;

import android.content.Context;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.IUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 09. 18..
 */
public class DMSHelper {

    /**
     * Convert long value to time
     *
     * @param ms
     * @return
     */
    public static String longToTime(Context context, long ms) {
        int SECOND = 1000;
        int MINUTE = 60 * SECOND;
        int HOUR = 60 * MINUTE;
        int DAY = 24 * HOUR;

        StringBuffer text = new StringBuffer("");
        if (ms > DAY) {
            text.append(ms / DAY).append(" " + context.getResources().getString(R.string.day));
            ms %= DAY;
        } else if (ms > HOUR && ms < DAY) {
            text.append(ms / HOUR).append(" " + context.getResources().getString(R.string.hour));
            ms %= HOUR;
        } else if (ms > MINUTE && ms < HOUR) {
            text.append(ms / MINUTE).append(" " + context.getResources().getString(R.string.minute));
            ms %= MINUTE;
        } else if (ms > SECOND && ms < MINUTE) {
            text.append(ms / SECOND).append(" " + context.getResources().getString(R.string.second));
            ms %= SECOND;
        }
        return text.toString();
    }

    /**
     * Get files extension
     *
     * @param file
     * @return
     */
    public static String getExtension(String file) {
        String ext = null;
        int i = file.lastIndexOf('.');

        if (i > 0 && i < file.length() - 1) {
            ext = file.substring(i).toLowerCase();
        }
        return ext;
    }

    /**
     * Search in Users
     *
     * @param userList
     * @param query
     * @return
     */
    public static List<IUser> performSearch(List<IUser> userList, String query) {
        List<IUser> filteredList = new ArrayList<>();
        String[] queryByWords = query.toLowerCase().split("\\s+");
        if (userList != null && !userList.isEmpty()) {
            for (IUser user : userList) {
                String search = user.getName();
                search = search.toLowerCase();
                for (String word : queryByWords) {
                    int numberOfMatches = queryByWords.length;
                    if (search.contains(word)) {
                        numberOfMatches--;
                    } else {
                        break;
                    }
                    if (numberOfMatches == 0) {
                        filteredList.add(user);
                    }
                }
            }
        }
        return filteredList;
    }

}
