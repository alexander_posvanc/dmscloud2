package com.mmsone.dmscloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

/**
 * Created by Alexander on 2015. 09. 24..
 */
public class TaskMessageActivity extends ActionBarActivity {

    private Toolbar mToolbar;
    private PostTask postTask;
    private PostWfTask postWfTask;
    private boolean isTask;
    private TextView mNameTv, mTimeTv, mDescriptionTv, mDueDate;
    private ImageView mTypeIv;
    private RelativeLayout mBackLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_message);

        //Get Task or WfTask
        Intent intent = getIntent();
        if (intent.getSerializableExtra("postWfTask") != null) {
            postWfTask = (PostWfTask) intent.getSerializableExtra("postWfTask");
            isTask = false;
        } else if (intent.getSerializableExtra("postTask") != null) {
            postTask = (PostTask) intent.getSerializableExtra("postTask");
            isTask = true;
        }

        mBackLayout = (RelativeLayout) findViewById(R.id.message_back_layout);
        mTypeIv = (ImageView) findViewById(R.id.task_message_typeIv);
        mNameTv = (TextView) findViewById(R.id.message_detail_name);
        mTimeTv = (TextView) findViewById(R.id.message_detail_time);
        mDescriptionTv = (TextView) findViewById(R.id.message_detail_message);
        mDueDate = (TextView) findViewById(R.id.task_message_dueDate);

        if (isTask) {
            mTypeIv.setImageResource(R.drawable.feladatok_kek);
            mNameTv.setText(postTask.user.formatedName);
            mTimeTv.setText(DMSHelper.longToTime(TaskMessageActivity.this, System.currentTimeMillis() - postTask.createdOn));
            mDescriptionTv.setText(postTask.message);
            mDueDate.setText(TaskMessageActivity.this.getResources().getString(R.string.due_date) + postTask.formatedDueDate);
        } else {
            mTypeIv.setImageResource(R.drawable.munkafolyamatok);
            mNameTv.setText(postWfTask.user.formatedName);
            mTimeTv.setText(DMSHelper.longToTime(TaskMessageActivity.this, System.currentTimeMillis() - postWfTask.createdOn));
            mDescriptionTv.setText(postWfTask.title + "  " + postWfTask.message);
            mDueDate.setText(TaskMessageActivity.this.getResources().getString(R.string.due_date) + postWfTask.formatedDueDate);
        }


        final EditText mMessageEt = (EditText) findViewById(R.id.send_messageEt);
        Button mSendMessageBtn = (Button) findViewById(R.id.sendMessageBtn);
        mSendMessageBtn.setTransformationMethod(null);


        //Send message
        mSendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageEt.getText() != null && !mMessageEt.getText().equals("")) {

                    if (isTask) {
                        new CreatePostSubCommentTask(postTask.id, postTask.subPostType, mMessageEt.getText().toString()).execute();
                    } else {
                        new CreatePostSubCommentTask(postWfTask.id, postWfTask.subPostType, mMessageEt.getText().toString()).execute();
                    }
                } else {
                    Toast.makeText(TaskMessageActivity.this, TaskMessageActivity.this.getResources().getString(R.string.message_task_activity_add_comment_first), Toast.LENGTH_SHORT).show();

                }
            }
        });
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setTitle(TaskMessageActivity.this.getResources().getString(R.string.message_activity_title));

        mBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /**
     * Create sub comment in background
     */
    public class CreatePostSubCommentTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mSubPostId;
        private int mSubPostType;
        private String mDescription;

        public CreatePostSubCommentTask(long subPostId, int subPostType, String description) {
            mSubPostId = subPostId;
            mDescription = description;
            mSubPostType = subPostType;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostSubComment(mSubPostId, mSubPostType, mDescription);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
//                if(mOnWallUpdateListener != null){
//                    mOnWallUpdateListener.onWallUpdateListener();
//                }

//                if(mPostDetailFragment != null){
//                    mPostDetailFragment.detailRefresh();
//                }

                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(TaskMessageActivity.this, R.string.post_sub_comment_create_succes_message, Toast.LENGTH_LONG).show();
                TaskMessageActivity.this.finish();
//                mDialog.dismiss();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
//            mDialog.dismiss();
            DialogHelper.showErrorDialog(TaskMessageActivity.this, ex);
        }
    }
}
