package com.mmsone.dmscloud.ui.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.IUser.EUserType;

public class MultiSelectionSpinner extends Spinner implements OnMultiChoiceClickListener {

    private String[] mItems = null;
    private List<IUser> userList;
    private boolean[] mSelection = null;
    private ArrayAdapter<String> mSimpleAdapter;
    private LinearLayout mRecipientsLayout;

    public MultiSelectionSpinner(Context context) {
        super(context);

        mSimpleAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
        super.setAdapter(mSimpleAdapter);
    }


    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        mSimpleAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
        super.setAdapter(mSimpleAdapter);
    }

    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (mSelection != null && which < mSelection.length) {
            mSelection[which] = isChecked;

            setRecipints();
        } else {
            throw new IllegalArgumentException("Argument 'which' is out of bounds.");
        }
    }

    /**
     * Set recipients
     */
    private void setRecipints() {
        mRecipientsLayout.removeAllViews();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i]) {
                View userView = View.inflate(getContext(), R.layout.view_user, null);
                TextView labelTv = (TextView) userView.findViewById(R.id.view_user_nameTv);

                labelTv.setText(mItems[i]);
                labelTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

                mRecipientsLayout.addView(userView);
            }
        }

    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(mItems, mSelection, this);
        builder.show();
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(List<IUser> items) {
        userList = new ArrayList<IUser>(items);

        mItems = new String[items.size()];
        for (int i = 0; i < items.size(); i++) {
            mItems[i] = items.get(i).getName();
        }

        mSelection = new boolean[mItems.length];
        mSimpleAdapter.clear();
        mSimpleAdapter.add(mItems[0]);
        Arrays.fill(mSelection, false);
    }

    public void setSelection(String[] selection) {
        for (String cell : selection) {
            for (int j = 0; j < mItems.length; ++j) {
                if (mItems[j].equals(cell)) {
                    mSelection[j] = true;
                }
            }
        }
    }

    public void setSelection(List<String> selection) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (String sel : selection) {
            for (int j = 0; j < mItems.length; ++j) {
                if (mItems[j].equals(sel)) {
                    mSelection[j] = true;
                }
            }
        }
        mSimpleAdapter.clear();
        mSimpleAdapter.add(buildSelectedItemString());
    }

    public void setSelection(int index) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        if (index >= 0 && index < mSelection.length) {
            mSelection[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index + " is out of bounds.");
        }
        mSimpleAdapter.clear();
        mSimpleAdapter.add(buildSelectedItemString());
        setRecipints();
    }

    public void setSelection(int[] selectedIndicies) {
        for (int i = 0; i < mSelection.length; i++) {
            mSelection[i] = false;
        }
        for (int index : selectedIndicies) {
            if (index >= 0 && index < mSelection.length) {
                mSelection[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index + " is out of bounds.");
            }
        }
        mSimpleAdapter.clear();
        mSimpleAdapter.add(buildSelectedItemString());
    }

    public List<String> getSelectedStrings() {
        List<String> selection = new LinkedList<String>();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i]) {
                selection.add(mItems[i]);
            }
        }
        return selection;
    }

    public Long[] getSelectedDistributionIds() {
        List<Long> selection = new ArrayList<Long>();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i] && userList.get(i).getType().equals(EUserType.DISTRIBUTION)) {
                selection.add(userList.get(i).getId());
            }
        }
        return (Long[]) selection.toArray(new Long[selection.size()]);
    }

    public Long[] getSelectedUserIds() {
        List<Long> selection = new ArrayList<Long>();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i] && userList.get(i).getType().equals(EUserType.USER)) {
                selection.add(userList.get(i).getId());
            }
        }
        return (Long[]) selection.toArray(new Long[selection.size()]);
    }

    public Long[] getSelectedGroupIds() {
        List<Long> selection = new ArrayList<Long>();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i] && userList.get(i).getType().equals(EUserType.GROUP)) {
                selection.add(userList.get(i).getId());
            }
        }
        return (Long[]) selection.toArray(new Long[selection.size()]);
    }

    public List<Integer> getSelectedIndicies() {
        List<Integer> selection = new LinkedList<Integer>();
        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;

                sb.append(mItems[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < mItems.length; ++i) {
            if (mSelection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(mItems[i]);
            }
        }
        return sb.toString();
    }

    public void setRecipientsLayout(LinearLayout recipientsLayout) {
        mRecipientsLayout = recipientsLayout;
    }
}