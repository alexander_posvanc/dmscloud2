package com.mmsone.dmscloud.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.TaskDetailAdapter;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.DMSCloudViewBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexander on 2015. 08. 14..
 */
public class PostDetailTaskFragment extends Fragment {
    private Post mPost;
    private RecyclerView mTaskListView;
    private TaskDetailAdapter mTaskDetailAdapter;
    private List<IPostChild> postChildren;
    private TextView taskCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View taskDetail = inflater.inflate(R.layout.post_detail_task_fragment, container, false);
        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        taskCount = (TextView) taskDetail.findViewById(R.id.task_count_Tv);
        mTaskListView = (RecyclerView) taskDetail.findViewById(R.id.taskFragment_recyclerView);
        postChildren = new ArrayList<>();

        for (IPostChild child : mPost.childList) {
            if (EPostChildType.PostTask.equals(child.getType()) || EPostChildType.PostWfTask.equals(child.getType())) {
                postChildren.add(child);
            }
        }

        taskCount.setText(getResources().getString(R.string.shared_with_first) + " " + postChildren.size() + getResources().getString(R.string.post_detail_second_task));


        mTaskDetailAdapter = new TaskDetailAdapter((ArrayList<IPostChild>) postChildren, getActivity(), mPost);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mTaskListView.setLayoutManager(mLayoutManager);
        mTaskListView.setHasFixedSize(true);

        mTaskListView.setAdapter(mTaskDetailAdapter);


        return taskDetail;
    }
}
