package com.mmsone.dmscloud.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.FileAdapter;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.FileUploadResponse;
import com.mmsone.dmscloud.helper.DialogHelper;

public class FileUploadFragment extends Fragment {

    private FileUploadActivity mActivity;
    private List<File> mFileList = new ArrayList<File>();
    private FileAdapter mFileAdapter;
    private long mPostId = 0;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (FileUploadActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_file_upload);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_upload, container, false);

        ListView fileListView = (ListView) view.findViewById(R.id.file_upload_listView);

        Bundle args = getArguments();
        if (args != null) {
            mPostId = args.getLong("postId");
        }
        Button okBtn = (Button) view.findViewById(R.id.file_upload_okBtn);
        okBtn.setTransformationMethod(null);
        okBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());
        okBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mFileList.isEmpty()) {
                    Toast.makeText(mActivity, R.string.file_upload_no_file, Toast.LENGTH_LONG).show();
                } else {
                    new FileUploadTask().execute();
                }
            }
        });

        Log.d("FILEUPLOAD", "PostId: " + mPostId);

        mFileAdapter = new FileAdapter(mActivity, mFileList);
        fileListView.setAdapter(mFileAdapter);
        fileListView.setEmptyView(view.findViewById(R.id.emptyElement));

        return view;
    }

    /**
     * Update file listview
     *
     * @param file
     */
    public void updateList(File file) {
        mFileList.add(file);
        if (mFileAdapter != null) {
            mFileAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Upload files in background
     */
    public class FileUploadTask extends SafeAsyncTask<Void, Void, String> {

        private ProgressDialog mProgressDialog;

        public FileUploadTask() {
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.file_upload_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doWorkInBackground(Void... params) throws Exception {
            if (mPostId == 0) {
                return ServerConnector.getInstance().fileUpload(mFileList);
            } else {
                return ServerConnector.getInstance().fileUpload(mFileList, mPostId);
            }

        }

        @Override
        protected void onSuccess(String respopnse) {
            super.onSuccess(respopnse);
            if (respopnse.equals(FileUploadResponse.SUCCESS)) {
                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(mActivity, R.string.file_upload_succes_message, Toast.LENGTH_LONG).show();
                mActivity.finish();
            } else if (respopnse.equals(FileUploadResponse.FILE_SIZE_LIMIT_EXCEEDED)) {
                DialogHelper.createErrorDialog(mActivity, R.string.err_file_size);
            } else if (respopnse.equals(FileUploadResponse.FORBIDDEN_EXTENSION)) {
                DialogHelper.createErrorDialog(mActivity, R.string.err_file_extension);
            } else if (respopnse.equals(FileUploadResponse.INSUFFICIENT_RIGHTS)) {
                DialogHelper.createErrorDialog(mActivity, R.string.err_insufficient_rights);
            } else if (respopnse.equals(FileUploadResponse.NOT_AUTHENTICATED)) {
                DialogHelper.createErrorDialog(mActivity, R.string.err_authentication);
            } else if (respopnse.equals(FileUploadResponse.UNSPECIFIED_ERROR)) {
                DialogHelper.createErrorDialog(mActivity, R.string.err_exceptin);
            }

            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }
}
