package com.mmsone.dmscloud.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

import java.util.List;

/**
 * Created by Alexander on 2015. 08. 25..
 */
public class MessageActivity extends ActionBarActivity {


    private ImageView mTriangle;
    private Toolbar mToolbar;
    private EditText mMessageEt;
    private Button mSendMessageBtn;
    private RelativeLayout mQuickAnswerLayout, mTriangleLayout;
    private LinearLayout mDefaultLayout;
    private ScrollView mMessageScrollLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        final PostComment postComment = (PostComment) getIntent().getSerializableExtra("postComment");

        mQuickAnswerLayout = (RelativeLayout) findViewById(R.id.fast_answer_layout);
        mDefaultLayout = (LinearLayout) findViewById(R.id.default_layout);
        TextView quickAnswerTv1 = (TextView) findViewById(R.id.message_quickanswer_1);
        TextView quickAnswerTv2 = (TextView) findViewById(R.id.message_quickanswer_2);
        TextView quickAnswerTv3 = (TextView) findViewById(R.id.message_quickanswer_3);
        TextView quickAnswerTv4 = (TextView) findViewById(R.id.message_quickanswer_4);
        TextView quickAnswerTv5 = (TextView) findViewById(R.id.message_quickanswer_5);
        TextView commentTv = (TextView) findViewById(R.id.message_detail_message);
        TextView nameTv = (TextView) findViewById(R.id.message_detail_name);
        TextView timeTv = (TextView) findViewById(R.id.message_detail_time);
        RelativeLayout backButton = (RelativeLayout) findViewById(R.id.message_back_layout);
        mTriangle = (ImageView) findViewById(R.id.triangle_top);
        final ImageView switchButton = (ImageView) findViewById(R.id.message_switch_quickmessageBtn);
        mMessageEt = (EditText) findViewById(R.id.send_messageEt);
        mSendMessageBtn = (Button) findViewById(R.id.sendMessageBtn);
        mSendMessageBtn.setTransformationMethod(null);
        mTriangleLayout = (RelativeLayout) findViewById(R.id.triangle_layout);
        mMessageScrollLayout = (ScrollView) findViewById(R.id.message_scroll_layout);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        timeTv.setText(DMSHelper.longToTime(this, System.currentTimeMillis() - postComment.createdOn));
        nameTv.setText(postComment.user.formatedName);
        commentTv.setText(postComment.message);


        if (postComment.quickResponseList == null || postComment.quickResponseList.isEmpty()) {
            mQuickAnswerLayout.setVisibility(View.GONE);
            mDefaultLayout.setVisibility(View.VISIBLE);
            mTriangleLayout.setVisibility(View.GONE);
            switchButton.setImageResource(R.drawable.billentyu);

        }

        //Add quick answers to TextViews
        if (postComment.quickResponseList != null && !postComment.quickResponseList.isEmpty()) {
            setUpQuickAnswerLayout(postComment.quickResponseList);
            switch (postComment.quickResponseList.size()) {
                case 1:
                    quickAnswerTv1.setText(postComment.quickResponseList.get(0));
                    break;
                case 2:
                    quickAnswerTv1.setText(postComment.quickResponseList.get(0));
                    quickAnswerTv2.setText(postComment.quickResponseList.get(1));
                    break;
                case 3:
                    quickAnswerTv1.setText(postComment.quickResponseList.get(0));
                    quickAnswerTv2.setText(postComment.quickResponseList.get(1));
                    quickAnswerTv3.setText(postComment.quickResponseList.get(2));
                    break;
                case 4:
                    quickAnswerTv1.setText(postComment.quickResponseList.get(0));
                    quickAnswerTv2.setText(postComment.quickResponseList.get(1));
                    quickAnswerTv3.setText(postComment.quickResponseList.get(2));
                    quickAnswerTv4.setText(postComment.quickResponseList.get(3));
                    break;
                case 5:
                    quickAnswerTv1.setText(postComment.quickResponseList.get(0));
                    quickAnswerTv2.setText(postComment.quickResponseList.get(1));
                    quickAnswerTv3.setText(postComment.quickResponseList.get(2));
                    quickAnswerTv4.setText(postComment.quickResponseList.get(3));
                    quickAnswerTv5.setText(postComment.quickResponseList.get(4));
                    break;
            }
        }

        //Switch between quickanswers and EditText
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQuickAnswerLayout.getVisibility() == View.VISIBLE) {
                    mQuickAnswerLayout.setVisibility(View.GONE);
                    switchButton.setImageResource(R.drawable.gyorsvalasz);
                    mDefaultLayout.setVisibility(View.VISIBLE);
                    mTriangleLayout.setVisibility(View.GONE);

                } else {
                    if (postComment.quickResponseList != null && !postComment.quickResponseList.isEmpty()) {
                        mQuickAnswerLayout.setVisibility(View.VISIBLE);
                        mDefaultLayout.setVisibility(View.GONE);
                        mTriangleLayout.setVisibility(View.VISIBLE);
                        switchButton.setImageResource(R.drawable.billentyu);
//                        mMessageScrollLayout.getLayoutParams().height = 200;
                    } else {
                        Toast.makeText(MessageActivity.this, MessageActivity.this.getResources().getString(R.string.message_activity_no_quickanswer), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        //Send Message
        mSendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageEt.getText() != null && !mMessageEt.getText().equals("")) {
                    new CreatePostSubCommentTask(postComment.id, postComment.subPostType, mMessageEt.getText().toString()).execute();
                } else {
                    Toast.makeText(MessageActivity.this, MessageActivity.this.getResources().getString(R.string.message_task_activity_add_comment_first), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Set up toolbar
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setTitle(MessageActivity.this.getResources().getString(R.string.message_activity_title));

    }


    /**
     * Set up layout for quick answers
     *
     * @param quickAnswerList
     */
    public void setUpQuickAnswerLayout(final List<String> quickAnswerList) {
        RelativeLayout quickAnswer1, quickAnswer2, quickAnswer3, quickAnswer4, quickAnswer5;
        View divider1, divider2, divider3, divider4;
        divider1 = findViewById(R.id.quick_answer_divider1);
        divider2 = findViewById(R.id.quick_answer_divider2);
        divider3 = findViewById(R.id.quick_answer_divider3);
        divider4 = findViewById(R.id.quick_answer_divider4);
        quickAnswer1 = (RelativeLayout) findViewById(R.id.quick_answer_layout1);
        quickAnswer2 = (RelativeLayout) findViewById(R.id.quick_answer_layout2);
        quickAnswer3 = (RelativeLayout) findViewById(R.id.quick_answer_layout3);
        quickAnswer4 = (RelativeLayout) findViewById(R.id.quick_answer_layout4);
        quickAnswer5 = (RelativeLayout) findViewById(R.id.quick_answer_layout5);
        switch (quickAnswerList.size()) {
            case 2:
                divider1.setVisibility(View.VISIBLE);
                quickAnswer2.setVisibility(View.VISIBLE);
                break;
            case 3:
                divider1.setVisibility(View.VISIBLE);
                quickAnswer2.setVisibility(View.VISIBLE);
                divider2.setVisibility(View.VISIBLE);
                quickAnswer3.setVisibility(View.VISIBLE);
                break;
            case 4:
                divider1.setVisibility(View.VISIBLE);
                quickAnswer2.setVisibility(View.VISIBLE);
                divider2.setVisibility(View.VISIBLE);
                quickAnswer3.setVisibility(View.VISIBLE);
                divider3.setVisibility(View.VISIBLE);
                quickAnswer4.setVisibility(View.VISIBLE);
                break;
            case 5:
                divider1.setVisibility(View.VISIBLE);
                quickAnswer2.setVisibility(View.VISIBLE);
                divider2.setVisibility(View.VISIBLE);
                quickAnswer3.setVisibility(View.VISIBLE);
                divider3.setVisibility(View.VISIBLE);
                quickAnswer4.setVisibility(View.VISIBLE);
                divider4.setVisibility(View.VISIBLE);
                quickAnswer5.setVisibility(View.VISIBLE);
                break;
        }

        quickAnswer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageEt.setText(quickAnswerList.get(0));
                mQuickAnswerLayout.setVisibility(View.GONE);
                mDefaultLayout.setVisibility(View.VISIBLE);
                mTriangleLayout.setVisibility(View.GONE);
            }
        });
        quickAnswer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageEt.setText(quickAnswerList.get(1));
                mQuickAnswerLayout.setVisibility(View.GONE);
                mDefaultLayout.setVisibility(View.VISIBLE);
                mTriangleLayout.setVisibility(View.GONE);
            }
        });
        quickAnswer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageEt.setText(quickAnswerList.get(2));
                mQuickAnswerLayout.setVisibility(View.GONE);
                mDefaultLayout.setVisibility(View.VISIBLE);
                mTriangleLayout.setVisibility(View.GONE);
            }
        });
        quickAnswer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageEt.setText(quickAnswerList.get(3));
                mQuickAnswerLayout.setVisibility(View.GONE);
                mDefaultLayout.setVisibility(View.VISIBLE);
                mTriangleLayout.setVisibility(View.GONE);
            }
        });
        quickAnswer5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageEt.setText(quickAnswerList.get(4));
                mQuickAnswerLayout.setVisibility(View.GONE);
                mDefaultLayout.setVisibility(View.VISIBLE);
                mTriangleLayout.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Create sub comment in background
     */
    public class CreatePostSubCommentTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mSubPostId;
        private int mSubPostType;
        private String mDescription;

        public CreatePostSubCommentTask(long subPostId, int subPostType, String description) {
            mSubPostId = subPostId;
            mDescription = description;
            mSubPostType = subPostType;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostSubComment(mSubPostId, mSubPostType, mDescription);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
//                if(mOnWallUpdateListener != null){
//                    mOnWallUpdateListener.onWallUpdateListener();
//                }

//                if(mPostDetailFragment != null){
//                    mPostDetailFragment.detailRefresh();
//                }

                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(MessageActivity.this, R.string.post_sub_comment_create_succes_message, Toast.LENGTH_LONG).show();
                MessageActivity.this.finish();
//                mDialog.dismiss();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
//            mDialog.dismiss();
            DialogHelper.showErrorDialog(MessageActivity.this, ex);
        }
    }


}
