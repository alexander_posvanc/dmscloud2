package com.mmsone.dmscloud.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.bean.PostSubTask;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.TaskSearch;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

public class TaskExecutionActivity extends ActionBarActivity {

    private Button mDueDateBtn;
    private RelativeLayout mOkBtn;
    private TextView mTaskTv, mUserTv;
    private EditText mDescriptionEt;
    private Spinner mStatusSpinner;
    private RadioGroup mExecutiveRg;
    private Toolbar mToolbar;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_execution);

        setTitle(R.string.title_task_execution);

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new NotificationButton(this).hideDmsIcon();

        mOkBtn = (RelativeLayout) findViewById(R.id.task_execution_okBtn);
        mStatusSpinner = (Spinner) findViewById(R.id.task_execution_statusSpinner);
        mDueDateBtn = (Button) findViewById(R.id.task_execution_dueDateBtn);
        mDueDateBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mDueDateBtn.setEnabled(false);
        mDescriptionEt = (EditText) findViewById(R.id.task_execution_descriptionEt);
        mDescriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mExecutiveRg = (RadioGroup) findViewById(R.id.task_execution_executiveRg);
        mExecutiveRg.setEnabled(false);
        mTaskTv = (TextView) findViewById(R.id.task_execution_taskTv);
        mTaskTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mUserTv = (TextView) findViewById(R.id.task_execution_userNameTv);

        try {
            mUser = UserManager.getInstance().getCurrentUser();
        } catch (DMSCloudDBException e) {
            e.printStackTrace();
        }

        mUserTv.setText("(" + mUser.formatedName + ")");


        ((TextView) findViewById(R.id.task_execution_descriptionTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) findViewById(R.id.task_execution_dueDateTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) findViewById(R.id.task_execution_statusTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());

        ((RadioButton) findViewById(R.id.task_execution_allUserRb)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        ((RadioButton) findViewById(R.id.task_execution_singleUserRb)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        final PostSubTask postTask = (PostSubTask) getIntent().getSerializableExtra("postSubTask");
        final TaskSearch taskSearch = (TaskSearch) getIntent().getSerializableExtra("taskSearch");
        final Long taskId = getIntent().getLongExtra("postTaskId", 0);

        if (postTask != null) {
            new GetTaskStatusTask(postTask.id).execute();
        }
        if (taskSearch != null) {
            new GetTaskStatusTask(taskSearch.id).execute();
        }

        if (taskId != null && taskId != 0) {
            new GetTaskStatusTask(taskId).execute();
        }
    }

    /**
     * Change task status in background
     */
    public class ChangeTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mTaskId;
        private int mTaskStatus;
        private String mMessage;


        public ChangeTaskStatusTask(final long taskId, final int status, final String message) {
            mTaskId = taskId;
            mTaskStatus = status;
            mMessage = message;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeTaskStatus(mTaskId, mTaskStatus, mMessage);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(TaskExecutionActivity.this, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                TaskExecutionActivity.this.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!TaskExecutionActivity.this.isFinishing()) {
                DialogHelper.showErrorDialog(TaskExecutionActivity.this, ex);
            }
        }
    }

    /**
     * Downloads task status in background
     */
    public class GetTaskStatusTask extends SafeAsyncTask<Void, Void, PostTask> {

        private long mTaskId;
        private ProgressDialog mProgressDialog;

        public GetTaskStatusTask(final long taskId) {
            mTaskId = taskId;
            mProgressDialog = new ProgressDialog(TaskExecutionActivity.this);
            mProgressDialog.setMessage(getString(R.string.loading_task_dialog_message));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected PostTask doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().getTaskStatusRequest(mTaskId);
        }

        @Override
        protected void onSuccess(final PostTask response) {
            super.onSuccess(response);

            mTaskTv.setText(TaskExecutionActivity.this.getResources().getString(R.string.task_execution_task_name) + response.message);
            mDueDateBtn.setText(response.formatedDueDate);
            if (response.recipient) {
                ((RadioButton) findViewById(R.id.task_execution_singleUserRb)).setChecked(true);
            } else {
                ((RadioButton) findViewById(R.id.task_execution_allUserRb)).setChecked(true);
            }

            ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(TaskExecutionActivity.this, R.layout.spinner_item, TaskStatus.getTaskExecutionStatusList(response.status));
            statusAdapter.setDropDownViewResource(R.layout.spinner_item);
            mStatusSpinner.setAdapter(statusAdapter);
            mStatusSpinner.setSelection(0);

            mOkBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (TaskStatus.getTaskStatusBy((String) mStatusSpinner.getSelectedItem())) {
                        case TaskStatus.TASK_ACCEPTED:
                            new ChangeTaskStatusTask(response.id, TaskStatus.TASK_ACCEPTED, null).execute();
                            break;
                        case TaskStatus.TASK_REJECTED:
                            new ChangeTaskStatusTask(response.id, TaskStatus.TASK_REJECTED, null).execute();
                            break;
                        case TaskStatus.TASK_CLOSED:
                            String description = mDescriptionEt.getText().toString();
                            if (description.isEmpty()) {
                                mDescriptionEt.setError(getString(R.string.err_empty_field));
                            } else {
                                new ChangeTaskStatusTask(response.id, TaskStatus.TASK_CLOSED, description).execute();
                            }
                            break;
                        default:
                            break;
                    }
                }
            });

            if (response.status == TaskStatus.TASK_REJECTED || response.status == TaskStatus.TASK_CLOSED) {
                mDescriptionEt.setEnabled(false);
                mDueDateBtn.setEnabled(false);
                ((RadioButton) findViewById(R.id.task_execution_singleUserRb)).setEnabled(false);
                ((RadioButton) findViewById(R.id.task_execution_allUserRb)).setEnabled(false);
                mStatusSpinner.setEnabled(false);
                mOkBtn.setEnabled(false);
            }

            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(TaskExecutionActivity.this, ex);
        }
    }
}
