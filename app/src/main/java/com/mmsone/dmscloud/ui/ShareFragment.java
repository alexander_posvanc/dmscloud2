package com.mmsone.dmscloud.ui;

import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.GetUserListTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.GroupAndDistributionManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;
import com.mmsone.dmscloud.ui.helper.MultiSelectionSpinner;

public class ShareFragment extends Fragment {

	public static final int DRAWER_LIST_POSITION = 5;

	private static final ILogger LOG = Logger.getLogger(ShareFragment.class);

	private PostActivity mActivity;
	private Button mOkBtn;
	private MultiSelectionSpinner mRecipientsSpinner;
	private EditText mDescriptionEt;
	private long mPostId;

	@Override
	public void onAttach(Activity activity) {
		mActivity = (PostActivity) activity;
		super.onAttach(activity);
		mActivity.setTitle(R.string.title_share);
		mActivity.setOptionMenuVisibility(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View shareView = inflater.inflate(R.layout.fragment_share, container, false);

		Bundle b = getArguments();
		mPostId = b.getLong("postId");

		mOkBtn = (Button) shareView.findViewById(R.id.share_okBtn);
		mOkBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		mRecipientsSpinner = (MultiSelectionSpinner) shareView.findViewById(R.id.share_recipientsSpinner);
		mRecipientsSpinner.setRecipientsLayout((LinearLayout) shareView.findViewById(R.id.share_recipientsLayout));

		mDescriptionEt = (EditText) shareView.findViewById(R.id.share_descriptionEt);
		mDescriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		((TextView) shareView.findViewById(R.id.share_recipientsTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		((TextView) shareView.findViewById(R.id.share_descriptionTitleTv)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				if (mDescriptionEt.isFocused())
					mDescriptionEt.clearFocus();

				mDescriptionEt.requestFocus();
				InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				manager.showSoftInput(mDescriptionEt, 0);
			}
		}, 200);
		
		return shareView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		new GetUserListTask(mActivity).execute();
		new GetGroupAndDistributionListTask().execute();
	}

	/**
	 * Set share fragment views
	 * 
	 * @throws com.mmsone.dmscloud.ex.DMSCloudDBException
	 */
	private void setViews() throws DMSCloudDBException {
		List<IUser> userList = DataBaseConnector.getInstance().getIUserList();
		userList.addAll(GroupAndDistributionManager.getInstance().getGroupList());
		userList.addAll(GroupAndDistributionManager.getInstance().getDistributionList());
		mRecipientsSpinner.setItems(userList);
		mRecipientsSpinner.setSelection(0);

		mOkBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new SharePostTaskTask(mPostId, mDescriptionEt.getText().toString(), mRecipientsSpinner.getSelectedUserIds(), mRecipientsSpinner.getSelectedGroupIds(), mRecipientsSpinner.getSelectedDistributionIds()).execute();
			}
		});
	}

	/**
	 * Downloads group and distribution list in background
	 */
	public class GetGroupAndDistributionListTask extends SafeAsyncTask<Void, Void, String> {

		private ProgressDialog mProgressDialog;

		public GetGroupAndDistributionListTask() {
			mProgressDialog = new ProgressDialog(getActivity());
			mProgressDialog.setCancelable(false);
			mProgressDialog.setMessage(getString(R.string.create_task_user_loading_message));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doWorkInBackground(Void... params) throws Exception {
			GroupAndDistributionManager.getInstance().parseDistributionList(new JSONObject(ServerConnector.getInstance().getDistributionListRequest()));
			return ServerConnector.getInstance().getGroupListRequest();
		}

		@Override
		protected void onSuccess(String response) {
			super.onSuccess(response);
			JSONObject groupListResponse = null;
			try {
				groupListResponse = new JSONObject(response);
				LOG.debug("GetGroupListTask - onSuccess: " + groupListResponse.toString());
				GroupAndDistributionManager.getInstance().parseGroupList(groupListResponse);

				setViews();
			} catch (Exception e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}
			mProgressDialog.dismiss();
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			mProgressDialog.dismiss();
			DialogHelper.showErrorDialog(getActivity(), ex);
		}
	}

	/**
	 * Share post in background
	 */
	public class SharePostTaskTask extends SafeAsyncTask<Void, Void, Boolean> {

		private long mPostId;
		private String mMessage;
		private Long[] mUserList;
		private Long[] mGroupList;
		private Long[] mDistributionList;

		public SharePostTaskTask(long postId, String message, Long[] userList, Long[] groupList, Long[] distributionList) {
			mPostId = postId;
			mMessage = message;
			mUserList = userList;
			mGroupList = groupList;
			mDistributionList = distributionList;
		}

		@Override
		protected Boolean doWorkInBackground(Void... params) throws Exception {
			return PostManager.getInstance().sharePost(mPostId, mMessage, mUserList, mGroupList, mDistributionList);
		}

		@Override
		protected void onSuccess(Boolean response) {
			super.onSuccess(response);
			if (response) {
				PrefManager.getInstance().setWallUpToDate(false);
				Toast.makeText(mActivity, R.string.task_share_succes_message, Toast.LENGTH_SHORT).show();
				mActivity.finish();
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			DialogHelper.showErrorDialog(getActivity(), ex);
		}
	}
}
