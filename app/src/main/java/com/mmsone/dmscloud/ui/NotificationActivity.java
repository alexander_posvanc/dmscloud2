package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.NotificationAdapter;
import com.mmsone.dmscloud.bo.bean.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 31..
 */
public class NotificationActivity extends ActionBarActivity {

    private NotificationAdapter mNotificationAdapter;
    private RecyclerView mNotificationRecyclerView;
    private Toolbar mToolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        mNotificationRecyclerView = (RecyclerView) findViewById(R.id.notification_RecyclerView);
        mNotificationAdapter = new NotificationAdapter(addNotifications());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager mLayoutManager = linearLayoutManager;

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.notification_activity_title));
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mNotificationRecyclerView.setLayoutManager(mLayoutManager);
        mNotificationRecyclerView.setHasFixedSize(true);
        mNotificationRecyclerView.setAdapter(mNotificationAdapter);

    }

    public List<Notification> addNotifications() {
        List<Notification> notifications = new ArrayList<>();

        Notification notificationHeader = new Notification();
        notificationHeader.notificationDate = System.currentTimeMillis();

        Notification notification = new Notification();
        notification.notificationName = "Sanyi";
        notification.notificationDate = 214241;
        notification.notificationText = "ÚJ FELADATOT OSZTOTTAK KI NEKED";

        Notification notification2 = new Notification();
        notification2.notificationName = "Dani";
        notification2.notificationDate = 214243;
        notification2.notificationText = "ÚJ HOZZÁSZÓLÁS ÉRKEZETT";

        notifications.add(notificationHeader);
        notifications.add(notification);
        notifications.add(notification);
        notifications.add(notification);
        notifications.add(notificationHeader);
        notifications.add(notification2);
        notifications.add(notification2);
        notifications.add(notification2);
        notifications.add(notification2);
        notifications.add(notification2);
        notifications.add(notification2);
        notifications.add(notification);


        return notifications;
    }


}
