package com.mmsone.dmscloud.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.listener.OnActivityFinishListener;
import com.mmsone.dmscloud.listener.OnAdvancedSarchListener;
import com.mmsone.dmscloud.ui.helper.SlidingFragmentBaseActivity;

public class AdvancedSearchActivity extends SlidingFragmentBaseActivity implements OnAdvancedSarchListener, OnActivityFinishListener {

	private Fragment mContent, mMenuFragment;

	private MenuItem mPostDetailMenu;

	public AdvancedSearchActivity() {
		super(R.string.app_name);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
			mMenuFragment = getSupportFragmentManager().getFragment(savedInstanceState, "mMenuFragment");
		}
		if (mContent == null) {
			mContent = new AdvancedSearchFragment();
		}
		if (mMenuFragment == null) {
			mMenuFragment = new AdvancedSearchOperationFragment();
		}

		setContentView(R.layout.view_post_content);
		getSupportFragmentManager().beginTransaction().replace(R.id.post_content_frameLayout, mContent).commit();

		setBehindContentView(R.layout.view_post_menu);
		getSupportFragmentManager().beginTransaction().replace(R.id.post_menu_frameLayout, mMenuFragment).commit();

		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		setSlidingActionBarEnabled(false);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
	}
	
	/**
	 * Switch fragment
	 * 
	 * @param fragment
	 */
	public void switchContent(Fragment fragment) {
		mContent = fragment;
		getSupportFragmentManager().beginTransaction().replace(R.id.post_content_frameLayout, fragment).commit();
		getSlidingMenu().showContent();
	}

	public void setOptionMenuVisibility(boolean visible) {
		if (mPostDetailMenu != null) {
			mPostDetailMenu.setVisible(visible);
		}
		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_post_detail, menu);
		mPostDetailMenu = menu.findItem(R.id.menu_item_post_detail_menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_post_detail_menu:
			getSlidingMenu().toggle(true);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Delete search filters 
	 */
	@Override
	public void onAdvancedSarchDelete() {
		getSlidingMenu().toggle(true);
		((AdvancedSearchFragment)mContent).onAdvancedSarchDelete();
		Toast.makeText(this, getString(R.string.search_filters_deleted_title), Toast.LENGTH_LONG).show();
	}

	/**
	 * Load search filters
	 */
	@Override
	public void onAdvancedSarchLoad() {
		getSlidingMenu().toggle(true);
		((AdvancedSearchFragment)mContent).onAdvancedSarchLoad();
	}
	
	/**
	 * Save search filters
	 */
	@Override
	public void onAdvancedSarchSave() {
		getSlidingMenu().toggle(true);
		((AdvancedSearchFragment)mContent).onAdvancedSarchSave();
	}

	@Override
	public void onFinish(boolean isSearch) {
		Intent returnIntent = new Intent();
		returnIntent.putExtra("isSearch",isSearch);
		setResult(RESULT_OK,returnIntent);
		finish();
	}
}
