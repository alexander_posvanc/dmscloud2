package com.mmsone.dmscloud.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.PostDrawerAdapter;
import com.mmsone.dmscloud.adapter.PostViewPagerAdapter;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static android.support.v4.view.ViewPager.OnPageChangeListener;

public class PostActivity extends ActionBarActivity implements OnPageChangeListener {

    private Fragment mContent, mMenuFragment;

    private MenuItem mPostDetailMenu;
    private Post mPost;
    private RelativeLayout mTaskTab, mMessageTab, mWorkFlowTab, mFileTab, mTabLayout;
    private LinearLayout mBottomTabs, mBottomMessageLayot;
    private ImageView mTaskImage, mMessageImage, mWorkFlowImage, mFileImage, mHideMessageLayout;
    private DrawerLayout mDrawerLayout;

    private Toolbar mToolbar;
    private CharSequence mDrawerTitle, mTitle;
    private RecyclerView mDrawerListView;
    private RelativeLayout bottomToolbarBtn1, bottomToolbarBtn2, bottomToolbarBtn3, postDrawerBtn;
    private ViewPager mViewPager;
    private PostViewPagerAdapter mPostViewPagerAdapter;
    private int mFragmentType;
    private Button mSendMessage;
    private EditText mMessageEt;
    private Intent mActivityIntent;

    public PostActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityIntent = getIntent();
        mPost = (Post) getIntent().getSerializableExtra("post");
        mFragmentType = getIntent().getIntExtra("type", 1);
        mPost.totalChildCount = mPost.totalChildCount + 10;

        Bundle args = new Bundle();
        args.putSerializable("post", mPost);

        if (mContent == null) {
            mContent = new PostDetailFragmentNew();
            mContent.setArguments(args);
        }

        setContentView(R.layout.view_post_content);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.addOnPageChangeListener(this);


        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setSupportActionBar(mToolbar);
        mTitle = mDrawerTitle = getTitle();
        setTitle(getResources().getString(R.string.post_activity_title));
        new NotificationButton(this).startNotificationFinish();

        mTaskImage = (ImageView) findViewById(R.id.post_content_tab1_image);
        mMessageImage = (ImageView) findViewById(R.id.post_content_tab2_image);
        mWorkFlowImage = (ImageView) findViewById(R.id.post_content_tab3_image);
        mFileImage = (ImageView) findViewById(R.id.post_content_tab4_image);

        mBottomMessageLayot = (LinearLayout) findViewById(R.id.post_message_layout);
        mBottomMessageLayot.setVisibility(View.GONE);
        slideMessageLayoutDown();
        mHideMessageLayout = (ImageView) findViewById(R.id.hide_messageLayout);
        bottomToolbarBtn1 = (RelativeLayout) findViewById(R.id.bottom_tool_bar_button1);
        bottomToolbarBtn2 = (RelativeLayout) findViewById(R.id.bottom_tool_bar_button2);
        bottomToolbarBtn3 = (RelativeLayout) findViewById(R.id.bottom_tool_bar_button3);
        mTabLayout = (RelativeLayout) findViewById(R.id.tool_bar_tab);
        mBottomTabs = (LinearLayout) findViewById(R.id.bottom_tab_layout);
        mSendMessage = (Button) findViewById(R.id.post_message_SendBtn);
        mMessageEt = (EditText) findViewById(R.id.post_message_Et);

        postDrawerBtn = (RelativeLayout) findViewById(R.id.post_drawer_btn);


        if (mPost.closed) {

        } else {
            mDrawerListView = (RecyclerView) findViewById(R.id.post_drawer_list);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mDrawerListView.setLayoutManager(mLayoutManager);
            mDrawerListView.setHasFixedSize(true);

            PostDrawerAdapter postDrawerAdapter = new PostDrawerAdapter(this, mPost);
            mDrawerListView.setAdapter(postDrawerAdapter);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.post_drawerLayout);
            mDrawerLayout.openDrawer(Gravity.RIGHT);
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        }

        postDrawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(PostActivity.this, getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        mDrawerLayout.openDrawer(Gravity.RIGHT);
                    }
                }
            }
        });

        bottomToolbarBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(PostActivity.this, getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    createTask();
                }
            }
        });

        bottomToolbarBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(PostActivity.this, getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    createWfTask();
                }
            }
        });

        bottomToolbarBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPost.closed) {
                    Toast.makeText(PostActivity.this, getResources().getString(R.string.title_closed_task), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(PostActivity.this, FileUploadActivity.class);
                    intent.putExtra("postId", mPost.id);
                    startActivity(intent);
                }

            }
        });

        mTaskTab = (RelativeLayout) findViewById(R.id.post_content_tab1);
        mTaskTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0, true);
                setFirstTabSelected();
            }
        });
        mMessageTab = (RelativeLayout) findViewById(R.id.post_content_tab2);
        mMessageTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1, true);
                setSecondTabSelected();
            }
        });
        mWorkFlowTab = (RelativeLayout) findViewById(R.id.post_content_tab3);
        mWorkFlowTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2, true);
                setThirdTabSelected();
            }
        });
        mFileTab = (RelativeLayout) findViewById(R.id.post_content_tab4);
        mFileTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(3, true);
                setFourthTabSelected();
            }
        });

        mHideMessageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideMessageLayoutDown();
            }
        });

        mSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageEt.getText().toString().isEmpty()) {
                    mMessageEt.setError(getString(R.string.err_empty_field));
                } else {
                    new CreatePostCommentTask(mPost.id, mMessageEt.getText().toString()).execute();
                }
            }
        });

        if (PrefManager.getInstance().isFirst()) {
            new GetWallPostChildTask().execute();
            PrefManager.getInstance().setIsFirst(false);
        }

        if (mPost.closed) {
//            getSlidingMenu().setSlidingEnabled(false);
        } else {
//            getSlidingMenu().setSlidingEnabled(true);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (PrefManager.getInstance().isChanged()) {
//            finish();
//        } else {
            Log.d("ONRESTART", "ONRESTART");
            mPost.totalChildCount = mPost.totalChildCount + 10;
            new GetWallPostChildTask().execute();
            setFirstTabSelected();
            if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    /**
     * Switch fragment
     *
     * @param fragment
     */
    public void switchContent(Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.post_content_frameLayout, fragment).commit();
//        getSlidingMenu().showContent();
    }

    public void setOptionMenuVisibility(boolean visible) {
        if (mPostDetailMenu != null) {
//            mPostDetailMenu.setVisible(visible);
        }
//        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_post_detail, menu);
//        mPostDetailMenu = menu.findItem(R.id.menu_item_post_detail_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_post_detail_menu:
                if (mPost.closed) {
                    Toast.makeText(getApplicationContext(), getString(R.string.title_closed_task), Toast.LENGTH_LONG).show();
                } else {
//                    getSlidingMenu().toggle(true);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //Show dialog to upload files(Maybe gets deleted)
    public void showFileDialog() {
        final Dialog dialog = new Dialog(PostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.file_upload_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        Drawable d = new ColorDrawable(Color.BLACK);
        d.setAlpha(0);
        dialog.getWindow().setBackgroundDrawable(d);

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        Window window = dialog.getWindow();
        wmlp.copyFrom(window.getAttributes());
        wmlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wmlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(wmlp);

        wmlp.gravity = Gravity.TOP;
        wmlp.y = 140;

        dialog.show();

        LinearLayout newPhoto, newFile;
        newPhoto = (LinearLayout) dialog.findViewById(R.id.dialog_photo_upload);
        newFile = (LinearLayout) dialog.findViewById(R.id.dialog_file_upload);
        newPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        newFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostActivity.this, FileUploadActivity.class));
                dialog.dismiss();
            }
        });
    }

    /**
     * Hide layout for adding comment
     */
    public void slideMessageLayoutDown() {
        mBottomMessageLayot.animate().translationY(200);
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                mBottomMessageLayot.setVisibility(View.GONE);
            }
        };
        handler.postDelayed(r, 200);

        //keyboard hide
        View view = this.getCurrentFocus();
        if (view != null) {
            final InputMethodManager inputMethodManager = (InputMethodManager) PostActivity.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show layout for adding comment
     */
    public void slideMessageLayoutUp() {
        mBottomMessageLayot.setVisibility(View.VISIBLE);
        mBottomMessageLayot.animate().translationY(0);
        mMessageEt.setFocusable(true);
        mMessageEt.requestFocus();
        //keyboard show
        final InputMethodManager inputMethodManager = (InputMethodManager) PostActivity.this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(mMessageEt, InputMethodManager.SHOW_IMPLICIT);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                setFirstTabSelected();
                break;
            case 1:
                setSecondTabSelected();
                break;
            case 2:
                setThirdTabSelected();
                break;
            case 3:
                setFourthTabSelected();
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * Downloads topic childs in background
     */
    public class GetWallPostChildTask extends SafeAsyncTask<Void, Void, Post> {

        private ProgressDialog mDialog;

        public GetWallPostChildTask() {
            mDialog = new ProgressDialog(PostActivity.this);
            mDialog.setMessage(getString(R.string.loading_wall_dialog_message));
            mDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            mDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Post doWorkInBackground(Void... params) throws Exception {
            Post post = PostManager.getInstance().parsePostChilds(mPost);

            Set<Long> userIdSet = new HashSet<Long>();
            Map<Long, User> userMap = new HashMap<Long, User>();

            userIdSet.add(post.createdBy);
            for (IPostChild child : post.childList) {
                userIdSet.add(child.getChildCreatedBy());
                if (child.getChildList() != null) {
                    for (IPostChild subChild : child.getChildList()) {
                        userIdSet.add(subChild.getChildCreatedBy());
                    }
                }
            }

            List<User> userList = DataBaseConnector.getInstance().getUserList(userIdSet);

            for (User user : userList) {
                userMap.put(user.id, user);
            }

            post.user = userMap.get(post.createdBy);
            for (IPostChild child : post.childList) {
                child.setUser(userMap.get(child.getChildCreatedBy()));
                if (child.getChildList() != null) {
                    for (IPostChild subChild : child.getChildList()) {
                        subChild.setUser(userMap.get(subChild.getChildCreatedBy()));
                    }
                }
            }
            return post;
        }

        @Override
        protected void onSuccess(Post post) {
            super.onSuccess(post);
            mPost = post;
            Bundle args = new Bundle();
            args.putSerializable("post", mPost);
            switch (mFragmentType) {
                case 1:
                    mPostViewPagerAdapter = new PostViewPagerAdapter(getSupportFragmentManager(), args);
                    mViewPager.setAdapter(mPostViewPagerAdapter);
                    break;
                case 2:
//                    mTabLayout.setVisibility(View.GONE);
                    mWorkFlowTab.setVisibility(View.GONE);
                    mMessageTab.setVisibility(View.GONE);
                    mFileTab.setVisibility(View.GONE);
                    mBottomTabs.setVisibility(View.GONE);
                    TechnicalPostDetailFragment technicalPostDetailFragment = new TechnicalPostDetailFragment();
                    technicalPostDetailFragment.setArguments(args);
                    getSupportFragmentManager().beginTransaction().replace(R.id.post_content_frameLayout, technicalPostDetailFragment).commit();
                    break;
            }

            mDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mDialog.dismiss();
            DialogHelper.showErrorDialog(PostActivity.this, ex);
        }
    }

    /**
     * Create post comment in background
     */
    public class CreatePostCommentTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mPostId;
        private String mDescription;

        public CreatePostCommentTask(long postId, String description) {
            mPostId = postId;
            mDescription = description;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostComment(mPostId, mDescription);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setWallUpToDate(false);
                PrefManager.getInstance().setIsFirst(true);
                Intent intent = mActivityIntent;
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                finish();
                Toast.makeText(PostActivity.this, R.string.post_sub_comment_create_succes_message, Toast.LENGTH_SHORT).show();
                PostActivity.this.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(PostActivity.this, ex);
        }
    }


    /**
     * Add comments to post
     */
    public void addCommentToPost() {
        Intent intent = new Intent(this, SharePostActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("post", mPost);
        args.putInt("fragment", 0);
        intent.putExtras(args);
        startActivity(intent);
    }

    /**
     * Create new Task to post
     */
    public void createTask() {
        Intent intent = new Intent(this, SharePostActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("post", mPost);
        args.putInt("fragment", 2);
        intent.putExtras(args);
        startActivity(intent);
    }

    /**
     * Create new WorkFlowTask to post
     */
    public void createWfTask() {
        Intent intent = new Intent(this, SharePostActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("post", mPost);
        args.putInt("fragment", 1);
        intent.putExtras(args);
        startActivity(intent);
    }

    /**
     * Getter for starting Intent
     *
     * @return
     */
    public Intent getmActivityIntent() {
        return mActivityIntent;
    }

    /**
     * First tab selected, layout and color changes
     */
    public void setFirstTabSelected() {
        mTaskTab.setBackgroundColor(Color.parseColor("#ffffff"));
        mTaskImage.setImageDrawable(getResources().getDrawable(R.drawable.tema));
        mMessageTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mMessageImage.setImageDrawable(getResources().getDrawable(R.drawable.hozzaszolas_feher));
        mWorkFlowTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mWorkFlowImage.setImageDrawable(getResources().getDrawable(R.drawable.feladatok_feher));
        mFileTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mFileImage.setImageDrawable(getResources().getDrawable(R.drawable.dokumentumok_feher));
    }

    /**
     * Second tab selected, layout and color changes
     */
    public void setSecondTabSelected() {
        mMessageTab.setBackgroundColor(Color.parseColor("#ffffff"));
        mTaskImage.setImageDrawable(getResources().getDrawable(R.drawable.tema_feher));
        mMessageImage.setImageDrawable(getResources().getDrawable(R.drawable.hozzaszolas));
        mWorkFlowImage.setImageDrawable(getResources().getDrawable(R.drawable.feladatok_feher));
        mFileImage.setImageDrawable(getResources().getDrawable(R.drawable.dokumentumok_feher));
        mTaskTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mWorkFlowTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mFileTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
    }

    /**
     * Third tab selected, layout and color changes
     */
    public void setThirdTabSelected() {
        mWorkFlowTab.setBackgroundColor(Color.parseColor("#ffffff"));
        mTaskImage.setImageDrawable(getResources().getDrawable(R.drawable.tema_feher));
        mWorkFlowImage.setImageDrawable(getResources().getDrawable(R.drawable.feladatok_kek));
        mFileImage.setImageDrawable(getResources().getDrawable(R.drawable.dokumentumok_feher));
        mMessageImage.setImageDrawable(getResources().getDrawable(R.drawable.hozzaszolas_feher));
        mMessageTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mTaskTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mFileTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
    }

    /**
     * Fourth tab selected, layout and color changes
     */
    public void setFourthTabSelected() {
        mFileTab.setBackgroundColor(Color.parseColor("#ffffff"));
        mTaskImage.setImageDrawable(getResources().getDrawable(R.drawable.tema_feher));
        mFileImage.setImageDrawable(getResources().getDrawable(R.drawable.dokumentumok));
        mMessageImage.setImageDrawable(getResources().getDrawable(R.drawable.hozzaszolas_feher));
        mWorkFlowImage.setImageDrawable(getResources().getDrawable(R.drawable.feladatok_feher));
        mMessageTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mTaskTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
        mWorkFlowTab.setBackgroundColor(getResources().getColor(R.color.color_secondary_drawer_item));
    }
}