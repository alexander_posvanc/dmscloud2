package com.mmsone.dmscloud.ui;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PoolObjectFactory;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.ViewPoolManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.DMSCloudViewBuilder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PostDetailFragment extends Fragment {

	private DisplayImageOptions mDisplayImageOptions;
	private ImageView mProfileIv;
	private TextView mCreatedOnDateTv, mCreatedOnTimeTv, mTopicIdTv, mCreatedByTv, mTitleTv, mMessageTv;
	private LinearLayout mMetaLayout, mSubPostLayout, mDocIconLayout;
	private RelativeLayout mSubLayoutContainer;
	private Post mPost;
	private ViewPoolManager mViewPoolManager;
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View postDetailView = inflater.inflate(R.layout.fragment_post_detail, container, false);
		
		/**
		 * List item view pool
		 */
		mViewPoolManager = new ViewPoolManager(new PoolObjectFactory<EPostChildType, View>() {

			@Override
			public View create(EPostChildType key) {
				switch (key) {
				case PostChat:
					return View.inflate(getActivity(), R.layout.list_item_post_chat, null);
				case PostComment:
					return View.inflate(getActivity(), R.layout.list_item_post_comment, null);
				case PostEmail:
					return View.inflate(getActivity(), R.layout.list_item_post_email, null);
				case PostSubComment:
					return View.inflate(getActivity(), R.layout.list_item_post_sub_comment, null);
				case PostSubTask:
					return View.inflate(getActivity(), R.layout.list_item_post_sub_task, null);
				case PostTask:
					return View.inflate(getActivity(), R.layout.list_item_post_task, null);
				case PostWfTask:
					return View.inflate(getActivity(), R.layout.list_item_post_wftask, null);
				}
				return null;
			}
		});

		mDisplayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();

		mProfileIv = (ImageView) postDetailView.findViewById(R.id.post_detail_profileIv);
		mCreatedOnDateTv = (TextView) postDetailView.findViewById(R.id.post_detail_createdOnDateTv);
		mCreatedOnTimeTv = (TextView) postDetailView.findViewById(R.id.post_detail_createdOnTimeTv);
		mTopicIdTv = (TextView) postDetailView.findViewById(R.id.post_detail_postIdTv);
		mCreatedByTv = (TextView) postDetailView.findViewById(R.id.post_detail_createdByTv);
		mTitleTv = (TextView) postDetailView.findViewById(R.id.post_detail_titleTv);
		mMessageTv = (TextView) postDetailView.findViewById(R.id.post_detail_messageTv);
		mMetaLayout = (LinearLayout) postDetailView.findViewById(R.id.post_detail_metaLayout);
		mSubPostLayout = (LinearLayout) postDetailView.findViewById(R.id.post_detail_subLayout);
		mDocIconLayout = (LinearLayout) postDetailView.findViewById(R.id.post_detail_docIconLL);
		mSubLayoutContainer = (RelativeLayout) postDetailView.findViewById(R.id.post_detail_subLayoutContainer);

		Bundle b = getArguments();
		mPost = (Post) b.getSerializable("post");

//		getActivity().setTitle(mPost.title);

		User user = null;
		try {
			user = DataBaseConnector.getInstance().getUser(mPost.createdBy);
		} catch (DMSCloudDBException e) {
			DialogHelper.showErrorDialog(getActivity(), e);
		}
		
		switch (mPost.postType) {
		case EMAIL:
			mProfileIv.setImageResource(R.drawable.email_kek);
			break;
		case MOBILE:
			mProfileIv.setImageResource(R.drawable.mobil_kek);
			break;
		case SCAN_FILE:
			mProfileIv.setImageResource(R.drawable.scanner_kek);
			break;
		case WEB_SERVICE:
			mProfileIv.setImageResource(R.drawable.webservice_kek);
			break;
		default:
			if (user != null && user.image != null) {
				ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + user.image, mProfileIv, mDisplayImageOptions);
			}
			break;
		}

		mCreatedOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(mPost.createdOn)));
		mCreatedOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		mCreatedOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(mPost.createdOn)));
		mCreatedOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		mTopicIdTv.setText("#" + mPost.postNumber);
		mTopicIdTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
		if(user != null){
			mCreatedByTv.setText(user.formatedName + " (" + mPost.overAllCount + ")");
		}
		mCreatedByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

		mTitleTv.setText(mPost.title);
		mTitleTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
		mMessageTv.setText(mPost.message);
		mMessageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		if (mPost.documentId != 0) {
			mDocIconLayout.setVisibility(View.VISIBLE);
			mDocIconLayout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Fragment fragment = new DocumentFragment();
					Bundle args = new Bundle();
					args.putLong("documentId", mPost.documentId);
					fragment.setArguments(args);
					((PostActivity) getActivity()).switchContent(fragment);
				}
			});
			DMSCloudViewBuilder.setMenuTouchDelegateArea(postDetailView, mDocIconLayout);

		} else {
			mDocIconLayout.setVisibility(View.INVISIBLE);
		}

		DMSCloudViewBuilder.addMetaViews(inflater, mMetaLayout, mPost.metaList);
		
		return postDetailView;
	}

	/**
	 * Refresh post detil
	 */
	public void detailRefresh() {
		new GetWallPostChildTask().execute();
	}
	
	@Override
	public void onResume() {
		new GetWallPostChildTask().execute();
		super.onResume();
	}

	/**
	 * Downloads topic childs in background
	 */
	public class GetWallPostChildTask extends SafeAsyncTask<Void, Void, Post> {

		private ProgressDialog mDialog;

		public GetWallPostChildTask() {
			mDialog = new ProgressDialog(getActivity());
			mDialog.setMessage(getString(R.string.loading_wall_dialog_message));
			mDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			mDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Post doWorkInBackground(Void... params) throws Exception {
			Post post = PostManager.getInstance().parsePostChilds(mPost);

			Set<Long> userIdSet = new HashSet<Long>();
			Map<Long, User> userMap = new HashMap<Long, User>();

			userIdSet.add(post.createdBy);
			for (IPostChild child : post.childList) {
				userIdSet.add(child.getChildCreatedBy());
				if(child.getChildList() != null){
					for (IPostChild subChild : child.getChildList()) {
						userIdSet.add(subChild.getChildCreatedBy());
					}
				}
			}

			List<User> userList = DataBaseConnector.getInstance().getUserList(userIdSet);

			for (User user : userList) {
				userMap.put(user.id, user);
			}

			post.user = userMap.get(post.createdBy);
			for (IPostChild child : post.childList) {
				child.setUser(userMap.get(child.getChildCreatedBy()));
				if(child.getChildList() != null){
					for (IPostChild subChild : child.getChildList()) {
						subChild.setUser(userMap.get(subChild.getChildCreatedBy()));
					}
				}
			}

			return post;
		}

		@Override
		protected void onSuccess(Post post) {
			super.onSuccess(post);
			if (post.childList.isEmpty()) {
				mSubLayoutContainer.setVisibility(View.GONE);
			} else {
				mSubLayoutContainer.setVisibility(View.VISIBLE);
				DMSCloudViewBuilder.addPostTaskViews(getActivity(), mDisplayImageOptions, mSubPostLayout, post.childList, true, PostDetailFragment.this, null, null, true, mViewPoolManager);
			}
			mDialog.dismiss();
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			mDialog.dismiss();
			DialogHelper.showErrorDialog(getActivity(), ex);
		}
	}
}
