package com.mmsone.dmscloud.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.CodeStoreArrayAdapter;
import com.mmsone.dmscloud.adapter.CustomerArrayAdapter;
import com.mmsone.dmscloud.adapter.ShareAdapter;
import com.mmsone.dmscloud.adapter.ShareDialogAdapter;
import com.mmsone.dmscloud.adapter.UserArrayAdapter;
import com.mmsone.dmscloud.async.GetUserListTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.CodeStoreManager;
import com.mmsone.dmscloud.bl.CustomerManager;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.GroupAndDistributionManager;
import com.mmsone.dmscloud.bl.MetaManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.ComponentType;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.MetaDataType;
import com.mmsone.dmscloud.bo.bean.CodeStore;
import com.mmsone.dmscloud.bo.bean.Customer;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.MetaDef;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.DMSHelper;
import com.mmsone.dmscloud.ui.helper.MultiSelectionSpinner;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

public class CreatePostActivity extends ActionBarActivity {

    public static final int DRAWER_LIST_POSITION = 3;

    private RelativeLayout mOkBtn;
    private EditText mTitleEt, mDescriptionEt;
    private LinearLayout mMetaLayout, mRecipientsLayout;
    private LinearLayout spinnerLinearLayout;
    private List<IUser> mUserList;
    private ShareDialogAdapter mShareDialogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        setTitle(R.string.title_create_post);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new NotificationButton(this).hideDmsIcon();


        mOkBtn = (RelativeLayout) findViewById(R.id.create_post_okBtn);
        mDescriptionEt = (EditText) findViewById(R.id.create_post_descriptionEt);
        mDescriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mTitleEt = (EditText) findViewById(R.id.create_post_titleEt);
        mTitleEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
//        mRecipientsSpinner = (MultiSelectionSpinner) findViewById(R.id.create_post_recipientsSpinner);
        mMetaLayout = (LinearLayout) findViewById(R.id.create_post_metaLyout);

        spinnerLinearLayout = (LinearLayout) findViewById(R.id.multiselector_dialog);
        mRecipientsLayout = (LinearLayout) findViewById(R.id.create_post_recipientsLayout);
//        mRecipientsSpinner.setRecipientsLayout(mRecipientsLayout);

        ((TextView) findViewById(R.id.create_post_mate_data_titleTv)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        ((TextView) findViewById(R.id.create_post_recipientsTitleTv)).setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        ((TextView) findViewById(R.id.create_post_titleTv)).setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        ((TextView) findViewById(R.id.create_post_descriptionTv)).setTypeface(FontManager.getInstance().getRobotoBoldTtf());

        MetaManager.getInstance().clearFillMetaMap();

        new GetUserListTask(this).execute();
        new GetGroupAndDistributionListTask().execute();
        new GetMetaDefListTask(MetaDataType.METADATA_COMMON_TYPE).execute();


    }


    /**
     * Create Dialog for choosing recipients
     *
     * @param userList
     */
    public void createRecipientDialog(List<IUser> userList) {
        RecyclerView mShareRecycleView;
        EditText mSearchView;
        Button closeBtn;
//        ShareDialogAdapter shareDialogAdapter = new ShareDialogAdapter(userList, CreatePostActivity.this, mRecipientsLayout);

        final Dialog dialog = new Dialog(CreatePostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.share_dialog_layout);
        mShareRecycleView = (RecyclerView) dialog.findViewById(R.id.share_fragment_recyclerView);
        closeBtn = (Button) dialog.findViewById(R.id.close_dialog_btn);
        mSearchView = (EditText) dialog.findViewById(R.id.share_searchEt);
        mSearchView.addTextChangedListener(new SearchWatcher(mSearchView, mShareDialogAdapter, userList));

        closeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CreatePostActivity.this);
        mShareRecycleView.setAdapter(mShareDialogAdapter);
        mShareRecycleView.setLayoutManager(mLayoutManager);
        mShareRecycleView.setHasFixedSize(true);

        dialog.show();


    }

    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {
        private EditText editText;
        private List<IUser> userList;
        private ShareDialogAdapter shareDialogAdapter;

        public SearchWatcher(EditText editText, ShareDialogAdapter shareDialogAdapter, List<IUser> userList) {
            this.editText = editText;
            this.userList = userList;
            this.shareDialogAdapter = shareDialogAdapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String searchQuery = editText.getText().toString();
            List<IUser> filteredList = DMSHelper.performSearch(userList, searchQuery);
            shareDialogAdapter.updateList(filteredList);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    public List<MetaDef> mMetaDefList;

    /**
     * Build meta views
     *
     * @param metaList
     * @throws DMSCloudDBException
     */
    public void buildMetaViews(final List<MetaDef> metaList) throws DMSCloudDBException {
        mMetaDefList = metaList;
        for (final MetaDef meta : metaList) {

            View metaView = View.inflate(CreatePostActivity.this, R.layout.view_post_fill_meta, null);

            final TextView labelTv = (TextView) metaView.findViewById(R.id.create_post_meta_labelTv);
            final Spinner spinner = (Spinner) metaView.findViewById(R.id.create_post_meta_valueSpinner);
            final EditText valueEt = (EditText) metaView.findViewById(R.id.create_post_meta_valueEt);
            valueEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            final Button dateBtn = (Button) metaView.findViewById(R.id.create_post_meta_dateValueBTn);
            dateBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            final Button timeBtn = (Button) metaView.findViewById(R.id.create_post_meta_timeValueBTn);
            timeBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
            final Button button = (Button) metaView.findViewById(R.id.create_post_meta_valueBTn);
            button.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

            labelTv.setText(meta.label);
            labelTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

            if (meta.required) {
                labelTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            }

            switch (meta.componentType) {

                case ComponentType.TIME:
                    final Calendar calendar = Calendar.getInstance();
                    timeBtn.setVisibility(View.VISIBLE);
                    timeBtn.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            TimePickerDialog tpd = new TimePickerDialog(CreatePostActivity.this, new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    try {
                                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        calendar.set(Calendar.MINUTE, minute);
                                        timeBtn.setText(Config.TIME_FORMAT.format(calendar.getTime()));
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, calendar.getTimeInMillis()));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }

                                }
                            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                            tpd.show();
                        }
                    });

                    if (meta.defaultValue != null && meta.defaultValue.dateValue != null) {
                        timeBtn.setText(Config.TIME_FORMAT.format(meta.defaultValue.dateValue));

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.dateValue));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    if (meta.readOnly) {
                        timeBtn.setEnabled(false);
                    }
                    break;
                case ComponentType.ADDRESS:
                    final AddressDialog dialog = new AddressDialog(CreatePostActivity.this, button);
                    button.setVisibility(View.VISIBLE);
                    button.setText(R.string.address_dialog_address_title);
                    button.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            dialog.show();
                        }
                    });

                    if (meta.defaultValue != null) {
                        button.setText(meta.defaultValue.value);

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    if (meta.readOnly) {
                        button.setEnabled(false);
                    }
                    break;
                case ComponentType.USER_AND_GROUP:
                    spinner.setVisibility(View.VISIBLE);
                    List<IUser> list = new ArrayList<IUser>();
                    if (!meta.required) {
                        list.add(null);
                    }
                    list.addAll(DataBaseConnector.getInstance().getIUserList());
                    ArrayAdapter<IUser> dataAdapter = new UserArrayAdapter(CreatePostActivity.this, R.layout.view_user_spinner_item, list.toArray(new IUser[]{}));
                    spinner.setAdapter(dataAdapter);

                    spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            IUser iUser = (IUser) spinner.getSelectedItem();
                            if (iUser != null) {
                                try {
                                    switch (iUser.getType()) {
                                        case GROUP:
                                            MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "1" + iUser.getId()));
                                            break;
                                        case USER:
                                            MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "0:" + iUser.getId()));
                                            break;
                                        default:
                                            break;
                                    }
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            MetaManager.getInstance().removeFillMeta(meta.id);
                        }
                    });

                    if (meta.readOnly) {
                        spinner.setEnabled(false);
                    }

                    if (meta.defaultValue != null) {
                        long value = Long.parseLong(meta.defaultValue.value.substring(2));
                        int index = 0;
                        for (int i = 0; i < spinner.getCount(); i++) {
                            IUser user = (IUser) spinner.getItemAtPosition(i);
                            if (user != null) {
                                if (user.toString().equalsIgnoreCase(DataBaseConnector.getInstance().getUser(value).formatedName)) {
                                    index = i;
                                    i = spinner.getCount();
                                }
                            }
                        }
                        spinner.setSelection(index);

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }
                    break;
                case ComponentType.NUMBER:
                    valueEt.setVisibility(View.VISIBLE);
                    valueEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                    if (meta.defaultValue != null) {
                        valueEt.setText(meta.defaultValue.value);

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    valueEt.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().isEmpty()) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                if (meta.required) {
                                    valueEt.setError(getString(R.string.err_empty_field));
                                }
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }
                    });

                    if (meta.required && valueEt.getText().toString().isEmpty()) {
                        valueEt.setError(getString(R.string.err_empty_field));
                    }

                    if (meta.readOnly) {
                        valueEt.setEnabled(false);
                    }
                    break;
                case ComponentType.CURRENCY:
                    valueEt.setVisibility(View.VISIBLE);
                    valueEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                    if (meta.defaultValue != null) {
                        valueEt.setText(meta.defaultValue.value);

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    valueEt.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().isEmpty()) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                if (meta.required) {
                                    valueEt.setError(getString(R.string.err_empty_field));
                                }
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }
                    });

                    if (meta.required && valueEt.getText().toString().isEmpty()) {
                        valueEt.setError(getString(R.string.err_empty_field));
                    }

                    if (meta.readOnly) {
                        valueEt.setEnabled(false);
                    }
                    break;
                case ComponentType.AUTO_NUMBER:
                    valueEt.setVisibility(View.VISIBLE);
                    valueEt.setText(meta.fieldName + " - " + meta.expectedValue);
                    valueEt.setEnabled(false);

                    if (meta.required && meta.defaultValue != null && meta.defaultValue.value != null) {
                        try {
                            MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                        } catch (JSONException e) {
                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                        }
                    }

                    if (meta.readOnly) {
                        valueEt.setEnabled(false);
                    }
                    break;
                case ComponentType.CUSTOMER:
                    spinner.setVisibility(View.VISIBLE);
                    new GetCustomerListTask(meta, spinner).execute();

                    spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Customer customer = (Customer) spinner.getSelectedItem();
                            if (customer != null) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + customer.id));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            MetaManager.getInstance().removeFillMeta(meta.id);
                        }
                    });
                    if (meta.readOnly) {
                        spinner.setEnabled(false);
                    }
                    break;
                case ComponentType.COMBO:
                    spinner.setVisibility(View.VISIBLE);
                    new GetCodeStoreTask(meta, spinner).execute();

                    spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            CodeStore codeStore = (CodeStore) spinner.getSelectedItem();
                            if (codeStore != null) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + codeStore.id));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            MetaManager.getInstance().removeFillMeta(meta.id);
                        }
                    });

                    if (meta.readOnly) {
                        spinner.setEnabled(false);
                    }
                    break;
                case ComponentType.LIST:
                    spinner.setVisibility(View.VISIBLE);
                    new GetCodeStoreTask(meta, spinner).execute();

                    spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            CodeStore codeStore = (CodeStore) spinner.getSelectedItem();
                            if (codeStore != null) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + codeStore.id));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            MetaManager.getInstance().removeFillMeta(meta.id);
                        }
                    });
                    if (meta.readOnly) {
                        spinner.setEnabled(false);
                    }
                    break;
                case ComponentType.TEXT:
                    valueEt.setVisibility(View.VISIBLE);
                    valueEt.setInputType(InputType.TYPE_CLASS_TEXT);

                    if (meta.defaultValue != null) {
                        valueEt.setText(meta.defaultValue.value);

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    valueEt.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().isEmpty()) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            } else {
                                if (meta.required) {
                                    valueEt.setError(getString(R.string.err_empty_field));
                                }
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }
                    });
                    if (meta.readOnly) {
                        valueEt.setEnabled(false);
                    }
                    break;
                case ComponentType.DATE:
                    final DatePickerFragment datePickerFragment = new DatePickerFragment(dateBtn, meta);

                    dateBtn.setVisibility(View.VISIBLE);
                    dateBtn.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            datePickerFragment.show(getSupportFragmentManager(), "datePicker");
                        }
                    });

                    if (meta.defaultValue != null && meta.defaultValue.dateValue != null) {
                        dateBtn.setText(Config.DATE_FORMAT.format(meta.defaultValue.dateValue));

                        if (meta.required) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.defaultValue.dateValue));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }
                    }

                    if (meta.readOnly) {
                        dateBtn.setEnabled(false);
                    }
                    break;
            }
            mMetaLayout.addView(metaView);
        }
    }

    /**
     * Create date picker dialog
     */
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private Button mButton;
        private final Calendar mCalendar = Calendar.getInstance();
        private MetaDef mMeta;

        public DatePickerFragment(Button button, MetaDef meta) {
            mButton = button;
            mMeta = meta;
            mButton.setText(Config.DATE_FORMAT.format(mCalendar.getTime()));
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar.set(year, month, day, 23, 59);
            mButton.setText(Config.DATE_FORMAT.format(mCalendar.getTime()));

            try {
                MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, mCalendar.getTimeInMillis()));
            } catch (JSONException e) {
                DialogHelper.showErrorDialog(getActivity(), e);
            }
        }
    }

    /**
     * Downloads customer list in background
     */
    public class GetCustomerListTask extends SafeAsyncTask<Void, Void, List<Customer>> {

        private Spinner mSpinner;
        private MetaDef mMeta;

        public GetCustomerListTask(MetaDef meta, Spinner customerSpinner) {
            mSpinner = customerSpinner;
            mMeta = meta;
        }

        @Override
        protected List<Customer> doWorkInBackground(Void... params) throws Exception {
            return CustomerManager.getInstance().parseCostumerList();
        }

        @Override
        protected void onSuccess(List<Customer> response) {
            super.onSuccess(response);
            List<Customer> list = new ArrayList<Customer>();

            if (!mMeta.required) {
                list.add(null);
            }
            list.addAll(response);
            ArrayAdapter<Customer> dataAdapter = new CustomerArrayAdapter(CreatePostActivity.this, R.layout.view_user_spinner_item, list.toArray(new Customer[]{}));
            mSpinner.setAdapter(dataAdapter);

            if (mMeta.defaultValue != null) {
                long value = Long.parseLong(mMeta.defaultValue.value);
                Customer currentCustomer = null;

                for (Customer customer : response) {
                    if (customer.id == value) {
                        currentCustomer = customer;
                    }
                }

                if (currentCustomer != null) {

                    int index = 0;
                    for (int i = 0; i < mSpinner.getCount(); i++) {
                        Customer customer = (Customer) mSpinner.getItemAtPosition(i);
                        if (customer != null) {
                            if (customer.toString().equalsIgnoreCase(currentCustomer.toString())) {
                                index = i;
                                i = mSpinner.getCount();
                            }
                        }
                    }
                    mSpinner.setSelection(index);

                    if (mMeta.required) {
                        try {
                            MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, "" + value));
                        } catch (JSONException e) {
                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                        }
                    }
                }
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(CreatePostActivity.this, ex);
        }
    }

    /**
     * Downloads code store in background
     */
    public class GetCodeStoreTask extends SafeAsyncTask<Void, Void, List<CodeStore>> {

        private Spinner mSpinner;
        private MetaDef mMeta;

        public GetCodeStoreTask(MetaDef meta, Spinner customerSpinner) {
            mSpinner = customerSpinner;
            mMeta = meta;
        }

        @Override
        protected List<CodeStore> doWorkInBackground(Void... params) throws Exception {
            return CodeStoreManager.getInstance().parseCodeStoreList(mMeta.codeStoreId);
        }

        @Override
        protected void onSuccess(List<CodeStore> response) {
            super.onSuccess(response);

            List<CodeStore> list = new ArrayList<CodeStore>();
            if (mMeta != null && !mMeta.required) {
                list.add(null);
            }
            list.addAll(response);
            ArrayAdapter<CodeStore> dataAdapter = new CodeStoreArrayAdapter(CreatePostActivity.this, R.layout.view_user_spinner_item, list.toArray(new CodeStore[]{}));
            mSpinner.setAdapter(dataAdapter);

            if (mMeta != null && mMeta.defaultValue != null && mMeta.defaultValue.value != null) {
                String[] values = mMeta.defaultValue.value.split(";");
                long value = 0;
                String firstValue = values[0];
                if (!TextUtils.isEmpty(firstValue)) {
                    value = Long.parseLong(firstValue);
                }
                CodeStore currentCodeStore = null;

                for (CodeStore codeStore : response) {
                    if (codeStore.id == value) {
                        currentCodeStore = codeStore;
                    }
                }

                if (currentCodeStore != null) {

                    int index = 0;
                    for (int i = 0; i < mSpinner.getCount(); i++) {
                        CodeStore customer = (CodeStore) mSpinner.getItemAtPosition(i);
                        if (customer != null) {
                            if (customer.toString().equalsIgnoreCase(currentCodeStore.toString())) {
                                index = i;
                                i = mSpinner.getCount();
                            }
                        }
                    }
                    mSpinner.setSelection(index);

                    if (mMeta.required) {
                        try {
                            MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, "" + value));
                        } catch (JSONException e) {
                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                        }
                    }
                }
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!CreatePostActivity.this.isFinishing()) {
                DialogHelper.showErrorDialog(CreatePostActivity.this, ex);
            }
        }
    }

    /**
     * Downloads meta def list in background
     */
    public class GetMetaDefListTask extends SafeAsyncTask<Void, Void, List<MetaDef>> {

        private int[] mUseTypeList;

        public GetMetaDefListTask(int metaDataType) {
            mUseTypeList = new int[1];
            mUseTypeList[0] = metaDataType;
        }

        @Override
        protected List<MetaDef> doWorkInBackground(Void... params) throws Exception {
            return MetaManager.getInstance().parseMetaDefList(ServerConnector.getInstance().getMetaDefListRequest(mUseTypeList));
        }

        @Override
        protected void onSuccess(List<MetaDef> metaDefList) {
            super.onSuccess(metaDefList);
            try {
                buildMetaViews(metaDefList);
            } catch (DMSCloudDBException e) {
                DialogHelper.showErrorDialog(CreatePostActivity.this, e);
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(CreatePostActivity.this, ex);
        }
    }

    /**
     * Set create post fragment views
     *
     * @throws DMSCloudDBException
     */
    private void setViews() throws DMSCloudDBException {
        final List<IUser> userList = DataBaseConnector.getInstance().getIUserList();
        userList.addAll(GroupAndDistributionManager.getInstance().getGroupList());
        userList.addAll(GroupAndDistributionManager.getInstance().getDistributionList());


        mUserList = new ArrayList<>(userList);
        mShareDialogAdapter = new ShareDialogAdapter(userList, CreatePostActivity.this, mRecipientsLayout);
        mShareDialogAdapter.getmArrayListOfUserIds().add(UserManager.getInstance().getCurrentUser().getId());
        mShareDialogAdapter.setRecipients(UserManager.getInstance().getCurrentUser().getId(), UserManager.getInstance().getCurrentUser().getName(), UserManager.getInstance().getCurrentUser().getType());

        spinnerLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                createRecipientDialog(mUserList);
            }
        });

        mOkBtn.setOnClickListener(new OnClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (mDescriptionEt.getText().toString().isEmpty()) {
                    mDescriptionEt.setError(getString(R.string.err_empty_field));
                } else if (mTitleEt.getText().toString().isEmpty()) {
                    mTitleEt.setError(getString(R.string.err_empty_field));
                } else {

                    String description = mDescriptionEt.getText().toString();
                    if (description.isEmpty()) {
                        mDescriptionEt.setError(getString(R.string.err_empty_field));
                    } else {
                        boolean valid = true;
                        List<FillMeta> fillMetaList = MetaManager.getInstance().getFillMetaList();

                        if (mMetaDefList != null && !mMetaDefList.isEmpty()) {
                            for (FillMeta fillMeta : fillMetaList) {
                                for (MetaDef metaDef : mMetaDefList) {
                                    if (fillMeta.id == metaDef.id) {
                                        if (metaDef.required) {
                                            if (!fillMeta.isValid()) {
                                                valid = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (valid) {
                            new CreatePostTask(mTitleEt.getText().toString(), mDescriptionEt.getText().toString(), mShareDialogAdapter.getmListOfUserIds(), mShareDialogAdapter.getmListOfGroupIds(), mShareDialogAdapter.getmListOfDistributionIds(), fillMetaList).execute();
                        } else {
                            DialogHelper.createErrorDialog(CreatePostActivity.this, R.string.wf_task_execution_required_title);
                        }
                    }

                }
            }
        });
    }

    /**
     * Create post in background
     */
    public class CreatePostTask extends SafeAsyncTask<Void, Void, Boolean> {

        private String mTitle, mMessage;
        private Long[] mUserList, mGroupList, mDistributionList;
        private List<FillMeta> mMetaList;

        public CreatePostTask(String title, String message, Long[] userList, Long[] groupList, Long[] distributionList, List<FillMeta> metaList) {
            mTitle = title;
            mMessage = message;
            mUserList = userList;
            mGroupList = groupList;
            mDistributionList = distributionList;
            mMetaList = metaList;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPost(mTitle, mMessage, mUserList, mGroupList, mDistributionList, mMetaList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setWallUpToDate(false);
                Toast.makeText(CreatePostActivity.this, CreatePostActivity.this.getResources().getString(R.string.create_post_success), Toast.LENGTH_LONG).show();
                CreatePostActivity.this.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!CreatePostActivity.this.isFinishing()) {
                DialogHelper.showErrorDialog(CreatePostActivity.this, ex);
            }
        }
    }

    /**
     * Downloads group and distribution list in background
     */
    public class GetGroupAndDistributionListTask extends SafeAsyncTask<Void, Void, String> {

        private ProgressDialog mProgressDialog;

        public GetGroupAndDistributionListTask() {
            mProgressDialog = new ProgressDialog(CreatePostActivity.this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.create_task_user_loading_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doWorkInBackground(Void... params) throws Exception {
            GroupAndDistributionManager.getInstance().parseDistributionList(new JSONObject(ServerConnector.getInstance().getDistributionListRequest()));
            return ServerConnector.getInstance().getGroupListRequest();
        }

        @Override
        protected void onSuccess(String response) {
            super.onSuccess(response);
            JSONObject groupListResponse = null;
            try {
                groupListResponse = new JSONObject(response);
                GroupAndDistributionManager.getInstance().parseGroupList(groupListResponse);

                setViews();
            } catch (Exception e) {
                if (!CreatePostActivity.this.isFinishing()) {
                    DialogHelper.showErrorDialog(CreatePostActivity.this, e);
                }
            }
            if (!CreatePostActivity.this.isFinishing()) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!CreatePostActivity.this.isFinishing()) {
                mProgressDialog.dismiss();
                DialogHelper.showErrorDialog(CreatePostActivity.this, ex);
            }
        }
    }
}
