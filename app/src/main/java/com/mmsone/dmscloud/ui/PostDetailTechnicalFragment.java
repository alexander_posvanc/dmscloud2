package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.TechnicalDetailAdapter;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 09. 18..
 */
public class PostDetailTechnicalFragment extends Fragment {

    private TextView technicalCount;
    private List<IPostChild> postChildren;
    private Post mPost;
    private RecyclerView mTechnicalListView;
    private TechnicalDetailAdapter mTechnicalDetailAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View technicalfrag = inflater.inflate(R.layout.fragment_detail_technical, container, false);

        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        technicalCount = (TextView) technicalfrag.findViewById(R.id.message_count_Tv);
        mTechnicalListView = (RecyclerView) technicalfrag.findViewById(R.id.technicalFragment_recyclerView);
        postChildren = new ArrayList<>();

        for (IPostChild child : mPost.childList) {
            if (EPostChildType.PostComment.equals(child.getType())) {
                PostComment postComment = (PostComment) child;
                if (postComment.technicalFile != null && postComment.technicalFile != "" && postComment.technicalFile != "null") {
                    postChildren.add(child);
                }
            }
        }

        technicalCount.setText(getResources().getString(R.string.shared_with_first) + " " + postChildren.size() + getResources().getString(R.string.post_detail_technical_count));

        mTechnicalDetailAdapter = new TechnicalDetailAdapter((ArrayList<IPostChild>) postChildren, getActivity(), mPost);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mTechnicalListView.setLayoutManager(mLayoutManager);
        mTechnicalListView.setHasFixedSize(true);

        mTechnicalListView.setAdapter(mTechnicalDetailAdapter);

        return technicalfrag;
    }
}
