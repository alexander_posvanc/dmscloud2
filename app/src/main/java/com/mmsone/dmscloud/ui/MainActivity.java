package com.mmsone.dmscloud.ui;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.DrawerAdapter;
import com.mmsone.dmscloud.adapter.MainDrawerAdapter;
import com.mmsone.dmscloud.adapter.TaskAdapter;
import com.mmsone.dmscloud.async.LogoutTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.DrawerItemManager;
import com.mmsone.dmscloud.bl.NotiManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.TaskManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.TaskSearchType;
import com.mmsone.dmscloud.bo.UserRole;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.TaskSearch;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.gcm.GCMManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.NotificationButton;
import com.mmsone.dmscloud.ui.helper.OnItemClickListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MainActivity extends ActionBarActivity {

    private DrawerLayout mDrawerLayout;
    //    private ListView mDrawerListView;
    private RecyclerView mDrawerListView;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle, mTitle;
    private boolean mIsDrawerLocked;
    private MainDrawerAdapter mMainDrawerAdapter;
    private Toolbar mToolbar;
    private SearchView mSearchView;
    private RelativeLayout bottomToobarBtn1, bottomToobarBtn2, mNotificationBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         * Set current user language
         */
        setLocale();

        new GetTaskListTask().execute();

        new NotificationButton(this).startNotification();
        bottomToobarBtn1 = (RelativeLayout) findViewById(R.id.bottom_tool_bar_button1);
        bottomToobarBtn2 = (RelativeLayout) findViewById(R.id.bottom_tool_bar_button3);

        bottomToobarBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (UserManager.getInstance().hasUserRole(UserRole.TASK_MANAGEMENT_ROLE)) {
                        startActivity(new Intent(MainActivity.this, CreatePostActivity.class));
                    } else {
                        DialogHelper.createErrorDialog(MainActivity.this, getString(R.string.err_insufficient_rights));
                    }
                } catch (DMSCloudDBException e) {
                    DialogHelper.showErrorDialog(MainActivity.this, e);
                }
            }
        });

        bottomToobarBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FileUploadActivity.class));

            }
        });

        mSearchView = (SearchView) findViewById(R.id.wall_search_Et);
        mSearchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search_hint) + "</font>"));
        mSearchView.setOnQueryTextListener(mSearchListener);


//        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
//            @Override
//            public boolean onClose() {
//                Toast.makeText(MainActivity.this, "closed", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        mTitle = mDrawerTitle = getResources().getString(R.string.title_wall);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawerLayout);
        mDrawerListView = (RecyclerView) findViewById(R.id.main_left_drawerListView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        mDrawerListView.setLayoutManager(mLayoutManager);
        mDrawerListView.setHasFixedSize(true);


        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.main_content_frameLayout);


        mMainDrawerAdapter = new MainDrawerAdapter(DrawerItemManager.getInstance(this).getMainDrawerItemList(this));
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerListView.setAdapter(mMainDrawerAdapter);

        mMainDrawerAdapter.SetOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                selectItem(position);
            }
        });


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                setTitle(mTitle);
                ActivityCompat.invalidateOptionsMenu(MainActivity.this);
            }

            public void onDrawerOpened(View drawerView) {
                setTitle(mDrawerTitle);
                ActivityCompat.invalidateOptionsMenu(MainActivity.this);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        onNewIntent(getIntent());

        if (savedInstanceState == null) {
            selectItem(WallFragment.DRAWER_LIST_POSITION);
        }
        /**
         * Send gcm registration
         */
        GCMManager.getInstance(this).registration();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            /**
             * Clear notification messages
             */
            if (extras.getBoolean("deleteNotifications")) {
                NotiManager.getInstance().clearMessageList();
            }
            /**
             * Show authentication exception and log out
             */
            if (extras.getBoolean("authenticationException")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this).setTitle(R.string.err_title).setMessage(R.string.err_authentication).setNeutralButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new LogoutTask(MainActivity.this).execute();
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        }
    }


    /**
     * Set action bar title
     */
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mTitle);
        }
    }

    public boolean isDrawerOpen() {
        if (mIsDrawerLocked) {
            return false;
        }
//        return true;
        return mDrawerLayout.isDrawerOpen(mDrawerListView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Select navigation drawer item
     *
     * @param position
     */
    public void selectItem(int position) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (position) {
            case WallFragment.DRAWER_LIST_POSITION:
                fragment = new WallFragment();
                fragmentManager.beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
                break;
            case TasksFragment.DRAWER_LIST_POSITION:
                fragment = new TasksFragment();
                fragmentManager.beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
                break;
            case CreatePostActivity.DRAWER_LIST_POSITION:
                try {
                    if (UserManager.getInstance().hasUserRole(UserRole.TASK_MANAGEMENT_ROLE)) {
                        startActivity(new Intent(MainActivity.this, CreatePostActivity.class));
                    } else {
                        DialogHelper.createErrorDialog(MainActivity.this, getString(R.string.err_insufficient_rights));
                    }
                } catch (DMSCloudDBException e) {
                    DialogHelper.showErrorDialog(MainActivity.this, e);
                }
                break;
            case FileUploadActivity.DRAWER_LIST_POSITION:
                startActivity(new Intent(this, FileUploadActivity.class));
                break;
            case DownloadsFragment.DRAWER_LIST_POSITION:
                fragment = new DownloadsFragmentNew();
                fragmentManager.beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
                break;
            case SettingsFragment.DRAWER_LIST_POSITION:
                fragment = new SettingsFragment();
                fragmentManager.beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
                break;
            case 7:
                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                    deleteDMSCloudDirectory(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.DOWNLOAD_DIR));
                    deleteDMSCloudDirectory(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.PIC_DIR));
                }
                GCMManager.getInstance(this).sendUnRegistrationIdToBackend();
                new LogoutTask(MainActivity.this).execute();
                break;
            default:
                break;
        }

        mDrawerLayout.closeDrawer(mDrawerListView);
    }

    private SearchView.OnQueryTextListener mSearchListener = new SearchView.OnQueryTextListener() {

        @Override
        public boolean onQueryTextSubmit(String query) {
//            startSimpleSearch(query);
            Bundle args = new Bundle();
            args.putString("query", query);
            args.putInt("id", mSearchView.getId());
            Fragment fragment = null;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragment = new WallFragment();
            fragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
            return true;
        }

        @Override
        public boolean onQueryTextChange(String query) {
            return true;
        }
    };

    public static boolean deleteDMSCloudDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDMSCloudDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    public void setLocale() {
        Resources resources = getResources();
        User currentUser = null;
        try {
            currentUser = UserManager.getInstance().getCurrentUser();
            if (currentUser != null) {
                Locale locale = null;

                locale = new Locale(currentUser.language);
                Configuration config = resources.getConfiguration();
                if (locale != null) {
                    config.locale = locale;
                    resources.updateConfiguration(config, resources.getDisplayMetrics());
                }
            }
        } catch (DMSCloudDBException e1) {
            DialogHelper.showErrorDialog(this, e1);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocale();
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Closing application
     */
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            setTitle(R.string.title_wall);
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        PrefManager.getInstance().setWallUpToDate(false);
    }


    /**
     * Downloads task list in background
     */
    public class GetTaskListTask extends SafeAsyncTask<Void, Void, List<TaskSearch>> {

        public GetTaskListTask() {
        }

        @Override
        protected List<TaskSearch> doWorkInBackground(Void... params) throws Exception {
            return TaskManager.getInstance().getTaskList();
        }

        @Override
        protected void onSuccess(List<TaskSearch> taksList) {
            Iterator<TaskSearch> i = taksList.iterator();
            while (i.hasNext()) {
                TaskSearch taskSearch = i.next();
                if (taskSearch.status == WorkflowStatus.TERMINATED) {
                    i.remove();
                }
            }

            mMainDrawerAdapter.setmTaskCount(taksList.size());
            super.onSuccess(taksList);

        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(MainActivity.this, ex);
        }
    }


    public MainDrawerAdapter getmMainDrawerAdapter() {
        return mMainDrawerAdapter;
    }


}