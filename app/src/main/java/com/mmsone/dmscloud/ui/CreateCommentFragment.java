package com.mmsone.dmscloud.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.helper.DialogHelper;

public class CreateCommentFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 2;

    private SharePostActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (SharePostActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_comment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_comment, container, false);

        Bundle b = getArguments();
        final long postId = b.getLong("postId");

        final EditText descriptionEt = (EditText) view.findViewById(R.id.comment_descriptionEt);
        descriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        Button okBtn = (Button) view.findViewById(R.id.comment_okBtn);
        okBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());

        okBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (descriptionEt.getText().toString().isEmpty()) {
                    descriptionEt.setError(getString(R.string.err_empty_field));
                } else {
                    new CreatePostCommentTask(postId, descriptionEt.getText().toString()).execute();
                }
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (descriptionEt.isFocused())
                    descriptionEt.clearFocus();

                descriptionEt.requestFocus();
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                manager.showSoftInput(descriptionEt, 0);
            }
        }, 200);

        return view;
    }

    /**
     * Create post comment in background
     */
    public class CreatePostCommentTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mPostId;
        private String mDescription;

        public CreatePostCommentTask(long postId, String description) {
            mPostId = postId;
            mDescription = description;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostComment(mPostId, mDescription);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setWallUpToDate(false);
                Toast.makeText(mActivity, R.string.post_sub_comment_create_succes_message, Toast.LENGTH_SHORT).show();
                mActivity.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }
}