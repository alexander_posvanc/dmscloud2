package com.mmsone.dmscloud.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.PostOperationDrawerAdapter;
import com.mmsone.dmscloud.async.HidePostTask;
import com.mmsone.dmscloud.async.MarkPostTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.DrawerItemManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.UserRole;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;

public class PostOperationFragment extends ListFragment {

	private Post mPost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle b = getArguments();
		mPost = (Post) b.getSerializable("post");
		return inflater.inflate(R.layout.fragment_post_operation_slidingmenu, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListAdapter(new PostOperationDrawerAdapter(getActivity(), DrawerItemManager.getInstance(getActivity()).getPostDrawerItemList(mPost)));
	}

	/**
	 * Select post operation menu item
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Bundle args = new Bundle();
		args.putLong("postId", mPost.id);

		Fragment fragment = null;

		switch (position) {
		case CreateTaskFragment.DRAWER_LIST_POSITION:
			try {
				if (UserManager.getInstance().hasUserRole(UserRole.TASK_MANAGEMENT_ROLE)) {
					fragment = new CreateTaskFragment();
					fragment.setArguments(args);
					switchFragment(fragment);
				} else {
					DialogHelper.createErrorDialog(getActivity(), getString(R.string.err_insufficient_rights));
				}
			} catch (DMSCloudDBException e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}
			break;
		case CreateWorkflowFragment.DRAWER_LIST_POSITION:
			fragment = new CreateWorkflowFragment();
			fragment.setArguments(args);
			switchFragment(fragment);
			break;
		case CreateCommentFragment.DRAWER_LIST_POSITION:
			fragment = new CreateCommentFragment();
			fragment.setArguments(args);
			switchFragment(fragment);
			break;
		case 3:
			try {
				if (UserManager.getInstance().hasUserRole(UserRole.HIDE_ROLE)) {
					new HidePostTask(mPost.id, getActivity()).execute();
				} else {
					DialogHelper.createErrorDialog(getActivity(), getString(R.string.err_insufficient_rights));
				}
			} catch (DMSCloudDBException e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}
			break;
		case 4:
			try {
				if (UserManager.getInstance().hasUserRole(UserRole.MARK_ROLE)) {
					new MarkPostTask(mPost.id, getActivity()).execute();
				} else {
					DialogHelper.createErrorDialog(getActivity(), getString(R.string.err_insufficient_rights));
				}
			} catch (DMSCloudDBException e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}

			break;
		case ShareFragment.DRAWER_LIST_POSITION:
			try {
				if (UserManager.getInstance().hasUserRole(UserRole.SHARE_ROLE)) {
					fragment = new ShareFragment();
					fragment.setArguments(args);
					switchFragment(fragment);
				} else {
					DialogHelper.createErrorDialog(getActivity(), getString(R.string.err_insufficient_rights));
				}
			} catch (DMSCloudDBException e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}
			break;
		case DocumentFragment.DRAWER_LIST_POSITION:
			if (mPost.documentId != 0) {
				fragment = new DocumentFragment();
				args = new Bundle();
				args.putLong("documentId", mPost.documentId);
				fragment.setArguments(args);
				switchFragment(fragment);
			} else {
				showSharedWithDialog();
			}
			break;
		case 7:
			showSharedWithDialog();
			break;

		default:
			break;
		}
	}

	/**
	 * Show shared with dialog
	 */
	private void showSharedWithDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.share_dialog_title);
		try {
			builder.setMessage(DataBaseConnector.getInstance().getSharedWithUsers(mPost.sharedWith, mPost.hiddenAt));
		} catch (DMSCloudDBException e) {
			DialogHelper.showErrorDialog(getActivity(), e);
		}
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		builder.show();
	}

	/**
	 * Switch fragment
	 * 
	 * @param fragment
	 */
	public void switchFragment(Fragment fragment) {
		if (getActivity() == null) {
			return;
		}
		if (getActivity() instanceof PostActivity) {
			PostActivity ra = (PostActivity) getActivity();
			ra.switchContent(fragment);
		}
	}
}
