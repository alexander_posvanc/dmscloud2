package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bo.bean.Post;

/**
 * Created by Alexander on 2015. 08. 25..
 */
public class SharePostActivity extends ActionBarActivity {

    private Toolbar mToolbar;
    private Post mPost;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Bundle args = getIntent().getExtras();
        mPost = (Post) args.getSerializable("post");
        int i = args.getInt("fragment");
        long documentId = args.getLong("documentId");
        if (i == 0) {
            args.putLong("postId", mPost.id);
            CreateCommentFragment createCommentFragment = new CreateCommentFragment();
            createCommentFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.share_container, createCommentFragment).commit();
        } else if (i == 1) {
            args.putLong("postId", mPost.id);
            CreateWorkflowFragment createWorkflowFragment = new CreateWorkflowFragment();
            createWorkflowFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.share_container, createWorkflowFragment).commit();

        } else if (i == 2) {
            args.putLong("postId", mPost.id);
            CreateTaskFragment createTaskFragment = new CreateTaskFragment();
            createTaskFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.share_container, createTaskFragment).commit();

        } else if (i == 3) {
            SharedWithFragment sharedWithFragment = new SharedWithFragment();
            sharedWithFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.share_container, sharedWithFragment).commit();
        } else if (i == 4) {
            DocumentFragment fragment = new DocumentFragment();
//            args.putLong("documentId", mPost.documentId);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.share_container, fragment).commit();

        }

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
