package com.mmsone.dmscloud.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.PostOperationDrawerAdapter;
import com.mmsone.dmscloud.bl.DrawerItemManager;
import com.mmsone.dmscloud.listener.OnAdvancedSarchListener;

public class AdvancedSearchOperationFragment extends ListFragment {

	private OnAdvancedSarchListener mAdvancedSarchListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mAdvancedSarchListener = (OnAdvancedSarchListener) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_post_operation_slidingmenu, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListAdapter(new PostOperationDrawerAdapter(getActivity(), DrawerItemManager.getInstance(getActivity()).getAdvancedSearchDrawerItemList()));
	}
	/**
	 * Select right menu item
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		switch (position) {
		case 0:
			mAdvancedSarchListener.onAdvancedSarchLoad();
			break;
		case 1:
			mAdvancedSarchListener.onAdvancedSarchSave();
			break;
		case 2:
			mAdvancedSarchListener.onAdvancedSarchDelete();
			break;
		default:
			break;
		}
	}
}