package com.mmsone.dmscloud.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.DownloadFileAdapter;
import com.mmsone.dmscloud.helper.DialogHelper;

public class DownloadsFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 5;

    private MainActivity mActivity;

    private List<File> mFileList = new ArrayList<File>();
    private DownloadFileAdapter mFileAdapter;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (MainActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_downloads);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_downloads, container, false);

        ListView fileListView = (ListView) view.findViewById(R.id.downloads_listView);

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File dmscloudDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.DOWNLOAD_DIR);

            dmscloudDir.mkdirs();

            File[] files = dmscloudDir.listFiles();
            mFileList.clear();
            for (File file : files) {
                mFileList.add(file);
            }

            mFileAdapter = new DownloadFileAdapter(mActivity, mFileList);
            fileListView.setAdapter(mFileAdapter);

            fileListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        File url = mFileList.get(position);

                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);

                        Uri uri = Uri.fromFile(url);

                        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                            intent.setDataAndType(uri, "application/msword");
                        } else if (url.toString().contains(".pdf")) {
                            intent.setDataAndType(uri, "application/pdf");
                        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                            intent.setDataAndType(uri, "application/vnd.ms-excel");
                        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                            intent.setDataAndType(uri, "application/zip");
                            ;
                        } else if (url.toString().contains(".rtf")) {
                            intent.setDataAndType(uri, "application/rtf");
                        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                            intent.setDataAndType(uri, "audio/x-wav");
                        } else if (url.toString().contains(".gif")) {
                            intent.setDataAndType(uri, "image/gif");
                        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                            intent.setDataAndType(uri, "image/jpeg");
                        } else if (url.toString().contains(".txt")) {
                            intent.setDataAndType(uri, "text/plain");
                        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                            intent.setDataAndType(uri, "video/*");
                        } else {
                            intent.setDataAndType(uri, "*/*");
                        }

                        mActivity.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        DialogHelper.showErrorDialog(mActivity, e);
                    }
                }
            });
        }

        return view;
    }
}
