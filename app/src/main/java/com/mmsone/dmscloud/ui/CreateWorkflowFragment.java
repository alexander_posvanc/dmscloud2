package com.mmsone.dmscloud.ui;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.WfDef;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

public class CreateWorkflowFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 1;

    private SharePostActivity mActivity;
    private long mPostId;
    private Spinner mWorkflowTypeSpinner;
    private EditText mDescriptionEt;
    private RelativeLayout mOkBtn;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (SharePostActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_create_workflow);
//        mActivity.setOptionMenuVisibility(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createWorkflowView = inflater.inflate(R.layout.fragment_create_workflow, container, false);

        mWorkflowTypeSpinner = (Spinner) createWorkflowView.findViewById(R.id.create_wf_typeSpinner);
        mDescriptionEt = (EditText) createWorkflowView.findViewById(R.id.create_wf_descriptionEt);
        mDescriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mOkBtn = (RelativeLayout) createWorkflowView.findViewById(R.id.create_wf_okBtn);

        new NotificationButton(getActivity()).hideDmsIcon();

        mOkBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String description = mDescriptionEt.getText().toString().trim();
                WfDef wfDef = ((WfDef) mWorkflowTypeSpinner.getSelectedItem());
                if (description.isEmpty()) {
                    mDescriptionEt.setError(getString(R.string.err_empty_field));
                } else if (wfDef == null) {
                    DialogHelper.createErrorDialog(mActivity, R.string.workflow_wf_def_choose_message);
                } else {
                    new CreatePostWfTaskTask(mPostId, description, wfDef.id).execute();
                }
            }
        });

        Bundle b = getArguments();
        mPostId = b.getLong("postId");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (mDescriptionEt.isFocused())
                    mDescriptionEt.clearFocus();

                mDescriptionEt.requestFocus();
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                manager.showSoftInput(mDescriptionEt, 0);
            }
        }, 200);

        return createWorkflowView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetWfDefListTask().execute();
    }

    /**
     * Download workflow def list in background
     */
    public class GetWfDefListTask extends SafeAsyncTask<Void, Void, List<WfDef>> {

        private ProgressDialog mProgressDialog;

        public GetWfDefListTask() {
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.workflow_wf_def_loading));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected List<WfDef> doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().getWfDefList();
        }

        @Override
        protected void onSuccess(List<WfDef> response) {
            super.onSuccess(response);
            ArrayAdapter<WfDef> adapter = new ArrayAdapter<WfDef>(mActivity, android.R.layout.simple_spinner_item, response);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mWorkflowTypeSpinner.setAdapter(adapter);
            mWorkflowTypeSpinner.setSelection(0);
            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }

    /**
     * Create workflow task in background
     */
    public class CreatePostWfTaskTask extends SafeAsyncTask<Void, Void, Boolean> {

        private ProgressDialog mProgressDialog;

        private long mPostId;
        private String mDescription;
        private long mWfDefId;

        public CreatePostWfTaskTask(long postId, String description, long wfDefId) {
            mPostId = postId;
            mDescription = description;
            mWfDefId = wfDefId;

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.workflow_create_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostWfTask(mPostId, mDescription, mWfDefId);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                mProgressDialog.dismiss();
                PrefManager.getInstance().setIsChanged(true);
                PrefManager.getInstance().setWallUpToDate(false);
                Toast.makeText(mActivity, getString(R.string.workflow_create_succes), Toast.LENGTH_LONG).show();
                mActivity.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }
}
