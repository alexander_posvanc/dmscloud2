package com.mmsone.dmscloud.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.ShareAdapter;
import com.mmsone.dmscloud.async.GetUserListTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.GroupAndDistributionManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;
import com.mmsone.dmscloud.ui.helper.DMSHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 26..
 */
public class ShareFragmentNew extends Fragment {

    private RecyclerView mShareRecycleView;
    private ShareAdapter mShareAdapter;
    private static final ILogger LOG = Logger.getLogger(ShareFragmentNew.class);
    private Post mPost;
    private EditText mSearchET;
    private List<IUser> mUserList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View shareView = inflater.inflate(R.layout.fragment_share_new, container, false);

        getActivity().setTitle(getResources().getString(R.string.share_activity_title));
        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        mShareRecycleView = (RecyclerView) shareView.findViewById(R.id.share_fragment_recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mShareRecycleView.setLayoutManager(mLayoutManager);
        mShareRecycleView.setHasFixedSize(true);

        mSearchET = (EditText) shareView.findViewById(R.id.share_searchEt);
        mSearchET.addTextChangedListener(new SearchWatcher());

//        mShareRecycleView.setAdapter(mShareAdapter);

        return shareView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetUserListTask(getActivity()).execute();
        new GetGroupAndDistributionListTask().execute();
    }

    /**
     * Set share fragment views
     *
     * @throws com.mmsone.dmscloud.ex.DMSCloudDBException
     */
    private void setViews() throws DMSCloudDBException {
        List<IUser> userList = DataBaseConnector.getInstance().getIUserList();
        userList.addAll(GroupAndDistributionManager.getInstance().getGroupList());
        userList.addAll(GroupAndDistributionManager.getInstance().getDistributionList());

        //userek törlése akikkel meg van osztva
        for (int i = 0; i < userList.size(); i++) {
            long id = userList.get(i).getId();
            for (int y = 0; y < mPost.sharedWith.size(); y++) {
                if (mPost.sharedWith.get(y) == id) {
                    userList.remove(userList.get(i));
                }
            }

        }
        mUserList = userList;
        mShareAdapter = new ShareAdapter(userList, mPost, getActivity());
        mShareRecycleView.setAdapter(mShareAdapter);
    }

    /**
     * Downloads group and distribution list in background
     */
    public class GetGroupAndDistributionListTask extends SafeAsyncTask<Void, Void, String> {

        private ProgressDialog mProgressDialog;

        public GetGroupAndDistributionListTask() {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.create_task_user_loading_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doWorkInBackground(Void... params) throws Exception {
            GroupAndDistributionManager.getInstance().parseDistributionList(new JSONObject(ServerConnector.getInstance().getDistributionListRequest()));
            return ServerConnector.getInstance().getGroupListRequest();
        }

        @Override
        protected void onSuccess(String response) {
            super.onSuccess(response);
            JSONObject groupListResponse = null;
            try {
                groupListResponse = new JSONObject(response);
                LOG.debug("GetGroupListTask - onSuccess: " + groupListResponse.toString());
                GroupAndDistributionManager.getInstance().parseGroupList(groupListResponse);

                setViews();
            } catch (Exception e) {
                DialogHelper.showErrorDialog(getActivity(), e);
            }
            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(getActivity(), ex);
        }
    }

    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String searchQuery = mSearchET.getText().toString();
            List<IUser> filteredList = DMSHelper.performSearch(mUserList, searchQuery);
            mShareAdapter.updateList(filteredList);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }




}
