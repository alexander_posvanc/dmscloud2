package com.mmsone.dmscloud.ui;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.FileUploadDrawerAdapter;
import com.mmsone.dmscloud.helper.FileUtils;
import com.mmsone.dmscloud.listener.OnFileUploadListener;
import com.mmsone.dmscloud.ui.helper.OnItemClickListener;
import com.mmsone.dmscloud.ui.helper.SlidingFragmentBaseActivity;

public class FileUploadActivity extends ActionBarActivity implements OnFileUploadListener {

    public static final int DRAWER_LIST_POSITION = 4;

    private Fragment mContent, mMenuFragment;

    private MenuItem mPostDetailMenu;
    private Toolbar mToolbar;
    private RecyclerView mDrawerListView;
    private DrawerLayout mDrawerLayout;
    private String mFileName = "dmscloud_";
    private static final int SELECT_PHOTO = 1;
    private static final int SELECT_CAMERA = 2;
    private OnFileUploadListener mFileUploadListener;
    private long mPostId;
    private ImageView mDrawerButton;

    public FileUploadActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
            mMenuFragment = getSupportFragmentManager().getFragment(savedInstanceState, "mMenuFragment");
        }
        if (mContent == null) {
            mContent = new FileUploadFragment();
        }
//        if (mMenuFragment == null) {
//            mMenuFragment = new FileUploadOperationFragment();
//        }

        // Get intent, action and MIME type
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            mPostId = intent.getLongExtra("postId", 0);
        }

        setContentView(R.layout.activity_file_upload);

        if (mPostId != 0) {
            Bundle args = new Bundle();
            args.putLong("postId", mPostId);
            mContent.setArguments(args);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.file_upload_container, mContent).commit();

        mFileUploadListener = this;

        mDrawerListView = (RecyclerView) findViewById(R.id.file_upload_drawer_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mDrawerListView.setLayoutManager(mLayoutManager);
        mDrawerListView.setHasFixedSize(true);

        FileUploadDrawerAdapter fileUploadDrawerAdapter = new FileUploadDrawerAdapter(this);
        mDrawerListView.setAdapter(fileUploadDrawerAdapter);
        fileUploadDrawerAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                selectItem(position);
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.file_upload_drawerLayout);
        mDrawerLayout.openDrawer(Gravity.RIGHT);
        mDrawerLayout.closeDrawer(Gravity.RIGHT);

        mDrawerButton = (ImageView) findViewById(R.id.file_upload_drawerToggle);

        mDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(mToolbar);
        setTitle(getResources().getString(R.string.file_upload_activity_title));
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Get intent, action and MIME type
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (imageUri != null) {
                    onListUpdate(FileUtils.getFile(FileUploadActivity.this, imageUri));
                }
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
            if (imageUris != null) {
                for (Uri imageUri : imageUris) {
                    onListUpdate(FileUtils.getFile(FileUploadActivity.this, imageUri));
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    /**
     * Select photographing, image pick from gallery, or file select from file dialog according to position
     *
     * @param position
     */
    public void selectItem(int position) {
        switch (position) {
            case 1:
                this.drawerClose();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mFileName = "dmscloud_" + System.currentTimeMillis() + ".jpg";
                File dmscloudDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.PIC_DIR);
                dmscloudDir.mkdirs();
                File f = new File(dmscloudDir, mFileName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                this.startActivityForResult(intent, SELECT_CAMERA);
                break;
            case 2:
                this.drawerClose();
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                intent1.putExtra("return-data", true);
                this.startActivityForResult(Intent.createChooser(intent1, "Complete action using"), SELECT_PHOTO);
                break;
            case 3:
                this.drawerClose();
                FileDialog fileDialog = new FileDialog(this, new File(Environment.getExternalStorageDirectory() + "//DIR//"));
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {
                        mFileUploadListener.onListUpdate(file);
                    }
                });
                fileDialog.showDialog();
                break;
        }
    }

    /**
     * Switch content
     *
     * @param fragment
     */
    public void switchContent(Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.file_upload_container, fragment).commit();
//        getSlidingMenu().showContent();
    }

    public void setOptionMenuVisibility(boolean visible) {
        if (mPostDetailMenu != null) {
            mPostDetailMenu.setVisible(visible);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_post_detail, menu);
//        mPostDetailMenu = menu.findItem(R.id.menu_item_post_detail_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_post_detail_menu:
//                getSlidingMenu().toggle(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SELECT_CAMERA:
                    File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.PIC_DIR);
                    for (File temp : f.listFiles()) {
                        if (temp.getName().equals(mFileName)) {
                            f = temp;
                            break;
                        }
                    }
                    mFileUploadListener.onListUpdate(f);
                    break;
                case SELECT_PHOTO:
                    mFileUploadListener.onListUpdate(FileUtils.getFile(this, data.getData()));
                    break;
            }
        }
    }

    /**
     * Update file list
     */
    @Override
    public void onListUpdate(File file) {
//        getSlidingMenu().toggle(true);
        ((FileUploadFragment) mContent).updateList(file);
    }

    public void drawerClose() {
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
    }
}
