package com.mmsone.dmscloud.ui.helper;

import java.util.Date;
import java.util.List;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.GetTaskStatusTask;
import com.mmsone.dmscloud.async.GetWfTaskStatusTask;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.ViewPoolManager;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.PostChat;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.bo.bean.PostEmail;
import com.mmsone.dmscloud.bo.bean.PostSubComment;
import com.mmsone.dmscloud.bo.bean.PostSubTask;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.bo.bean.ShowMeta;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.DocumentFragment;
import com.mmsone.dmscloud.ui.MainActivity;
import com.mmsone.dmscloud.ui.PostActivity;
import com.mmsone.dmscloud.ui.PostDetailFragment;
import com.mmsone.dmscloud.ui.SubCommentDialog;
import com.mmsone.dmscloud.ui.TaskExecutionActivity;
import com.mmsone.dmscloud.ui.WfTaskExecutionActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class DMSCloudViewBuilder {

	/**
	 * Add show meta to list item
	 * 
	 * @param inflater
	 * @param metaLayout
	 * @param metaList
	 */

	public static void addMetaViews(LayoutInflater inflater, LinearLayout metaLayout, List<ShowMeta> metaList) {
		metaLayout.removeAllViews();

		TextView valueTv, labelTv;
		View metaView;

		for (ShowMeta meta : metaList) {
			metaView = inflater.inflate(R.layout.view_meta, null);
			labelTv = (TextView) metaView.findViewById(R.id.meta_labelTv);
			labelTv.setText(meta.label);
			labelTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
			valueTv = (TextView) metaView.findViewById(R.id.meta_valueTv);
			valueTv.setText(meta.value);
			valueTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

			metaLayout.addView(metaView);
		}
	}

	public static class ViewTag {
		public EPostChildType type;
		public ViewGroup subSubHolder;
	}

	/**
	 * Build topic sub items
	 * 
	 * @param context
	 * @param options
	 * @param subPostLayout
	 * @param postTaskList
	 * @param showAll
	 * @param postDetailFragment
	 * @param mainActivity
	 * @param onWallUpdateListener
	 * @param isChildBuild
	 * @param viewPoolManager
	 */

	public static void addPostTaskViews(final Context context, DisplayImageOptions options, LinearLayout subPostLayout, List<IPostChild> postTaskList, boolean showAll, final PostDetailFragment postDetailFragment, final MainActivity mainActivity, final OnWallUpdateListener onWallUpdateListener, boolean isChildBuild, ViewPoolManager viewPoolManager) {
		subPostLayout.removeAllViews();

		for (IPostChild iPostChild : postTaskList) {
			if (iPostChild.getType().equals(EPostChildType.PostTask)) {
				final PostTask postTask = (PostTask) iPostChild;

				View postTaskView = viewPoolManager.getView(EPostChildType.PostTask);

				ImageView profileIv = (ImageView) postTaskView.findViewById(R.id.post_task_profileIv);
				TextView createdOnDateTv = (TextView) postTaskView.findViewById(R.id.post_task_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) postTaskView.findViewById(R.id.post_task_createdOnTimeTv);
				TextView messageTv = (TextView) postTaskView.findViewById(R.id.post_task_messageTv);
				TextView createdByTv = (TextView) postTaskView.findViewById(R.id.post_task_createdByTv);
				TextView dueDateTv = (TextView) postTaskView.findViewById(R.id.post_task_dueDateTv);
				LinearLayout subLayout = (LinearLayout) postTaskView.findViewById(R.id.post_task_subLayout);
				LinearLayout iconLayout = (LinearLayout) postTaskView.findViewById(R.id.post_task_iconLL);
				final LinearLayout menuLayout = (LinearLayout) postTaskView.findViewById(R.id.post_task_menuLL);
				LinearLayout docIconLayout = (LinearLayout) postTaskView.findViewById(R.id.post_task_docIconLL);
				final RelativeLayout subContainerLayout = (RelativeLayout) postTaskView.findViewById(R.id.post_task_subContainerLayout);
				final LinearLayout menuContainer = (LinearLayout) postTaskView.findViewById(R.id.post_task_menuContainer);
				LinearLayout replyBtn = (LinearLayout) postTaskView.findViewById(R.id.post_task_menuReplyBtn);
				final LinearLayout taskAcceptOrRejectLayout = (LinearLayout) postTaskView.findViewById(R.id.post_sub_task_menuTaskAcceptOrRejectLayout);
				final LinearLayout taskAcceptBtn = (LinearLayout) postTaskView.findViewById(R.id.post_task_menuTaskAcceptBtn);
				final LinearLayout taskRejectBtn = (LinearLayout) postTaskView.findViewById(R.id.post_task_menuTaskrRejectBtn);

				Object tag = postTaskView.getTag();
				if (tag != null) {
					((ViewTag) tag).type = EPostChildType.PostTask;
					((ViewTag) tag).subSubHolder = subLayout;
				} else {
					ViewTag viewTag = new ViewTag();
					viewTag.type = EPostChildType.PostTask;
					viewTag.subSubHolder = subLayout;
					postTaskView.setTag(viewTag);
				}

				if (postTask.recipient) {
					switch (postTask.status) {
					case TaskStatus.TASK_ASSIGNED:
						taskAcceptBtn.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								new GetTaskStatusTask(context, postTask.id, TaskStatus.TASK_ACCEPTED, postDetailFragment, onWallUpdateListener).execute();
							}
						});

						taskRejectBtn.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								new GetTaskStatusTask(context, postTask.id, TaskStatus.TASK_REJECTED, postDetailFragment, onWallUpdateListener).execute();
							}
						});
						break;
					default:
						taskAcceptOrRejectLayout.setVisibility(View.GONE);
						break;
					}
				} else {
					taskAcceptOrRejectLayout.setVisibility(View.GONE);
				}

				replyBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						SubCommentDialog dialog = new SubCommentDialog(context, postTask.id, postTask.subPostType, onWallUpdateListener, postDetailFragment, null);
						dialog.show();
					}
				});

				menuContainer.setVisibility(View.GONE);
				menuLayout.setOnClickListener(new OnClickListener() {

					boolean isVisibel = false;

					@Override
					public void onClick(View v) {
						if (!isVisibel) {
							menuContainer.setVisibility(View.VISIBLE);
							isVisibel = true;
						} else {
							menuContainer.setVisibility(View.GONE);
							isVisibel = false;
						}
					}
				});

				setMenuTouchDelegateArea(postTaskView, menuLayout);

				if (postTask.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
				}

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postTask.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postTask.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}
				messageTv.setText(postTask.message);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				if (postTask.user != null && postTask.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postTask.user.image, profileIv, options);
					createdByTv.setText(postTask.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
				dueDateTv.setText(postTask.formatedDueDate);
				dueDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postTask.childList.isEmpty() || !isChildBuild) {
					subContainerLayout.setVisibility(View.GONE);
				} else {
					subContainerLayout.setVisibility(View.VISIBLE);
					addPostTaskSubViews(context, options, subLayout, postTask.childList, showAll, postDetailFragment, mainActivity, viewPoolManager);
				}

				if (postTask.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_tasks_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_tasks);
				}

				subPostLayout.addView(postTaskView);
			} else if (iPostChild.getType().equals(EPostChildType.PostEmail)) {
				final PostEmail postEmail = (PostEmail) iPostChild;

				View subEmialItemLayout = viewPoolManager.getView(EPostChildType.PostEmail);

				ImageView profileIv = (ImageView) subEmialItemLayout.findViewById(R.id.post_email_profileIv);
				TextView createdOnDateTv = (TextView) subEmialItemLayout.findViewById(R.id.post_email_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) subEmialItemLayout.findViewById(R.id.post_email_createdOnTimeTv);
				TextView messageTv = (TextView) subEmialItemLayout.findViewById(R.id.post_email_messageTv);
				TextView createdByTv = (TextView) subEmialItemLayout.findViewById(R.id.post_email_createdByTv);
				LinearLayout subLayout = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_subLayout);
				final LinearLayout menuLayout = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_menuLL);
				LinearLayout iconLayout = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_iconLL);
				LinearLayout docIconLayout = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_docIconLL);
				final RelativeLayout subContainerLayout = (RelativeLayout) subEmialItemLayout.findViewById(R.id.post_email_subContainerLayout);
				final LinearLayout menuContainer = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_menuContainer);
				final LinearLayout replyBtn = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_menuReplyBtn);
				final LinearLayout dokumentumBtn = (LinearLayout) subEmialItemLayout.findViewById(R.id.post_email_menuDokumentumBtn);

				Object tag = subEmialItemLayout.getTag();
				if (tag != null) {
					((ViewTag) tag).type = EPostChildType.PostEmail;
					((ViewTag) tag).subSubHolder = subLayout;
				} else {
					ViewTag viewTag = new ViewTag();
					viewTag.type = EPostChildType.PostEmail;
					viewTag.subSubHolder = subLayout;
					subEmialItemLayout.setTag(viewTag);
				}

				if (postEmail.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
					dokumentumBtn.setVisibility(View.GONE);
				}

				replyBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						SubCommentDialog dialog = new SubCommentDialog(context, postEmail.id, postEmail.subPostType, onWallUpdateListener, postDetailFragment, null);
						dialog.show();
					}
				});

				dokumentumBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Fragment fragment = new DocumentFragment();
						Bundle args = new Bundle();
						args.putLong("documentId", postEmail.documentId);
						fragment.setArguments(args);
						if (postDetailFragment != null) {
							if (postDetailFragment.getActivity() instanceof PostActivity) {
								PostActivity ra = (PostActivity) postDetailFragment.getActivity();
								ra.switchContent(fragment);
							}
						}
						if (mainActivity != null) {
							if (mainActivity instanceof MainActivity) {
								mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
							}
						}
					}
				});

				menuContainer.setVisibility(View.GONE);
				menuLayout.setOnClickListener(new OnClickListener() {

					boolean isVisibel = false;

					@Override
					public void onClick(View v) {
						if (!isVisibel) {
							menuContainer.setVisibility(View.VISIBLE);
							isVisibel = true;
						} else {
							menuContainer.setVisibility(View.GONE);
							isVisibel = false;
						}
					}
				});

				setMenuTouchDelegateArea(subEmialItemLayout, menuLayout);

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postEmail.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postEmail.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}

				messageTv.setText(postEmail.body);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				if (postEmail.user != null && postEmail.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postEmail.user.image, profileIv, options);
					createdByTv.setText(postEmail.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postEmail.childList.isEmpty() || !isChildBuild) {
					subContainerLayout.setVisibility(View.GONE);
				} else {
					subContainerLayout.setVisibility(View.VISIBLE);
					addPostTaskSubViews(context, options, subLayout, postEmail.childList, showAll, postDetailFragment, mainActivity, viewPoolManager);
				}

				if (postEmail.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_email_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_email);
				}

				subPostLayout.addView(subEmialItemLayout);
			} else if (iPostChild.getType().equals(EPostChildType.PostWfTask)) {
				final PostWfTask postWfTask = (PostWfTask) iPostChild;

				View wfTaskView = viewPoolManager.getView(EPostChildType.PostWfTask);

				ImageView profileIv = (ImageView) wfTaskView.findViewById(R.id.post_wftask_profileIv);
				TextView createdOnDateTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_createdOnTimeTv);
				TextView messageTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_messageTv);
				TextView createdByTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_createdByTv);
				TextView titleTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_titleTv);
				TextView dueDateTv = (TextView) wfTaskView.findViewById(R.id.post_wftask_dueDateTv);
				LinearLayout subLayout = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_subLayout);
				LinearLayout statsLL = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_statusLL);
				LinearLayout iconLayout = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_iconLL);
				final LinearLayout menuLayout = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_menuLL);
				LinearLayout docIconLayout = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_docIconLL);
				final RelativeLayout subContainerLayout = (RelativeLayout) wfTaskView.findViewById(R.id.post_wftask_subContainerLayout);
				final LinearLayout menuContainer = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_menuContainer);
				final LinearLayout replyBtn = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_menuReplyBtn);
				final LinearLayout workflowBtn = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_menuWorkflowBtn);
				final LinearLayout workflowAcceptBtn = (LinearLayout) wfTaskView.findViewById(R.id.post_wftask_menuWorkflowAccpetBtn);

				Object tag = wfTaskView.getTag();
				if (tag != null) {
					((ViewTag) tag).type = EPostChildType.PostWfTask;
					((ViewTag) tag).subSubHolder = subLayout;
				} else {
					ViewTag viewTag = new ViewTag();
					viewTag.type = EPostChildType.PostWfTask;
					viewTag.subSubHolder = subLayout;
					wfTaskView.setTag(viewTag);
				}

				if (postWfTask.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
				}

				replyBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						SubCommentDialog dialog = new SubCommentDialog(context, postWfTask.id, postWfTask.subPostType, onWallUpdateListener, postDetailFragment, null);
						dialog.show();
					}
				});

				statsLL.setBackgroundResource(WorkflowStatus.getStatusIcon(postWfTask.status));

				switch (postWfTask.status) {
				case WorkflowStatus.CREATED:
					workflowAcceptBtn.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							new GetWfTaskStatusTask(context, postWfTask.id, postDetailFragment, onWallUpdateListener).execute();
						}
					});
					workflowBtn.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent(context, WfTaskExecutionActivity.class);
							intent.putExtra("postWfTask", postWfTask);
							context.startActivity(intent);
						}
					});
					break;
				case WorkflowStatus.ACCEPTED:
					workflowAcceptBtn.setVisibility(View.GONE);
					workflowBtn.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent(context, WfTaskExecutionActivity.class);
							intent.putExtra("postWfTask", postWfTask);
							context.startActivity(intent);
						}
					});
					break;
				default:
					workflowBtn.setVisibility(View.GONE);
					workflowAcceptBtn.setVisibility(View.GONE);
					break;
				}

				if (!postWfTask.recipient) {
					workflowBtn.setVisibility(View.GONE);
					workflowAcceptBtn.setVisibility(View.GONE);
				}

				menuContainer.setVisibility(View.GONE);
				menuLayout.setOnClickListener(new OnClickListener() {

					boolean isVisibel = false;

					@Override
					public void onClick(View v) {
						if (!isVisibel) {
							menuContainer.setVisibility(View.VISIBLE);
							isVisibel = true;
						} else {
							menuContainer.setVisibility(View.GONE);
							isVisibel = false;
						}
					}
				});

				setMenuTouchDelegateArea(wfTaskView, menuLayout);

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postWfTask.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postWfTask.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				dueDateTv.setText(postWfTask.formatedDueDate);
				dueDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}

				messageTv.setText(postWfTask.message);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				titleTv.setText(postWfTask.title);
				titleTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postWfTask.user != null && postWfTask.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postWfTask.user.image, profileIv, options);
					createdByTv.setText(postWfTask.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postWfTask.childList.isEmpty() || !isChildBuild) {
					subContainerLayout.setVisibility(View.GONE);
				} else {
					subContainerLayout.setVisibility(View.VISIBLE);
					addPostTaskSubViews(context, options, subLayout, postWfTask.childList, showAll, postDetailFragment, mainActivity, viewPoolManager);
				}

				if (postWfTask.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_workflow_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_workflow);
				}

				subPostLayout.addView(wfTaskView);
			} else if (iPostChild.getType().equals(EPostChildType.PostChat)) {
				PostChat postChat = (PostChat) iPostChild;

				View postChatView = viewPoolManager.getView(EPostChildType.PostChat);

				Object tag = postChatView.getTag();
				if (tag != null) {
					((ViewTag) tag).type = EPostChildType.PostChat;
					((ViewTag) tag).subSubHolder = null;
				} else {
					ViewTag viewTag = new ViewTag();
					viewTag.type = EPostChildType.PostChat;
					viewTag.subSubHolder = null;
					postChatView.setTag(viewTag);
				}

				ImageView profileIv = (ImageView) postChatView.findViewById(R.id.post_chat_profileIv);
				TextView createdOnDateTv = (TextView) postChatView.findViewById(R.id.post_chat_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) postChatView.findViewById(R.id.post_chat_createdOnTimeTv);
				TextView messageTv = (TextView) postChatView.findViewById(R.id.post_chat_messageTv);
				TextView createdByTv = (TextView) postChatView.findViewById(R.id.post_chat_createdByTv);
				LinearLayout iconLayout = (LinearLayout) postChatView.findViewById(R.id.post_chat_chatBtn);
				LinearLayout docIconLayout = (LinearLayout) postChatView.findViewById(R.id.post_chat_docIconLL);

				if (postChat.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
				}

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postChat.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postChat.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}

				messageTv.setText(postChat.message);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				if (postChat.user != null && postChat.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postChat.user.image, profileIv, options);
					createdByTv.setText(postChat.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postChat.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_chat_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_chat);
				}

				subPostLayout.addView(postChatView);
			} else if (iPostChild.getType().equals(EPostChildType.PostComment)) {
				final PostComment postComment = (PostComment) iPostChild;

				View subCommentItemLayout = viewPoolManager.getView(EPostChildType.PostComment);

				ImageView profileIv = (ImageView) subCommentItemLayout.findViewById(R.id.post_comment_profileIv);
				TextView createdOnDateTv = (TextView) subCommentItemLayout.findViewById(R.id.post_comment_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) subCommentItemLayout.findViewById(R.id.post_comment_createdOnTimeTv);
				TextView messageTv = (TextView) subCommentItemLayout.findViewById(R.id.post_comment_messageTv);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());
				TextView createdByTv = (TextView) subCommentItemLayout.findViewById(R.id.post_comment_createdByTv);
				LinearLayout subLayout = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_subLayout);
				final LinearLayout menuLayout = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuLL);
				LinearLayout iconLayout = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_iconLL);
				LinearLayout docIconLayout = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_docIconLL);
				final RelativeLayout subContainerLayout = (RelativeLayout) subCommentItemLayout.findViewById(R.id.post_comment_subContainerLayout);
				final LinearLayout menuContainer = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuContainer);
				final LinearLayout replyBtn = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuReplyBtn);
				final LinearLayout dokumentumBtn = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuDokumentumBtn);

				Object tag = subCommentItemLayout.getTag();
				if (tag != null) {
					((ViewTag) tag).type = EPostChildType.PostComment;
					((ViewTag) tag).subSubHolder = subLayout;
				} else {
					ViewTag viewTag = new ViewTag();
					viewTag.type = EPostChildType.PostComment;
					viewTag.subSubHolder = subLayout;
					subCommentItemLayout.setTag(viewTag);
				}

				if (postComment.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
					dokumentumBtn.setVisibility(View.GONE);
				}

				replyBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						SubCommentDialog dialog = new SubCommentDialog(context, postComment.id, postComment.subPostType, onWallUpdateListener, postDetailFragment, null);
						dialog.show();
					}
				});

				int quickCommentArraySize = postComment.quickResponseList.size();
				switch (quickCommentArraySize) {
				case 1:
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(0), subCommentItemLayout, R.id.post_comment_menuReplyQickOneBtn, R.id.post_comment_menuReplyQickOneTv, R.id.post_comment_menuReplyQickOneSeparatorLayout, postDetailFragment, onWallUpdateListener);
					break;
				case 2:
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(0), subCommentItemLayout, R.id.post_comment_menuReplyQickOneBtn, R.id.post_comment_menuReplyQickOneTv, R.id.post_comment_menuReplyQickOneSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(1), subCommentItemLayout, R.id.post_comment_menuReplyQickTwoBtn, R.id.post_comment_menuReplyQickTwoTv, R.id.post_comment_menuReplyQickTwoSeparatorLayout, postDetailFragment, onWallUpdateListener);
					break;
				case 3:
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(0), subCommentItemLayout, R.id.post_comment_menuReplyQickOneBtn, R.id.post_comment_menuReplyQickOneTv, R.id.post_comment_menuReplyQickOneSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(1), subCommentItemLayout, R.id.post_comment_menuReplyQickTwoBtn, R.id.post_comment_menuReplyQickTwoTv, R.id.post_comment_menuReplyQickTwoSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(2), subCommentItemLayout, R.id.post_comment_menuReplyQickThreeBtn, R.id.post_comment_menuReplyQickThreeTv, R.id.post_comment_menuReplyQickThreeSeparatorLayout, postDetailFragment, onWallUpdateListener);
					break;
				case 4:
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(0), subCommentItemLayout, R.id.post_comment_menuReplyQickOneBtn, R.id.post_comment_menuReplyQickOneTv, R.id.post_comment_menuReplyQickOneSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(1), subCommentItemLayout, R.id.post_comment_menuReplyQickTwoBtn, R.id.post_comment_menuReplyQickTwoTv, R.id.post_comment_menuReplyQickTwoSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(2), subCommentItemLayout, R.id.post_comment_menuReplyQickThreeBtn, R.id.post_comment_menuReplyQickThreeTv, R.id.post_comment_menuReplyQickThreeSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(3), subCommentItemLayout, R.id.post_comment_menuReplyQickFourBtn, R.id.post_comment_menuReplyQickFourTv, R.id.post_comment_menuReplyQickFourSeparatorLayout, postDetailFragment, onWallUpdateListener);
					break;
				case 5:
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(0), subCommentItemLayout, R.id.post_comment_menuReplyQickOneBtn, R.id.post_comment_menuReplyQickOneTv, R.id.post_comment_menuReplyQickOneSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(1), subCommentItemLayout, R.id.post_comment_menuReplyQickTwoBtn, R.id.post_comment_menuReplyQickTwoTv, R.id.post_comment_menuReplyQickTwoSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(2), subCommentItemLayout, R.id.post_comment_menuReplyQickThreeBtn, R.id.post_comment_menuReplyQickThreeTv, R.id.post_comment_menuReplyQickThreeSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(3), subCommentItemLayout, R.id.post_comment_menuReplyQickFourBtn, R.id.post_comment_menuReplyQickFourTv, R.id.post_comment_menuReplyQickFourSeparatorLayout, postDetailFragment, onWallUpdateListener);
					addSubcommentQickResponseItem(context, postComment, postComment.quickResponseList.get(4), subCommentItemLayout, R.id.post_comment_menuReplyQickFiveBtn, R.id.post_comment_menuReplyQickFiveTv, R.id.post_comment_menuReplyQickFiveSeparatorLayout, postDetailFragment, onWallUpdateListener);
					break;
				default:
					break;
				}
				
				if (!showAll) {
					messageTv.setLines(2);
				}

				
				if(postComment.technicalFile != null && !postComment.technicalFile.isEmpty() && postComment.technicalFile != "null"){
//					LinearLayout downloadBtn = (LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuDownloadBtn);
//					downloadBtn.setVisibility(View.VISIBLE);
//					downloadBtn.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//							DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + postComment.technicalFile));
//							request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
//							request.setDescription(postComment.message);
//							request.setTitle(context.getString(R.string.document_download_title));
//							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//								request.allowScanningByMediaScanner();
//								request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//							}
//							request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, postComment.message);
//							((DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
//						}
//					});
//					((LinearLayout) subCommentItemLayout.findViewById(R.id.post_comment_menuDownloadSepLayout)).setVisibility(View.VISIBLE);
					
		
					String text = "<a href=''>"+ postComment.message +"</a>";
					messageTv.setText(Html.fromHtml(text));
					messageTv.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + postComment.technicalFile));
							
							String url = getExtension(postComment.message);
							if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
								request.setMimeType("application/msword");
							} else if (url.toString().contains(".pdf")) {
								request.setMimeType("application/pdf");
							} else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
								request.setMimeType("application/vnd.ms-powerpoint");
							} else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
								request.setMimeType( "application/vnd.ms-excel");
							} else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
								request.setMimeType( "application/zip");;
							} else if (url.toString().contains(".rtf")) {
								request.setMimeType( "application/rtf");
							} else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
								request.setMimeType("audio/x-wav");
							} else if (url.toString().contains(".gif")) {
								request.setMimeType("image/gif");
							} else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
								request.setMimeType( "image/jpeg");
							} else if (url.toString().contains(".txt")) {
								request.setMimeType("text/plain");
							} else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
								request.setMimeType( "video/*");
							} else {
								request.setMimeType( "*/*");
							}
							
							request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
							request.setDescription(postComment.message);
							request.setTitle(context.getString(R.string.document_download_title));
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								request.allowScanningByMediaScanner();
								request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
							}
							request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, postComment.message);
							((DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
							
						}
					});
				}else{
					messageTv.setText(postComment.message);
					messageTv.setAutoLinkMask(Linkify.WEB_URLS);
					messageTv.setLinksClickable(true);
				}

				dokumentumBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Fragment fragment = new DocumentFragment();
						Bundle args = new Bundle();
						args.putLong("documentId", postComment.documentId);
						fragment.setArguments(args);
						if (postDetailFragment != null) {
							if (postDetailFragment.getActivity() instanceof PostActivity) {
								PostActivity ra = (PostActivity) postDetailFragment.getActivity();
								ra.switchContent(fragment);
							}
						}
						if (mainActivity != null) {
							if (mainActivity instanceof MainActivity) {
								mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
							}
						}
					}
				});

				menuContainer.setVisibility(View.GONE);
				menuLayout.setOnClickListener(new OnClickListener() {

					boolean isVisibel = false;

					@Override
					public void onClick(View v) {
						if (!isVisibel) {
							menuContainer.setVisibility(View.VISIBLE);
							isVisibel = true;
						} else {
							menuContainer.setVisibility(View.GONE);
							isVisibel = false;
						}
					}
				});

				setMenuTouchDelegateArea(subCommentItemLayout, menuLayout);

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postComment.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postComment.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

			
				
				if (postComment.user != null && postComment.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postComment.user.image, profileIv, options);
					createdByTv.setText(postComment.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postComment.childList.isEmpty() || !isChildBuild) {
					subContainerLayout.setVisibility(View.GONE);
				} else {
					subContainerLayout.setVisibility(View.VISIBLE);
					addPostTaskSubViews(context, options, subLayout, postComment.childList, showAll, postDetailFragment, mainActivity, viewPoolManager);
				}

				if (postComment.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_comment_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_comment);
				}

				subPostLayout.addView(subCommentItemLayout);
			}
		}
	}
	
	private static String getExtension(String file) {
	    String ext = null;
	    int i = file.lastIndexOf('.');

	    if (i > 0 &&  i < file.length()-1) {
	        ext = file.substring(i).toLowerCase();
	    }
	    return ext;
	}

	private static void addSubcommentQickResponseItem(final Context context, final PostComment postComment, final String response, View subCommentItemLayout, int btnId, int textId, int sepLayoutID, final PostDetailFragment postDetailFragment, final OnWallUpdateListener onWallUpdateListener) {
		LinearLayout oneLayout = (LinearLayout) subCommentItemLayout.findViewById(btnId);
		oneLayout.setVisibility(View.VISIBLE);
		oneLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SubCommentDialog dialog = new SubCommentDialog(context, postComment.id, postComment.subPostType, onWallUpdateListener, postDetailFragment, response);
				dialog.show();
			}
		});
		((TextView) subCommentItemLayout.findViewById(textId)).setText(response);
		((LinearLayout) subCommentItemLayout.findViewById(sepLayoutID)).setVisibility(View.VISIBLE);
	}

	/**
	 * 
	 * Build topic sub sub items view
	 * 
	 * @param context
	 * @param options
	 * @param subLayout
	 * @param childs
	 * @param showAll
	 * @param postDetailFragment
	 * @param mainActivity
	 * @param viewPoolManager
	 */
	private static void addPostTaskSubViews(final Context context, DisplayImageOptions options, LinearLayout subLayout, List<IPostChild> childs, boolean showAll, final PostDetailFragment postDetailFragment, final MainActivity mainActivity, ViewPoolManager viewPoolManager) {
		subLayout.removeAllViews();

		int count = 0;

		for (IPostChild iPostSubCild : childs) {
			if (iPostSubCild.getType().equals(EPostChildType.PostSubTask)) {

				final PostSubTask postSubTask = (PostSubTask) iPostSubCild;

				View postSubTaskView = viewPoolManager.getView(EPostChildType.PostSubTask);
				postSubTaskView.setTag(EPostChildType.PostSubTask);

				ImageView profileIv = (ImageView) postSubTaskView.findViewById(R.id.post_sub_task_profileIv);
				TextView createdOnDateTv = (TextView) postSubTaskView.findViewById(R.id.post_sub_task_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) postSubTaskView.findViewById(R.id.post_sub_task_createdOnTimeTv);
				TextView messageTv = (TextView) postSubTaskView.findViewById(R.id.post_sub_task_messageTv);
				TextView createdByTv = (TextView) postSubTaskView.findViewById(R.id.post_sub_task_createdByTv);
				TextView dueDateTv = (TextView) postSubTaskView.findViewById(R.id.post_sub_task_dueDateTv);
				LinearLayout statusLL = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_statusLL);
				LinearLayout iconLayout = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_iconLL);
				final LinearLayout menuLayout = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_menuLL);
				LinearLayout docIconLayout = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_docIconLL);
				final LinearLayout menuContainer = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_menuContainer);
				final LinearLayout dokumentumBtn = (LinearLayout) postSubTaskView.findViewById(R.id.post_sub_task_menuDokumentumBtn);
				final LinearLayout taskExecutionBtn = (LinearLayout) postSubTaskView.findViewById(R.id.post_task_taskExecutionBtn);

				menuContainer.setVisibility(View.GONE);
				menuLayout.setOnClickListener(new OnClickListener() {

					boolean isVisibel = false;

					@Override
					public void onClick(View v) {
						if (!isVisibel) {
							menuContainer.setVisibility(View.VISIBLE);
							isVisibel = true;
						} else {
							menuContainer.setVisibility(View.GONE);
							isVisibel = false;
						}
					}
				});

				if (postSubTask.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
					dokumentumBtn.setVisibility(View.GONE);
				}

				dueDateTv.setText(postSubTask.formatedDueDate);
				dueDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postSubTask.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postSubTask.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}

				messageTv.setText(postSubTask.message);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				dokumentumBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Fragment fragment = new DocumentFragment();
						Bundle args = new Bundle();
						args.putLong("documentId", postSubTask.documentId);
						fragment.setArguments(args);
						if (postDetailFragment != null) {
							if (postDetailFragment.getActivity() instanceof PostActivity) {
								PostActivity ra = (PostActivity) postDetailFragment.getActivity();
								ra.switchContent(fragment);
							}
						}
						if (mainActivity != null) {
							if (mainActivity instanceof MainActivity) {
								mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_content_frameLayout, fragment).commit();
							}
						}
					}
				});

				taskExecutionBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(context, TaskExecutionActivity.class);
						intent.putExtra("postSubTask", postSubTask);
						context.startActivity(intent);
					}
				});

				if (!postSubTask.recipient) {
					taskExecutionBtn.setVisibility(View.GONE);
				}

				setMenuTouchDelegateArea(postSubTaskView, menuLayout);

				if (postSubTask.user != null && postSubTask.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postSubTask.user.image, profileIv, options);
					createdByTv.setText(postSubTask.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				switch (postSubTask.status) {
				case TaskStatus.TASK_ACCEPTED:
					statusLL.setBackgroundResource(R.drawable.ic_check);
					break;
				case TaskStatus.TASK_ASSIGNED:
					statusLL.setBackgroundResource(R.drawable.ic_arrow);
					break;
				case TaskStatus.TASK_CLOSED:
					statusLL.setBackgroundResource(R.drawable.ic_green_circle);
					break;
				case TaskStatus.TASK_REJECTED:
					statusLL.setBackgroundResource(R.drawable.ic_x);
					break;
				case TaskStatus.TASK_REDIRECTED:
					statusLL.setBackgroundResource(R.drawable.ic_arrow_down);
					break;
				default:
					break;
				}

				if (postSubTask.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_tasks_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_tasks);
				}

				subLayout.addView(postSubTaskView);
			} else if (iPostSubCild.getType().equals(EPostChildType.PostSubComment)) {
				PostSubComment postSubComment = (PostSubComment) iPostSubCild;

				View postSubCommentView = viewPoolManager.getView(EPostChildType.PostSubComment);
				postSubCommentView.setTag(EPostChildType.PostSubComment);

				ImageView profileIv = (ImageView) postSubCommentView.findViewById(R.id.post_sub_comment_profileIv);
				TextView createdOnDateTv = (TextView) postSubCommentView.findViewById(R.id.post_sub_comment_createdOnDateTv);
				TextView createdOnTimeTv = (TextView) postSubCommentView.findViewById(R.id.post_sub_comment_createdOnTimeTv);
				TextView messageTv = (TextView) postSubCommentView.findViewById(R.id.post_sub_comment_messageTv);
				TextView createdByTv = (TextView) postSubCommentView.findViewById(R.id.post_sub_comment_createdByTv);
				LinearLayout iconLayout = (LinearLayout) postSubCommentView.findViewById(R.id.post_sub_comment_iconLL);
				LinearLayout docIconLayout = (LinearLayout) postSubCommentView.findViewById(R.id.post_sub_comment_docIconLL);

				if (postSubComment.documentId != 0) {
					docIconLayout.setVisibility(View.VISIBLE);
				} else {
					docIconLayout.setVisibility(View.INVISIBLE);
				}

				createdOnDateTv.setText(Config.LIST_ITEM_DATE_FORMAT.format(new Date(postSubComment.createdOn)));
				createdOnDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				createdOnTimeTv.setText(Config.TIME_FORMAT.format(new Date(postSubComment.createdOn)));
				createdOnTimeTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (!showAll) {
					messageTv.setLines(2);
				}

				messageTv.setText(postSubComment.message);
				messageTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

				if (postSubComment.user != null && postSubComment.user.image != null) {
					ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + postSubComment.user.image, profileIv, options);
					createdByTv.setText(postSubComment.user.formatedName);
				}

				createdByTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

				if (postSubComment.unreaded) {
					iconLayout.setBackgroundResource(R.drawable.ic_comment_orange);
				} else {
					iconLayout.setBackgroundResource(R.drawable.ic_comment);
				}

				subLayout.addView(postSubCommentView);
			}

			count++;

			if (!showAll && count == 1) {
				return;
			}
		}
	}

	private static final int MENU_BTN_DELEGATE_AREA = 200;

	/**
	 * Set list item menu button touch area
	 * 
	 * @param main
	 * @param sub
	 */
	public static void setMenuTouchDelegateArea(View main, final LinearLayout sub) {
		main.post(new Runnable() {
			public void run() {
				Rect delegateArea = new Rect();
				LinearLayout delegate = sub;
				delegate.getHitRect(delegateArea);
				delegateArea.top -= MENU_BTN_DELEGATE_AREA;
				delegateArea.bottom += MENU_BTN_DELEGATE_AREA;
				delegateArea.left -= MENU_BTN_DELEGATE_AREA;
				delegateArea.right += MENU_BTN_DELEGATE_AREA;
				TouchDelegate expandedArea = new TouchDelegate(delegateArea, delegate);

				if (View.class.isInstance(delegate.getParent())) {
					((View) delegate.getParent()).setTouchDelegate(expandedArea);
				}
			};
		});
	}
}
