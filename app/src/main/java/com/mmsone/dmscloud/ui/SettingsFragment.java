package com.mmsone.dmscloud.ui;

import android.app.Activity;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.ui.helper.PreferenceCompatFragment;

public class SettingsFragment extends PreferenceCompatFragment {

    public static final int DRAWER_LIST_POSITION = 6;

    private MainActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (MainActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_settings);
    }

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.preferences);
        /**
         * Enable or disable push
         */
        CheckBoxPreference pref = (CheckBoxPreference) getPreferenceManager().findPreference("notificationPref");
        pref.setChecked(PrefManager.getInstance().isPush());
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue instanceof Boolean) {
                    PrefManager.getInstance().setPush((Boolean) newValue);
                }
                return true;
            }
        });
    }
}