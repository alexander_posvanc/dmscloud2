package com.mmsone.dmscloud.ui;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;
import com.mmsone.dmscloud.ui.helper.DMSHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Alexander on 2015. 09. 08..
 */
public class TechnicalPostDetailFragment extends Fragment {

    private Post mPost;
    private LinearLayout mChildLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View technical = inflater.inflate(R.layout.fragment_technical_post_detail, container, false);
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).build();

        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        mChildLayout = (LinearLayout) technical.findViewById(R.id.techincal_detail_subLayouts);
        TextView postTitle = (TextView) technical.findViewById(R.id.post_card_item_title_Tv);
        TextView profileName = (TextView) technical.findViewById(R.id.post_card_item_profileName_Tv);
        TextView postDate = (TextView) technical.findViewById(R.id.post_card_item_post_date_Tv);
        ImageView profilePicture = (ImageView) technical.findViewById(R.id.post_card_item_profile_Iv);

        postTitle.setText(mPost.title);
        postTitle.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        profileName.setText(mPost.user.formatedName);
        profileName.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
        postDate.setText(mPost.formatedCreatedOn);
        ImageLoader.getInstance().displayImage(PrefManager.getInstance().getServerUrl() + mPost.user.image, profilePicture, displayImageOptions);


        addChildViews(mPost);
        return technical;
    }


    public void addChildViews(final Post post) {
        LinearLayout[] linearLayouts = new LinearLayout[post.childList.size()];
        TextView[] textViews = new TextView[post.childList.size()];
        TextView[] nameViews = new TextView[post.childList.size()];
        TextView[] timeViews = new TextView[post.childList.size()];
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        for (int i = 0; i < post.childList.size(); i++) {
            linearLayouts[i] = (LinearLayout) inflater.inflate(R.layout.technical_comment_item, null);
            textViews[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_message);
            nameViews[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_name);
            timeViews[i] = (TextView) linearLayouts[i].findViewById(R.id.subchild_layout_time);

            textViews[i].setText(((PostComment) post.childList.get(i)).message);
            nameViews[i].setText(((PostComment) post.childList.get(i)).user.formatedName);
            nameViews[i].setTypeface(FontManager.getInstance().getRobotoBoldTtf());
            timeViews[i].setText(DMSHelper.longToTime(getActivity(), System.currentTimeMillis() - ((PostComment) post.childList.get(i)).createdOn));

            if (((PostComment) post.childList.get(i)).technicalFile != null && ((PostComment) post.childList.get(i)).technicalFile != "" && ((PostComment) post.childList.get(i)).technicalFile != "null") {
                textViews[i].setTextColor(getActivity().getResources().getColor(R.color.color_primary_actionbar));

                //download file
                final int finalI = i;
                textViews[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(PrefManager.getInstance().getServerUrl() + ((PostComment) post.childList.get(finalI)).technicalFile));

                        String url = DMSHelper.getExtension(((PostComment) post.childList.get(finalI)).message);
                        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                            request.setMimeType("application/msword");
                        } else if (url.toString().contains(".pdf")) {
                            request.setMimeType("application/pdf");
                        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                            request.setMimeType("application/vnd.ms-powerpoint");
                        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                            request.setMimeType("application/vnd.ms-excel");
                        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                            request.setMimeType("application/zip");
                            ;
                        } else if (url.toString().contains(".rtf")) {
                            request.setMimeType("application/rtf");
                        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                            request.setMimeType("audio/x-wav");
                        } else if (url.toString().contains(".gif")) {
                            request.setMimeType("image/gif");
                        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                            request.setMimeType("image/jpeg");
                        } else if (url.toString().contains(".txt")) {
                            request.setMimeType("text/plain");
                        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                            request.setMimeType("video/*");
                        } else {
                            request.setMimeType("*/*");
                        }

                        request.addRequestHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
                        request.setDescription(((PostComment) post.childList.get(finalI)).message);
                        request.setTitle(getActivity().getString(R.string.document_download_title));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        }
                        request.setDestinationInExternalPublicDir(Config.DOWNLOAD_DIR, ((PostComment) post.childList.get(finalI)).message);
                        ((DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE)).enqueue(request);
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.downloading), Toast.LENGTH_SHORT).show();

                    }
                });
            }

            mChildLayout.addView(linearLayouts[i]);
        }
    }
}
