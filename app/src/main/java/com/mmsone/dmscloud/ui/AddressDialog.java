  package com.mmsone.dmscloud.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface.OnCancelListener;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.CodeStoreManager;
import com.mmsone.dmscloud.bl.CountryManager;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bo.bean.City;
import com.mmsone.dmscloud.bo.bean.CodeStore;
import com.mmsone.dmscloud.bo.bean.County;
import com.mmsone.dmscloud.helper.DialogHelper;

public class AddressDialog {

	private Dialog mDialog;
	private Activity mActivity;
	private Button mAddressBtn;
	private Button mNegativeButton, mPositiveButton;
	private Spinner mAddressTypeSpinner, mCountrySpinner, mCountySpinner, mCitySpinner;

	public static enum EDialogStyle {
		DARK, LIGHT
	}

	public AddressDialog(Activity activity, Button button) {
		mActivity = activity;
		mDialog = new Dialog(activity);
		mAddressBtn = button;
		setup(mActivity, EDialogStyle.LIGHT);
	}

	public AddressDialog(Activity activity, EDialogStyle style) {
		mDialog = new Dialog(activity);
		setup(activity, style);
	}

	public AddressDialog(Activity activity, int theme, boolean cancelable, OnCancelListener onCancelListener, EDialogStyle style) {
		mDialog = new Dialog(activity, theme);
		mDialog.setCancelable(cancelable);
		mDialog.setOnCancelListener(onCancelListener);
		setup(activity, style);
	}

	/**
	 * Setup address dialog
	 * 
	 * @param activity
	 * @param style
	 */
	private void setup(Activity activity, EDialogStyle style) {
		mActivity = activity;

		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		mDialog.setContentView(R.layout.dialog_address);

		mNegativeButton = (Button) mDialog.findViewById(R.id.negativeButton);
		mNegativeButton.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mNegativeButton.getLayoutParams();
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.weight = 0.5f;
		mNegativeButton.setLayoutParams(params);

		mPositiveButton = (Button) mDialog.findViewById(R.id.postiveButton);
		mPositiveButton.setTypeface(FontManager.getInstance().getRobotoLightTtf());

		params = (LinearLayout.LayoutParams) mPositiveButton.getLayoutParams();
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.weight = 0.5f;
		mPositiveButton.setLayoutParams(params);

		((TextView) mDialog.findViewById(R.id.addres_dialog_addressTypeTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mAddressTypeSpinner = (Spinner) mDialog.findViewById(R.id.addres_dialog_addressTypeSpinner);

		((TextView) mDialog.findViewById(R.id.addres_dialog_countryTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCountrySpinner = (Spinner) mDialog.findViewById(R.id.addres_dialog_countrySpinner);

		((TextView) mDialog.findViewById(R.id.addres_dialog_countyTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCountySpinner = (Spinner) mDialog.findViewById(R.id.addres_dialog_countySpinner);

		((TextView) mDialog.findViewById(R.id.addres_dialog_cityTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCitySpinner = (Spinner) mDialog.findViewById(R.id.addres_dialog_citySpinner);
		mCitySpinner.setAdapter(new ArrayAdapter<City>(mActivity, android.R.layout.simple_spinner_item, new ArrayList<City>()));

		new GetAddressTypeCodeStoreTask(mAddressTypeSpinner).execute();
		new GetCountryCodeStoreTask(mCountrySpinner).execute();

		mCountrySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				CodeStore county = (CodeStore) parent.getItemAtPosition(position);
				if (county != null) {
					new GetCountryTask(mCountySpinner, county.id).execute();
					mCitySpinner.setAdapter(null);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mCountySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				County county = (County) parent.getItemAtPosition(position);
				if (county != null && !county.childList.isEmpty()) {
					ArrayAdapter<City> cityAdapter = new ArrayAdapter<City>(mActivity, android.R.layout.simple_spinner_item, county.childList);
					cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mCitySpinner.setAdapter(cityAdapter);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mNegativeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		mPositiveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mAddressBtn.setText(getAddress());
				dismiss();
			}
		});
	}

	private String getAddress() {
		CodeStore countryCodeStor = (CodeStore) mCountrySpinner.getSelectedItem();
		City city = (City) mCitySpinner.getSelectedItem();

		return countryCodeStor.text + " " + city.name;
	}
	
	/**
	 * Build address value string
	 * @return
	 */
	public String getAddressValue() {
		CodeStore addresTypeCodeStor = (CodeStore) mAddressTypeSpinner.getSelectedItem();
		CodeStore countryCodeStor = (CodeStore) mCountrySpinner.getSelectedItem();
		County county = (County) mCountySpinner.getSelectedItem();
		City city = (City) mCitySpinner.getSelectedItem();

		if (addresTypeCodeStor != null && countryCodeStor != null && county != null && city != null) {
			return new StringBuilder().append(addresTypeCodeStor.id).append(":").append(countryCodeStor.id).append(":").append(county.id).append(":").append(city.id).toString();
		}

		return null;
	}
	
	/**
	 * Downloads address type code stores in background
	 */ 
	public class GetAddressTypeCodeStoreTask extends SafeAsyncTask<Void, Void, List<CodeStore>> {

		private Spinner mSpinner;
		private ProgressDialog mProgressDialog;

		public GetAddressTypeCodeStoreTask(Spinner customerSpinner) {
			mSpinner = customerSpinner;
			mProgressDialog = new ProgressDialog(mActivity);
			mProgressDialog.setMessage(mActivity.getString(R.string.loading_wall_dialog_message));
		}

		@Override
		protected List<CodeStore> doWorkInBackground(Void... params) throws Exception {
			return CodeStoreManager.getInstance().parseCodeStoreList("ADDRESS_TYPE");
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected void onSuccess(List<CodeStore> response) {
			super.onSuccess(response);

			ArrayAdapter<CodeStore> dataAdapter = new ArrayAdapter<CodeStore>(mActivity, android.R.layout.simple_spinner_item, response);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinner.setAdapter(dataAdapter);
			mSpinner.setSelection(0);

			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				DialogHelper.showErrorDialog(mActivity, ex);
			}
		}
	}

	/**
	 * Downloads country code stores in background
	 */ 
	public class GetCountryCodeStoreTask extends SafeAsyncTask<Void, Void, List<CodeStore>> {

		private Spinner mSpinner;
		private ProgressDialog mProgressDialog;

		public GetCountryCodeStoreTask(Spinner customerSpinner) {
			mSpinner = customerSpinner;
			mProgressDialog = new ProgressDialog(mActivity);
			mProgressDialog.setMessage(mActivity.getString(R.string.loading_wall_dialog_message));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected List<CodeStore> doWorkInBackground(Void... params) throws Exception {
			return CodeStoreManager.getInstance().parseCodeStoreList("COUNTRY");
		}

		@Override
		protected void onSuccess(List<CodeStore> response) {
			super.onSuccess(response);

			ArrayAdapter<CodeStore> dataAdapter = new ArrayAdapter<CodeStore>(mActivity, android.R.layout.simple_spinner_item, response);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinner.setAdapter(dataAdapter);
			mSpinner.setSelection(0);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				DialogHelper.showErrorDialog(mActivity, ex);
			}
		}
	}

	/**
	 * Downloads countrys in background
	 */ 
	public class GetCountryTask extends SafeAsyncTask<Void, Void, List<County>> {

		private Spinner mCountySpinner;
		private long mCountryId;
		private ProgressDialog mProgressDialog;

		public GetCountryTask(Spinner countySpinner, long countryId) {
			mCountySpinner = countySpinner;
			mCountryId = countryId;
			mProgressDialog = new ProgressDialog(mActivity);
			mProgressDialog.setMessage(mActivity.getString(R.string.loading_wall_dialog_message));
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected List<County> doWorkInBackground(Void... params) throws Exception {
			List<County> coList = DataBaseConnector.getInstance().getCountyList(mCountryId);
			if (coList != null && !coList.isEmpty()) {
				return coList;
			} else {
				return CountryManager.getInstance().parseCountryResponse(mCountryId);
			}
		};

		@Override
		protected void onSuccess(List<County> response) {
			super.onSuccess(response);

			ArrayAdapter<County> countyAdapter = new ArrayAdapter<County>(mActivity, android.R.layout.simple_spinner_item, response);
			countyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mCountySpinner.setAdapter(countyAdapter);
			mCitySpinner.setSelection(0);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				DialogHelper.showErrorDialog(mActivity, ex);
			}
		}
	}

	public void show() {
		mDialog.show();
	}

	public boolean isShowing() {
		return mDialog.isShowing();
	}

	public void setCancelable(boolean flag) {
		mDialog.setCancelable(flag);
	}

	public void setPositiveButton(int textId, final View.OnClickListener listener) {
		mPositiveButton.setVisibility(View.VISIBLE);

		mPositiveButton.setText(textId);
		mPositiveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (listener != null) {
					listener.onClick(v);
				}
			}
		});
	}

	public void setPositiveButton(String text, final View.OnClickListener listener) {
		mPositiveButton.setVisibility(View.VISIBLE);

		mPositiveButton.setText(text);
		mPositiveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listener != null) {
					listener.onClick(v);
				}
			}
		});
	}

	public void setNegativeButton(int textId, final View.OnClickListener listener) {
		mNegativeButton.setVisibility(View.VISIBLE);

		mNegativeButton.setText(textId);
		mNegativeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listener != null) {
					listener.onClick(v);
				}
			}
		});
	}

	public void setNegativeButton(String text, final View.OnClickListener listener) {
		mNegativeButton.setVisibility(View.VISIBLE);

		mNegativeButton.setText(text);
		mNegativeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listener != null) {
					listener.onClick(v);
				}
			}
		});
	}

	public void dismiss() {
		mDialog.dismiss();
	}

	public void cancel() {
		mDialog.cancel();
	}
}