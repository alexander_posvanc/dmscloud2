package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.DownloadsAdapter;
import com.mmsone.dmscloud.bo.bean.DownloadedFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Alexander on 2015. 09. 01..
 */
public class DownloadsFragmentNew extends Fragment {

    private RecyclerView mDownloadsRecyclerView;
    private DownloadsAdapter mDownloadsAdapter;
    private List<DownloadedFile> mDownLoadedFileList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_downloads_new, container, false);

        mDownloadsRecyclerView = (RecyclerView) view.findViewById(R.id.downloads_RecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = linearLayoutManager;

        mDownloadsRecyclerView.setLayoutManager(mLayoutManager);
        mDownloadsRecyclerView.setHasFixedSize(true);

        mDownLoadedFileList = new ArrayList<>();

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File dmscloudDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Config.DOWNLOAD_DIR);

            dmscloudDir.mkdirs();

            File[] files = dmscloudDir.listFiles();
            mDownLoadedFileList.clear();
            for (File file : files) {
                mDownLoadedFileList.add(new DownloadedFile(file, new Date(file.lastModified())));
            }
        }
        Collections.reverse(mDownLoadedFileList);
        if (mDownLoadedFileList != null && !mDownLoadedFileList.isEmpty()) {
            mDownLoadedFileList.add(0, new DownloadedFile(null, mDownLoadedFileList.get(0).lastModified));
        }

        for (int i = 1; i < mDownLoadedFileList.size(); i++) {
            if (!sameDay(mDownLoadedFileList.get(i).lastModified, mDownLoadedFileList.get(i - 1).lastModified)) {
                mDownLoadedFileList.add(i, new DownloadedFile(null, mDownLoadedFileList.get(i).lastModified));
            }
        }
        mDownloadsAdapter = new DownloadsAdapter(mDownLoadedFileList, getActivity());
        mDownloadsRecyclerView.setAdapter(mDownloadsAdapter);

        getActivity().setTitle(getActivity().getResources().getString(R.string.download_fragment_title));


        return view;
    }

    public boolean sameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return sameDay;
    }
}
