package com.mmsone.dmscloud.ui.helper;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

/**
 * Created by Alexander on 2015. 09. 04..
 */
public class SVGHelper {

    private static SVGHelper mInstance;
    private Context mContext;

    public static SVGHelper getInstance(Context context) {
        if (mInstance == null) {
            return new SVGHelper(context);
        } else {
            return mInstance;
        }
    }

    private SVGHelper(Context context) {
        mContext = context;
    }

    public void setSvgToImageView(ImageView imageView, int drawable) {
        SVG svg = SVGParser.getSVGFromResource(mContext.getResources(), drawable);
        imageView.setImageDrawable(svg.createPictureDrawable());
    }

}
