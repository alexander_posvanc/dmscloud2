package com.mmsone.dmscloud.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.CodeStoreArrayAdapter;
import com.mmsone.dmscloud.adapter.CustomerArrayAdapter;
import com.mmsone.dmscloud.adapter.ResultCodeStoreArrayAdapter;
import com.mmsone.dmscloud.adapter.UserArrayAdapter;
import com.mmsone.dmscloud.async.GetUserListTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.CodeStoreManager;
import com.mmsone.dmscloud.bl.CustomerManager;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.MetaManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.ComponentType;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.CodeStore;
import com.mmsone.dmscloud.bo.bean.Customer;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.FillMetaDef;
import com.mmsone.dmscloud.bo.bean.PostWfTask;
import com.mmsone.dmscloud.bo.bean.TaskSearch;
import com.mmsone.dmscloud.bo.bean.WfTaskStatus;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

public class WfTaskExecutionActivity extends ActionBarActivity {

    private RelativeLayout mOkBtn;
    private TextView mStepNameTv;
    private Button mDueDateTv;
    private EditText mDescriptionEt;
    private Spinner mStatusSpinner, mResultSpinner;
    private LinearLayout mMetaLayout;
    private Toolbar mToolbar;
    public List<FillMetaDef> mFillMetaDefList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wf_task_execution);

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        setTitle(R.string.title_workflow);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new NotificationButton(this).hideDmsIcon();


        mOkBtn = (RelativeLayout) findViewById(R.id.wf_task_execution_okBtn);

        mStepNameTv = (TextView) findViewById(R.id.wf_task_execution_stepNameTv);
        mStepNameTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mDueDateTv = (Button) findViewById(R.id.wf_task_execution_dueDateTv);
        mDueDateTv.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mDueDateTv.setEnabled(false);

        mDescriptionEt = (EditText) findViewById(R.id.wf_task_execution_descriptionEt);


        mStatusSpinner = (Spinner) findViewById(R.id.wf_task_execution_statusSpinner);
        mResultSpinner = (Spinner) findViewById(R.id.wf_task_execution_resultSpinner);
        mMetaLayout = (LinearLayout) findViewById(R.id.wf_task_execution_metaLayout);

        ((TextView) findViewById(R.id.wf_task_execution_descriptionTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) findViewById(R.id.wf_task_execution_statusTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) findViewById(R.id.wf_task_execution_resultTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) findViewById(R.id.wf_task_execution_metaTitleTv)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        TableRow tableRow = (TableRow) findViewById(R.id.tableRow4);

        final TaskSearch taskSearch = (TaskSearch) getIntent().getSerializableExtra("taskSearch");
        final PostWfTask postWfTask = (PostWfTask) getIntent().getSerializableExtra("postWfTask");

        new GetUserListTask(this).execute();

        if (taskSearch != null) {
            if (taskSearch.status == WorkflowStatus.CREATED) {
                tableRow.setVisibility(View.GONE);
            }
            new GetWfTaskStatusTask(taskSearch.id).execute();
        }
        if (postWfTask != null) {
            if (postWfTask.status == WorkflowStatus.CREATED) {
                tableRow.setVisibility(View.GONE);
            }
            new GetWfTaskStatusTask(postWfTask.id).execute();
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (mDescriptionEt.isFocused())
                    mDescriptionEt.clearFocus();

                mDescriptionEt.requestFocus();
                InputMethodManager manager = (InputMethodManager) WfTaskExecutionActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                manager.showSoftInput(mDescriptionEt, 0);
            }
        }, 200);
    }

    /**
     * Downloads code store in background
     */
    public class GetCodeStoreTask extends SafeAsyncTask<Void, Void, List<CodeStore>> {

        private Spinner mSpinner;
        private long mCodeStoreId;
        private FillMetaDef mMeta;

        public GetCodeStoreTask(long codeStoreId, FillMetaDef meta, Spinner resultSpinner) {
            mSpinner = resultSpinner;
            mCodeStoreId = codeStoreId;
            mMeta = meta;
        }

        @Override
        protected List<CodeStore> doWorkInBackground(Void... params) throws Exception {
            return CodeStoreManager.getInstance().parseCodeStoreList(mCodeStoreId);
        }

        @Override
        protected void onSuccess(List<CodeStore> response) {
            super.onSuccess(response);
            List<CodeStore> list = new ArrayList<CodeStore>();
            if (mMeta != null && !mMeta.required) {
                list.add(null);
            }
            list.addAll(response);
            ArrayAdapter<CodeStore> dataAdapter = new ResultCodeStoreArrayAdapter(WfTaskExecutionActivity.this, R.layout.spinner_dropdown_item, list.toArray(new CodeStore[]{}));
//            dataAdapter.setDropDownViewResource(R.layout.spinner_item);
            mSpinner.setAdapter(dataAdapter);

            if (mMeta != null && mMeta.currentValue != null && mMeta.defaultValue.value != null) {

                String[] values = mMeta.defaultValue.value.split(";");
                long value = 0;
                String firstValue = values[0];
                if (!TextUtils.isEmpty(firstValue)) {
                    value = Long.parseLong(firstValue);
                }

                CodeStore currentCodeStore = null;

                for (CodeStore codeStore : response) {
                    if (codeStore.id == value) {
                        currentCodeStore = codeStore;
                    }
                }

                if (mMeta.currentValue != null && currentCodeStore != null) {

                    int index = 0;
                    for (int i = 0; i < mSpinner.getCount(); i++) {
                        CodeStore customer = (CodeStore) mSpinner.getItemAtPosition(i);
                        if (customer != null) {
                            if (customer.toString().equalsIgnoreCase(currentCodeStore.toString())) {
                                index = i;
                                i = mSpinner.getCount();
                            }
                        }
                    }
                    mSpinner.setSelection(index);

                    if (mMeta.required) {
                        try {
                            MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, "" + value));
                        } catch (JSONException e) {
                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                        }
                    }
                }
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!WfTaskExecutionActivity.this.isFinishing()) {
                DialogHelper.showErrorDialog(WfTaskExecutionActivity.this, ex);
            }
        }
    }

    /**
     * Change workflow task status in background
     */
    public class ChangeWfTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

        private long mWfTaskId;
        private int mStatus, mResultId;
        private String mMessage;
        private List<FillMeta> mMetaList;

        public ChangeWfTaskStatusTask(final long wfTaskId, final String message, final int status, final int resultId, final List<FillMeta> metaList) {
            mWfTaskId = wfTaskId;
            mStatus = status;
            mMessage = message;
            mResultId = resultId;
            mMetaList = metaList;
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().changeWfTaskStatus(mWfTaskId, mMessage, mStatus, mResultId, mMetaList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                PrefManager.getInstance().setTasksUpToDate(false);
                PrefManager.getInstance().setIsChanged(true);
                Toast.makeText(WfTaskExecutionActivity.this, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
                WfTaskExecutionActivity.this.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (!WfTaskExecutionActivity.this.isFinishing()) {
                DialogHelper.showErrorDialog(WfTaskExecutionActivity.this, ex);
            }
        }
    }

    /**
     * Download workflow task status in background
     */
    public class GetWfTaskStatusTask extends SafeAsyncTask<Void, Void, WfTaskStatus> {

        private long mWfTaskId;
        private ProgressDialog mProgressDialog;

        public GetWfTaskStatusTask(final long wfTaskId) {
            mWfTaskId = wfTaskId;
            mProgressDialog = new ProgressDialog(WfTaskExecutionActivity.this);
            mProgressDialog.setMessage(getString(R.string.wf_task_execution_loading_message));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected WfTaskStatus doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().getWfTaskStatusRequest(mWfTaskId);
        }

        @Override
        protected void onSuccess(final WfTaskStatus response) {
            super.onSuccess(response);

            mStepNameTv.setText(WfTaskExecutionActivity.this.getResources().getString(R.string.wf_execution_step_name) + response.stepName);
            mDueDateTv.setText(response.formatedDueDate);
            mDescriptionEt.setText(response.message);

            ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(WfTaskExecutionActivity.this, R.layout.spinner_item, WorkflowStatus.getWorkflowStatusList(response.status));
            statusAdapter.setDropDownViewResource(R.layout.spinner_item);
            mStatusSpinner.setAdapter(statusAdapter);
            mStatusSpinner.setSelection(0);

            try {
                buildMetaViews(response);
            } catch (DMSCloudDBException e) {
                DialogHelper.showErrorDialog(WfTaskExecutionActivity.this, e);
            }

            mOkBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    String description = mDescriptionEt.getText().toString();
                    if (description.isEmpty()) {
                        mDescriptionEt.setError(getString(R.string.err_empty_field));
                    } else {
                        boolean valid = true;
                        List<FillMeta> fillMetaList = MetaManager.getInstance().getFillMetaList();

                        if (mFillMetaDefList != null && !mFillMetaDefList.isEmpty()) {
                            for (FillMeta fillMeta : fillMetaList) {
                                for (FillMetaDef fillMetaDef : mFillMetaDefList) {
                                    if (fillMeta.id == fillMetaDef.id) {
                                        if (fillMetaDef.required) {
                                            if (!fillMeta.isValid()) {
                                                valid = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (valid) {
                            new ChangeWfTaskStatusTask(response.id, description, WorkflowStatus.getWorkflowStatusBy(((String) mStatusSpinner.getSelectedItem())), ((CodeStore) mResultSpinner.getSelectedItem()).id, fillMetaList).execute();
                        } else {
                            DialogHelper.createErrorDialog(WfTaskExecutionActivity.this, R.string.wf_task_execution_required_title);
                        }
                    }
                }
            });

            new GetCodeStoreTask(response.resultType, null, mResultSpinner).execute();

            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(WfTaskExecutionActivity.this, ex);
        }
    }

    /**
     * Build fill meta def views
     *
     * @param wfTaskStatus
     * @throws DMSCloudDBException
     */
    public void buildMetaViews(final WfTaskStatus wfTaskStatus) throws DMSCloudDBException {
        MetaManager.getInstance().clearFillMetaMap();

        mFillMetaDefList = wfTaskStatus.metaList;


        for (final FillMetaDef meta : wfTaskStatus.metaList) {
            if (!meta.hidden) {
                View metaView = View.inflate(WfTaskExecutionActivity.this, R.layout.view_post_fill_meta, null);

                final TextView labelTv = (TextView) metaView.findViewById(R.id.create_post_meta_labelTv);
                final Spinner spinner = (Spinner) metaView.findViewById(R.id.create_post_meta_valueSpinner);
                final EditText valueEt = (EditText) metaView.findViewById(R.id.create_post_meta_valueEt);
                valueEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
                final Button dateBtn = (Button) metaView.findViewById(R.id.create_post_meta_dateValueBTn);
                dateBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
                final Button timeBtn = (Button) metaView.findViewById(R.id.create_post_meta_timeValueBTn);
                timeBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
                final Button button = (Button) metaView.findViewById(R.id.create_post_meta_valueBTn);
                button.setTypeface(FontManager.getInstance().getRobotoMediumTtf());

                labelTv.setText(meta.label);
                labelTv.setTypeface(FontManager.getInstance().getRobotoLightTtf());

                if (meta.required) {
                    labelTv.setTypeface(FontManager.getInstance().getRobotoBoldTtf());
                }

                switch (meta.componentType) {
                    case ComponentType.TIME:
                        final Calendar calendar = Calendar.getInstance();
                        timeBtn.setVisibility(View.VISIBLE);
                        timeBtn.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                TimePickerDialog tpd = new TimePickerDialog(WfTaskExecutionActivity.this, new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                        try {
                                            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                            calendar.set(Calendar.MINUTE, minute);
                                            timeBtn.setText(Config.TIME_FORMAT.format(calendar.getTime()));
                                            MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, calendar.getTimeInMillis()));
                                        } catch (JSONException e) {
                                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                                        }

                                    }
                                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                                tpd.show();
                            }
                        });

                        if (meta.currentValue != null && meta.currentValue.dateValue != null) {
                            timeBtn.setText(Config.TIME_FORMAT.format(meta.currentValue.dateValue));

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.dateValue));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        if (meta.readOnly) {
                            timeBtn.setEnabled(false);
                        }
                        break;
                    case ComponentType.ADDRESS:
                        final AddressDialog dialog = new AddressDialog(WfTaskExecutionActivity.this, button);
                        button.setVisibility(View.VISIBLE);
                        button.setText(R.string.address_dialog_address_title);
                        button.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                dialog.show();
                            }
                        });

                        if (meta.currentValue != null) {
                            button.setText(meta.currentValue.value);

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        if (meta.readOnly) {
                            button.setEnabled(false);
                        }
                        break;
                    case ComponentType.USER_AND_GROUP:
                        spinner.setVisibility(View.VISIBLE);
                        List<IUser> list = new ArrayList<IUser>();
                        if (!meta.required) {
                            list.add(null);
                        }
                        list.addAll(DataBaseConnector.getInstance().getIUserList());
                        ArrayAdapter<IUser> dataAdapter = new UserArrayAdapter(WfTaskExecutionActivity.this, R.layout.view_user_spinner_item, list.toArray(new IUser[]{}));
                        spinner.setAdapter(dataAdapter);

                        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                IUser iUser = (IUser) spinner.getSelectedItem();
                                if (iUser != null) {
                                    try {
                                        switch (iUser.getType()) {
                                            case GROUP:
                                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "1" + iUser.getId()));
                                                break;
                                            case USER:
                                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "0:" + iUser.getId()));
                                                break;
                                            default:
                                                break;
                                        }
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                MetaManager.getInstance().removeFillMeta(meta.id);
                            }
                        });

                        if (meta.readOnly) {
                            spinner.setEnabled(false);
                        }

                        if (meta.currentValue != null) {
                            long value = Long.parseLong(meta.currentValue.value.substring(2));
                            int index = 0;
                            for (int i = 0; i < spinner.getCount(); i++) {
                                IUser user = (IUser) spinner.getItemAtPosition(i);
                                if (user != null) {
                                    if (user.toString().equalsIgnoreCase(DataBaseConnector.getInstance().getUser(value).formatedName)) {
                                        index = i;
                                        i = spinner.getCount();
                                    }
                                }
                            }
                            spinner.setSelection(index);

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }
                        break;
                    case ComponentType.NUMBER:
                        valueEt.setVisibility(View.VISIBLE);
                        valueEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        if (meta.currentValue != null) {
                            valueEt.setText(meta.currentValue.value);

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        valueEt.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if (!s.toString().isEmpty()) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    if (meta.required) {
                                        valueEt.setError(getString(R.string.err_empty_field));
                                    }
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }
                        });

                        if (meta.required && valueEt.getText().toString().isEmpty()) {
                            valueEt.setError(getString(R.string.err_empty_field));
                        }

                        if (meta.readOnly) {
                            valueEt.setEnabled(false);
                        }
                        break;
                    case ComponentType.CURRENCY:
                        valueEt.setVisibility(View.VISIBLE);
                        valueEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        if (meta.currentValue != null) {
                            valueEt.setText(meta.currentValue.value);

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        valueEt.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if (!s.toString().isEmpty()) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    if (meta.required) {
                                        valueEt.setError(getString(R.string.err_empty_field));
                                    }
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }

                                }
                            }
                        });

                        if (meta.required && valueEt.getText().toString().isEmpty()) {
                            valueEt.setError(getString(R.string.err_empty_field));
                        }

                        if (meta.readOnly) {
                            valueEt.setEnabled(false);
                        }

                        break;
                    case ComponentType.AUTO_NUMBER:
                        valueEt.setVisibility(View.VISIBLE);
                        valueEt.setText(meta.fieldName + " - " + meta.expectedValue);
                        valueEt.setEnabled(false);

                        if (meta.required && meta.currentValue != null && meta.currentValue.value != null) {
                            try {
                                MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                            } catch (JSONException e) {
                                DialogHelper.showErrorDialog(getApplicationContext(), e);
                            }
                        }

                        if (meta.readOnly) {
                            valueEt.setEnabled(false);
                        }
                        break;
                    case ComponentType.CUSTOMER:
                        spinner.setVisibility(View.VISIBLE);
                        new GetCustomerListTask(spinner, meta).execute();

                        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                Customer customer = (Customer) spinner.getSelectedItem();
                                if (customer != null) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + customer.id));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                MetaManager.getInstance().removeFillMeta(meta.id);
                            }
                        });
                        if (meta.readOnly) {
                            spinner.setEnabled(false);
                        }

                        break;
                    case ComponentType.COMBO:
                        spinner.setVisibility(View.VISIBLE);
                        new GetCodeStoreTask(meta.codeStoreId, meta, spinner).execute();

                        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CodeStore codeStore = (CodeStore) spinner.getSelectedItem();
                                if (codeStore != null) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + codeStore.id));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                MetaManager.getInstance().removeFillMeta(meta.id);
                            }
                        });

                        if (meta.readOnly) {
                            spinner.setEnabled(false);
                        }

                        break;
                    case ComponentType.LIST:
                        spinner.setVisibility(View.VISIBLE);
                        new GetCodeStoreTask(meta.codeStoreId, meta, spinner).execute();

                        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CodeStore codeStore = (CodeStore) spinner.getSelectedItem();
                                if (codeStore != null) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, "" + codeStore.id));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                MetaManager.getInstance().removeFillMeta(meta.id);
                            }
                        });
                        if (meta.readOnly) {
                            spinner.setEnabled(false);
                        }
                        break;
                    case ComponentType.TEXT:
                        valueEt.setVisibility(View.VISIBLE);
                        valueEt.setInputType(InputType.TYPE_CLASS_TEXT);

                        if (meta.currentValue != null) {
                            valueEt.setText(meta.currentValue.value);

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.value));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        valueEt.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if (!s.toString().isEmpty()) {
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, s.toString()));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                } else {
                                    if (meta.required) {
                                        valueEt.setError(getString(R.string.err_empty_field));
                                    }
                                    try {
                                        MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, null));
                                    } catch (JSONException e) {
                                        DialogHelper.showErrorDialog(getApplicationContext(), e);
                                    }
                                }
                            }
                        });

                        if (meta.required && valueEt.getText().toString().isEmpty()) {
                            valueEt.setError(getString(R.string.err_empty_field));
                        }

                        if (meta.readOnly) {
                            valueEt.setEnabled(false);
                        }
                        break;
                    case ComponentType.DATE:
                        final DatePickerFragment datePickerFragment = new DatePickerFragment(dateBtn, meta);

                        dateBtn.setVisibility(View.VISIBLE);
                        dateBtn.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                datePickerFragment.show(getSupportFragmentManager(), "datePicker");
                            }
                        });

                        if (meta.currentValue != null && meta.currentValue.dateValue != null) {
                            dateBtn.setText(Config.DATE_FORMAT.format(meta.currentValue.dateValue));

                            if (meta.required) {
                                try {
                                    MetaManager.getInstance().addFillMeta(meta.id, new FillMeta(meta.id, meta.currentValue.dateValue));
                                } catch (JSONException e) {
                                    DialogHelper.showErrorDialog(getApplicationContext(), e);
                                }
                            }
                        }

                        if (meta.readOnly) {
                            dateBtn.setEnabled(false);
                        }
                        break;
                }

                mMetaLayout.addView(metaView);
            }
        }

        if (wfTaskStatus.metaList == null || wfTaskStatus.metaList.isEmpty()) {
            ((TextView) findViewById(R.id.wf_task_execution_requiredTitleTv)).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.wf_task_execution_metaTitleTv)).setVisibility(View.GONE);
        }
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private Button mButton;
        private final Calendar mCalendar = Calendar.getInstance();
        private FillMetaDef mMeta;

        public DatePickerFragment(Button button, FillMetaDef meta) {
            mButton = button;
            mMeta = meta;
            mButton.setText(Config.DATE_FORMAT.format(mCalendar.getTime()));
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar.set(year, month, day, 23, 59);
            mButton.setText(Config.DATE_FORMAT.format(mCalendar.getTime()));

            try {
                MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, mCalendar.getTimeInMillis()));
            } catch (JSONException e) {
                DialogHelper.showErrorDialog(getActivity(), e);
            }
        }
    }

    /**
     * Downloads customer list in background
     */
    public class GetCustomerListTask extends SafeAsyncTask<Void, Void, List<Customer>> {

        private Spinner mSpinner;
        private FillMetaDef mMeta;

        public GetCustomerListTask(Spinner customerSpinner, FillMetaDef meta) {
            mSpinner = customerSpinner;
            mMeta = meta;
        }

        @Override
        protected List<Customer> doWorkInBackground(Void... params) throws Exception {
            return CustomerManager.getInstance().parseCostumerList();
        }

        @Override
        protected void onSuccess(List<Customer> response) {
            super.onSuccess(response);
            List<Customer> list = new ArrayList<Customer>();

            if (!mMeta.required) {
                list.add(null);
            }
            list.addAll(response);
            ArrayAdapter<Customer> dataAdapter = new CustomerArrayAdapter(WfTaskExecutionActivity.this, R.layout.view_user_spinner_item, list.toArray(new Customer[]{}));
            mSpinner.setAdapter(dataAdapter);

            if (mMeta.currentValue != null) {
                long value = Long.parseLong(mMeta.currentValue.value);
                Customer currentCustomer = null;

                for (Customer customer : response) {
                    if (customer.id == value) {
                        currentCustomer = customer;
                    }
                }

                if (currentCustomer != null) {

                    int index = 0;
                    for (int i = 0; i < mSpinner.getCount(); i++) {
                        Customer customer = (Customer) mSpinner.getItemAtPosition(i);
                        if (customer != null) {
                            if (customer.toString().equalsIgnoreCase(currentCustomer.toString())) {
                                index = i;
                                i = mSpinner.getCount();
                            }
                        }
                    }
                    mSpinner.setSelection(index);

                    if (mMeta.required) {
                        try {
                            MetaManager.getInstance().addFillMeta(mMeta.id, new FillMeta(mMeta.id, "" + value));
                        } catch (JSONException e) {
                            DialogHelper.showErrorDialog(getApplicationContext(), e);
                        }
                    }
                }
            }

        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            DialogHelper.showErrorDialog(WfTaskExecutionActivity.this, ex);
        }
    }
}
