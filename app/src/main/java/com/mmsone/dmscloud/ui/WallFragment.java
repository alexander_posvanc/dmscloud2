package com.mmsone.dmscloud.ui;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.PostAdapter;
import com.mmsone.dmscloud.adapter.WallAdapter;
import com.mmsone.dmscloud.async.GetCountryTask;
import com.mmsone.dmscloud.async.LogoutTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.OttoManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.SearchManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.ESearchFilterDesignation;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.OttoObject;
import com.mmsone.dmscloud.bo.UserRole;
import com.mmsone.dmscloud.bo.WallResponse;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.SearchFilter;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.AuthenticationException;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.gcm.GCMManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.squareup.otto.Subscribe;

public class WallFragment extends Fragment implements OnWallUpdateListener {

    public static final int DRAWER_LIST_POSITION = 1;

    private MainActivity mActivity;
    private PostAdapter mPostAdapter;
    private ListView mPostListView;
    private MenuItem mSearchItem;
    private SwipeRefreshLayout mSwipeView;
    private SearchView mSearchView;
    private List<Post> mPostList;
    private EndlessScrollListener mListener;
    private TextView mNoItemTv;
    private WallAdapter mWallAdapter;
    private int previousTotal = 0;
    private RecyclerView mPostRecyclerView;
    private String mQuery;
    private int mId;

    public boolean mIsLoading = true;

    @Override
    public void onAttach(Activity activity) {
        mActivity = (MainActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_wall);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View wallView = inflater.inflate(R.layout.fragment_wall, container, false);


        mNoItemTv = (TextView) wallView.findViewById(R.id.wall_postNoItem);
        mSwipeView = (SwipeRefreshLayout) wallView.findViewById(R.id.wall_swipe);
        mSwipeView.setEnabled(false);

        mPostList = new ArrayList<Post>();

        mPostRecyclerView = (RecyclerView) wallView.findViewById(R.id.wall_postRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = linearLayoutManager;

        mListener = new EndlessScrollListener(linearLayoutManager);
        PrefManager.getInstance().setIsFirst(true);

        mPostRecyclerView.setLayoutManager(mLayoutManager);
        mPostRecyclerView.setHasFixedSize(true);

        mWallAdapter = new WallAdapter(mPostList, getActivity(), WallFragment.this);
        mPostRecyclerView.setAdapter(mWallAdapter);

        mSwipeView.setColorSchemeResources(R.color.color_dmsclooud_purple, R.color.color_dmsclooud_pink, R.color.color_dmsclooud_green, R.color.color_dmsclooud_blue);
        mSwipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFirstWallRequest();

            }
        });

//        mSwipeView.setRefreshing(true);
//        mSwipeView.setEnabled(true);

        Bundle b = getArguments();
        if (b != null && !b.isEmpty()) {
            mQuery = b.getString("query");
            mId = b.getInt("id");
            startSimpleSearch(mQuery, mId);
        }

        if (mQuery == null || mQuery == "") {
            mPostRecyclerView.setOnScrollListener(mListener);
            PrefManager.getInstance().setIsFirstSroll(true);
        }

//        mPostListView.setOnScrollListener(mListener);
//        OttoManager.getInstance().getBus().register(this);

        return wallView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Unregister wall fragment from otto manager
//        OttoManager.getInstance().getBus().unregister(this);
    }

    /**
     * Wall paging
     */
    public class EndlessScrollListener extends RecyclerView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 1;
        private boolean loading = true; // True if we are still waiting for the last set of data to load.
        int firstVisibleItem, visibleItemCount, totalItemCount;

        private LinearLayoutManager mLinearLayoutManager;

        public EndlessScrollListener(LinearLayoutManager linearLayoutManager) {
            this.mLinearLayoutManager = linearLayoutManager;
            previousTotal = 0;

        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!PrefManager.getInstance().isFirstScroll()) {
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (totalItemCount <= 3) {
                    currentPage = 1;
                }

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached
                    new GetWallResponseTask(currentPage * Config.WALL_ROW_COUNT, Config.WALL_ROW_COUNT).execute();

                    // Do something
                    currentPage++;
                    loading = true;
                }
            } else {
                PrefManager.getInstance().setIsFirstSroll(false);
            }
        }
    }

    /**
     * Update wall list
     */
    @Override
    public void onResume() {
        super.onResume();
        if (!PrefManager.getInstance().isWallUpToDate() && !mSwipeView.isRefreshing()) {
            SearchManager.getInstance().crearFilterList();
            PrefManager.getInstance().setIsFirstSroll(true);

            mPostList.clear();
//            mPostAdapter.notifyDataSetChanged();
            mWallAdapter.refreshPostList(mPostList);

//            mListener.resetPage();
            previousTotal = 0;

            if (mQuery == null || mQuery == "") {
                new GetWallResponseTask(Config.WALL_FROM, Config.WALL_ROW_COUNT).execute();
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PrefManager.getInstance().setIsFirstSroll(true);

        mPostList.clear();
        mWallAdapter.refreshPostList(mPostList);

        previousTotal = 0;
        if (mQuery == null || mQuery == "") {
            new GetWallResponseTask(Config.WALL_FROM, Config.WALL_ROW_COUNT).execute();
        }
        if (PrefManager.getInstance().toBeUpdated()) {
            new GetCountryTask(mActivity, Config.COUNTRY_ID).execute();
        }
    }

    /**
     * Start text search
     *
     * @param query
     */
    private void startSimpleSearch(String query, int id) {
        try {
            SearchManager.getInstance().addSearcFilter(id, new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_TEXT, query));
        } catch (JSONException e) {
            DialogHelper.showErrorDialog(mActivity, e);
        }
        mPostList.clear();
//        mPostAdapter.notifyDataSetChanged();
        mWallAdapter.refreshPostList(mPostList);

//        mListener.resetPage();
        previousTotal = 0;

        new GetWallResponseTask(Config.WALL_FROM, Config.WALL_ROW_COUNT).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_wall_create_post:
                try {
                    if (UserManager.getInstance().hasUserRole(UserRole.TASK_MANAGEMENT_ROLE)) {
                        startActivity(new Intent(mActivity, CreatePostActivity.class));
                    } else {
                        DialogHelper.createErrorDialog(getActivity(), getString(R.string.err_insufficient_rights));
                    }
                } catch (DMSCloudDBException e) {
                    DialogHelper.showErrorDialog(getActivity(), e);
                }
                return true;
            case R.id.menu_item_wall_advanced_search:
                startActivityForResult(new Intent(mActivity, AdvancedSearchActivity.class), 111);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isSearch", false)) {
                    mPostList.clear();
//                    mPostAdapter.notifyDataSetChanged();
                    mWallAdapter.refreshPostList(mPostList);

//                    mListener.resetPage();
                    previousTotal = 0;

                    new GetWallResponseTask(Config.WALL_FROM, Config.WALL_ROW_COUNT).execute();
                }
            }
        }
    }

    /**
     * Downloads wall response in background
     */
    public class GetWallResponseTask extends SafeAsyncTask<Void, Void, WallResponse> {

        private int mFrom, mRowCount;

        public GetWallResponseTask(final int from, final int rowCount) {
            mRowCount = rowCount;
            mFrom = from;
        }

        @Override
        protected void onPreExecute() {
            mIsLoading = true;
            //SwipeView BugFix
            mSwipeView.setProgressViewOffset(false, 0,
                    (int) TypedValue.applyDimension(
                            TypedValue.COMPLEX_UNIT_DIP,
                            24,
                            getResources().getDisplayMetrics()));
            mSwipeView.setRefreshing(true);
            mSwipeView.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected WallResponse doWorkInBackground(Void... params) throws Exception {
            WallResponse wallResponse = PostManager.getInstance().getWallResponse(mFrom, mRowCount, getActivity());

            Set<Long> userIdSet = new HashSet<Long>();
            Map<Long, User> userMap = new HashMap<Long, User>();

            for (Post post : wallResponse.elementList) {
                userIdSet.add(post.createdBy);
                for (IPostChild child : post.childList) {
                    userIdSet.add(child.getChildCreatedBy());
                    if (child.getChildList() != null) {
                        for (IPostChild subChild : child.getChildList()) {
                            userIdSet.add(subChild.getChildCreatedBy());
                        }
                    }
                }
            }

            List<User> userList = DataBaseConnector.getInstance().getUserList(userIdSet);

            for (User user : userList) {
                userMap.put(user.id, user);
            }
            /**
             * Set wall elements users
             */
            for (Post post : wallResponse.elementList) {
                post.user = userMap.get(post.createdBy);
                for (IPostChild child : post.childList) {
                    child.setUser(userMap.get(child.getChildCreatedBy()));
                    if (child.getChildList() != null) {
                        for (IPostChild subChild : child.getChildList()) {
                            subChild.setUser(userMap.get(subChild.getChildCreatedBy()));
                        }
                    }
                }
            }

            return wallResponse;
        }

        @Override
        protected void onFinished() {
            mIsLoading = false;
            mSwipeView.setRefreshing(false);
            mSwipeView.setEnabled(true);
        }

        @Override
        protected void onSuccess(WallResponse wallResponse) {
            super.onSuccess(wallResponse);
            mPostList.addAll(wallResponse.elementList);
//            mPostAdapter.notifyDataSetChanged();
            mWallAdapter.refreshPostList(mPostList);
            PrefManager.getInstance().setWallUpToDate(true);
            PrefManager.getInstance().setIsChanged(false);
            PrefManager.getInstance().setIsFirst(true);
            SearchManager.getInstance().crearFilterList();

            if (mPostList.isEmpty()) {
                mNoItemTv.setVisibility(View.VISIBLE);
            } else {
                mNoItemTv.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            if (ex instanceof AuthenticationException) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.err_auth), Toast.LENGTH_LONG).show();
//                GCMManager.getInstance(getActivity()).sendUnRegistrationIdToBackend();
                new LogoutTask((MainActivity) getActivity()).execute();
            } else {
                DialogHelper.showErrorDialog(mActivity, ex);
            }
        }
    }

    /**
     * Update wall list
     */
    @Override
    public void onWallUpdateListener() {
        if (mIsLoading && !mSwipeView.isRefreshing()) {
            return;
        }

//        SearchManager.getInstance().crearFilterList();

        mPostList.clear();
//        mPostAdapter.notifyDataSetChanged();
        mWallAdapter.refreshPostList(mPostList);


//        mListener.resetPage();
        previousTotal = 0;

        new GetWallResponseTask(Config.WALL_FROM, Config.WALL_ROW_COUNT).execute();
        mSwipeView.setRefreshing(false);

    }

    /**
     * Update wall and send gcm registration
     *
     * @param ottoObject
     */
    @Subscribe
    public void refresh(OttoObject ottoObject) {
        onWallUpdateListener();
        GCMManager.getInstance(mActivity).registration();
    }

    /**
     * Gets the first 3 Post from the server
     * sets previusTotal to 0 to get the first 3 item from server
     * sets EndlesScrollLstener to recyclerView scroll
     * sets mQuery string to null
     */
    public void getFirstWallRequest() {
        mPostRecyclerView.setOnScrollListener(mListener);
        onWallUpdateListener();
        previousTotal = 0;
        mPostRecyclerView.getRecycledViewPool().clear();
        mQuery = null;
    }


    public WallAdapter getmWallAdapter() {
        return mWallAdapter;
    }
}