package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.MessageDetailAdapter;
import com.mmsone.dmscloud.adapter.TaskDetailAdapter;
import com.mmsone.dmscloud.bo.EPostChildType;
import com.mmsone.dmscloud.bo.IPostChild;
import com.mmsone.dmscloud.bo.bean.Post;
import com.mmsone.dmscloud.bo.bean.PostComment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015. 08. 14..
 */
public class PostDetailMessageFragment extends Fragment {
    private TextView messageCount;
    private List<IPostChild> postChildren;
    private Post mPost;
    private RecyclerView mMessageListView;
    private MessageDetailAdapter mMessageDetailAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View messageDetail = inflater.inflate(R.layout.post_detail_message_fragment, container, false);
        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        messageCount = (TextView) messageDetail.findViewById(R.id.message_count_Tv);
        mMessageListView = (RecyclerView) messageDetail.findViewById(R.id.messageFragment_recyclerView);
        postChildren = new ArrayList<>();

        for (IPostChild child : mPost.childList) {
            if (EPostChildType.PostComment.equals(child.getType())) {
                PostComment postComment = (PostComment) child;
                if (postComment.technicalFile == null || postComment.technicalFile == "" || postComment.technicalFile == "null") {
                    postChildren.add(child);
                }
            }
        }

        messageCount.setText(getResources().getString(R.string.shared_with_first) + " " + postChildren.size() + getResources().getString(R.string.post_detail_second_message));

        mMessageDetailAdapter = new MessageDetailAdapter((ArrayList<IPostChild>) postChildren, getActivity(), mPost);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mMessageListView.setLayoutManager(mLayoutManager);
        mMessageListView.setHasFixedSize(true);

        mMessageListView.setAdapter(mMessageDetailAdapter);


        return messageDetail;
    }
}
