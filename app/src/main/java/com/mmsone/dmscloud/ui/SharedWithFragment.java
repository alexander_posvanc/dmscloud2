package com.mmsone.dmscloud.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.SharedWithAdapter;
import com.mmsone.dmscloud.bo.bean.Post;

/**
 * Created by Alexander on 2015. 08. 25..
 */
public class SharedWithFragment extends Fragment {

    private RecyclerView mSharedWithRecyclerView;
    private SharedWithAdapter mSharedWithAdapter;
    private Post mPost;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View sharedView = inflater.inflate(R.layout.fragment_shared_with, container, false);

        Bundle b = getArguments();
        mPost = (Post) b.getSerializable("post");

        getActivity().setTitle(getResources().getString(R.string.share_activity_title));

        mSharedWithRecyclerView = (RecyclerView) sharedView.findViewById(R.id.sharedWith_recyclerView);
        mSharedWithAdapter = new SharedWithAdapter(mPost);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mSharedWithRecyclerView.setLayoutManager(mLayoutManager);
        mSharedWithRecyclerView.setHasFixedSize(true);
        mSharedWithRecyclerView.setAdapter(mSharedWithAdapter);

        TextView countText = (TextView) sharedView.findViewById(R.id.shared_with_countTv);
        countText.setText(getResources().getString(R.string.shared_with_first) + " " + mPost.sharedWith.size() + getResources().getString(R.string.shared_with_second));

        final ShareFragmentNew shareFragment = new ShareFragmentNew();
        Bundle args = new Bundle();
        args.putSerializable("post", mPost);
        shareFragment.setArguments(args);

        RelativeLayout shareBtn = (RelativeLayout) sharedView.findViewById(R.id.share_with_btn);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.share_container, shareFragment).commit();
            }
        });

        return sharedView;
    }
}
