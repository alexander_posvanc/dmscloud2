package com.mmsone.dmscloud.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.ShareDialogAdapter;
import com.mmsone.dmscloud.async.GetUserListTask;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.GroupAndDistributionManager;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;
import com.mmsone.dmscloud.ui.helper.DMSHelper;
import com.mmsone.dmscloud.ui.helper.MultiSelectionSpinner;
import com.mmsone.dmscloud.ui.helper.NotificationButton;

public class CreateTaskFragment extends Fragment {

    public static final int DRAWER_LIST_POSITION = 0;

    private static final ILogger LOG = Logger.getLogger(CreateTaskFragment.class);

    private SharePostActivity mActivity;
    private Button mDueDateBtn;
    private RelativeLayout mOkBtn;
    private MultiSelectionSpinner mRecipientsSpinner;
    private EditText mDescriptionEt;
    private RadioGroup mExecutiveRg;
    private long mPostId;
    private boolean mAllUser = false;
    private static Date mDueDate;
    private List<IUser> mUserList;
    private ShareDialogAdapter mShareDialogAdapter;
    private LinearLayout mRecipientLayout;
    private LinearLayout mSpinnerLayout;


    @Override
    public void onAttach(Activity activity) {
        mActivity = (SharePostActivity) activity;
        super.onAttach(activity);
        mActivity.setTitle(R.string.title_create_task);
//        mActivity.setOptionMenuVisibility(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View createTaskView = inflater.inflate(R.layout.fragment_create_task, container, false);

        mOkBtn = (RelativeLayout) createTaskView.findViewById(R.id.create_task_okBtn);
        mSpinnerLayout = (LinearLayout) createTaskView.findViewById(R.id.multiselector_dialog);
        mRecipientLayout = (LinearLayout) createTaskView.findViewById(R.id.create_task_recipientsLayout);
        mDueDateBtn = (Button) createTaskView.findViewById(R.id.create_task_dueDateBtn);

        new NotificationButton(getActivity()).hideDmsIcon();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        mDueDate = cal.getTime();

        mDueDateBtn.setText(Config.DATE_TIME_FORMAT.format(mDueDate));
        mDueDateBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mDescriptionEt = (EditText) createTaskView.findViewById(R.id.create_task_descriptionEt);
        mDescriptionEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        mExecutiveRg = (RadioGroup) createTaskView.findViewById(R.id.create_task_executiveRg);

        ((TextView) createTaskView.findViewById(R.id.create_task_recipientsTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) createTaskView.findViewById(R.id.create_task_dueDateTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) createTaskView.findViewById(R.id.create_task_executiveTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
        ((TextView) createTaskView.findViewById(R.id.create_task_descriptionTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());

        ((RadioButton) createTaskView.findViewById(R.id.create_task_allUserRb)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());
        ((RadioButton) createTaskView.findViewById(R.id.create_task_singleUserRb)).setTypeface(FontManager.getInstance().getRobotoMediumTtf());

        Bundle b = getArguments();
        mPostId = b.getLong("postId");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (mDescriptionEt.isFocused())
                    mDescriptionEt.clearFocus();

                mDescriptionEt.requestFocus();
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                manager.showSoftInput(mDescriptionEt, 0);
            }
        }, 200);

        return createTaskView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GetUserListTask(mActivity).execute();
        new GetGroupAndDistributionListTask().execute();
    }

    /**
     * Create Dialog for choosing recipients
     *
     * @param userList
     */
    public void createRecipientDialog(List<IUser> userList) {
        RecyclerView mShareRecycleView;
        EditText mSearchView;
        Button closeBtn;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.share_dialog_layout);
        mShareRecycleView = (RecyclerView) dialog.findViewById(R.id.share_fragment_recyclerView);
        closeBtn = (Button) dialog.findViewById(R.id.close_dialog_btn);
        mSearchView = (EditText) dialog.findViewById(R.id.share_searchEt);
        mSearchView.addTextChangedListener(new SearchWatcher(mSearchView, mShareDialogAdapter, userList));

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mShareRecycleView.setAdapter(mShareDialogAdapter);
        mShareRecycleView.setLayoutManager(mLayoutManager);
        mShareRecycleView.setHasFixedSize(true);

        dialog.show();


    }

    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {
        private EditText editText;
        private List<IUser> userList;
        private ShareDialogAdapter shareDialogAdapter;

        public SearchWatcher(EditText editText, ShareDialogAdapter shareDialogAdapter, List<IUser> userList) {
            this.editText = editText;
            this.userList = userList;
            this.shareDialogAdapter = shareDialogAdapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String searchQuery = editText.getText().toString();
            List<IUser> filteredList = DMSHelper.performSearch(userList, searchQuery);
            shareDialogAdapter.updateList(filteredList);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }


    /**
     * Set views
     *
     * @throws DMSCloudDBException
     */
    private void setViews() throws DMSCloudDBException {
        List<IUser> userList = DataBaseConnector.getInstance().getIUserList();
        userList.addAll(GroupAndDistributionManager.getInstance().getGroupList());
        userList.addAll(GroupAndDistributionManager.getInstance().getDistributionList());

        mUserList = new ArrayList<>(userList);
        mShareDialogAdapter = new ShareDialogAdapter(userList, getActivity(), mRecipientLayout);
        mSpinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createRecipientDialog(mUserList);
            }
        });

        mExecutiveRg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.create_task_allUserRb) {
                    mAllUser = true;
                } else {
                    mAllUser = false;
                }
            }
        });

        mDueDateBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogFragment datePickerFragment = new DatePickerFragment(mDueDateBtn);
                datePickerFragment.show(getChildFragmentManager(), "datePicker");
            }
        });

        mOkBtn.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (mDescriptionEt.getText().toString().isEmpty()) {
                    mDescriptionEt.setError(getString(R.string.err_empty_field));
                } else {
                    new CreatePostTaskTask(mPostId, mDescriptionEt.getText().toString(), mDueDate.getTime(), mAllUser, mShareDialogAdapter.getmListOfUserIds(), mShareDialogAdapter.getmListOfGroupIds(), mShareDialogAdapter.getmListOfDistributionIds()).execute();
                }
            }
        });
    }

    /**
     * Create date picker dialog
     */
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private Button mDueDateButton;
        private final Calendar mCalendar = Calendar.getInstance();

        public DatePickerFragment(Button button) {
            mDueDateButton = button;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mCalendar.set(year, month, day, 23, 59);
            mDueDate = mCalendar.getTime();
            mDueDateButton.setText(Config.DATE_TIME_FORMAT.format(mDueDate));
        }
    }

    /**
     * Downloads group and distribution list in background
     */
    public class GetGroupAndDistributionListTask extends SafeAsyncTask<Void, Void, String> {

        private ProgressDialog mProgressDialog;

        public GetGroupAndDistributionListTask() {
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.create_task_user_loading_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doWorkInBackground(Void... params) throws Exception {
            GroupAndDistributionManager.getInstance().parseDistributionList(new JSONObject(ServerConnector.getInstance().getDistributionListRequest()));
            return ServerConnector.getInstance().getGroupListRequest();
        }

        @Override
        protected void onSuccess(String response) {
            super.onSuccess(response);
            JSONObject groupListResponse = null;
            try {
                groupListResponse = new JSONObject(response);
                LOG.debug("GetGroupListTask - onSuccess: " + groupListResponse.toString());
                GroupAndDistributionManager.getInstance().parseGroupList(groupListResponse);

                setViews();
            } catch (JSONException e) {
                DialogHelper.showErrorDialog(mActivity, e);
            } catch (DMSCloudDBException e) {
                DialogHelper.showErrorDialog(mActivity, e);
            }
            mProgressDialog.dismiss();
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }

    /**
     * Create post task in background
     */
    public class CreatePostTaskTask extends SafeAsyncTask<Void, Void, Boolean> {

        private ProgressDialog mProgressDialog;

        private long mPostId;
        private String mMessage;
        private long mDueDate;
        private boolean mEveryUser;
        private Long[] mUserList;
        private Long[] mGroupList;
        private Long[] mDistributionList;

        public CreatePostTaskTask(long postId, String message, long dueDate, boolean everyUser, Long[] userList, Long[] groupList, Long[] distributionList) {
            mPostId = postId;
            mMessage = message;
            mDueDate = dueDate;
            mEveryUser = everyUser;
            mUserList = userList;
            mGroupList = groupList;
            mDistributionList = distributionList;

            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.create_task_message));
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Boolean doWorkInBackground(Void... params) throws Exception {
            return PostManager.getInstance().createPostTask(mPostId, mMessage, mDueDate, mEveryUser, mUserList, mGroupList, mDistributionList);
        }

        @Override
        protected void onSuccess(Boolean response) {
            super.onSuccess(response);
            if (response) {
                mProgressDialog.dismiss();
                PrefManager.getInstance().setIsChanged(true);
                PrefManager.getInstance().setWallUpToDate(false);
                Toast.makeText(mActivity, R.string.create_task_succes_message, Toast.LENGTH_LONG).show();
                mActivity.finish();
            }
        }

        @Override
        protected void onException(Exception ex) {
            super.onException(ex);
            mProgressDialog.dismiss();
            DialogHelper.showErrorDialog(mActivity, ex);
        }
    }
}
