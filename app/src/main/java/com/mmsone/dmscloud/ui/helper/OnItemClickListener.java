package com.mmsone.dmscloud.ui.helper;

import android.view.View;

/**
 * Created by Alexander on 2015. 08. 05..
 */
public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
