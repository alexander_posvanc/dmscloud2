package com.mmsone.dmscloud.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.adapter.ArrayAdapterWithEmptyItem;
import com.mmsone.dmscloud.adapter.UserArrayAdapter;
import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bl.FontManager;
import com.mmsone.dmscloud.bl.SearchManager;
import com.mmsone.dmscloud.bo.ESearchFilterDesignation;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.SubTopicType;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.WallResponse;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.SavedSearch;
import com.mmsone.dmscloud.bo.bean.SearchFilter;
import com.mmsone.dmscloud.bo.bean.SearchFilter.EDateInterval;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnActivityFinishListener;

public class AdvancedSearchFragment extends Fragment {

	private AdvancedSearchActivity mActivity;

	private OnActivityFinishListener mActivityFinishListener;
	private EditText mPostIdValueEt;
	private Spinner mCreatedByValueSpinner, mCreatedBySubTopicValueSpinner, mSubTopicTypeValueSpinner, mSubTopicStateValueSpinner;

	private Button mCreatedFromValueBtn, mCreatedToValueBtn, mCreatedToSubTopicValueBtn, mCreatedFromSubTopicValueBtn;
	private List<IUser> mUserList;
	private ArrayAdapter<IUser> mUserAdapter;
	private ArrayAdapter<String> mSubTopicStateAdapter;
	private String mSubTopicType;
	private List<String> mSubTopicTypeList;

	@Override
	public void onAttach(Activity activity) {
		mActivity = (AdvancedSearchActivity) activity;
		super.onAttach(activity);
		mActivity.setTitle(R.string.title_advanced_search);
		mActivityFinishListener = (AdvancedSearchActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_advanced_search, container, false);

		mSubTopicTypeList = new ArrayList<String>();
		mSubTopicTypeList.add(null);
		mSubTopicTypeList.addAll(SubTopicType.getSubTopicTypeList());

		Button okBtn = (Button) view.findViewById(R.id.advanced_search_okBtn);
		okBtn.setTypeface(FontManager.getInstance().getRobotoLightTtf());
		okBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!SearchManager.getInstance().getFilterList().isEmpty()) {
					mActivityFinishListener.onFinish(true);
				} else {
					Toast.makeText(mActivity, R.string.search_no_search_filter_title, Toast.LENGTH_LONG).show();
				}
			}
		});

		/*
		 * post id
		 */
		((TextView) view.findViewById(R.id.search_postIdTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mPostIdValueEt = (EditText) view.findViewById(R.id.search_postIdValueEt);
		mPostIdValueEt.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		mPostIdValueEt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!s.toString().isEmpty()) {
					try {
						SearchManager.getInstance().addSearcFilter(mPostIdValueEt.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_POST_NUMBER, s.toString()));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				} else {
					SearchManager.getInstance().removeSearcFilter(mPostIdValueEt.getId());
				}
			}
		});

		/*
		 * created by
		 */
		((TextView) view.findViewById(R.id.search_createdByTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedByValueSpinner = (Spinner) view.findViewById(R.id.search_createdByValueSpinner);

		mUserList = new ArrayList<IUser>();
		mUserList.add(null);
		try {
			mUserList.addAll(DataBaseConnector.getInstance().getUserList());
		} catch (DMSCloudDBException e1) {
			DialogHelper.showErrorDialog(mActivity, e1);
		}

		mUserAdapter = new UserArrayAdapter(mActivity, R.layout.view_user_spinner_item, mUserList.toArray(new IUser[] {}));
		mCreatedByValueSpinner.setAdapter(mUserAdapter);
		mCreatedByValueSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				User user = (User) parent.getItemAtPosition(position);
				if (user != null) {
					try {
						SearchManager.getInstance().addSearcFilter(mCreatedByValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_CREATOR, "" + user.id));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				}else{
					SearchManager.getInstance().removeSearcFilter(mCreatedByValueSpinner.getId());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				SearchManager.getInstance().removeSearcFilter(mCreatedByValueSpinner.getId());
			}
		});

		/*
		 * created from an to
		 */

		((TextView) view.findViewById(R.id.search_createdFromTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedFromValueBtn = (Button) view.findViewById(R.id.search_createdFromValueBtn);
		mCreatedFromValueBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		mCreatedFromValueBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment datePickerFragment = new DatePickerFragment(mCreatedFromValueBtn, ESearchFilterDesignation.FILTER_SEARCH_CREATED, EDateInterval.FROM);
				datePickerFragment.show(mActivity.getSupportFragmentManager(), "datePicker");
			}
		});

		((TextView) view.findViewById(R.id.search_createdToTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedToValueBtn = (Button) view.findViewById(R.id.search_createdToValueBtn);
		mCreatedToValueBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		mCreatedToValueBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment datePickerFragment = new DatePickerFragment(mCreatedToValueBtn, ESearchFilterDesignation.FILTER_SEARCH_CREATED, EDateInterval.TO);
				datePickerFragment.show(mActivity.getSupportFragmentManager(), "datePicker");
			}
		});

		/*
		 * created by sub topic
		 */

		((TextView) view.findViewById(R.id.search_createdBySubTopicTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedBySubTopicValueSpinner = (Spinner) view.findViewById(R.id.search_createdBySubTopicValueSpinner);
		mCreatedBySubTopicValueSpinner.setAdapter(mUserAdapter);
		mCreatedBySubTopicValueSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				User user = (User) parent.getItemAtPosition(position);
				if (user != null) {
					try {
						SearchManager.getInstance().addSearcFilter(mCreatedBySubTopicValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_CREATOR, "" + mUserList.get(position).getId()));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				}else{
					SearchManager.getInstance().removeSearcFilter(mCreatedBySubTopicValueSpinner.getId());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				SearchManager.getInstance().removeSearcFilter(mCreatedBySubTopicValueSpinner.getId());
			}
		});

		/*
		 * created sub topic from and to
		 */

		((TextView) view.findViewById(R.id.search_createdFromSubTopicTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedFromSubTopicValueBtn = (Button) view.findViewById(R.id.search_createdFromSubTopicValueBtn);
		mCreatedFromSubTopicValueBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		mCreatedFromSubTopicValueBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment datePickerFragment = new DatePickerFragment(mCreatedFromSubTopicValueBtn, ESearchFilterDesignation.FILTER_SEARCH_SUB_CREATED, EDateInterval.FROM);
				datePickerFragment.show(mActivity.getSupportFragmentManager(), "datePicker");
			}
		});

		((TextView) view.findViewById(R.id.search_createdToSubTopicTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mCreatedToSubTopicValueBtn = (Button) view.findViewById(R.id.search_createdToSubTopicValueBtn);
		mCreatedToSubTopicValueBtn.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		mCreatedToSubTopicValueBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment datePickerFragment = new DatePickerFragment(mCreatedToSubTopicValueBtn, ESearchFilterDesignation.FILTER_SEARCH_SUB_CREATED, EDateInterval.TO);
				datePickerFragment.show(mActivity.getSupportFragmentManager(), "datePicker");
			}
		});

		/*
		 * sub topic type
		 */
		((TextView) view.findViewById(R.id.search_subTopicTypeTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());
		mSubTopicTypeValueSpinner = (Spinner) view.findViewById(R.id.search_subTopicTypeValueSpinner);

		((TextView) view.findViewById(R.id.search_subTopicStateTitleTv)).setTypeface(FontManager.getInstance().getRobotoLightTtf());

		mSubTopicStateValueSpinner = (Spinner) view.findViewById(R.id.search_subTopicStateValueSpinner);
		mSubTopicStateValueSpinner.setAdapter(mSubTopicStateAdapter);

		ArrayAdapter<String> subTopicTypeAdapter = new ArrayAdapterWithEmptyItem(mActivity, R.layout.view_user_spinner_item, mSubTopicTypeList.toArray(new String[] {}));
		subTopicTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mSubTopicTypeValueSpinner.setAdapter(subTopicTypeAdapter);
		mSubTopicTypeValueSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mSubTopicType = mSubTopicTypeList.get(position);
				if (mSubTopicType != null) {
					try {
						SearchManager.getInstance().addSearcFilter(mSubTopicTypeValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_TYPE, SubTopicType.getSubTopicIdBy(mSubTopicType)));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}

					SearchManager.getInstance().removeSearcFilter(mSubTopicStateValueSpinner.getId());

					mSubTopicStateValueSpinner.setAdapter(null);

					if (mSubTopicType.equals(SubTopicType.getSubTopicNameBy(SubTopicType.TASK))) {
						mSubTopicStateAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, TaskStatus.getAllTaskStatusList());
						mSubTopicStateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						mSubTopicStateValueSpinner.setAdapter(mSubTopicStateAdapter);
						mSubTopicStateValueSpinner.setSelection(0);
					}
					if (mSubTopicType.equals(SubTopicType.getSubTopicNameBy(SubTopicType.WORKFLOW))) {
						mSubTopicStateAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, WorkflowStatus.getAllWorkflowStatusList());
						mSubTopicStateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						mSubTopicStateValueSpinner.setAdapter(mSubTopicStateAdapter);
						mSubTopicStateValueSpinner.setSelection(0);
					}
				} else {
					SearchManager.getInstance().removeSearcFilter(mSubTopicStateValueSpinner.getId());
					mSubTopicStateValueSpinner.setAdapter(null);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				SearchManager.getInstance().removeSearcFilter(mSubTopicTypeValueSpinner.getId());
			}
		});

		/*
		 * sub topic state
		 */
		mSubTopicStateValueSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String subTopicType = SubTopicType.getSubTopicIdBy(mSubTopicType);
				if (subTopicType.equals(SubTopicType.TASK)) {
					try {
						SearchManager.getInstance().addSearcFilter(mSubTopicStateValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_TASK_STATUS, "" + TaskStatus.getTaskStatusBy(TaskStatus.getAllTaskStatusList().get(position))));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				}

				if (subTopicType.equals(SubTopicType.WORKFLOW)) {
					try {
						SearchManager.getInstance().addSearcFilter(mSubTopicStateValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_WF_STATUS, "" + WorkflowStatus.getWorkflowStatusBy(WorkflowStatus.getAllWorkflowStatusList().get(position))));
					} catch (JSONException e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				SearchManager.getInstance().removeSearcFilter(mSubTopicStateValueSpinner.getId());
			}
		});

		SearchManager.getInstance().crearFilterList();

		return view;
	}

	/**
	 * Loading search filters
	 * 
	 * @param searchFilterList
	 * @throws JSONException
	 * @throws NumberFormatException
	 * @throws DMSCloudDBException
	 */
	public void loadSearchFilters(List<SearchFilter> searchFilterList) throws JSONException, NumberFormatException, DMSCloudDBException {
		onAdvancedSarchDelete();
		for (SearchFilter searchFilter : searchFilterList) {
			switch (searchFilter.designation) {
			case FILTER_SEARCH_POST_NUMBER:
				mPostIdValueEt.setText(searchFilter.value);
				SearchManager.getInstance().addSearcFilter(mPostIdValueEt.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_POST_NUMBER, searchFilter.value));
				break;
			case FILTER_SEARCH_CREATED:
				if (searchFilter.dateTil != 0) {
					mCreatedToValueBtn.setText(Config.DATE_FORMAT.format(searchFilter.dateTil));
					SearchManager.getInstance().addSearcFilter(mCreatedToValueBtn.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_CREATED, EDateInterval.TO, searchFilter.dateFrom, searchFilter.dateTil));
				}

				if (searchFilter.dateFrom != 0) {
					mCreatedFromValueBtn.setText(Config.DATE_FORMAT.format(searchFilter.dateFrom));
					SearchManager.getInstance().addSearcFilter(mCreatedFromValueBtn.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_CREATED, EDateInterval.FROM, searchFilter.dateFrom, searchFilter.dateTil));
				}
				break;
			case FILTER_SEARCH_SUB_CREATED:
				if (searchFilter.dateFrom != 0) {
					mCreatedFromSubTopicValueBtn.setText(Config.DATE_FORMAT.format(searchFilter.dateFrom));
					SearchManager.getInstance().addSearcFilter(mCreatedFromSubTopicValueBtn.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_CREATED, EDateInterval.FROM, searchFilter.dateFrom, searchFilter.dateTil));

				}
				if (searchFilter.dateTil != 0) {
					mCreatedToSubTopicValueBtn.setText(Config.DATE_FORMAT.format(searchFilter.dateTil));
					SearchManager.getInstance().addSearcFilter(mCreatedToSubTopicValueBtn.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_CREATED, EDateInterval.TO, searchFilter.dateFrom, searchFilter.dateTil));
				}
				break;
			case FILTER_SEARCH_SUB_TYPE:
				int tindex = 0;
				for (int i = 0; i < mSubTopicTypeValueSpinner.getCount(); i++) {
					String type = (String) mSubTopicTypeValueSpinner.getItemAtPosition(i);
					if (type != null) {
						if (type.toString().equalsIgnoreCase(SubTopicType.getSubTopicNameBy(searchFilter.value))) {
							tindex = i;
							i = mSubTopicTypeValueSpinner.getCount();
						}
					}
				}
				mSubTopicTypeValueSpinner.setSelection(tindex);
				SearchManager.getInstance().addSearcFilter(mSubTopicTypeValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_TYPE, searchFilter.value));
				break;
			case FILTER_SEARCH_CREATOR:
				int index = 0;
				for (int i = 0; i < mCreatedByValueSpinner.getCount(); i++) {
					User user = (User) mCreatedByValueSpinner.getItemAtPosition(i);
					if (user != null) {
						if (user.toString().equalsIgnoreCase(DataBaseConnector.getInstance().getUser(Long.parseLong(searchFilter.value)).formatedName)) {
							index = i;
							i = mCreatedByValueSpinner.getCount();
						}
					}
				}
				mCreatedByValueSpinner.setSelection(index);
				SearchManager.getInstance().addSearcFilter(mCreatedByValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_CREATOR, searchFilter.value));
				break;
			case FILTER_SEARCH_SUB_CREATOR:
				int subCreatorIndex = 0;
				for (int i = 0; i < mCreatedBySubTopicValueSpinner.getCount(); i++) {
					User user = (User) mCreatedBySubTopicValueSpinner.getItemAtPosition(i);
					if (user != null) {
						if (user.toString().equalsIgnoreCase(DataBaseConnector.getInstance().getUser(Long.parseLong(searchFilter.value)).formatedName)) {
							subCreatorIndex = i;
							i = mCreatedBySubTopicValueSpinner.getCount();
						}
					}
				}
				mCreatedBySubTopicValueSpinner.setSelection(subCreatorIndex);
				SearchManager.getInstance().addSearcFilter(mCreatedBySubTopicValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_CREATOR, searchFilter.value));
				break;
			case FILTER_SEARCH_SUB_TASK_STATUS:
				int sindex = 0;
				for (int i = 0; i < mSubTopicStateValueSpinner.getCount(); i++) {
					String type = (String) mSubTopicStateValueSpinner.getItemAtPosition(i);
					if (type != null) {
						if (type.toString().equalsIgnoreCase(TaskStatus.getTaskStatusBy(Integer.valueOf(searchFilter.value)))) {
							sindex = i;
							i = mSubTopicStateValueSpinner.getCount();
						}
					}
				}
				mSubTopicStateValueSpinner.setSelection(sindex);
				SearchManager.getInstance().addSearcFilter(mSubTopicStateValueSpinner.getId(), new SearchFilter(ESearchFilterDesignation.FILTER_SEARCH_SUB_TASK_STATUS, searchFilter.value));
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Downloads saved search list in background
	 */
	public class GetSavedSearchListTask extends SafeAsyncTask<Void, Void, List<SavedSearch>> {

		private ProgressDialog mProgressDialog;

		public GetSavedSearchListTask() {
			mProgressDialog = new ProgressDialog(mActivity);
			mProgressDialog.setMessage(getString(R.string.loading_searchs_message));
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog.show();
		}

		@Override
		protected List<SavedSearch> doWorkInBackground(Void... params) throws Exception {
			return SearchManager.getInstance().getSavedSearchList();
		}

		@Override
		protected void onSuccess(final List<SavedSearch> response) {
			super.onSuccess(response);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				if (!mActivity.isFinishing()) {
					if (response != null && !response.isEmpty()) {
						final AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();
						View convertView = (View) mActivity.getLayoutInflater().inflate(R.layout.dialog_load_saved_search, null);
						alertDialog.setView(convertView);
						alertDialog.setTitle(R.string.search_load_title);
						ListView listView = (ListView) convertView.findViewById(R.id.saved_search_listView);
						listView.setAdapter(new ArrayAdapter<SavedSearch>(mActivity, android.R.layout.simple_list_item_1, response));
						listView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
								new GetSavedSearchTask(response.get(position).id).execute();
								alertDialog.dismiss();
							}
						});
						alertDialog.show();
					} else {
						Toast.makeText(mActivity, getString(R.string.search_no_saved_search_title), Toast.LENGTH_LONG).show();
					}
				}
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				DialogHelper.showErrorDialog(mActivity, ex);
			}
		}
	}

	/**
	 *Downloads saved search in background 
	 *
	 */
	public class GetSavedSearchTask extends SafeAsyncTask<Void, Void, List<SearchFilter>> {

		private long mSavedSearchId;
		private ProgressDialog mProgressDialog;

		public GetSavedSearchTask(long savedSearchId) {
			mSavedSearchId = savedSearchId;
			mProgressDialog = new ProgressDialog(mActivity);
			mProgressDialog.setMessage(getString(R.string.loading_search_message));
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog.show();
		}

		@Override
		protected List<SearchFilter> doWorkInBackground(Void... params) throws Exception {
			return SearchManager.getInstance().getSavedSearch(mSavedSearchId);
		}

		@Override
		protected void onSuccess(List<SearchFilter> result) {
			super.onSuccess(result);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				if (result != null && !result.isEmpty()) {
					try {
						loadSearchFilters(result);
					} catch (Exception e) {
						DialogHelper.showErrorDialog(mActivity, e);
					}
				}
			}
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			if (!mActivity.isFinishing()) {
				mProgressDialog.dismiss();
				DialogHelper.showErrorDialog(mActivity, ex);
			}
		}
	}

	/**
	 * Save search in background
	 * 
	 */
	public class SaveSavedSearchTask extends SafeAsyncTask<Void, Void, WallResponse> {

		private String mSaveName;

		public SaveSavedSearchTask(String saveName) {
			mSaveName = saveName;
		}

		@Override
		protected WallResponse doWorkInBackground(Void... params) throws Exception {
			return SearchManager.getInstance().saveSavedSearch(mSaveName, SearchManager.getInstance().getFilterList());
		}

		@Override
		protected void onSuccess(WallResponse result) {
			super.onSuccess(result);
			Toast.makeText(mActivity, getString(R.string.search_filters_saved_title), Toast.LENGTH_LONG).show();
			mActivityFinishListener.onFinish(false);
		}

		@Override
		protected void onException(Exception ex) {
			super.onException(ex);
			DialogHelper.showErrorDialog(mActivity, ex);
		}
	}

	/**
	 * Create date picker dialog
	 */
	public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		private Button mButton;
		private final Calendar mCalendar = Calendar.getInstance();
		private EDateInterval mDateInterval;
		private ESearchFilterDesignation mSearchFilterDesignation;

		public DatePickerFragment(Button button, ESearchFilterDesignation searchFilterDesignation, EDateInterval interval) {
			mButton = button;
			mDateInterval = interval;
			mSearchFilterDesignation = searchFilterDesignation;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();

			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			mCalendar.set(year, month, day, 23, 59);
			mButton.setText(Config.DATE_FORMAT.format(mCalendar.getTime()));
			try {
				SearchManager.getInstance().addSearcFilter(mButton.getId(), new SearchFilter(mSearchFilterDesignation, mDateInterval, mCalendar.getTimeInMillis(), mCalendar.getTimeInMillis()));
			} catch (JSONException e) {
				DialogHelper.showErrorDialog(getActivity(), e);
			}
		}
	}

	/**
	 * Set filed to defaults
	 */
	public void onAdvancedSarchDelete() {
		mPostIdValueEt.setText("");
		mCreatedFromValueBtn.setText(R.string.search_created_from_title);
		mCreatedToValueBtn.setText(R.string.search_created_to_title);
		mCreatedFromSubTopicValueBtn.setText(R.string.search_created_from_title);
		mCreatedToSubTopicValueBtn.setText(R.string.search_created_to_title);
		mCreatedByValueSpinner.setSelection(0);
		mCreatedBySubTopicValueSpinner.setSelection(0);
		mSubTopicTypeValueSpinner.setSelection(0);

		SearchManager.getInstance().crearFilterList();

		mSubTopicType = null;
	}

	/**
	 * Execute saved search list task
	 */
	public void onAdvancedSarchLoad() {
		new GetSavedSearchListTask().execute();
	}

	/**
	 * Create save dialog and execute save search task
	 */
	public void onAdvancedSarchSave() {
		final EditText input = new EditText(mActivity);
		input.setTypeface(FontManager.getInstance().getRobotoMediumTtf());
		input.setInputType(InputType.TYPE_CLASS_TEXT);

		new AlertDialog.Builder(mActivity).setTitle(R.string.search_save_title).setView(input).setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String name = input.getText().toString().trim();
				if (!name.isEmpty() && !SearchManager.getInstance().isFilterListEmpty()) {
					new SaveSavedSearchTask(name).execute();
				}
			}
		}).setNegativeButton(R.string.btn_cancel, null).show();
	}
}
