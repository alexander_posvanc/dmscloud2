package com.mmsone.dmscloud.gcm;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;

public class GCMManager {

	private static final ILogger LOG = Logger.getLogger(GCMManager.class);

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private String SENDER_ID = "657913799223"; 

	private static GCMManager mIstance = null;

	private GoogleCloudMessaging mGoogleCloudMessaging;
	private Activity mActivity;
	private String mRegId;

	protected GCMManager(Activity activity) {
		mActivity = activity;
	}

	public static GCMManager getInstance(Activity activity) {
		if (mIstance == null) {
			mIstance = new GCMManager(activity);
		}
		return mIstance;
	}

	/**
	 * GCM registration
	 * 
	 */
	public void registration() {
		if (checkPlayServices()) {
			
			mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(mActivity);
			mRegId = getRegistrationId();
			
			if (mRegId.isEmpty()) {
				registerInBackground();
			}else{
				sendRegistrationIdToBackend();
			}
		} else {
			LOG.debug("No valid Google Play Services APK found.");
		}
	}

	/**
	 * Check play services
	 * 
	 * @return
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				if(!mActivity.isFinishing()){
					GooglePlayServicesUtil.getErrorDialog(resultCode, mActivity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
				}
			} else {
				if(!mActivity.isFinishing()){
					Toast.makeText(mActivity, "This device is not supported.", Toast.LENGTH_LONG).show();
				}
			}
			return false;
		}
		return true;
	}

	/**
	 * Get registration id
	 * 
	 * @return
	 */
	private String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences(mActivity);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			LOG.debug("Registration not found.");
			return "";
		}

		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(mActivity);
		if (registeredVersion != currentVersion) {
			LOG.debug("App version changed.");
			return "";
		}
		return registrationId;
	}
	
	/**
	 * Get gcm shared preference
	 * 
	 * @param context
	 * @return
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		return mActivity.getSharedPreferences(mActivity.getClass().getSimpleName(), Context.MODE_PRIVATE);
	}

	/**
	 * Get app version
	 * 
	 * @param context
	 * @return
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Register gcm in background
	 * 
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (mGoogleCloudMessaging == null) {
						mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(mActivity);
					}
					mRegId = mGoogleCloudMessaging.register(SENDER_ID);
					
					msg = "Device registered, registration ID=" + mRegId;
					
					sendRegistrationIdToBackend();

					storeRegistrationId(mActivity, mRegId);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				LOG.debug("GCMManager registerInBackground msg: "+msg);
			}
		}.execute(null, null, null);
	}

	private void sendRegistrationIdToBackend() {
		new RegisterPushTask(mRegId).execute();
	}
	
	public void sendUnRegistrationIdToBackend() {
		new UnRegisterPushTask(mRegId).execute();
	}

	/**
	 * Store gcm reg id in shared preferences
	 * 
	 * @param context
	 * @param regId
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		LOG.debug("Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
}
