package com.mmsone.dmscloud.gcm;

import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;

public class UnRegisterPushTask extends SafeAsyncTask<Void, Void, String> {
	
	private static final ILogger LOG = Logger.getLogger(UnRegisterPushTask.class);

	private String mRegId;
	
	/**
	 * Unregister push in background
	 * 
	 * @param regId
	 */
	public UnRegisterPushTask(String regId){
		mRegId = regId;
	}
	
	@Override
	protected String doWorkInBackground(Void... params) throws Exception {
		return ServerConnector.getInstance().unregisterPushRequest(mRegId);
	}

	@Override
	protected void onSuccess(String response) {
		super.onSuccess(response);
		LOG.debug("UnRegisterPushTask onSuccess: "+ response.toString());
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		LOG.debug("UnRegisterPushTask onException: "+ ex.getLocalizedMessage());
	}
}