package com.mmsone.dmscloud.gcm;

import org.json.JSONObject;

import com.mmsone.dmscloud.async.SafeAsyncTask;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.ExceptionManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;

public class RegisterPushTask extends SafeAsyncTask<Void, Void, String> {

	private static final ILogger LOG = Logger.getLogger(RegisterPushTask.class);

	private String mRegId;

	/**
	 * Register push in background
	 * 
	 * @param regId
	 */
	public RegisterPushTask(String regId) {
		mRegId = regId;
	}

	@Override
	protected String doWorkInBackground(Void... params) throws Exception {
		return ServerConnector.getInstance().registerPushRequest(mRegId);
	}

	@Override
	protected void onSuccess(String response) {
		super.onSuccess(response);
		JSONObject registerPushResponse;
		try {
			registerPushResponse = new JSONObject(response);
			ExceptionManager.getInstance().checkException(registerPushResponse);
			//save push ek in shared preference
			PrefManager.getInstance().setPushEk(registerPushResponse.optString("ek"));
		} catch (Exception e) {
			LOG.debug("RegisterPushTask onSuccess ex: " + e.getLocalizedMessage());
		}
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		LOG.debug("RegisterPushTask onException: " + ex.getLocalizedMessage());
	}
}