package com.mmsone.dmscloud.gcm;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.NotiManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;
import com.mmsone.dmscloud.ui.MainActivity;

public class GCMIntentService extends IntentService {

	private static final ILogger LOG = Logger.getLogger(GCMIntentService.class);

	private NotificationManager mNotificationManager;

	public GCMIntentService() {
		super("GCMIntentService");
	}

	/**
	 * Handle gcm message
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				LOG.debug("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				LOG.debug("Deleted messages on server: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				try {
					if (UserManager.getInstance().getCurrentUser().id == Long.parseLong((String) extras.get("userId"))) {
						
						NotiManager.getInstance().addMessage(decodeGcmMassage((String) extras.get("dmsm")));
						sendNotification();

					}
				} catch (Exception e1) {
					LOG.debug("GCMIntentService: " + e1.getLocalizedMessage());
				}
			}
		}
		GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	/**
	 * Decode gcm message
	 * 
	 * @param message
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidAlgorithmParameterException
	 */
	private String decodeGcmMassage(String message) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		byte[] encrypted;
		byte[] raw;

		byte[] byteArray = message.getBytes("UTF-8");

		encrypted = Base64.decode(byteArray, Base64.DEFAULT);

		byteArray = PrefManager.getInstance().getPushEk().getBytes("UTF-8");
		raw = Base64.decode(byteArray, Base64.DEFAULT);

		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] decrypted = cipher.doFinal(encrypted);

		return new String(decrypted, "UTF-8");
	}

	/**
	 * Build and show notification
	 * 
	 */
	private void sendNotification() {
		if (PrefManager.getInstance().isPush()) {
			mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

			Intent notificationIntent = new Intent(this, MainActivity.class);
			notificationIntent.putExtra("deleteNotifications", true);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
			
			String message = NotiManager.getInstance().getMessages();

			mBuilder.setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher).
			setContentTitle(message).
			setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
			.setContentInfo("" + NotiManager.getInstance().getMessagesCount())
			.setContentIntent(contentIntent).setStyle(new NotificationCompat.BigTextStyle(mBuilder).bigText(message));

			mNotificationManager.notify(0, mBuilder.build());
		}
	}

}