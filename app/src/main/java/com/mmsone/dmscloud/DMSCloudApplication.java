package com.mmsone.dmscloud;

import java.io.IOException;
import java.net.HttpURLConnection;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.logger.ILoggerConfig;
import com.mmsone.dmscloud.logger.Logger;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class DMSCloudApplication extends Application {

	private static Context mApplicationContext;
	private static String mDeviceId;
	
	@Override
	public void onCreate() {
		super.onCreate();

		mApplicationContext = getApplicationContext();
		mDeviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

		/**
		 * Logger initialization
		 */
		Logger.init(new ILoggerConfig() {

			@Override
			public boolean isEnabled() {
				return Config.IS_DEVELOPER_MODE;
			}
		});

		initImageLoader(mApplicationContext);
	}

	public static Context getContext() {
		return mApplicationContext;
	}

	public static String getDeviceId() {
		return mDeviceId;
	}

	/**
	 * Image loader initialization
	 * 
	 * @param context
	 */
	private void initImageLoader(Context context) {
		ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(context).threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory().imageDownloader(new DMSCloudImageDownaloder(getApplicationContext())).tasksProcessingOrder(QueueProcessingType.LIFO).build());
	}

	/**
	 * Custom Image Downloader with JSessionId
	 */
	class DMSCloudImageDownaloder extends BaseImageDownloader {

		public DMSCloudImageDownaloder(Context context) {
			super(context);
		}

		public DMSCloudImageDownaloder(Context context, int connectTimeout, int readTimeout) {
			super(context, connectTimeout, readTimeout);
		}

		@Override
		protected HttpURLConnection createConnection(String url, Object extra) throws IOException {
			HttpURLConnection httpURLConnection = super.createConnection(url, extra);
			httpURLConnection.setRequestProperty("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
			return httpURLConnection;
		}
	}
}