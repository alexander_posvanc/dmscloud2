package com.mmsone.dmscloud.ex;

public class InsufficientRightsException   extends DMSCloudException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -3741750702431099937L;
	
	public InsufficientRightsException() {
		super("");
	}

	public InsufficientRightsException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "InsufficientRightsException";
	}
}
