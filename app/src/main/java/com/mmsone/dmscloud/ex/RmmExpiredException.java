package com.mmsone.dmscloud.ex;

public class RmmExpiredException   extends DMSCloudException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -4687662327701365349L;
	
	public RmmExpiredException() {
		super("");
	}

	public RmmExpiredException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "RmmExpiredException";
	}
}
