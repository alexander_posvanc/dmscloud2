package com.mmsone.dmscloud.ex;

public class AuthenticationException extends DMSCloudException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8564574757682895205L;

	public AuthenticationException() {
		super("");
	}

	public AuthenticationException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "AuthenticationException";
	}
}