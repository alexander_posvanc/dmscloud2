package com.mmsone.dmscloud.ex;

public class DMSCloudDBException extends DMSCloudException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8564574757682895205L;

	public DMSCloudDBException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "DMSCloudDBException";
	}
}

