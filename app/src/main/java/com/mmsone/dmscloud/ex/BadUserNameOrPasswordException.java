package com.mmsone.dmscloud.ex;

public class BadUserNameOrPasswordException  extends DMSCloudException {



	/**
	 * 
	 */
	private static final long serialVersionUID = 5595495827800747785L;

	public BadUserNameOrPasswordException() {
		super("");
	}
	
	public BadUserNameOrPasswordException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "BadUserNameOrPasswordException";
	}
}
