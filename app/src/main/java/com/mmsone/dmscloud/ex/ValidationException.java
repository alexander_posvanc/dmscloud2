package com.mmsone.dmscloud.ex;

public class ValidationException  extends DMSCloudException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -608732660898533883L;
	
	public ValidationException() {
		super("");
	}

	public ValidationException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "ValidationException";
	}
}


