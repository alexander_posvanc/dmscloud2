package com.mmsone.dmscloud.ex;

import android.util.Log;

public abstract class DMSCloudException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5314766017358805134L;

	private static final String LOG_DEFAULT = "The following exception was constructed: ";

	public DMSCloudException(String string) {
		super(string);
	}

	public DMSCloudException() {
		super();
	}

	public DMSCloudException(Exception e) {
		super(e);
		StringBuilder sb = new StringBuilder(LOG_DEFAULT);
		sb.append(getName());
		Log.d(sb.toString(), e.getLocalizedMessage());
	}

	public DMSCloudException(String msg, Exception e) {
		super(msg, e);
		Log.d(msg, e.getLocalizedMessage());
	}

	public abstract String getName();
}
