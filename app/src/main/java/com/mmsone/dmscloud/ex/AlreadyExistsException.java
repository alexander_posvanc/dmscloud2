package com.mmsone.dmscloud.ex;

public class AlreadyExistsException extends DMSCloudException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4151761123226752818L;

	public AlreadyExistsException() {
		super("");
	}

	public AlreadyExistsException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "AlreadyExistsException";
	}
}
