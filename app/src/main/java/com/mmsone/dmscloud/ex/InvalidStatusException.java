package com.mmsone.dmscloud.ex;

public class InvalidStatusException   extends DMSCloudException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3159930116453807644L;
	
	public InvalidStatusException() {
		super("");
	}

	public InvalidStatusException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "InvalidStatusException";
	}
}
