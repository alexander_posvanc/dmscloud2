package com.mmsone.dmscloud.ex;

public class RaceConditionException   extends DMSCloudException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8896062140883912020L;
	
	public RaceConditionException() {
		super("");
	}

	public RaceConditionException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "RaceConditionException";
	}
}
