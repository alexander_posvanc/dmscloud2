package com.mmsone.dmscloud.ex;

public class ProtocolMismatchException   extends DMSCloudException {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1036864421412239341L;

	public ProtocolMismatchException() {
		super("");
	}
	
	public ProtocolMismatchException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "ProtocolMismatchException";
	}
}
