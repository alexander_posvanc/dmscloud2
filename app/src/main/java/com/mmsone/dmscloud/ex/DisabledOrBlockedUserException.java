package com.mmsone.dmscloud.ex;

public class DisabledOrBlockedUserException   extends DMSCloudException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7432577654886909654L;
	
	public DisabledOrBlockedUserException() {
		super("");
	}

	public DisabledOrBlockedUserException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "DisabledOrBlockedUserException";
	}
}
