package com.mmsone.dmscloud.ex;

public class ErrorException extends DMSCloudException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4647965300886437585L;

	public ErrorException() {
		super("");
	}

	public ErrorException(String string) {
		super(string);
	}

	@Override
	public String getName() {
		return "ErrorException";
	}
}
