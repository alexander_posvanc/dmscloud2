package com.mmsone.dmscloud.async;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.PostActivity;

/**
 * Created by Alexander on 2015. 09. 21..
 */
public class RankPostTask extends SafeAsyncTask<Void, Void, Boolean> {
    private long postId;
    private Long subPostId;
    private Integer subPostType;
    private Activity activity;

    public RankPostTask(long postId, long subPostId, int subPostType, Activity activity) {
        this.postId = postId;
        this.activity = activity;
        this.subPostId = subPostId;
        this.subPostType = subPostType;
    }

    public RankPostTask(long postId, Activity activity) {
        this.postId = postId;
        this.activity = activity;
    }

    @Override
    protected Boolean doWorkInBackground(Void... params) throws Exception {
        if (subPostId != null) {
            return PostManager.getInstance().rankSubPost(postId, subPostId, subPostType);
        } else {
            return PostManager.getInstance().rankPost(postId);
        }
    }

    @Override
    protected void onSuccess(Boolean aBoolean) {
        super.onSuccess(aBoolean);
        Toast.makeText(activity, "Értékelés sikeres", Toast.LENGTH_SHORT).show();
        if (subPostType == null) {
            activity.finish();
        } else {
            PrefManager.getInstance().setIsFirst(true);
            Intent intent = ((PostActivity) activity).getmActivityIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            activity.startActivity(intent);
            activity.finish();
        }
    }

    @Override
    protected void onException(Exception ex) {
        super.onException(ex);
        DialogHelper.showErrorDialog(activity, ex);

    }
}