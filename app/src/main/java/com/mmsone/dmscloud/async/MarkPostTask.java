package com.mmsone.dmscloud.async;

import android.app.Activity;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.helper.DialogHelper;

public class MarkPostTask extends SafeAsyncTask<Void, Void, Boolean> {

	private long mPostId;
	private Activity mActivity;

	/**
	 * Mark post in background
	 * 
	 * @param postId
	 * @param activity
	 */
	public MarkPostTask(long postId, Activity activity) {
		mPostId = postId;
		mActivity = activity;
	}

	@Override
	protected Boolean doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().markPost(mPostId);
	}

	@Override
	protected void onSuccess(Boolean response) {
		super.onSuccess(response);
		if (response) {
			PrefManager.getInstance().setWallUpToDate(false);
			Toast.makeText(mActivity, R.string.toast_mark_post, Toast.LENGTH_SHORT).show();
			mActivity.finish();
		}
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		DialogHelper.showErrorDialog(mActivity, ex);
	}
}