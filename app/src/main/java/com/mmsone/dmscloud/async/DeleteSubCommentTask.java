package com.mmsone.dmscloud.async;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.PostActivity;

/**
 * Created by Alexander on 2015. 09. 21..
 */
public class DeleteSubCommentTask extends SafeAsyncTask<Void, Void, Boolean> {
    private long id;
    private Activity activity;


    public DeleteSubCommentTask(long id, Activity activity) {
        this.id = id;
        this.activity = activity;
    }

    @Override
    protected Boolean doWorkInBackground(Void... params) throws Exception {

        return PostManager.getInstance().deleteSubComment(id);

    }

    @Override
    protected void onSuccess(Boolean aBoolean) {
        super.onSuccess(aBoolean);
        Toast.makeText(activity, "Törlés sikeres", Toast.LENGTH_SHORT).show();
        PrefManager.getInstance().setIsFirst(true);
        Intent intent = ((PostActivity) activity).getmActivityIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.startActivity(intent);
        activity.finish();
//        activity.finish();
//        activity.recreate();
    }

    @Override
    protected void onException(Exception ex) {
        super.onException(ex);
        DialogHelper.showErrorDialog(activity, ex);

    }
}