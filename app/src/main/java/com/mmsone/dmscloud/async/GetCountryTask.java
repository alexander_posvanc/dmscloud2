package com.mmsone.dmscloud.async;

import java.util.List;

import android.content.Context;

import com.mmsone.dmscloud.bl.CountryManager;
import com.mmsone.dmscloud.bo.bean.County;
import com.mmsone.dmscloud.helper.DialogHelper;

public class GetCountryTask extends SafeAsyncTask<Void, Void, List<County>> {

	private long mCountryId;
	private Context mContext;

	
	/**
	 * Downloads countrys by countryId in background
	 * 
	 * @param context
	 * @param countryId
	 */
	public GetCountryTask(Context context, long countryId) {
		mCountryId = countryId;
		mContext = context;
	}

	@Override
	protected List<County> doWorkInBackground(Void... params) throws Exception {
		return CountryManager.getInstance().parseCountryResponse(mCountryId);
	};

	@Override
	protected void onSuccess(List<County> response) {
		super.onSuccess(response);
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		DialogHelper.showErrorDialog(mContext, ex);
	}
}