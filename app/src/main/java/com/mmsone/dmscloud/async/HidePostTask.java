package com.mmsone.dmscloud.async;

import android.app.Activity;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.helper.DialogHelper;

public class HidePostTask extends SafeAsyncTask<Void, Void, Boolean> {

	private long mPostId;
	private Activity mActivity;

	/**
	 * Hide post in background
	 * 
	 * @param postId
	 * @param activity
	 */
	public HidePostTask(long postId, Activity activity) {
		mPostId = postId;
		mActivity = activity;
	}

	@Override
	protected Boolean doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().hidePost(mPostId);
	}

	@Override
	protected void onSuccess(Boolean response) {
		super.onSuccess(response);
		if (response) {
			PrefManager.getInstance().setWallUpToDate(false);
			Toast.makeText(mActivity, R.string.toast_hide_post, Toast.LENGTH_SHORT).show();
			mActivity.finish();
		}
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		DialogHelper.showErrorDialog(mActivity, ex);
	}
}