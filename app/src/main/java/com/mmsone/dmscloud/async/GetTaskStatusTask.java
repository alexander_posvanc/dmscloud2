package com.mmsone.dmscloud.async;

import android.app.ProgressDialog;
import android.content.Context;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bo.TaskStatus;
import com.mmsone.dmscloud.bo.bean.PostTask;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.PostDetailFragment;

public class GetTaskStatusTask extends SafeAsyncTask<Void, Void, PostTask> {

	private long mTaskId;
	private ProgressDialog mProgressDialog;
	private Context mContext;
	private int mStatus;
	private PostDetailFragment mPostDetailFragment;
	private OnWallUpdateListener mOnWallUpdateListener;

	/**
	 * Download task status in background
	 * 
	 * @param context
	 * @param taskId
	 * @param status
	 * @param postDetailFragment
	 * @param onWallUpdateListener
	 */
	public GetTaskStatusTask(final Context context, final long taskId, int status, final PostDetailFragment postDetailFragment, final OnWallUpdateListener onWallUpdateListener) {
		mTaskId = taskId;
		mContext = context;
		mStatus = status;
		mPostDetailFragment = postDetailFragment;
		mOnWallUpdateListener = onWallUpdateListener;
		mProgressDialog = new ProgressDialog(context);
		if (status == TaskStatus.TASK_ACCEPTED) {
			mProgressDialog.setMessage(context.getString(R.string.popup_item_task_accept_title));
		}
		if (status == TaskStatus.TASK_REJECTED) {
			mProgressDialog.setMessage(context.getString(R.string.popup_item_task_reject_title));
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.show();
	}

	@Override
	protected PostTask doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().getTaskStatusRequest(mTaskId);
	}

	@Override
	protected void onSuccess(final PostTask response) {
		super.onSuccess(response);
		new ChangeTaskStatusTask(mContext, response.id, mStatus, response.message, mProgressDialog, mPostDetailFragment, mOnWallUpdateListener).execute();
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		mProgressDialog.dismiss();
		DialogHelper.showErrorDialog(mContext, ex);
	}
}