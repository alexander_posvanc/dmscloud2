package com.mmsone.dmscloud.async;

import android.content.Intent;

import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.bl.LoginManager;
import com.mmsone.dmscloud.bl.OttoManager;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.OttoObject;
import com.mmsone.dmscloud.ex.AuthenticationException;
import com.mmsone.dmscloud.ui.MainActivity;

public class RmmLoginTask extends SafeAsyncTask<Void, Void, Void> {

	/**
	 *  RmmLogin in background
	 */
	public RmmLoginTask(){
		
	}
	@Override
	protected Void doWorkInBackground(Void... params) throws Exception {
		return LoginManager.getInstance().rmmLogin();
	}

	@Override
	protected void onSuccess(Void response) {
		super.onSuccess(response);
		OttoManager.getInstance().getBus().post(new OttoObject(true));
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		//if rmm login has authentication exception start main activity and show error dialog
		if (ex instanceof AuthenticationException && !PrefManager.getInstance().isAuthEx()) {
			PrefManager.getInstance().setAuthEx(true);
			Intent intent = new Intent(DMSCloudApplication.getContext(), MainActivity.class);
			intent.putExtra("authenticationException", true);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			DMSCloudApplication.getContext().startActivity(intent);
		}
	}
}