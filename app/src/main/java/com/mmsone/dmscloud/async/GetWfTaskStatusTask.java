package com.mmsone.dmscloud.async;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bo.WorkflowStatus;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.FillMetaDef;
import com.mmsone.dmscloud.bo.bean.WfTaskStatus;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.PostDetailFragment;

public class GetWfTaskStatusTask extends SafeAsyncTask<Void, Void, WfTaskStatus> {

	private long mWfTaskId;
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private PostDetailFragment mPostDetailFragment;
	private OnWallUpdateListener mOnWallUpdateListener;

	/**
	 * Download workflow task status in background
	 *
	 * @param context
	 * @param wfTaskId
	 * @param postDetailFragment
	 * @param onWallUpdateListener
	 */
	public GetWfTaskStatusTask(final Context context, final long wfTaskId, final PostDetailFragment postDetailFragment, final OnWallUpdateListener onWallUpdateListener) {
		mWfTaskId = wfTaskId;
		mContext = context;
		mPostDetailFragment = postDetailFragment;
		mOnWallUpdateListener = onWallUpdateListener;
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage(context.getString(R.string.popup_item_workflow_accept_title));
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.show();
	}

	@Override
	protected WfTaskStatus doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().getWfTaskStatusRequest(mWfTaskId);
	}

	@Override
	protected void onSuccess(final WfTaskStatus response) {
		super.onSuccess(response);
		List<FillMeta> metaList = new ArrayList<FillMeta>();
		for (final FillMetaDef meta : response.metaList) {
			if (meta.currentValue != null) {
				metaList.add(meta.currentValue);
			}
		}
		new ChangeWfTaskStatusTask(mContext, mProgressDialog, response.id, response.message, WorkflowStatus.ACCEPTED, null, metaList, mPostDetailFragment, mOnWallUpdateListener).execute();
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		mProgressDialog.dismiss();
		DialogHelper.showErrorDialog(mContext, ex);
	}
}
