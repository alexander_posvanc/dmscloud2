package com.mmsone.dmscloud.async;

import android.content.Context;

import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.helper.DialogHelper;

public  class GetUserListTask extends SafeAsyncTask<Void, Void, Boolean> {

	private Context mContext;
	
	/**
	 * Download user list in background
	 * 
	 * @param context
	 */
	public GetUserListTask(Context context) {
		mContext = context;
	}

	@Override
	protected Boolean doWorkInBackground(Void... params) throws Exception {
		return UserManager.getInstance().parseUserList();
		
	}
	@Override
	protected void onSuccess(Boolean response) {
		super.onSuccess(response);
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		DialogHelper.showErrorDialog(mContext, ex);
	}
}