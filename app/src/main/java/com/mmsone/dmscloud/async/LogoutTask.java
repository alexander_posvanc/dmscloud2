package com.mmsone.dmscloud.async;

import android.app.ProgressDialog;
import android.content.Intent;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bc.DataBaseConnector;
import com.mmsone.dmscloud.bc.ServerConnector;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.ex.DMSCloudDBException;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.ui.LoginActivity;
import com.mmsone.dmscloud.ui.MainActivity;

public class LogoutTask extends SafeAsyncTask<Void, Void, String> {

	private MainActivity mActivity;
	private ProgressDialog mProgressDialog;

	/**
	 * Logout in background
	 * 
	 * @param mainActivity
	 */
	public LogoutTask(MainActivity mainActivity) {
		mActivity = mainActivity;
		mProgressDialog = new ProgressDialog(mainActivity);
		mProgressDialog.setMessage(mActivity.getString(R.string.title_logout));
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.show();
	}

	@Override
	protected String doWorkInBackground(Void... params) throws Exception {
		return ServerConnector.getInstance().logoutRequest();
	}

	@Override
	protected void onSuccess(String response) {
		super.onSuccess(response);
		logout();
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		logout();
	}

	/**
	 * Log out
	 */
	private void logout() {
		try {
			//Drop database tables
			DataBaseConnector.getInstance().dropDb();
		} catch (DMSCloudDBException e) {
			DialogHelper.showErrorDialog(mActivity, e);
		}
		//set user logged out
		PrefManager.getInstance().setUserLogged(false);
		PrefManager.getInstance().setAuthEx(false);
		mProgressDialog.dismiss();
		//start login activity
		mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
		mActivity.finish();
	}
}
