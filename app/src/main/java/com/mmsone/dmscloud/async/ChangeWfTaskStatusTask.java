package com.mmsone.dmscloud.async;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.PostDetailFragment;

public class ChangeWfTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

	private long mWfTaskId;
	private int mStatus;
	private Integer mResultId;
	private String mMessage;
	private List<FillMeta> mMetaList;
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private PostDetailFragment mPostDetailFragment;
	private OnWallUpdateListener mOnWallUpdateListener;

	/**
	 * Change workflow task status in background
	 * 
	 * @param context
	 * @param progressDialog
	 * @param wfTaskId
	 * @param message
	 * @param status
	 * @param resultId
	 * @param metaList
	 * @param postDetailFragment
	 * @param onWallUpdateListener
	 */
	public ChangeWfTaskStatusTask(Context context, ProgressDialog progressDialog, final long wfTaskId, final String message, final int status, final Integer resultId, final List<FillMeta> metaList, final PostDetailFragment postDetailFragment, final OnWallUpdateListener onWallUpdateListener) {
		mContext = context;
		mWfTaskId = wfTaskId;
		mStatus = status;
		mMessage = message;
		mResultId = resultId;
		mMetaList = metaList;
		mPostDetailFragment = postDetailFragment;
		mOnWallUpdateListener = onWallUpdateListener;
		mProgressDialog = progressDialog;
	}

	@Override
	protected void onPreExecute() {
		mProgressDialog.show();
		super.onPreExecute();
	}

	@Override
	protected Boolean doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().changeWfTaskStatus(mWfTaskId, mMessage, mStatus, mResultId, mMetaList);
	}

	@Override
	protected void onSuccess(Boolean response) {
		super.onSuccess(response);
		if (response) {
			Toast.makeText(mContext, R.string.wf_task_execution_loading_message, Toast.LENGTH_SHORT).show();
			if (mOnWallUpdateListener != null) {
				mOnWallUpdateListener.onWallUpdateListener();
			}
			if (mPostDetailFragment != null) {
				mPostDetailFragment.detailRefresh();
			}
		}
		mProgressDialog.dismiss();
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		mProgressDialog.dismiss();
		DialogHelper.showErrorDialog(mContext, ex);
	}
}