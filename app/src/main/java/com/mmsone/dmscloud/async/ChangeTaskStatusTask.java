package com.mmsone.dmscloud.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.mmsone.dmscloud.R;
import com.mmsone.dmscloud.bl.PostManager;
import com.mmsone.dmscloud.helper.DialogHelper;
import com.mmsone.dmscloud.listener.OnWallUpdateListener;
import com.mmsone.dmscloud.ui.PostDetailFragment;

public class ChangeTaskStatusTask extends SafeAsyncTask<Void, Void, Boolean> {

	private long mTaskId;
	private int mTaskStatus;
	private String mMessage;
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private PostDetailFragment mPostDetailFragment;
	private OnWallUpdateListener mOnWallUpdateListener;

	/**
	 * Change task status in background
	 * 
	 * @param context
	 * @param taskId
	 * @param status
	 * @param message
	 * @param progressDialog
	 * @param postDetailFragment
	 * @param onWallUpdateListener
	 */
	public ChangeTaskStatusTask(final Context context, final long taskId, final int status, final String message, ProgressDialog progressDialog,final PostDetailFragment postDetailFragment, final OnWallUpdateListener onWallUpdateListener) {
		mTaskId = taskId;
		mTaskStatus = status;
		mMessage = message;
		mContext = context;
		mPostDetailFragment = postDetailFragment;
		mOnWallUpdateListener = onWallUpdateListener;
		mProgressDialog = progressDialog;
	}

	@Override
	protected void onPreExecute() {
		mProgressDialog.show();
		super.onPreExecute();
	}

	@Override
	protected Boolean doWorkInBackground(Void... params) throws Exception {
		return PostManager.getInstance().changeTaskStatus(mTaskId, mTaskStatus, mMessage);
	}

	@Override
	protected void onSuccess(Boolean response) {
		super.onSuccess(response);
		if (response) {
			Toast.makeText(mContext, R.string.task_status_change_succes_message, Toast.LENGTH_SHORT).show();
			if(mOnWallUpdateListener != null){
				mOnWallUpdateListener.onWallUpdateListener();
			}
			if(mPostDetailFragment != null){
				mPostDetailFragment.detailRefresh();
			}
		}
		mProgressDialog.dismiss();
	}

	@Override
	protected void onException(Exception ex) {
		super.onException(ex);
		DialogHelper.showErrorDialog(mContext, ex);
		mProgressDialog.dismiss();
	}
}