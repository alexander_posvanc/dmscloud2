package com.mmsone.dmscloud.bc;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Build;
import android.util.Log;

import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.bl.PrefManager;
import com.mmsone.dmscloud.bo.ERequestType;
import com.mmsone.dmscloud.bo.bean.FillMeta;
import com.mmsone.dmscloud.bo.bean.SearchFilter;
import com.mmsone.dmscloud.logger.ILogger;
import com.mmsone.dmscloud.logger.Logger;

public class ServerConnector {

    private static final ILogger LOG = Logger.getLogger(ServerConnector.class);

    private static ServerConnector mInstance;

    public static ServerConnector getInstance() {
        if (mInstance == null) {
            mInstance = new ServerConnector();
        }
        return mInstance;
    }

    /**
     * Build login request
     *
     * @param password
     * @param userName
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String unpLoginRequest(final String password, final String userName) throws ClientProtocolException, IOException, JSONException {
        JSONObject loginObject = new JSONObject();

        loginObject.putOpt("clientInfo", Build.MANUFACTURER + " " + Build.MODEL);
        loginObject.putOpt("password", password);
        loginObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        loginObject.putOpt("requestType", ERequestType.UnpLoginRequest.name());
        loginObject.putOpt("userName", userName);
        loginObject.putOpt("machineId", DMSCloudApplication.getDeviceId());

        HttpResponse res = post(loginObject.toString());

        /**
         * Store cookies in shared preferences
         */
        Header[] headers = res.getAllHeaders();
        for (int i = 0; i < headers.length; i++) {
            Header header = headers[i];
            if (header.getName().equals("Set-Cookie")) {
                if (header.getValue().contains(PrefManager.JSESSIONID)) {
                    PrefManager.getInstance().setCookie(PrefManager.JSESSIONID, header.getValue());
                    LOG.debug(header.getValue());
                }
                if (header.getValue().contains(PrefManager.C0)) {
                    PrefManager.getInstance().setCookie(PrefManager.C0, header.getValue());
                    LOG.debug(header.getValue());
                }
            }
        }

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build rmm login request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String rmmLoginRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject loginObject = new JSONObject();

        loginObject.putOpt("clientInfo", Build.MANUFACTURER + " " + Build.MODEL);
        loginObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        loginObject.putOpt("requestType", ERequestType.RmmLoginRequest.name());
        loginObject.putOpt("machineId", DMSCloudApplication.getDeviceId());

        HttpPost httpPost = new HttpPost(PrefManager.getInstance().getServerUrl() + Config.SERVER_JSON_URL);
        httpPost.setEntity(new StringEntity(loginObject.toString(), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.C0));

        HttpResponse res = new DefaultHttpClient().execute(httpPost);

        Header[] headers = res.getAllHeaders();
        for (int i = 0; i < headers.length; i++) {
            Header header = headers[i];
            if (header.getName().equals("Set-Cookie")) {
                if (header.getValue().contains(PrefManager.JSESSIONID)) {
                    PrefManager.getInstance().setCookie(PrefManager.JSESSIONID, header.getValue());
                    LOG.debug(header.getValue());
                }
                if (header.getValue().contains(PrefManager.C0)) {
                    PrefManager.getInstance().setCookie(PrefManager.C0, header.getValue());
                    LOG.debug(header.getValue());
                }
            }
        }

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build logout request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String logoutRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject logoutObject = new JSONObject();

        logoutObject.putOpt("clientInfo", null);
        logoutObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        logoutObject.putOpt("requestType", ERequestType.LogoutRequest.name());
        logoutObject.putOpt("machineId", DMSCloudApplication.getDeviceId());

        HttpResponse res = post(logoutObject.toString());
        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get user list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getUserListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetUserListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get wall request
     *
     * @param from
     * @param rowCount
     * @param filterList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWallRequestWithFilterList(final int from, final int rowCount, final List<SearchFilter> filterList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWallRequest.name());
        jsonObject.putOpt("from", from);
        jsonObject.putOpt("rowCount", rowCount);
        if (filterList != null && !filterList.isEmpty()) {
            jsonObject.putOpt("filterList", getFilterListJsonArray(filterList));
        }
        HttpResponse res = post(jsonObject.toString());
        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Convert filter list to json array
     *
     * @param filterList
     * @return
     */
    private JSONArray getFilterListJsonArray(final List<SearchFilter> filterList) {
        JSONArray jsonArray = new JSONArray();
        for (SearchFilter searchFilter : filterList) {
            jsonArray.put(searchFilter.getJsonObject());
        }
        return jsonArray;
    }

    /**
     * Build get wall sub request
     *
     * @param from
     * @param rowCount
     * @param postId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWallSubRequest(final int from, final int rowCount, final long postId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWallSubRequest.name());
        jsonObject.putOpt("from", from);
        jsonObject.putOpt("rowCount", rowCount);
        jsonObject.putOpt("postId", postId);

        HttpResponse res = post(jsonObject.toString());
        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get wall post meta request
     *
     * @param from
     * @param rowCount
     * @param postId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWallPostMetaRequest(final int from, final int rowCount, final long postId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWallPostMetaRequest.name());
        jsonObject.putOpt("from", from);
        jsonObject.putOpt("rowCount", rowCount);
        jsonObject.putOpt("postId", postId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get code store request
     *
     * @param typeId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getCodeStoreRequest(final long typeId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetCodeStoreRequest.name());
        jsonObject.putOpt("typeId", typeId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get code store request
     *
     * @param shortKey
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getCodeStoreRequest(final String shortKey) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetCodeStoreRequest.name());
        jsonObject.putOpt("shortKey", shortKey);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get country request
     *
     * @param countryId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getCountryRequest(final long countryId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetCountryRequest.name());
        jsonObject.putOpt("countryId", countryId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get customer list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getCustomerListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetCustomerListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get group list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getGroupListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetGroupListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get distribution list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getDistributionListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetDistributionListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get meta def list request
     *
     * @param useTypeList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getMetaDefListRequest(final int[] useTypeList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetMetaDefListRequest.name());
        jsonObject.putOpt("useTypeList", getJsonArray(useTypeList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get saved search list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getSavedSearchListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetSavedSearchListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get saved search request
     *
     * @param savedSearchId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getSavedSearchRequest(final long savedSearchId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetSavedSearchRequest.name());
        jsonObject.putOpt("savedSearchId", savedSearchId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build saved search save request
     *
     * @param saveName
     * @param from
     * @param rowCount
     * @param filterList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String saveSavedSearchRequest(final String saveName, final int from, final int rowCount, final List<SearchFilter> filterList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWallRequest.name());
        jsonObject.putOpt("saveName", saveName);
        jsonObject.putOpt("from", from);
        jsonObject.putOpt("rowCount", rowCount);
        jsonObject.putOpt("filterList", getFilterListJsonArray(filterList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get wfdef list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWfDefListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWfDefListRequest.name());
        jsonObject.putOpt("userStart", true);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build share post request
     *
     * @param postId
     * @param description
     * @param userList
     * @param groupList
     * @param distributionList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String sharePostRequest(final long postId, final String description, final Long[] userList, final Long[] groupList, final Long[] distributionList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.SharePostRequest.name());
        jsonObject.putOpt("postId", postId);
        jsonObject.putOpt("description", description);
        jsonObject.putOpt("userList", getJsonArray(userList));
        jsonObject.putOpt("groupList", getJsonArray(groupList));
        jsonObject.putOpt("distributionList", getJsonArray(distributionList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build hide post request
     *
     * @param postId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String hidePostRequest(final long postId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.HidePostRequest.name());
        jsonObject.putOpt("postId", postId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build mark post request
     *
     * @param postId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String markPostRequest(final long postId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.MarkPostRequest.name());
        jsonObject.putOpt("postId", postId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build create post request
     *
     * @param title
     * @param message
     * @param userList
     * @param groupList
     * @param distributionList
     * @param metaList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String createPostRequest(final String title, final String message, final Long[] userList, final Long[] groupList, final Long[] distributionList, final List<FillMeta> metaList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.CreatePostRequest.name());
        jsonObject.putOpt("title", title);
        jsonObject.putOpt("message", message);
        jsonObject.putOpt("userList", getJsonArray(userList));
        jsonObject.putOpt("groupList", getJsonArray(groupList));
        jsonObject.putOpt("distributionList", getJsonArray(distributionList));
        jsonObject.putOpt("metaList", getFillMetaJsonArray(metaList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Convert fill meta list to json arrray
     *
     * @param metaList
     * @return
     */
    private JSONArray getFillMetaJsonArray(final List<FillMeta> metaList) {
        JSONArray jsonArray = new JSONArray();
        for (FillMeta fillMeta : metaList) {
            if (fillMeta != null) {
                jsonArray.put(fillMeta.getJsonObject());
            }
        }
        return jsonArray;
    }

    /**
     * Build create post comment request
     *
     * @param postId
     * @param description
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String createPostCommentRequest(final long postId, final String description) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.CreatePostCommentRequest.name());
        jsonObject.putOpt("postId", postId);
        jsonObject.putOpt("description", description);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build create post sub comment request
     *
     * @param subPostId
     * @param subPostType
     * @param description
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String createPostSubCommentRequest(final long subPostId, final int subPostType, final String description) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.CreatePostSubCommentRequest.name());
        jsonObject.putOpt("subPostId", subPostId);
        jsonObject.putOpt("subPostType", subPostType);
        jsonObject.putOpt("description", description);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build post task request
     *
     * @param postId
     * @param message
     * @param dueDate
     * @param everyUser
     * @param userList
     * @param groupList
     * @param distributionList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String createPostTaskRequest(final long postId, final String message, final long dueDate, final boolean everyUser, final Long[] userList, final Long[] groupList, final Long[] distributionList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.CreatePostTaskRequest.name());
        jsonObject.putOpt("postId", postId);
        jsonObject.putOpt("message", message);
        jsonObject.putOpt("dueDate", dueDate);
        jsonObject.putOpt("everyUser", everyUser);
        jsonObject.putOpt("userList", getJsonArray(userList));
        jsonObject.putOpt("groupList", getJsonArray(groupList));
        jsonObject.putOpt("distributionList", getJsonArray(distributionList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Convert int array to json array
     *
     * @param array
     * @return
     */
    private JSONArray getJsonArray(final int[] array) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < array.length; i++) {
            jsonArray.put(array[i]);
        }
        return jsonArray;
    }

    /**
     * Convert long array to json array
     *
     * @param array
     * @return
     */
    private JSONArray getJsonArray(final Long[] array) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < array.length; i++) {
            jsonArray.put(array[i]);
        }
        return jsonArray;
    }

    /**
     * Build task status request
     *
     * @param taskId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getTaskStatusRequest(final long taskId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetTaskStatusRequest.name());
        jsonObject.putOpt("taskId", taskId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build change task status request
     *
     * @param taskId
     * @param status
     * @param message
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String changeTaskStatusRequest(final long taskId, final int status, final String message) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.ChangeTaskStatusRequest.name());
        jsonObject.putOpt("taskId", taskId);
        jsonObject.putOpt("status", status);
        jsonObject.putOpt("message", message);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build change task status request
     *
     * @param taskId
     * @param status
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String changeTaskStatusRequest(final long taskId, final int status) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.ChangeTaskStatusRequest.name());
        jsonObject.putOpt("taskId", taskId);
        jsonObject.putOpt("status", status);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get task list request
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getTaskListRequest() throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetTaskListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get document request
     *
     * @param documentId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getDocumentRequest(final long documentId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetDocumentRequest.name());
        jsonObject.putOpt("documentId", documentId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get wf task status request
     *
     * @param wfTaskId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWfTaskStatusRequest(final long wfTaskId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWfTaskStatusRequest.name());
        jsonObject.putOpt("wfTaskId", wfTaskId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build change task status request
     *
     * @param wfTaskId
     * @param message
     * @param status
     * @param resultId
     * @param metaList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String changeWfTaskStatusRequest(final long wfTaskId, final String message, final int status, final Integer resultId, final List<FillMeta> metaList) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.ChangeWfTaskStatusRequest.name());
        jsonObject.putOpt("wfTaskId", wfTaskId);
        jsonObject.putOpt("message", message);
        jsonObject.putOpt("status", status);
        if (resultId != null) {
            jsonObject.putOpt("resultId", resultId);
        }
        jsonObject.putOpt("metaList", getFillMetaJsonArray(metaList));

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build register push request
     *
     * @param registrationId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String registerPushRequest(final String registrationId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.RegisterPushRequest.name());
        jsonObject.putOpt("registrationId", registrationId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build unregister push request
     *
     * @param registrationId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String unregisterPushRequest(final String registrationId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.UnregisterPushRequest.name());
        jsonObject.putOpt("registrationId", registrationId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build get wf def list request
     *
     * @param postId
     * @param description
     * @param wfDefId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String getWfDefListRequest(final long postId, final String description, final long wfDefId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.GetWfDefListRequest.name());

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Build create post wf task request
     *
     * @param postId
     * @param description
     * @param wfDefId
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    public String createPostWfTaskRequest(final long postId, final String description, final long wfDefId) throws ClientProtocolException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.CreatePostWfTaskRequest.name());
        jsonObject.putOpt("postId", postId);
        jsonObject.putOpt("description", description);
        jsonObject.putOpt("wfDefId", wfDefId);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * HTTP post
     *
     * @param json
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    private HttpResponse post(final String json) throws ClientProtocolException, IOException {
        LOG.debug(json);
        HttpPost httpPost = new HttpPost(PrefManager.getInstance().getServerUrl() + Config.SERVER_JSON_URL);
        httpPost.setEntity(new StringEntity(json, "UTF-8"));
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));

        return new DefaultHttpClient().execute(httpPost);
    }

    /**
     * Build file upload request
     *
     * @param fileList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public String fileUpload(List<File> fileList) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(PrefManager.getInstance().getServerUrl() + Config.SERVER_FILE_UPLOAD_URL);

        httpPost.setHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
        httpPost.setHeader("Connection", "keep-alive");

        MultipartEntity entity = new MultipartEntity();

        for (int i = 0; i < fileList.size(); i++) {
            File file = fileList.get(i);

            FormBodyPart formBodyPart = new FormBodyPart("file" + i, new FileBody(file));

            formBodyPart.addField("type", "file");
            formBodyPart.addField("name", "file" + i);
            formBodyPart.addField("id", "file" + i);

            entity.addPart(formBodyPart);
        }
        httpPost.setEntity(entity);
        HttpResponse respopnse = new DefaultHttpClient().execute(httpPost);

        return EntityUtils.toString(respopnse.getEntity(), "UTF-8");
    }

    /**
     * Build file upload request for technical subpost
     *
     * @param fileList
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public String fileUpload(List<File> fileList, long id) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(PrefManager.getInstance().getServerUrl() + Config.SERVER_FILE_UPLOAD_URL);

        httpPost.setHeader("Cookie", PrefManager.getInstance().getCookie(PrefManager.JSESSIONID));
        httpPost.setHeader("Connection", "keep-alive");

        MultipartEntity entity = new MultipartEntity();

        for (int i = 0; i < fileList.size(); i++) {
            File file = fileList.get(i);

            FormBodyPart formBodyPart = new FormBodyPart("file" + i, new FileBody(file));

            formBodyPart.addField("type", "file");
            formBodyPart.addField("name", "file" + i);
            formBodyPart.addField("id", "file" + i);

            entity.addPart(formBodyPart);

            Log.d("FileConnector", formBodyPart.toString());
        }
        entity.addPart("postId", new StringBody(String.valueOf(id)));


        httpPost.setEntity(entity);
        HttpResponse respopnse = new DefaultHttpClient().execute(httpPost);


        return EntityUtils.toString(respopnse.getEntity(), "UTF-8");
    }

    /**
     * Rank Post request
     *
     * @param postId
     * @return
     * @throws JSONException
     * @throws IOException
     */
    public String rankPointRequest(long postId) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.RankPointRequest.name());
        jsonObject.putOpt("postId", postId);


        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

    /**
     * Rank SubPost
     *
     * @param postId
     * @param subPostId
     * @param subPostType
     * @return
     * @throws JSONException
     * @throws IOException
     */
    public String rankPointRequest(long postId, long subPostId, int subPostType) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.RankPointRequest.name());
        jsonObject.putOpt("postId", postId);
        jsonObject.putOpt("subPostId", subPostId);
        jsonObject.putOpt("subPostType", subPostType);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }


    /**
     * Delete SubComment request
     *
     * @param id
     * @return
     * @throws JSONException
     * @throws IOException
     */
    public String deletePostSubCommentRequest(long id) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.putOpt("protocolVersion", PrefManager.getInstance().getProtocolVersion());
        jsonObject.putOpt("requestType", ERequestType.DeletePostSubCommentRequest.name());
        jsonObject.putOpt("id", id);

        HttpResponse res = post(jsonObject.toString());

        return EntityUtils.toString(res.getEntity(), "UTF-8");
    }

}