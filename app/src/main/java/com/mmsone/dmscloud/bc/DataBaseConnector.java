package com.mmsone.dmscloud.bc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.mmsone.dmscloud.Config;
import com.mmsone.dmscloud.DMSCloudApplication;
import com.mmsone.dmscloud.bl.UserManager;
import com.mmsone.dmscloud.bo.IUser;
import com.mmsone.dmscloud.bo.LoginResponse;
import com.mmsone.dmscloud.bo.bean.City;
import com.mmsone.dmscloud.bo.bean.County;
import com.mmsone.dmscloud.bo.bean.User;
import com.mmsone.dmscloud.ex.DMSCloudDBException;

public class DataBaseConnector extends OrmLiteSqliteOpenHelper {

    private static DataBaseConnector mInstance;

    public static DataBaseConnector getInstance() {
        if (mInstance == null) {
            mInstance = new DataBaseConnector(DMSCloudApplication.getContext());
        }
        return mInstance;
    }

    private DataBaseConnector(Context context) {
        super(context, Config.DB_NAME, null, Config.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, LoginResponse.class);
            TableUtils.createTable(connectionSource, City.class);
            TableUtils.createTable(connectionSource, County.class);
        } catch (SQLException e) {
            if (Config.IS_DEVELOPER_MODE) {
                Log.d("Can't create database", e.getLocalizedMessage());
            }
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Drop database tables
     *
     * @throws DMSCloudDBException
     */
    public void dropDb() throws DMSCloudDBException {
        try {
            TableUtils.dropTable(connectionSource, User.class, false);
            TableUtils.dropTable(connectionSource, LoginResponse.class, false);
            TableUtils.dropTable(connectionSource, City.class, false);
            TableUtils.dropTable(connectionSource, County.class, false);

            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, LoginResponse.class);
            TableUtils.createTable(connectionSource, City.class);
            TableUtils.createTable(connectionSource, County.class);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2, int arg3) {
        // version 1, no possible upgrade yet
    }

    /**
     * Store login response in database
     *
     * @param loginResponse
     * @throws DMSCloudDBException
     */
    public void addLoginResponse(LoginResponse loginResponse) throws DMSCloudDBException {
        try {
            getDao(LoginResponse.class).createOrUpdate(loginResponse);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    /**
     * Update login response after rmm login
     *
     * @param loginResponse
     * @throws DMSCloudDBException
     */
    public void updateLoginResponse(LoginResponse loginResponse) throws DMSCloudDBException {
        try {
            getDao(LoginResponse.class).update(loginResponse);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    public LoginResponse getLoginResponse() throws DMSCloudDBException {
        try {
            Dao<LoginResponse, Long> userDao = getDao(LoginResponse.class);
            return userDao.queryBuilder().queryForFirst();
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    /**
     * Store user in database
     *
     * @param user
     * @throws DMSCloudDBException
     */
    public void addUser(User user) throws DMSCloudDBException {
        try {
            getDao(User.class).createOrUpdate(user);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    public User getUser(long id) throws DMSCloudDBException {
        try {
            Dao<User, Long> userDao = getDao(User.class);
            return userDao.queryForId(id);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }


    public List<User> getUserList() throws DMSCloudDBException {
        try {
            return getDao(User.class).queryBuilder().query();
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    public List<User> getUserList(Set<Long> userIds) throws Exception {
        List<User> list = null;
        try {
            list = getDao(User.class).queryBuilder().where().in("id", userIds).query();

            Set<Long> iDs = new HashSet<Long>();
            for (User user : list) {
                iDs.add(user.id);
            }

            if (!iDs.containsAll(userIds)) {
                UserManager.getInstance().parseUserList();
                list = getDao(User.class).queryBuilder().where().in("id", userIds).query();
            }
            return list;
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    public List<IUser> getIUserList() throws DMSCloudDBException {
        return new ArrayList<IUser>(getUserList());
    }

    /**
     * Store county in database
     *
     * @param county
     * @throws DMSCloudDBException
     */
    public void addCounty(County county) throws DMSCloudDBException {
        try {
            getDao(County.class).createOrUpdate(county);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    /**
     * Store city in database
     *
     * @param city
     * @throws DMSCloudDBException
     */
    public void addCity(City city) throws DMSCloudDBException {
        try {
            getDao(City.class).createOrUpdate(city);
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    public List<County> getCountyList(long countryId) throws DMSCloudDBException {
        List<County> counties = new ArrayList<County>();
        try {
            counties = getDao(County.class).queryBuilder().where().eq("countryId", countryId).query();
            for (County county : counties) {
                county.childList = new ArrayList<City>(getCityList(county.id));
            }
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }

        return counties;
    }

    public List<City> getCityList(long countyId) throws DMSCloudDBException {
        try {
            return getDao(City.class).queryBuilder().where().eq("countyId", countyId).query();
        } catch (java.sql.SQLException e) {
            throw new DMSCloudDBException("Database operation failed");
        }
    }

    /**
     * Create shared with and hidden at users string
     *
     * @param sharedWithList
     * @param hiddenAtList
     * @return
     * @throws DMSCloudDBException
     */
    public String getSharedWithUsers(List<Long> sharedWithList, List<Long> hiddenAtList) throws DMSCloudDBException {
        String sharedWith = "";
        User user = null;
        for (Long id : sharedWithList) {
            user = getUser(id);
            sharedWith += user.formatedName + ", ";
        }

        for (Long id : hiddenAtList) {
            user = getUser(id);
            sharedWith += user.formatedName + "(h), ";
        }
        return sharedWith.substring(0, sharedWith.length() - 2);
    }

    /**
     * Get list of shared with users
     *
     * @param sharedWithList
     * @return
     * @throws DMSCloudDBException
     */
    public List<User> getSharedWithUsersList(List<Long> sharedWithList) throws DMSCloudDBException {
        User user = null;
        List<User> userList = new ArrayList<>();
        for (Long id : sharedWithList) {
            user = getUser(id);
            userList.add(user);
        }

        return userList;
    }

    /**
     * Get list of shared with users
     *
     * @param sharedWithList
     * @return
     * @throws DMSCloudDBException
     */
    public List<User> getNotSharedWithUsersList(List<Long> sharedWithList) throws DMSCloudDBException {
        User user = null;
        List<User> userList;
        userList = getUserList();
        for (Long id : sharedWithList) {
            user = getUser(id);
            userList.remove(user);
        }

        return userList;
    }
}
